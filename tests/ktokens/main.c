/*
 * Copyright (C) 2007-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shCompiler.
 *
 * shCompiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shCompiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shCompiler.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>
#include <shktokens.h>

//#define GENERATE_TEST

static shKtokens la;
static char tokens[5][100] = {"", "", "", "", ""};
static int start = 0;
static int end = 0;

void read_one(void)
{
	int j;
	const char *str;

	if (scanf("%99s", tokens[end]) == 1)
		str = tokens[end];
	else
	{
		strcpy(tokens[end], "");
		str = NULL;
	}
#ifndef GENERATE_TEST
	sh_ktokens_push(&la, str, str);
#endif

	for (j = 0; j < 5; ++j)
	{
#ifdef GENERATE_TEST
		str = tokens[(start + j) % 5];
#else
		str = sh_ktokens_at(&la, NULL, j);
#endif
		printf("%s%s", j?" ":"", str?str:"");
	}
	printf("\n");

	end = (end + 1) % 5;
	if (end == start)
		start = (start + 1) % 5;
}

int main()
{
	int i;

	if (sh_ktokens_init(&la, 5))
		return -1;

	for (i = 0; i < 120; ++i)
		read_one();

	sh_ktokens_clear(&la);
	start = end = 0;
	for (i = 0; i < 5; ++i)
		strcpy(tokens[i], "");

	for (i = 0; i < 150; ++i)
		read_one();

	sh_ktokens_free(&la);
	return 0;
}
