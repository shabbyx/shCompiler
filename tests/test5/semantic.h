/*
 * Copyright (C) 2007-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shCompiler.
 *
 * shCompiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shCompiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shCompiler.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SEMANTIC_H_BY_CYCLOPS
#define SEMANTIC_H_BY_CYCLOPS

#include <string>
#include <shcompiler.h>

using namespace std;

void initializeSemantics(shParser *parser);
extern "C" void stop_evaluation(shParser *parser);

void addToAlphabet(shParser *parser);
void newMachine(shParser *parser);
void machineName(shParser *parser);
void machineDefEnd(shParser *parser);
void maybeNewState(shParser *parser);
void isStart(shParser *parser);
void isNotStart(shParser *parser);
void isFinal(shParser *parser);
void isNotFinal(shParser *parser);
void addStateProperties(shParser *parser);
void checkToHaveStartAndFinal(shParser *parser);
void newTransitionDef(shParser *parser);
void newTransition(shParser *parser);
void newMachineCall(shParser *parser);
void tidistate(shParser *parser);
void tidostate(shParser *parser);
void tidomachine(shParser *parser);
void tidomachinestate(shParser *parser);
void tidiwild(shParser *parser);
void tidiletter(shParser *parser);
void tidiblank(shParser *parser);
void tidodirection(shParser *parser);
void tidowild(shParser *parser);
void tidoblank(shParser *parser);
void tidoletter(shParser *parser);
void newValue(shParser *parser);
void machineToRun(shParser *parser);
void machineInput(shParser *parser);
void runMachine(shParser *parser);

#endif
