/*
 * Copyright (C) 2007-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shCompiler.
 *
 * shCompiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shCompiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shCompiler.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <shcompiler.h>
#include <test_settings.h>
#include <test_defaults.h>
#include "semantic.h"

#define DO_PARSE 1
#define DO_LEX 1
#define DO_LEX_DETAILED 0

static shLexer lexer;
static shParser parser;

static void lowerCase(shLexer *lexer, char **t)
{
	char *p;
	for (p = *t; *p; ++p)
		*p = tolower(*p);
}

int main(int argc, char **argv)
{
	struct prog_settings settings = {0};
	settings.letter_file = "grammar/letters.in";
	settings.token_file = "grammar/tokens.in";
	settings.keyword_file = "grammar/keywords.in";
	settings.grammar_file = "grammar/grammar.in";
	settings.input_file = "test.file";
	settings.k = 3;
	if (get_settings(argc, argv, &settings))
		return EXIT_FAILURE;

	if (test_init_lexer(&lexer, &settings))
		return EXIT_FAILURE;

	/* give lexer callbacks */
	sh_lexer_set_match_function(&lexer, "MACHINE", lowerCase);
	sh_lexer_set_match_function(&lexer, "STATE", lowerCase);
	sh_lexer_set_match_function(&lexer, "LETTER", lowerCase);
	sh_lexer_set_match_function(&lexer, "STRING", lowerCase);

#if DO_LEX
	/* the code below tests functionality of the lexer alone */
	test_print_tokens(&lexer, DO_LEX_DETAILED);
#endif

#if DO_PARSE
	if (test_init_parser(&parser, &lexer, &settings, SH_PARSER_LLK))
		return EXIT_FAILURE;

	/* give parser callbacks */
	sh_parser_set_action_routine(&parser, "@addToAlphabet", addToAlphabet);
	sh_parser_set_action_routine(&parser, "@newMachine", newMachine);
	sh_parser_set_action_routine(&parser, "@machineName", machineName);
	sh_parser_set_action_routine(&parser, "@machineDefEnd", machineDefEnd);
	sh_parser_set_action_routine(&parser, "@maybeNewState", maybeNewState);
	sh_parser_set_action_routine(&parser, "@isStart", isStart);
	sh_parser_set_action_routine(&parser, "@isNotStart", isNotStart);
	sh_parser_set_action_routine(&parser, "@isFinal", isFinal);
	sh_parser_set_action_routine(&parser, "@isNotFinal", isNotFinal);
	sh_parser_set_action_routine(&parser, "@addStateProperties", addStateProperties);
	sh_parser_set_action_routine(&parser, "@checkToHaveStartAndFinal", checkToHaveStartAndFinal);
	sh_parser_set_action_routine(&parser, "@newTransitionDef", newTransitionDef);
	sh_parser_set_action_routine(&parser, "@newTransition", newTransition);
	sh_parser_set_action_routine(&parser, "@newMachineCall", newMachineCall);
	sh_parser_set_action_routine(&parser, "@tidistate", tidistate);
	sh_parser_set_action_routine(&parser, "@tidostate", tidostate);
	sh_parser_set_action_routine(&parser, "@tidomachine", tidomachine);
	sh_parser_set_action_routine(&parser, "@tidomachinestate", tidomachinestate);
	sh_parser_set_action_routine(&parser, "@tidiwild", tidiwild);
	sh_parser_set_action_routine(&parser, "@tidiletter", tidiletter);
	sh_parser_set_action_routine(&parser, "@tidiblank", tidiblank);
	sh_parser_set_action_routine(&parser, "@tidodirection", tidodirection);
	sh_parser_set_action_routine(&parser, "@tidowild", tidowild);
	sh_parser_set_action_routine(&parser, "@tidoblank", tidoblank);
	sh_parser_set_action_routine(&parser, "@tidoletter", tidoletter);
	sh_parser_set_action_routine(&parser, "@newValue", newValue);
	sh_parser_set_action_routine(&parser, "@machineToRun", machineToRun);
	sh_parser_set_action_routine(&parser, "@machineInput", machineInput);
	sh_parser_set_action_routine(&parser, "@runMachine", runMachine);

	initializeSemantics(&parser);

	/* do the actual parsing */
	if (test_open_file(&lexer, &settings) == 0)
		test_parse_file(&parser);
#endif

	return 0;
}
