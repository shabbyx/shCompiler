/*
 * Copyright (C) 2007-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shCompiler.
 *
 * shCompiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shCompiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shCompiler.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "tape.h"

shTMTape::shTMTape()
{
	shTMTInitialize();
}

shTMTape::shTMTape(string s)
{
	shTMTInitialize(s);
}

shTMTape::shTMTape(const shTMTape &tm)
{
	p_right = tm.p_right;
	p_left = tm.p_left;
	p_head = tm.p_head;
}

void shTMTape::shTMTInitialize(string s)
{
	p_right.clear();
	p_left.clear();
	p_right.push_back(BLANK);	// because head should be on the blank before input!
	for (int i = 0; i < (int)s.size(); ++i)
		p_right.push_back(s[i]);
	p_head = 0;
}

char shTMTape::shTMTCurrent()
{
	if (p_head >= 0)
		return p_right[p_head];
	return p_left[-p_head-1];
}

shTMTape &shTMTape::operator =(const shTMTape &tm)
{
	p_right = tm.p_right;
	p_left = tm.p_left;
	p_head = tm.p_head;
	return *this;
}

void shTMTape::shTMTLeft()
{
	--p_head;
	if (-p_head-1 == (int)p_left.size())
		p_left.push_back(BLANK);
}

void shTMTape::shTMTRight()
{
	++p_head;
	if (p_head == (int)p_right.size())
		p_right.push_back(BLANK);
}

void shTMTape::shTMTWrite(char c)
{
	if (p_head >= 0)
		p_right[p_head] = c;
	else
		p_left[-p_head-1] = c;
}

string shTMTape::shTMTTape()
{
	int l = p_left.size()-1;
	string res = "";
	for (; l >= 0; --l)
		if (p_left[l] != BLANK || l == -p_head-1)
			break;
	bool hasAnyLeft = false;
	for (; l >= 0; --l)
	{
		hasAnyLeft = true;
		if (l == -p_head-1)
			res += 'H';
		res += p_left[l];
	}
	int r = p_right.size()-1;
	for (; r >= 0; --r)
		if (p_right[r] != BLANK || r == p_head)
			break;
	int i = 0;
	if (!hasAnyLeft)
		for (i = 0; i <= r; ++i)
			if (p_right[i] != BLANK || i == p_head)
				break;
	for (; i <= r; ++i)
	{
		if (i == p_head)
			res += 'H';
		res += p_right[i];
	}
	return res;
}
