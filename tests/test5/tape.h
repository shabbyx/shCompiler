/*
 * Copyright (C) 2007-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shCompiler.
 *
 * shCompiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shCompiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shCompiler.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TAPE_H_BY_CYCLOPS
#define TAPE_H_BY_CYCLOPS

#include <vector>
#include <string>

using namespace std;

#define BLANK ' '

class shTMTape
{
private:
	vector<char> p_right;		// for index >= 0
	vector<char> p_left;		// for index < 0
	int p_head;
public:
	shTMTape();
	shTMTape(string);
	shTMTape(const shTMTape &);
	char shTMTCurrent();
	shTMTape &operator =(const shTMTape &);
	void shTMTInitialize(string s = "");
	void shTMTLeft();
	void shTMTRight();
	void shTMTWrite(char);
	string shTMTTape();
};

#endif
