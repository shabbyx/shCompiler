/*
 * Copyright (C) 2007-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shCompiler.
 *
 * shCompiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shCompiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shCompiler.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cstdio>
#include <cstring>
#include <iostream>
#include <fstream>
#include <stack>
#include <cstring>
#include <map>
#include "semantic.h"
#include "tape.h"

using namespace std;

//#define SH_DEBUG

struct shTMMachineCall
{
	int machine;			// which machine it calls
	int returnState;		// which state it should move to when machine halted
};

#define WILD_CARD -1

struct shTMTransitionInput
{
	int state;
	char letter;
	bool operator <(const shTMTransitionInput &rhs) const	// for map
	{
		if (state < rhs.state)
			return true;
		if (state > rhs.state)
			return false;
		if (letter < rhs.letter)
			return true;
		return false;
	}
};

struct shTMTransitionOutput
{
	int state;
	enum
	{
		RIGHT = 0,
		LEFT,
		NOMOVE
	} direction;
	char letter;
};

#define NO_START_STATE -1

struct shTMAutomata			// The tape is global, the automatas are machine specific
{
	map<string, int> stateCode;
	int startState;
	vector<bool> isFinal;		// says if state i is a final state or not
	map<int, shTMMachineCall> machineCall;	// which machine to call if a particular state is reached
						// (and no transitions were possible)
	map<shTMTransitionInput, shTMTransitionOutput> transition;	// transitions!
	bool erroneous;			// in case there was an error in definition
	void clear() { erroneous = false; stateCode.clear(); startState = NO_START_STATE; isFinal.clear();
		machineCall.clear(); transition.clear(); }
};

struct shTMRunningMachine
{
	int machine;			// machine that is running
	int state;			// state in which that machine is
};

static map<string, int> machineCode;
static vector<shTMAutomata> machine;
static shTMTape tape;
static stack<shTMRunningMachine> rs;		// run stack
static bool evaluate;				// if lex/parse error, do nothing!
static shTMAutomata mid;			// machine in definition
static string machineInDefinitionName;		// name of mid
static bool letterValid[256];
static vector<char> alphabet;
static bool defStart;				// state being defined is start
static bool defFinal;				// state begin defined is final
static bool hadFinal;				// tells if a final state had been declared in machine definition
static int mtr;					// machine to run

struct shTMTransitionInDef
{
	// normal input cannot be used
	bool hasWildCard;
	int state;
	vector<char> letters;
	shTMTransitionOutput tido;
	shTMMachineCall tidmc;
} tid;

#define stack_clear(s) while (!s.empty()) s.pop()

void initializeSemantics(shParser *parser)
{
	evaluate = true;
	machineCode.clear();
	machine.clear();
	stack_clear(rs);
	tape.shTMTInitialize();
	mid.clear();
	alphabet.clear();
	machineInDefinitionName = "";
	memset(letterValid, 0, sizeof(letterValid));
	letterValid[(int)' '] = true;
}

extern "C"
void stop_evaluation(shParser *parser)
{
	evaluate = false;
}

void continue_evaluation(shParser *parser)
{
	evaluate = true;
}

#define ERROR(...) do { mid.erroneous = true; stop_evaluation(parser); sh_lexer_error(parser->lexer, __VA_ARGS__); } while (0)

void addToAlphabet(shParser *parser)
{
	letterValid[(int)parser->lexer->current_token[0]] = true;
	alphabet.push_back(parser->lexer->current_token[0]);
}

void newMachine(shParser *parser)
{
	mid.clear();
	hadFinal = false;
}

void machineName(shParser *parser)
{
	machineInDefinitionName = parser->lexer->current_token;
	if (machineCode.find(parser->lexer->current_token) != machineCode.end())
		ERROR("error: machine %s already defined", parser->lexer->current_token);
}

void machineDefEnd(shParser *parser)
{
	if (machineCode.find(machineInDefinitionName) == machineCode.end())
	{
		machineCode[machineInDefinitionName] = machine.size();
		machine.push_back(mid);
	}
#ifdef SH_DEBUG
	cout << "New " << (mid.erroneous?"erroneous ":"") << "machine defined [" << machineInDefinitionName << "]!" << endl;
	cout << "States:" << endl;
	for (map<string, int>::iterator i = mid.stateCode.begin(); i != mid.stateCode.end(); ++i)
		cout << ((i->second == mid.startState)?"start ":"      ") <<
			(mid.isFinal[i->second]?"final ":"      ") << i->second << " (" << i->first << ")" << endl;
	if (mid.transition.size() > 0)
		cout << "Transitions:" << endl;
	for (map<shTMTransitionInput, shTMTransitionOutput>::iterator i = mid.transition.begin();
			i != mid.transition.end(); ++i)
		cout << "(" << i->first.state << ", " << i->first.letter << ") -> (" << i->second.state <<
			", " << ((i->second.direction == shTMTransitionOutput::RIGHT)?"right":
				(i->second.direction == shTMTransitionOutput::LEFT)?"left":"no move") << ", " <<
			i->second.letter << ")" << endl;
	if (mid.machineCall.size() > 0)
		cout << "Calls:" << endl;
	for (map<int, shTMMachineCall>::iterator i = mid.machineCall.begin(); i != mid.machineCall.end(); ++i)
		cout << i->first << " -> (machine " << i->second.machine << ", " << i->second.returnState << ")" << endl;
	cout << endl;
#endif
}

void maybeNewState(shParser *parser)
{
	if (mid.stateCode.find(parser->lexer->current_token) == mid.stateCode.end())
	{
		mid.stateCode[parser->lexer->current_token] = mid.isFinal.size();	// Ok, this might not be a really good idea,
								// but isFinal here is used to find state count
		mid.isFinal.push_back(false);			// each state is, by default, not final
	}
}

void isStart(shParser *parser)
{
	defStart = true;
}

void isNotStart(shParser *parser)
{
	defStart = false;
}

void isFinal(shParser *parser)
{
	defFinal = true;
	hadFinal = true;
}

void isNotFinal(shParser *parser)
{
	defFinal = false;
}

void addStateProperties(shParser *parser)
{
	if (defFinal)
		mid.isFinal[mid.stateCode[parser->lexer->current_token]] = true;
	if (defStart)
		mid.startState = mid.stateCode[parser->lexer->current_token];
}

void checkToHaveStartAndFinal(shParser *parser)
{
	if (!hadFinal)
		ERROR("error: no final state defined for this machine");
	if (mid.startState == NO_START_STATE)
		ERROR("error: no start state defined for this machine");
}

void newTransitionDef(shParser *parser)
{
	tid.hasWildCard = false;
	tid.letters.clear();
}

void newTransition(shParser *parser)
{
	shTMTransitionInput i;
	if (tid.hasWildCard)
	{
		i.state = tid.state;
		if (tid.tido.letter == WILD_CARD)
			for (vector<char>::iterator c = alphabet.begin(); c != alphabet.end(); ++c)
			{
				i.letter = *c;
				tid.tido.letter = *c;
				if (mid.transition.find(i) == mid.transition.end())	// if undefined, add
					mid.transition[i] = tid.tido;
			}
		else
			for (vector<char>::iterator c = alphabet.begin(); c != alphabet.end(); ++c)
			{
				i.letter = *c;
				if (mid.transition.find(i) == mid.transition.end())	// if undefined, add
					mid.transition[i] = tid.tido;
			}
	}
	else
	{
		i.state = tid.state;
		for (vector<char>::iterator c = tid.letters.begin(); c != tid.letters.end(); ++c)
		{
			i.letter = *c;
			if (mid.transition.find(i) != mid.transition.end())
				ERROR("error: transition for state %d and letter '%c' is already defined", i.state, i.letter);
			mid.transition[i] = tid.tido;
		}
	}
}

void newMachineCall(shParser *parser)
{
	if (mid.machineCall.find(tid.state) != mid.machineCall.end())
		ERROR("error: machine call is already defined for state %d", tid.state);
	if (machine[tid.tidmc.machine].erroneous)
		mid.erroneous = true;
	mid.machineCall[tid.state] = tid.tidmc;
}

void tidistate(shParser *parser)
{
	tid.state = mid.stateCode[parser->lexer->current_token];
}

void tidostate(shParser *parser)
{
	tid.tido.state = mid.stateCode[parser->lexer->current_token];
}

void tidomachine(shParser *parser)
{
	tid.tidmc.machine = machineCode[parser->lexer->current_token];
}

void tidomachinestate(shParser *parser)
{
	tid.tidmc.returnState = mid.stateCode[parser->lexer->current_token];
}

void tidiwild(shParser *parser)
{
	tid.hasWildCard = true;
}

void tidiletter(shParser *parser)
{
	if (!letterValid[(int)parser->lexer->current_token[0]])
		ERROR("error: invalid input letter '%c'", parser->lexer->current_token[0]);
	tid.letters.push_back(parser->lexer->current_token[0]);
}

void tidiblank(shParser *parser)
{
	tid.letters.push_back(BLANK);
}

void tidodirection(shParser *parser)
{
	if (strcmp(parser->lexer->current_token, "mright") == 0) tid.tido.direction = shTMTransitionOutput::RIGHT;
	else if (strcmp(parser->lexer->current_token, "mleft") == 0) tid.tido.direction = shTMTransitionOutput::LEFT;
	else if (strcmp(parser->lexer->current_token, "mnomove") == 0) tid.tido.direction = shTMTransitionOutput::NOMOVE;
	else ERROR("error: unknown direction %s", parser->lexer->current_token);
}

void tidowild(shParser *parser)
{
	tid.tido.letter = WILD_CARD;
}

void tidoblank(shParser *parser)
{
	tid.tido.letter = BLANK;
}

void tidoletter(shParser *parser)
{
	if (!letterValid[(int)parser->lexer->current_token[0]])
		ERROR("error: invalid output letter '%c'", parser->lexer->current_token[0]);
	tid.tido.letter = parser->lexer->current_token[0];
}

static string valueToComputeString;

void newValue(shParser *parser)
{
	stack_clear(rs);
	continue_evaluation(parser);
	valueToComputeString = "";
}

void machineToRun(shParser *parser)
{
	valueToComputeString += parser->lexer->current_token;
	if (machineCode.find(parser->lexer->current_token) == machineCode.end())
		ERROR("error: no such machine %s", parser->lexer->current_token);
	mtr = machineCode[parser->lexer->current_token];
	if (machine[mtr].erroneous)
	{
		stop_evaluation(parser);
		cout << "Interpretation error at line " << parser->lexer->line << ": Cannot run machine "
			<< parser->lexer->current_token << ". There is an error in its definition!" << endl;
	}
}

void machineInput(shParser *parser)
{
	string inp = "";
	for (int i = 1; i < (int)strlen(parser->lexer->current_token)-1; ++i)	// remove " from both sides
		if (letterValid[(int)parser->lexer->current_token[i]])
			inp += parser->lexer->current_token[i];
		else
			cout << "Warning at line " << parser->lexer->line << ": Invalid character '" << parser->lexer->current_token[i] <<
				"' in input string! Removing it!" << endl;
	valueToComputeString += "(\"";
	valueToComputeString += inp;
	valueToComputeString += "\") = ";
	tape.shTMTInitialize(inp);
}

void runMachine(shParser *parser)
{
	cout << valueToComputeString;
	if (!evaluate)
	{
		cout << "Error evaluating" << endl;
		return;
	}
	shTMRunningMachine rm;
	rm.machine = mtr;
	rm.state = machine[mtr].startState;
	rs.push(rm);
	while (!rs.empty())
	{
		int m = rs.top().machine;
		int s = rs.top().state;
		shTMTransitionInput ti;
		ti.state = s;
		ti.letter = tape.shTMTCurrent();
		if (machine[m].transition.find(ti) != machine[m].transition.end())
		{
			shTMTransitionOutput to = machine[m].transition[ti];
			rs.top().state = to.state;
			tape.shTMTWrite(to.letter);
			switch (to.direction)
			{
				case shTMTransitionOutput::RIGHT: tape.shTMTRight(); break;
				case shTMTransitionOutput::LEFT: tape.shTMTLeft(); break;
				case shTMTransitionOutput::NOMOVE: break;
				default: ERROR("error: undefined direction");
			}
		}
		else if (machine[m].machineCall.find(s) != machine[m].machineCall.end())
		{
			shTMMachineCall mc = machine[m].machineCall[s];
			rs.top().state = mc.returnState;		// set the return state
			rm.machine = mc.machine;
			rm.state = machine[mc.machine].startState;
			rs.push(rm);					// run the called machine
		}
		else
		{
			// No transition and no machine calls applicable. The TM has halted
			if (machine[m].isFinal[s])			// Halted in a final state
			{
				rs.pop();				// remove this machine, return state already set
			}
			else						// Halted in a non-final state
			{
				cout << "Machine halted on a non-final state with tape \"" <<
					tape.shTMTTape() << "\"" << endl;
				return;
			}
		}
	}
	cout << "\"" << tape.shTMTTape() << "\"" << endl;
}
