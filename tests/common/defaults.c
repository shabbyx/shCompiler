/*
 * Copyright (C) 2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shCompiler.
 *
 * shCompiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shCompiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shCompiler.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "test_defaults.h"

static sh_lexer_error_handler default_lexer_eh;
static sh_parser_llk_pleh default_llk_pleh;
static sh_parser_lrk_pleh default_lrk_pleh;
void stop_evaluation(shParser *parser);

static void lexer_eh(shLexer *lexer, char *fc, int *lb, int *f, int *l, int *c)
{
	sh_lexer_error_custom(lexer, NULL, 0, 0, "\n");
	sh_lexer_error_custom(lexer, lexer->file_name, *l, *c, "warning: no token can be matched, probably because of an invalid character (is it '");
	if (fc[*f] == '\n')
		sh_lexer_error_custom(lexer, NULL, 0, 0, "\\n");
	else if (fc[*f] == '\r')
		sh_lexer_error_custom(lexer, NULL, 0, 0, "\\r");
	else if (fc[*f] == '\t')
		sh_lexer_error_custom(lexer, NULL, 0, 0, "\\t");
	else if (fc[*f] >= ' ' && fc[*f] < 127)
		sh_lexer_error_custom(lexer, NULL, 0, 0, "%c", fc[*f]);
	else
		sh_lexer_error_custom(lexer, NULL, 0, 0, "\\%o", (unsigned int)fc[*f]);
	sh_lexer_error_custom(lexer, NULL, 0, 0, "'?)\n");
	default_lexer_eh(lexer, fc, lb, f, l, c);
}

static bool parser_llk_pleh(shParser *parser, const char *cur, shKtokens *la)
{
	stop_evaluation(parser);
	return default_llk_pleh(parser, cur, la);
}

static bool parser_lrk_pleh(shParser *parser, shKtokens *la)
{
	stop_evaluation(parser);
	return default_lrk_pleh(parser, la);
}

void test_print_tokens(shLexer *lexer, bool detailed)
{
	const char *token;
	int line = 0;
	const char *type;
	while (token = sh_lexer_next(lexer, &type), token != NULL)
	{
		if (!detailed)
		{
			/* if (lexer->space_met && line == lexer->line) */
				printf(" ");
			if (line != lexer->line)
			{
				line = lexer->line;
				printf("\n%d: ", line);
			}
			printf("%s", token);
		}
		else
			printf("%d:%d: %s is of type: %s\n", lexer->line, lexer->column, token, type);
	}
	printf("\nlexer test done\n");
}

int test_init_lexer(shLexer *lexer, struct prog_settings *settings)
{
	int error = 0;

	lexer->error_out = stdout;
	if ((error = sh_lexer_tokens(lexer, settings->letter_file, settings->token_file, settings->keyword_file, settings->rebuild_all)))
	{
		fprintf(stderr, "error: reading letters, tokens and keywords file (error: %d)\n", error);
		return error;
	}

	/* give lexer callbacks */
	default_lexer_eh = sh_lexer_set_error_handler(lexer, lexer_eh);

	if (settings->input_file)
		if ((error = test_open_file(lexer, settings)))
			fprintf(stderr, "error: opening input file '%s' (error: %d)\n", settings->input_file, error);
	return error;
}

int test_init_parser(shParser *parser, shLexer *lexer, struct prog_settings *settings, sh_grammar_type gtype)
{
	/* get the parser to read the grammar and check for errors in the grammar or incompatibility with grammar type */
	sh_parser_bind(parser, lexer);
	switch (sh_parser_read_grammar(parser, settings->grammar_file, gtype, settings->k, settings->rebuild_parser))
	{
	case SH_PARSER_NO_FILE: fprintf(stderr, "error: missing file when reading grammar\n"); return EXIT_FAILURE;
	case SH_PARSER_BAD_LEXER: fprintf(stderr, "error: bad lexer when reading grammar\n"); return EXIT_FAILURE;
	case SH_PARSER_GRAMMAR_ERROR: fprintf(stderr, "error: syntax error in grammar file\n"); return EXIT_FAILURE;
	case SH_PARSER_AMBIGUOUS: fprintf(stderr, "error: ambiguous grammar\n"); return EXIT_FAILURE;
	case SH_PARSER_NO_MEM: fprintf(stderr, "error: out of memory when reading grammar\n"); return EXIT_FAILURE;
	case SH_PARSER_LEFT_RECURSIVE: fprintf(stderr, "error: grammar is left recursive\n"); return EXIT_FAILURE;
	case SH_PARSER_SUCCESS:
		break;
	default:
		fprintf(stderr, "error: other error when reading grammar\n"); break;
		return EXIT_FAILURE;
	}

	if (gtype == SH_PARSER_LLK)
		default_llk_pleh = sh_parser_set_llk_pleh(parser, parser_llk_pleh);
	else
		default_lrk_pleh = sh_parser_set_lrk_pleh(parser, parser_lrk_pleh);

	return 0;
}

int test_open_file(shLexer *lexer, struct prog_settings *settings)
{
	if (sh_lexer_file(lexer, settings->input_file) == SH_LEXER_NO_FILE)
	{
		fprintf(stderr, "error: could not open file\n");
		return -1;
	}
	return 0;
}

void test_parse_file(shParser *parser)
{
	switch (sh_parser_parse(parser))
	{
	case SH_PARSER_BAD_LEXER: fprintf(stderr, "error: bad lexer when reading grammar\n"); break;
	case SH_PARSER_NO_RULES: fprintf(stderr, "error: no rules loaded for parser to parse\n"); break;
	case SH_PARSER_PARSE_ERROR: fprintf(stderr, "error: parse error\n"); break;
	case SH_PARSER_NO_MEM: fprintf(stderr, "error: out of memory during parse\n"); break;
		break;
	case SH_PARSER_SUCCESS:
		printf("Parse successful!\n");
		break;
	default:
		fprintf(stderr, "error: other error\n"); break;
		break;
	}
}
