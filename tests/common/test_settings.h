/*
 * Copyright (C) 2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shCompiler.
 *
 * shCompiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shCompiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shCompiler.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TEST_SETTINGS_H
#define TEST_SETTINGS_H

#include <shparser.h>

struct prog_settings
{
	const char *letter_file;
	const char *token_file;
	const char *keyword_file;
	const char *grammar_file;
	const char *input_file;
	unsigned int k;
	/* sh_grammar_type grammar_type; */ /* TODO: one day, perhaps you could just choose grammar type! */
	bool rebuild_all;
	bool rebuild_parser;
};

#ifdef __cplusplus
extern "C" {
#endif

int get_settings(int argc, char **argv, struct prog_settings *settings);

#ifdef __cplusplus
}
#endif

#endif
