/*
 * Copyright (C) 2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shCompiler.
 *
 * shCompiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shCompiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shCompiler.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <stdio.h>
#include "test_settings.h"

int get_settings(int argc, char **argv, struct prog_settings *settings)
{
	int i;
	for (i = 1; i < argc; ++i)
		if (strcmp(argv[i], "--help") == 0 || strcmp(argv[i], "-h") == 0)
		{
			printf("Usage: %s [-l <letter_file>] [-t <token_file>] [-k <keyword_file>]\n"
				"          [-g <grammar_file>] [-[1-9]\\+] [rebuild-parse|rebuild-all]\n"	/*  [-ll|-lr] */
				"          [-f <input file>]\n",
				argv[0]);
			return -1;
		}
		else if (strcmp(argv[i], "-l") == 0)
		{
			if (i + 1 >= argc) goto missing_argument;
			settings->letter_file = argv[++i];
		}
		else if (strcmp(argv[i], "-t") == 0)
		{
			if (i + 1 >= argc) goto missing_argument;
			settings->token_file = argv[++i];
		}
		else if (strcmp(argv[i], "-k") == 0)
		{
			if (i + 1 >= argc) goto missing_argument;
			settings->keyword_file = argv[++i];
		}
		else if (strcmp(argv[i], "-g") == 0)
		{
			if (i + 1 >= argc) goto missing_argument;
			settings->grammar_file = argv[++i];
		}
		else if (strcmp(argv[i], "-f") == 0)
		{
			if (i + 1 >= argc) goto missing_argument;
			settings->input_file = argv[++i];
		}
#if 0
		else if (strcmp(argv[i], "-ll") == 0)
			settings->grammar_type = SH_PARSER_LLK;
		else if (strcmp(argv[i], "-lr") == 0)
			settings->grammar_type = SH_PARSER_LRK;
#endif
		else if (argv[i][0] == '-' && sscanf(argv[i] + 1, "%u", &settings->k) == 1)
		{
			if (settings->k < 1) goto invalid_k;
		}
		else if (strcmp(argv[i], "rebuild-all") == 0)
		{
			settings->rebuild_all = true;
			settings->rebuild_parser = true;
		}
		else if (strcmp(argv[i], "rebuild-parse") == 0)
			settings->rebuild_parser = true;

	return 0;
missing_argument:
	printf("Missing argument after '%s'\n", argv[i]);
	goto exit_fail;
invalid_k:
	printf("Invalid value for k (%u)\n", settings->k);
	goto exit_fail;
exit_fail:
	printf("See --help\n");
	return -1;
}
