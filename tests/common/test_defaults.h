/*
 * Copyright (C) 2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shCompiler.
 *
 * shCompiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shCompiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shCompiler.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TEST_DEFAULTS_H
#define TEST_DEFAULTS_H

#include <shparser.h>
#include "test_settings.h"

#ifdef __cplusplus
extern "C" {
#endif

/* default test operations */
void test_print_tokens(shLexer *lexer, bool detailed);
int test_init_lexer(shLexer *lexer, struct prog_settings *settings);
int test_init_parser(shParser *parser, shLexer *lexer, struct prog_settings *settings, sh_grammar_type gtype);
int test_open_file(shLexer *lexer, struct prog_settings *settings);
void test_parse_file(shParser *parser);

#ifdef __cplusplus
}
#endif

#endif
