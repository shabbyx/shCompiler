/*
 * Copyright (C) 2007-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shCompiler.
 *
 * shCompiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shCompiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shCompiler.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <shcompiler.h>
#include <test_settings.h>
#include <test_defaults.h>

static shLexer lexer;
static shParser parser;

void stop_evaluation(shParser *parser) {}

int main(int argc, char **argv)
{
	int ret = EXIT_FAILURE;
	struct prog_settings settings = {
		.letter_file = "grammar/letters.in",
		.token_file = "grammar/tokens.in",
		.keyword_file = "grammar/keywords.in",
		.grammar_file = "grammar/grammar.in",
		.input_file = NULL,
		.k = 1,
	};
	if (get_settings(argc, argv, &settings))
		return EXIT_FAILURE;

	if (sh_lexer_init(&lexer))
		return EXIT_FAILURE;
	if (test_init_lexer(&lexer, &settings))
		goto exit_cleanup_lexer;

	if (sh_parser_init(&parser))
		goto exit_cleanup_lexer;

	/* get the parser to read the grammar and check for errors in the grammar or incompatibility with grammar type */
	sh_parser_ambiguity_resolution(&parser, SH_PARSER_ACCEPT_FIRST);
	if (test_init_parser(&parser, &lexer, &settings, SH_PARSER_LLK))
		goto exit_cleanup_parser;

	ret = 0;
exit_cleanup_parser:
	sh_parser_free(&parser);
exit_cleanup_lexer:
	sh_lexer_free(&lexer);

	return ret;
}
