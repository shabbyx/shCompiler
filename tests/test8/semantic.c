/*
 * Copyright (C) 2007-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shCompiler.
 *
 * shCompiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shCompiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shCompiler.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "semantic.h"

struct num_op
{
	enum p_type
	{
		NUMBER = 0,
		ADD,
		SUB,
		MUL,
		DIV
	} type;
	double value;
};

struct data
{
	shStack/* num_op */ semantics_stack;
	bool evaluate;				/* if lex/parse error, do nothing! */
};

static void _num_op_cpy(void *to, void *from)
{
	*(struct num_op *)to = *(struct num_op *)from;
}

static struct num_op _stack_pop(shStack *s)
{
	struct num_op top = *(struct num_op *)sh_stack_top(s);
	sh_stack_pop(s);
	return top;
}

int semantics_init(shParser *parser)
{
	struct data *d = malloc(sizeof *d);
	if (d == NULL)
		return -1;

	d->evaluate = true;
	sh_stack_init(&d->semantics_stack, _num_op_cpy, sizeof(struct num_op));

	parser->lexer->user_data = d;
	return 0;
}

void semantics_free(shParser *parser)
{
	struct data *d = parser->lexer->user_data;
	sh_stack_free(&d->semantics_stack);
	free(parser->lexer->user_data);
}

void stop_evaluation(shParser *parser)
{
	struct data *d = parser->lexer->user_data;
	d->evaluate = false;
}

void init(shParser *parser)
{
	struct data *d = parser->lexer->user_data;
	sh_stack_clear(&d->semantics_stack);
	d->evaluate = true;
}

#define ERROR_NOT_NUM(n) \
	do {\
		sh_lexer_error(parser->lexer, "internal error: there should have been a number on top of the stack, but it is of type %d", n.type);\
		stop_evaluation(parser);\
		return;\
	} while (0)

#define ERROR_NOT_OP(n) \
	do {\
		sh_lexer_error(parser->lexer, "internal error: there should have been an operator on top of the stack, but it is a number");\
		stop_evaluation(parser);\
		return;\
	} while (0)

#define ERROR_WRONG_STACK(n) \
	do {\
		sh_lexer_error(parser->lexer, "inernal error: semantic stack has %zu elements, while it should have had at least %d",\
			d->semantics_stack.size, n);\
		stop_evaluation(parser);\
		return;\
	} while (0)

void print_result(shParser *parser)
{
	struct data *d = parser->lexer->user_data;

	if (!d->evaluate)
		return;
	if (d->semantics_stack.size != 1)
	{
		sh_lexer_error(parser->lexer, "internal error: semantics stack has %zu elements, while it should have had 1", d->semantics_stack.size);
		stop_evaluation(parser);
		return;
	}

	struct num_op res = _stack_pop(&d->semantics_stack);			/* get the value on the stack */
	if (res.type != NUMBER)
		ERROR_NOT_NUM(res);

	printf("The expression evaluates to %lg\n", res.value);			/* print it */
}

void push_op_in_stack(shParser *parser)
{
	struct data *d = parser->lexer->user_data;
	const char *cur = parser->lexer->current_token;

	if (!d->evaluate)
		return;

	struct num_op op;
	if (strcmp(cur, "+") == 0)					/* set up operator */
		op.type = ADD;
	else if (strcmp(cur, "-") == 0)
		op.type = SUB;
	else if (strcmp(cur, "*") == 0)
		op.type = MUL;
	else if (strcmp(cur, "/") == 0)
		op.type = DIV;
	else
	{
		sh_lexer_error(parser->lexer, "internal error: '%s' received as operator", cur);
		stop_evaluation(parser);
		return;
	}

	sh_stack_push(&d->semantics_stack, &op);			/* push it in stack */
}

void compute(shParser *parser)
{
	struct data *d = parser->lexer->user_data;

	if (!d->evaluate)
		return;
	if (d->semantics_stack.size < 3)
		ERROR_WRONG_STACK(3);

	struct num_op operand2 = _stack_pop(&d->semantics_stack);	/* get the right operand from the stack */
	struct num_op op = _stack_pop(&d->semantics_stack);		/* get the operator from the stack */
	struct num_op operand1 = _stack_pop(&d->semantics_stack);	/* get the left operand from the stack */

	if (operand1.type != NUMBER)
		ERROR_NOT_NUM(operand1);
	if (operand2.type != NUMBER)
		ERROR_NOT_NUM(operand2);
	if (op.type != ADD && op.type != SUB && op.type != MUL && op.type != DIV)
		ERROR_NOT_OP(op);

	struct num_op res;
	res.type = NUMBER;

	switch (op.type)						/* compute the result */
	{
		case ADD:
			res.value = operand1.value + operand2.value;
			break;
		case SUB:
			res.value = operand1.value - operand2.value;
			break;
		case MUL:
			res.value = operand1.value * operand2.value;
			break;
		case DIV:
			if (operand2.value < 1e-10 && operand2.value > -1e-10)
			{
				sh_lexer_error(parser->lexer, "error: division by zero");
				stop_evaluation(parser);
				return;
			}
			res.value = operand1.value / operand2.value;
			break;
		default:
			sh_lexer_error(parser->lexer, "internal error: this should not have happened under any circumstances");
			stop_evaluation(parser);
			return;
	}
	printf("%lg <- %lg%c%lg\n", res.value, operand1.value,
			op.type == ADD?'+':op.type == SUB?'-':op.type == MUL?'*':'/',
			operand2.value);

	sh_stack_push(&d->semantics_stack, &res);			/* push it in stack */
}

void push_number_in_stack(shParser *parser)
{
	struct data *d = parser->lexer->user_data;

	if (!d->evaluate)
		return;

	struct num_op num;
	num.type = NUMBER;						/* set up number */

	if (sscanf(parser->lexer->current_token, "%lf", &num.value) != 1)
	{
		sh_lexer_error(parser->lexer, "internal error: '%s' received as real number", parser->lexer->current_token);
		stop_evaluation(parser);
		return;
	}

	sh_stack_push(&d->semantics_stack, &num);			/* push it in stack */
}

void negate(shParser *parser)
{
	struct data *d = parser->lexer->user_data;

	if (!d->evaluate)
		return;
	if (d->semantics_stack.size < 1)
		ERROR_WRONG_STACK(1);

	struct num_op res = _stack_pop(&d->semantics_stack);		/* get the value on the stack */
	if (res.type != NUMBER)
		ERROR_NOT_NUM(res);

	res.value = -res.value;						/* negate it */
	printf("%lg <- -%lg\n", res.value, -res.value);

	sh_stack_push(&d->semantics_stack, &res);			/* push it in stack */
}
