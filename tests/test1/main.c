/*
 * Copyright (C) 2007-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shCompiler.
 *
 * shCompiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shCompiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shCompiler.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <shcompiler.h>
#include <test_settings.h>
#include <test_defaults.h>
#include "semantic.h"

#define DO_PARSE 1
#define DO_LEX 1
#define DO_LEX_DETAILED 0

static shLexer lexer;
static shParser parser;

int main(int argc, char **argv)
{
	int ret = EXIT_FAILURE;
	struct prog_settings settings = {
		.letter_file = "grammar/letters.in",
		.token_file = "grammar/tokens.in",
		.keyword_file = "grammar/keywords.in",
		.grammar_file = "grammar/grammar.in",
		.input_file = "test.file",
		.k = 1,
	};
	if (get_settings(argc, argv, &settings))
		return EXIT_FAILURE;

	if (sh_lexer_init(&lexer))
		return EXIT_FAILURE;
	if (test_init_lexer(&lexer, &settings))
		goto exit_cleanup_lexer;
	/* redo read file to make sure proper cleanup is done */
	if (sh_lexer_file(&lexer, settings.input_file) == SH_LEXER_NO_FILE)
	{
		fprintf(stderr, "error: could not open file\n");
		goto exit_cleanup_lexer;
	}

#if DO_LEX
	/* the code below tests functionality of the lexer alone */
	test_print_tokens(&lexer, DO_LEX_DETAILED);
#endif

	if (sh_parser_init(&parser))
		goto exit_cleanup_lexer;

#ifdef DO_PARSE
	if (test_init_parser(&parser, &lexer, &settings, SH_PARSER_LLK))
		goto exit_cleanup_parser;

	/* give parser callbacks */
	sh_parser_set_action_routine(&parser, "@init", init);
	sh_parser_set_action_routine(&parser, "@print_result", print_result);
	sh_parser_set_action_routine(&parser, "@push_op_in_stack", push_op_in_stack);
	sh_parser_set_action_routine(&parser, "@compute", compute);
	sh_parser_set_action_routine(&parser, "@push_number_in_stack", push_number_in_stack);
	sh_parser_set_action_routine(&parser, "@negate", negate);

	if (semantics_init(&parser))
	{
		fprintf(stderr, "error: out of memory\n");
		goto exit_cleanup_parser;
	}

	/* do the actual parsing */
	if (test_open_file(&lexer, &settings) == 0)
		test_parse_file(&parser);

	/* continue the parse from stdin, to make sure it switches over and everything is ok */
	if (sh_lexer_interactive(&lexer, NULL, "stdin", NULL) != SH_LEXER_SUCCESS)
		fprintf(stderr, "error: failed to initialize lexer in interactive mode\n");
	else
		test_parse_file(&parser);

	semantics_free(&parser);
#endif

	ret = 0;
exit_cleanup_parser:
	sh_parser_free(&parser);
exit_cleanup_lexer:
	sh_lexer_free(&lexer);

	return ret;
}
