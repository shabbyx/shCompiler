/*
 * Copyright (C) 2007-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shCompiler.
 *
 * shCompiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shCompiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shCompiler.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SEMANTIC_H_BY_CYCLOPS
#define SEMANTIC_H_BY_CYCLOPS

#include <shcompiler.h>

void stop_evaluation(shParser *parser);

int semantics_init(shParser *parser);
void semantics_free(shParser *parser);

void init(shParser *parser);
void print_result(shParser *parser);
void push_op_in_stack(shParser *parser);
void compute(shParser *parser);
void push_number_in_stack(shParser *parser);
void negate(shParser *parser);

#endif
