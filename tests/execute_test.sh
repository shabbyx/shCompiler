#! /bin/bash

run_test()
{
  # run the test with given arguments
  echo "Running: ./test $1 < '$srcdir/input'"
  ./test $1 < "$srcdir/input" > temp 2>&1

  # remove path information from output errors and warnings
  sed -i 's,^.*/tests/[^/]*/,,g' temp

  # make sure it matches what's expected
  diff "$srcdir/$2" temp
  res=$?

  echo ""
  return $res
}

run_valgrind_test()
{
  # run the valgrind test on the executable itself
  echo "Running: valgrind --leak-check=full ./.libs/lt-test $1 < '$srcdir/input'"
  result=$(valgrind --leak-check=full ./.libs/lt-test $1 < "$srcdir/input" 2>&1 | grep --color=never 'in use at exit:\|== Invalid')

  # write the result to be logged
  echo "$result"
  echo ""

  # the result is in the shape: ==PID==     in use at exit: X bytes in Y blocks
  [ "$(echo "$result" | sed 's/==[0-9]*==\s*//')" = "in use at exit: 0 bytes in 0 blocks" ]
}

# rebuild-all tests are only done if output-rebuild exists (ktokens test for example doesn't have this)
[ -f "$srcdir/output-rebuild" ]; has_rebuild=$?
# normal build is done if it's possible to write the cache file (with make distcheck, that directory is read-only)
# this is unless has_rebuild is false, so normal run is the only option (again, ktokens is an example)
[ "$has_rebuild" -ne 0 -o -w "$srcdir/grammar" ]; normal_run=$?

# make sure the program is run first, so .libs/lt-test is created
# - rebuild-all is done first, so if the cache files don't exist, they are created
# - test without rebuild is done only if target directory is writable
if [ "$has_rebuild" -eq 0 ]; then
  run_test "rebuild-all $arguments" output-rebuild || exit $?
fi
if [ "$normal_run" -eq 0 ]; then
  run_test "$arguments" output || exit $?
fi

# then if valgrind exists, run valgrind tests too
# - run test without rebuild if cache exists
# - run test with rebuild-all as well
if [ "$have_valgrind" = y ]; then
  [ -x ./.libs/lt-test ] || exit 1

  if [ "$normal_run" -eq 0 ]; then
    run_valgrind_test "$arguments" || exit $?
  fi
  if [ "$has_rebuild" -eq 0 ]; then
    run_valgrind_test "rebuild-all $arguments" || exit $?
  fi
fi
