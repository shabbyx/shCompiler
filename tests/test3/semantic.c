/*
 * Copyright (C) 2007-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shCompiler.
 *
 * shCompiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shCompiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shCompiler.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "semantic.h"

struct num_op_var
{
	enum p_type
	{
		NUMBER = 0,
		VARIABLE,
		ADD,
		SUB,
		MUL,
		DIV
	} type;
	double value;		/* if number */
	char *name;		/* if variable */
};

struct variable_data
{
	double value;
	bool initialized;
};

struct data
{
	shStack/* num_op_var */ semantics_stack;
	shMap/* char *->struct variable_data */ variables;

	bool evaluate;				/* if lex/parse error, do nothing! */
	bool ignore;
	int nested;
};

static void _num_op_var_cpy(void *to, void *from)
{
	*(struct num_op_var *)to = *(struct num_op_var *)from;
}

static void _charp_cpy(void *to, void *from)
{
	*(char **)to = from;
}

static int _charp_cmp(const void *a, const void *b)
{
	return strcmp(*(char * const *)a, *(char * const *)b);
}

static void _var_data_cpy(void *to, void *from)
{
	*(struct variable_data *)to = *(struct variable_data *)from;
}

static struct num_op_var _stack_pop(shStack *s)
{
	struct num_op_var top = *(struct num_op_var *)sh_stack_top(s);
	sh_stack_pop(s);
	return top;
}

static bool _free_variable_names(shNode *n, void *extra)
{
	free(*(char **)n->key);
	return true;
}

int semantics_init(shParser *parser)
{
	struct data *d = malloc(sizeof *d);
	if (d == NULL)
		return -1;

	*d = (struct data){ .evaluate = true, };
	d->evaluate = true;
	sh_stack_init(&d->semantics_stack, _num_op_var_cpy, sizeof(struct num_op_var));
	sh_map_init(&d->variables, _charp_cpy, sizeof(char *), _var_data_cpy, sizeof(struct variable_data), _charp_cmp);

	parser->lexer->user_data = d;
	return 0;
}

void semantics_free(shParser *parser)
{
	struct data *d = parser->lexer->user_data;
	sh_stack_free(&d->semantics_stack);
	sh_map_for_each(&d->variables, _free_variable_names);
	sh_map_free(&d->variables);
	free(parser->lexer->user_data);
}

void stop_evaluation(shParser *parser)
{
	struct data *d = parser->lexer->user_data;
	d->evaluate = false;
}

void init(shParser *parser)
{
	struct data *d = parser->lexer->user_data;
	sh_stack_clear(&d->semantics_stack);
	sh_map_clear(&d->variables);
}

#define ERROR_NOT_VAR(n) \
	do {\
		sh_lexer_error(parser->lexer, "internal error: there should have been a variable on top of the stack, but it is of type %d", n.type);\
		stop_evaluation(parser);\
		return;\
	} while (0)

#define ERROR_NOT_NUM_OR_VAR(n) \
	do {\
		sh_lexer_error(parser->lexer, "internal error: there should have been a number or variable on top of the stack, but it is of type %d", n.type);\
		stop_evaluation(parser);\
		return;\
	} while (0)

#define ERROR_NOT_OP(n) \
	do {\
		sh_lexer_error(parser->lexer, "internal error: there should have been an operator on top of the stack, but it is a number");\
		stop_evaluation(parser);\
		return;\
	} while (0)

#define ERROR_WRONG_STACK(n) \
	do {\
		sh_lexer_error(parser->lexer, "inernal error: semantic stack has %zu elements, while it should have had at least %d",\
			d->semantics_stack.size, n);\
		stop_evaluation(parser);\
		return;\
	} while (0)

static double _get_value(shParser *parser, struct num_op_var *stack_element)
{
	struct data *d = parser->lexer->user_data;

	if (stack_element->type == VARIABLE)
	{
		shNode n;
		struct variable_data uninitialized = {0};
		int res = sh_map_insert(&d->variables, stack_element->name, &uninitialized, &n);

		if (res < 0)
		{
			sh_parser_stop(parser, SH_PARSER_NO_MEM);
			return 0;
		}
		struct variable_data *vd = n.data;

		/* if just inserted, or was previously inserted but flagged as uninitialized, warn */
		if (res == 0 || !vd->initialized)
			sh_lexer_error(parser->lexer, "warning: use of uninitialized variable '%s'", stack_element->name);

		return vd->value;
	}

	return stack_element->value;
}

static bool _print_variable(shNode *n, void *extra)
{
	struct variable_data *vd = n->data;

	printf("%s: ", *(char **)n->key);

	if (vd->initialized)
		printf("%lg\n", vd->value);
	else
		printf("uninitialized\n");

	return true;
}

void print_result(shParser *parser)
{
	struct data *d = parser->lexer->user_data;

	if (!d->evaluate)
		return;
	if (d->semantics_stack.size != 0)
	{
		sh_lexer_error(parser->lexer, "internal error: parse stack has %zu elements, while it should have been empty",
				d->semantics_stack.size);
		stop_evaluation(parser);
		return;
	}

	sh_map_inorder(&d->variables, _print_variable);
}

static int _add_variable(shParser *parser, const char *name, shNode *n)
{
	struct data *d = parser->lexer->user_data;

	if (sh_map_find(&d->variables, &name, n))
		return 0;

	struct variable_data vd = {0};
	char *name_dup = strdup(name);

	/* if new, add it to the variables list */
	if (name_dup == NULL || sh_map_insert(&d->variables, name_dup, &vd, n) != 0)
	{
		sh_parser_stop(parser, SH_PARSER_NO_MEM);
		free(name_dup);
		return -1;
	}

	return 1;
}

void push_variable_in_stack_possibly_new(shParser *parser)
{
	struct data *d = parser->lexer->user_data;

	if (!d->evaluate || d->ignore)
		return;

	shNode n;
	int res = _add_variable(parser, parser->lexer->current_token, &n);
	if (res < 0)
		return;

	struct num_op_var var = {
		.type = VARIABLE,					/* create a reference to the variable */
		.name = *(char **)n.key,
	};

	sh_stack_push(&d->semantics_stack, &var);			/* push it in stack */
}

void store_in_variable(shParser *parser)
{
	struct data *d = parser->lexer->user_data;

	if (!d->evaluate || d->ignore)
		return;
	if (d->semantics_stack.size < 2)
		ERROR_WRONG_STACK(2);

	struct num_op_var rhs = _stack_pop(&d->semantics_stack);	/* get the right hand side from the stack */
	struct num_op_var var = _stack_pop(&d->semantics_stack);	/* get the variable from the stack */
	if (rhs.type != NUMBER && rhs.type != VARIABLE)
		ERROR_NOT_NUM_OR_VAR(rhs);
	if (var.type != VARIABLE)
		ERROR_NOT_VAR(var);

	double val = _get_value(parser, &rhs);;
	shNode n;
	if (!sh_map_find(&d->variables, &var.name, &n))
		sh_lexer_error(parser->lexer, "internal error: previously defined variable '%s' missing from symbol table", var.name);
	else
		*(struct variable_data *)n.data = (struct variable_data){
			.value = val,					/* set the variable to the value of RHS */
			.initialized = true,
		};
}

void push_op_in_stack(shParser *parser)
{
	struct data *d = parser->lexer->user_data;
	const char *cur = parser->lexer->current_token;

	if (!d->evaluate || d->ignore)
		return;

	struct num_op_var op;
	if (strcmp(cur, "+") == 0)					/* set up operator */
		op.type = ADD;
	else if (strcmp(cur, "-") == 0)
		op.type = SUB;
	else if (strcmp(cur, "*") == 0)
		op.type = MUL;
	else if (strcmp(cur, "/") == 0)
		op.type = DIV;
	else
	{
		sh_lexer_error(parser->lexer, "internal error: '%s' received as operator", cur);
		stop_evaluation(parser);
		return;
	}

	sh_stack_push(&d->semantics_stack, &op);			/* push it in stack */
}

void compute(shParser *parser)
{
	struct data *d = parser->lexer->user_data;

	if (!d->evaluate || d->ignore)
		return;
	if (d->semantics_stack.size < 3)
		ERROR_WRONG_STACK(3);

	struct num_op_var operand2 = _stack_pop(&d->semantics_stack);	/* get the right operand from the stack */
	struct num_op_var op = _stack_pop(&d->semantics_stack);		/* get the operator from the stack */
	struct num_op_var operand1 = _stack_pop(&d->semantics_stack);	/* get the left operand from the stack */

	if (operand1.type != NUMBER && operand1.type != VARIABLE)
		ERROR_NOT_NUM_OR_VAR(operand1);
	if (operand2.type != NUMBER && operand2.type != VARIABLE)
		ERROR_NOT_NUM_OR_VAR(operand2);
	if (op.type != ADD && op.type != SUB && op.type != MUL && op.type != DIV)
		ERROR_NOT_OP(op);

	struct num_op_var res;
	res.type = NUMBER;

	double val1 = _get_value(parser, &operand1), val2 = _get_value(parser, &operand2);
	switch (op.type)						/* compute the result */
	{
		case ADD:
			res.value = val1 + val2;
			break;
		case SUB:
			res.value = val1 - val2;
			break;
		case MUL:
			res.value = val1 * val2;
			break;
		case DIV:
			if (val2 < 1e-10 && val2 > -1e-10)
			{
				sh_lexer_error(parser->lexer, "error: division by zero");
				stop_evaluation(parser);
				return;
			}
			res.value = val1 / val2;
			break;
		default:
			sh_lexer_error(parser->lexer, "internal error: this should not have happened under any circumstances");
			stop_evaluation(parser);
			return;
	}
	printf("%lg <- %lg%c%lg\n", res.value, val1,
			op.type == ADD?'+':op.type == SUB?'-':op.type == MUL?'*':'/',
			val2);

	sh_stack_push(&d->semantics_stack, &res);			/* push it in stack */
}

void push_variable_in_stack(shParser *parser)
{
	struct data *d = parser->lexer->user_data;

	if (!d->evaluate || d->ignore)
		return;

	shNode n;
	int res = _add_variable(parser, parser->lexer->current_token, &n);
	if (res < 0)
		return;

	struct num_op_var var = {
		.type = VARIABLE,					/* set up variable */
		.name = *(char **)n.key,
	};

	if (res > 0)
		sh_lexer_error(parser->lexer, "warning: use of uninitialized variable '%s'", var.name);

	sh_stack_push(&d->semantics_stack, &var);			/* push it in stack */
}

void push_number_in_stack(shParser *parser)
{
	struct data *d = parser->lexer->user_data;

	if (!d->evaluate || d->ignore)
		return;

	struct num_op_var num = { .type = NUMBER, };			/* set up number */

	if (sscanf(parser->lexer->current_token, "%lf", &num.value) != 1)
	{
		sh_lexer_error(parser->lexer, "internal error: '%s' received as real number", parser->lexer->current_token);
		stop_evaluation(parser);
		return;
	}

	sh_stack_push(&d->semantics_stack, &num);			/* push it in stack */
}

void negate(shParser *parser)
{
	struct data *d = parser->lexer->user_data;

	if (!d->evaluate || d->ignore)
		return;
	if (d->semantics_stack.size < 1)
		ERROR_WRONG_STACK(1);

	struct num_op_var res = _stack_pop(&d->semantics_stack);	/* get the value on the stack */
	if (res.type != NUMBER && res.type != VARIABLE)
		ERROR_NOT_NUM_OR_VAR(res);

	double val = _get_value(parser, &res);
	res.value = -val;						/* negate it */
	printf("%lg <- -%lg\n", res.value, -res.value);

	sh_stack_push(&d->semantics_stack, &res);			/* push it in stack */
}

void possibly_ignore_if(shParser *parser)
{
	struct data *d = parser->lexer->user_data;

	if (!d->evaluate)
		return;
	if (d->ignore && d->nested > 0)
	{
		++d->nested;
		return;
	}

	if (d->semantics_stack.size < 1)
		ERROR_WRONG_STACK(1);

	struct num_op_var cond = *(struct num_op_var *)sh_stack_top(&d->semantics_stack);	/* get the value of if condition on the stack */
	if (cond.type != NUMBER && cond.type != VARIABLE)
		ERROR_NOT_NUM_OR_VAR(cond);

	double val = _get_value(parser, &cond);
	if (val < 1e-10 && val > -1e-10)				/* if 0, then ignore if */
	{
		d->ignore = true;
		d->nested = 1;
	}
	else
		d->ignore = false;
}

void possibly_ignore_else(shParser *parser)
{
	struct data *d = parser->lexer->user_data;

	if (!d->evaluate)
		return;
	if (d->ignore && d->nested > 0)
	{
		++d->nested;
		return;
	}
	if (d->semantics_stack.size < 1)
		ERROR_WRONG_STACK(1);

	struct num_op_var cond = *(struct num_op_var *)sh_stack_top(&d->semantics_stack);	/* get the value of if condition on the stack */
	if (cond.type != NUMBER && cond.type != VARIABLE)
		ERROR_NOT_NUM_OR_VAR(cond);

	double val = _get_value(parser, &cond);
	if (val < 1e-10 && val > -1e-10)				/* if not 0, then ignore else */
		d->ignore = false;
	else
	{
		d->ignore = true;
		d->nested = 1;
	}
}

void if_part_over(shParser *parser)
{
	struct data *d = parser->lexer->user_data;

	--d->nested;
	if (d->nested == 0)
		d->ignore = false;
}

void else_part_over(shParser *parser)
{
	struct data *d = parser->lexer->user_data;

	--d->nested;
	if (d->nested == 0)
		d->ignore = false;
}

void if_finished(shParser *parser)
{
	struct data *d = parser->lexer->user_data;

	if (!d->evaluate || d->ignore)
		return;
	if (d->semantics_stack.size < 1)
		ERROR_WRONG_STACK(1);

	sh_stack_pop(&d->semantics_stack);				/* remove the evaluated condition from the stack */
	d->nested = 0;
}
