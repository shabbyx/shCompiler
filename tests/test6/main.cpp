/*
 * Copyright (C) 2007-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shCompiler.
 *
 * shCompiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shCompiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shCompiler.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <shcompiler.h>
#include <test_settings.h>
#include <test_defaults.h>
#include "semantic.h"

#define DO_PARSE 1
#define DO_LEX 1
#define DO_LEX_DETAILED 0

static shLexer lexer;
static shParser parser;

int main(int argc, char **argv)
{
	struct prog_settings settings = {0};
	settings.letter_file = "grammar/letters.in";
	settings.token_file = "grammar/tokens.in";
	settings.keyword_file = "grammar/keywords.in";
	settings.grammar_file = "grammar/grammar.in";
	settings.input_file = "test.file";
	settings.k = 1;
	if (get_settings(argc, argv, &settings))
		return EXIT_FAILURE;

	if (test_init_lexer(&lexer, &settings))
		return EXIT_FAILURE;

#if DO_LEX
	/* the code below tests functionality of the lexer alone */
	test_print_tokens(&lexer, DO_LEX_DETAILED);
#endif

#ifdef DO_PARSE
	if (test_init_parser(&parser, &lexer, &settings, SH_PARSER_LLK))
		return EXIT_FAILURE;

	/* give parser callbacks */
	sh_parser_set_action_routine(&parser, "@newFunction", newFunction);
	sh_parser_set_action_routine(&parser, "@funcDefEnd", funcDefEnd);
	sh_parser_set_action_routine(&parser, "@funcIsRecursion", funcIsRecursion);
	sh_parser_set_action_routine(&parser, "@funcIsNotRecursion", funcIsNotRecursion);
	sh_parser_set_action_routine(&parser, "@addArgAsFunction", addArgAsFunction);
	sh_parser_set_action_routine(&parser, "@addArgAsValue", addArgAsValue);
	sh_parser_set_action_routine(&parser, "@setArgName", setArgName);
	sh_parser_set_action_routine(&parser, "@startCheckingRecursionArgs", startCheckingRecursionArgs);
	sh_parser_set_action_routine(&parser, "@checkRecursionFirstArg", checkRecursionFirstArg);
	sh_parser_set_action_routine(&parser, "@checkWithDefName", checkWithDefName);
	sh_parser_set_action_routine(&parser, "@checkArgCount", checkArgCount);
	sh_parser_set_action_routine(&parser, "@selfNotAllowed", selfNotAllowed);
	sh_parser_set_action_routine(&parser, "@selfAllowed", selfAllowed);
	sh_parser_set_action_routine(&parser, "@initValueDef", initValueDef);
	sh_parser_set_action_routine(&parser, "@no0", no0);
	sh_parser_set_action_routine(&parser, "@with0", with0);
	sh_parser_set_action_routine(&parser, "@valueDefNewComp", valueDefNewComp);
	sh_parser_set_action_routine(&parser, "@valueDefNumber", valueDefNumber);
	sh_parser_set_action_routine(&parser, "@valueDefZero", valueDefZero);
	sh_parser_set_action_routine(&parser, "@valueDefOne", valueDefOne);
	sh_parser_set_action_routine(&parser, "@valueDefPrevious", valueDefPrevious);
	sh_parser_set_action_routine(&parser, "@valueDefID", valueDefID);
	sh_parser_set_action_routine(&parser, "@valueDefIsFuncCall", valueDefIsFuncCall);
	sh_parser_set_action_routine(&parser, "@valueDefAddToArgs", valueDefAddToArgs);
	sh_parser_set_action_routine(&parser, "@setValueDefToF1", setValueDefToF1);
	sh_parser_set_action_routine(&parser, "@setValueDefToF2", setValueDefToF2);
	sh_parser_set_action_routine(&parser, "@print", print);
	sh_parser_set_action_routine(&parser, "@printValue", printValue);
	sh_parser_set_action_routine(&parser, "@initValue", initValue);
	sh_parser_set_action_routine(&parser, "@valueNewComp", valueNewComp);
	sh_parser_set_action_routine(&parser, "@valueNumber", valueNumber);
	sh_parser_set_action_routine(&parser, "@valueZero", valueZero);
	sh_parser_set_action_routine(&parser, "@valueOne", valueOne);
	sh_parser_set_action_routine(&parser, "@valueID", valueID);
	sh_parser_set_action_routine(&parser, "@valueIsFuncCall", valueIsFuncCall);
	sh_parser_set_action_routine(&parser, "@valueCompute", valueCompute);
	sh_parser_set_action_routine(&parser, "@valueAddToArgs", valueAddToArgs);

	initializeSemantics(&parser);

	/* do the actual parsing */
	if (test_open_file(&lexer, &settings) == 0)
		test_parse_file(&parser);
#endif

	return 0;
}
