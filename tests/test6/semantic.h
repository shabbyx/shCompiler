/*
 * Copyright (C) 2007-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shCompiler.
 *
 * shCompiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shCompiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shCompiler.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SEMANTIC_H_BY_CYCLOPS
#define SEMANTIC_H_BY_CYCLOPS

#include <string>
#include <list>
#include <vector>
#include <map>
#include <shcompiler.h>

using namespace std;

void initializeSemantics(shParser *parser);
extern "C" void stop_evaluation(shParser *parser);

void newFunction(shParser *parser);
void funcDefEnd(shParser *parser);
void funcIsRecursion(shParser *parser);
void funcIsNotRecursion(shParser *parser);
void addArgAsFunction(shParser *parser);
void addArgAsValue(shParser *parser);
void setArgName(shParser *parser);
void startCheckingRecursionArgs(shParser *parser);
void checkRecursionFirstArg(shParser *parser);
void checkWithDefName(shParser *parser);
void checkArgCount(shParser *parser);
void selfNotAllowed(shParser *parser);
void selfAllowed(shParser *parser);
void initValueDef(shParser *parser);
void no0(shParser *parser);
void with0(shParser *parser);
void valueDefNewComp(shParser *parser);
void valueDefNumber(shParser *parser);
void valueDefZero(shParser *parser);
void valueDefOne(shParser *parser);
void valueDefPrevious(shParser *parser);
void valueDefID(shParser *parser);
void valueDefIsFuncCall(shParser *parser);
void valueDefAddToArgs(shParser *parser);
void setValueDefToF1(shParser *parser);
void setValueDefToF2(shParser *parser);
void print(shParser *parser);
void printValue(shParser *parser);
void initValue(shParser *parser);
void valueNewComp(shParser *parser);
void valueNumber(shParser *parser);
void valueZero(shParser *parser);
void valueOne(shParser *parser);
void valueID(shParser *parser);
void valueIsFuncCall(shParser *parser);
void valueCompute(shParser *parser);
void valueAddToArgs(shParser *parser);

#endif
