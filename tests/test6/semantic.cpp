/*
 * Copyright (C) 2007-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shCompiler.
 *
 * shCompiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shCompiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shCompiler.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cstdio>
#include <cstring>
#include <iostream>
#include <fstream>
#include <stack>
#include "semantic.h"

using namespace std;

//#define SH_DEBUG

#define SH_UNDEFINED_FUNCTION		-1
#define SH_SELF_FUNCTION		-2

struct shArgument
{
	bool isFunc;
	string name;
};

struct shComposition
{
	bool isSimple;		// like 2, a, x
	enum
	{
		NUMBER = 0,
		ARGUMENT,
		GLOBAL,		// that is always a function
		SELF,
		UNDEFINED,
		ARGUMENT0
	} type;
	int value_index;	// if 2 -> 2, if arg -> index in arg list, if global function, index in functions
				// if undefined -> SH_UNDEFINED_FUNCTION
				// if self (in recursion) -> SH_SELF_FUNCTION
	// If not simple, the following is used:
	vector<shComposition> funcArgs;
	void clear() { funcArgs.clear(); }
};

struct shFunctionType
{
	bool isRecursion;
	bool needsPrev;		// If it was: f(x+1) = x -> false, if it was f(x+1)=s(f(x)) -> true, only if recursion
	shComposition f1;
	shComposition f2;	// if it was recursion, there are two compositions defining the function
	void clear() { f1.clear(); f2.clear(); }
};

struct shFunction
{
	vector<shArgument> arg;
	bool erroneous;		// if erroneous, won't be evaluated
//	bool returnsFunction;	// if returns function -> true
	shFunctionType func;
	void clear() { arg.clear(); func.clear(); erroneous = false; }
};

struct shValue
{
	int v;
	bool isFunc;
};

struct shStackElement
{
	shFunction func;
	vector<shValue> arg;	// actual arguments
	shValue returnValue;	// the return value!
	bool isBeingEvaluated;	// if in evaluation -> true, if finished -> false
	void clear() { arg.clear(); func.clear(); isBeingEvaluated = false; }
};

static map<string, int> functionCode;		// maps function names to their codes
static vector<shFunction> function;		// list of functions
static stack<shStackElement> es;		// evaluation stack
static bool evaluate;				// if lex/parse error, do nothing!
static shFunction fid;				// the function that is currently being defined
static string funcInDefinitionName;		// name of the function in definition
static bool funcInDefHasNamePreviouslyUsed;
static bool inFunctionDefinition;		// if was in funcion definition
static bool canUseSelf;
static bool usedPrevious;
static bool firstPartOfRecursion;
static stack<shComposition> ds;			// definition stack

#define stack_clear(s) while(!s.empty()) s.pop()

void initializeSemantics(shParser *parser)
{
	evaluate = true;
	functionCode.clear();
	function.clear();
	stack_clear(es);
	stack_clear(ds);
	fid.clear();
	funcInDefHasNamePreviouslyUsed = false;
	inFunctionDefinition = false;
	canUseSelf = false;
	usedPrevious = false;
	firstPartOfRecursion = false;
	// Initialize s
	functionCode["s"] = 0;
	shFunction s;			// function 0 is s, no need to specify it
	s.erroneous = false;
	shArgument s_arg;
	s_arg.name = "";
	s_arg.isFunc = false;
	s.arg.push_back(s_arg);
	s.func.isRecursion = false;
	s.func.needsPrev = false;
	s.func.f1.isSimple = false;
	s.func.f1.type = shComposition::GLOBAL;
	s.func.f1.value_index = 0;
	shComposition fa;
	fa.isSimple = true;
	fa.type = shComposition::ARGUMENT;
	fa.value_index = 0;		// it says that s(x) = x, but when computing, I ++ it!
	s.func.f1.funcArgs.push_back(fa);
	function.push_back(s);
}

void stop_evaluation(shParser *parser)
{
	evaluate = false;
}

void continue_evaluation(shParser *parser)
{
	evaluate = true;
}

#define ERROR(...) do { fid.erroneous = true; stop_evaluation(parser); sh_lexer_error(parser->lexer, __VA_ARGS__); } while (0)

// utilities
int argIndex(shFunction &f, string name)
{
	int c = 0;
	for (vector<shArgument>::iterator i = f.arg.begin(); i != f.arg.end(); ++i, ++c)
		if (i->name == name)
			return c;
	return -1;
}

int funcIndex(string name)
{
	if (functionCode.find(name) == functionCode.end())
		return -1;
	return functionCode[name];
}

#ifdef SH_DEBUG
static void _pv(shComposition c, int d)
{
	for (int i = 0; i < d; ++i)
		cout << "  ";
	switch (c.type)
	{
		case shComposition::NUMBER:
			cout << c.value_index << endl;
			break;
		case shComposition::ARGUMENT:
			cout << "argument #" << c.value_index;
			if (c.isSimple)
				cout << endl;
			else
			{
				cout << "(" << endl;
				for (vector<shComposition>::iterator i = c.funcArgs.begin();
						i != c.funcArgs.end(); ++i)
					_pv(*i, d+1);
				for (int i = 0; i < d; ++i)
					cout << "  ";
				cout << ")" << endl;
			}
			break;
		case shComposition::GLOBAL:
			cout << "function #" << c.value_index;
			cout << "(" << endl;
			for (vector<shComposition>::iterator i = c.funcArgs.begin();
					i != c.funcArgs.end(); ++i)
				_pv(*i, d+1);
			for (int i = 0; i < d; ++i)
				cout << "  ";
			cout << ")" << endl;
			break;
		case shComposition::SELF:
			cout << "self" << endl;
			break;
		case shComposition::UNDEFINED:
			cout << "undefined" << endl;
			break;
		default:
			cout << "WTF?" << endl;
			break;
	}
}

void printValue(shFunction &f)
{
	cout << "New " << (f.erroneous?"erroneous ":"") << (f.func.isRecursion?"recursive":"composite") <<
		" function defined: " << funcInDefinitionName <<
		((f.arg.size() != 0)?" with parameters:":" without any parameters!") << endl;
	cout << (f.func.needsPrev?"\t->uses previous value<-":"\t->doesn't need previous value<-") << endl;
	for (int i = 0; i < (int)f.arg.size(); ++i)
		cout << f.arg[i].name << (f.arg[i].isFunc?": function":": value") << endl;
	cout << "value=";
	_pv(f.func.f1, 0);
	if (f.func.isRecursion)
	{
		cout << "second value=";
		_pv(f.func.f2, 0);
	}
}
#endif
// seitilitu

void newFunction(shParser *parser)
{
	funcInDefinitionName = parser->lexer->current_token;
	fid.clear();
	inFunctionDefinition = true;
	if (functionCode.find(parser->lexer->current_token) != functionCode.end())
	{
		funcInDefHasNamePreviouslyUsed = true;
		ERROR("error: a function with this name has already been defined");
	}
	funcInDefHasNamePreviouslyUsed = false;
	usedPrevious = false;
}

void funcDefEnd(shParser *parser)
{
	if (usedPrevious)
		fid.func.needsPrev = true;
	else
		fid.func.needsPrev = false;
	if (!funcInDefHasNamePreviouslyUsed)
	{
		functionCode[funcInDefinitionName] = function.size();
		function.push_back(fid);
	}
#ifdef SH_DEBUG
	printValue(fid);
#endif
	fid.clear();
	inFunctionDefinition = false;
}

void funcIsRecursion(shParser *parser)
{
	fid.func.isRecursion = true;
}

void funcIsNotRecursion(shParser *parser)
{
	fid.func.isRecursion = false;
}

void addArgAsFunction(shParser *parser)
{
	shArgument a;
	a.isFunc = true;
	fid.arg.push_back(a);
}

void addArgAsValue(shParser *parser)
{
	shArgument a;
	a.isFunc = false;
	fid.arg.push_back(a);
}

void setArgName(shParser *parser)
{
	fid.arg[fid.arg.size()-1].name = parser->lexer->current_token;
}

static int argToCheck = -100;

void startCheckingRecursionArgs(shParser *parser)
{
	argToCheck = 1;
}

void checkRecursionFirstArg(shParser *parser)
{
	if (fid.arg.size() == 0)
		ERROR("error: cannot have recursion with no parameters");
	if (strcmp(parser->lexer->current_token, fid.arg[0].name.c_str()) != 0)
		ERROR("error: argument 0 (%s), must match definition name (%s)", parser->lexer->current_token, fid.arg[0].name.c_str());
}

void checkWithDefName(shParser *parser)
{
	if (argToCheck == (int)fid.arg.size())
	{
		++argToCheck;
		ERROR("error: too many parameters");
	}
	if (strcmp(parser->lexer->current_token, fid.arg[argToCheck].name.c_str()) != 0)
	{
		++argToCheck;
		ERROR("error: argument %d (%s), must match definition name (%s)", argToCheck - 1, parser->lexer->current_token, fid.arg[argToCheck - 1].name.c_str());
	}
	++argToCheck;
}

void checkArgCount(shParser *parser)
{
	if (argToCheck < (int)fid.arg.size())
		ERROR("error: too few parameters");
}

void selfNotAllowed(shParser *parser)
{
	canUseSelf = false;
}

void selfAllowed(shParser *parser)
{
	canUseSelf = true;
}

void initValueDef(shParser *parser)
{
	stack_clear(ds);
}

void no0(shParser *parser)
{
	firstPartOfRecursion = true;
}

void with0(shParser *parser)
{
	firstPartOfRecursion = false;
}

void valueDefNewComp(shParser *parser)
{
	shComposition c;
	ds.push(c);
}

void valueDefNumber(shParser *parser)
{
	ds.top().isSimple = true;
	ds.top().type = shComposition::NUMBER;
	int v;
	sscanf(parser->lexer->current_token, "%d", &v);
	ds.top().value_index = v;
}

void valueDefZero(shParser *parser)
{
	ds.top().isSimple = true;
	ds.top().type = shComposition::NUMBER;
	ds.top().value_index = 0;
}

void valueDefOne(shParser *parser)
{
	ds.top().isSimple = true;
	ds.top().type = shComposition::NUMBER;
	ds.top().value_index = 1;
}

void valueDefPrevious(shParser *parser)
{
	if (canUseSelf)
	{
		ds.top().isSimple = true;
		ds.top().type = shComposition::SELF;
		ds.top().value_index = SH_SELF_FUNCTION;
		usedPrevious = true;
	}
	else
		ERROR("error: cannot use previous at this position");
}

void valueDefID(shParser *parser)
{
	ds.top().isSimple = true;	// If is function, changes later
	int index = argIndex(fid, parser->lexer->current_token);
	if (index != -1)		// then is arg
	{
		if (index == 0 && firstPartOfRecursion)
			ERROR("error: cannot use first argument here");
		ds.top().type = shComposition::ARGUMENT;
		ds.top().value_index = index;
		return;
	}
	index = funcIndex(parser->lexer->current_token);	// if not an argument
	if (index != -1)		// then is global function
	{
		ds.top().type = shComposition::GLOBAL;
		ds.top().value_index = index;
		return;
	}
	ds.top().type = shComposition::UNDEFINED;	// undefined
	ds.top().value_index = SH_UNDEFINED_FUNCTION;
	fid.erroneous = true;
	ERROR("error: undefined id %s", parser->lexer->current_token);
}

void valueDefIsFuncCall(shParser *parser)
{
	ds.top().isSimple = false;
	if (ds.top().type == shComposition::GLOBAL)
		if (function[ds.top().value_index].erroneous)
			fid.erroneous = true;
}

void valueDefAddToArgs(shParser *parser)
{
	shComposition c = ds.top();
	ds.pop();
	ds.top().funcArgs.push_back(c);
}

void setValueDefToF1(shParser *parser)
{
#ifdef SH_DEBUG
	if (ds.size() != 1)
		ERROR("error: DS is not empty");
#endif
	fid.func.f1 = ds.top();
}

void setValueDefToF2(shParser *parser)
{
#ifdef SH_DEBUG
	if (ds.size() != 1)
		ERROR("error: DS is not empty");
#endif
	fid.func.f2 = ds.top();
}

string valueString;

void print(shParser *parser)
{
	valueString += parser->lexer->current_token;
}

void printValue(shParser *parser)
{
	cout << valueString;
	if (!evaluate)
		cout << " = error while evaluating!" << endl;
	else if (es.size() != 1)
		cout << " = error while evaluating! es has size: " << es.size() << endl;
	else
		cout << " = " << (es.top().returnValue.isFunc?"(function)":"") << es.top().returnValue.v << endl;
}

void initValue(shParser *parser)
{
	stack_clear(es);
	continue_evaluation(parser);
	valueString = "";
}

void valueNewComp(shParser *parser)
{
	if (!evaluate)
		return;
	shStackElement s;
	s.isBeingEvaluated = true;
	es.push(s);
}

void valueNumber(shParser *parser)
{
	if (!evaluate)
		return;
	es.top().isBeingEvaluated = false;
	int v;
	sscanf(parser->lexer->current_token, "%d", &v);
	es.top().returnValue.v = v;
	es.top().returnValue.isFunc = false;
}

void valueZero(shParser *parser)
{
	if (!evaluate)
		return;
	es.top().isBeingEvaluated = false;
	es.top().returnValue.v = 0;
	es.top().returnValue.isFunc = false;
}

void valueOne(shParser *parser)
{
	if (!evaluate)
		return;
	es.top().isBeingEvaluated = false;
	es.top().returnValue.v = 1;
	es.top().returnValue.isFunc = false;
}

void valueID(shParser *parser)
{
	if (!evaluate)
		return;
	es.top().isBeingEvaluated = false;
	int i = funcIndex(parser->lexer->current_token);
	if (i == -1)
		ERROR("error: undefined id %s", parser->lexer->current_token);
	es.top().returnValue.v = i;
	es.top().returnValue.isFunc = true;
}

void valueIsFuncCall(shParser *parser)
{
	if (!evaluate)
		return;
	es.top().isBeingEvaluated = true;
	es.top().func = function[funcIndex(parser->lexer->current_token)];
	if (es.top().func.erroneous)
		ERROR("error: function %s is erroneous", parser->lexer->current_token);
}

bool argsAreOK(shFunction &f, vector<shValue> &v)
{
	if (v.size() < f.arg.size())
	{
		cout << "Too few parameters given to function!" << endl;
		return false;
	}
	if (v.size() > f.arg.size())
	{
		cout << "Too many parameters given to function!" << endl;
		return false;
	}
	bool err = false;
	for (int i = 0; i < (int)v.size(); ++i)
		if (f.arg[i].isFunc != v[i].isFunc)
		{
			err = true;
			cout << "Type mismatch in parameter " << i << endl;
		}
	return !err;
}

void replaceArgs(shComposition &c, vector<shValue> &v, int secondOfRecursion = false)
{
	if (c.type == shComposition::ARGUMENT)
	{
		if (secondOfRecursion && c.value_index == 0)
			c.type = shComposition::ARGUMENT0;
		else if (v[c.value_index].isFunc)
			c.type = shComposition::GLOBAL;
		else
			c.type = shComposition::NUMBER;
		c.value_index = v[c.value_index].v;
	}
	if (!c.isSimple)
		for (vector<shComposition>::iterator i = c.funcArgs.begin(); i != c.funcArgs.end(); ++i)
			replaceArgs(*i, v, secondOfRecursion);
}

shValue computeComposition(shParser *parser, shComposition &c, shValue prev, int arg0 = -1);

void computeFunction(shParser *parser)		// Here, we have a function with its arguments computed
{
	if (!evaluate)
		return;
	shValue prev;
	if (!argsAreOK(es.top().func, es.top().arg))
	{
		stop_evaluation(parser);
		return;			// cannot compute
	}
	replaceArgs(es.top().func.func.f1, es.top().arg);
	if (es.top().func.func.isRecursion)
	{
		if (es.top().arg[0].v == 0)	// Use first formula to compute
			es.top().returnValue = computeComposition(parser, es.top().func.func.f1, prev);
		else
		{
			replaceArgs(es.top().func.func.f2, es.top().arg, true);
			if (es.top().func.func.needsPrev)
			{
				prev = computeComposition(parser, es.top().func.func.f1, prev);
				for (int i = 1; i < es.top().arg[0].v; ++i)
					prev = computeComposition(parser, es.top().func.func.f2, prev, i-1);
				es.top().returnValue = computeComposition(parser, es.top().func.func.f2, prev, es.top().arg[0].v-1);
			}
			else
				es.top().returnValue = computeComposition(parser, es.top().func.func.f2, prev, es.top().arg[0].v-1);
		}
	}
	else
	{
		es.top().returnValue = computeComposition(parser, es.top().func.func.f1, prev);
	}
}

shValue computeComposition(shParser *parser, shComposition &c, shValue prev, int arg0)
				// Here, we have a composition with arguments replaced and prev passed (if recursive)
{
	shValue v;
	shStackElement s;
	if (!evaluate)
		return v;
	switch (c.type)
	{
		case shComposition::NUMBER:
			v.v = c.value_index;
			v.isFunc = false;
			break;
		case shComposition::ARGUMENT0:
			v.v = arg0;
			v.isFunc = false;
			break;
		case shComposition::ARGUMENT:
			cout << "Error! I should have replaced all arguments!" << endl;
			stop_evaluation(parser);
			break;
		case shComposition::GLOBAL:
			if (c.isSimple)
			{
				v.v = c.value_index;
				v.isFunc = true;
			}
			else
			{
				if (c.value_index == 0)
				{
					// This is the only place any computation is actually done! Pretty simple, eh?
					v = computeComposition(parser, c.funcArgs[0], prev, arg0);
					++v.v;
				}
				else
				{
					s.func = function[c.value_index];
					s.isBeingEvaluated = true;
					es.push(s);
					for (vector<shComposition>::iterator i = c.funcArgs.begin();
							i != c.funcArgs.end(); ++i)
						es.top().arg.push_back(computeComposition(parser, *i, prev, arg0));
					computeFunction(parser);
					v = es.top().returnValue;
					es.pop();
				}
			}
			break;
		case shComposition::SELF:
			v = prev;
			break;
		case shComposition::UNDEFINED:
			cout << "Error! Requested computation of an unknown type!" << endl;
			break;
		default:
			cout << "Error! Composition should have had a type!" << endl;
			stop_evaluation(parser);
			break;
	}
	return v;
}

void valueCompute(shParser *parser)
{
	if (!evaluate)
		return;
	if (!es.top().isBeingEvaluated)
		return;			// There is nothing to compute!
	computeFunction(parser);
}

void valueAddToArgs(shParser *parser)
{
	if (!evaluate)
		return;
	shValue rv = es.top().returnValue;
	es.pop();
	es.top().arg.push_back(rv);
}
