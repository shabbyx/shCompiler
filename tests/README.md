Tests are as follows:

- Test1: A simple LL1 grammer. After reading from file, starts reading from stdin
- Test2: A simple CLR1 grammer. In this test, there is also an example of error production rules
- Test3: The same grammar as in Test2 but in the form of an ambiguous LL1 grammar. In this example,
	the letter files contains a redefinition of a letter, which must be ignored by the lexer
- Test4: The same grammar as in Test2 but in the form of an ambiguous CLR1 grammar
- Test5: Turing Machine interpreter. Has an LL1 grammar, but parsed with LL3
- Test6: Primitive Recursive interpreter. Has an LL1 grammar
- Test7: The grammar of DocThis. The grammar is work in progress and have changed in DocThis itself.
	This was added because the grammar has cycling follows which is one case shCompiler needs
	to be tested with.
- Test8: A simple CLR1 grammar, used to test correctness of parser when CLR(k), k > 1 is used.
- Test9 and Test10: More tests to test remove follow cycle
- Test11: Testing results of sh_parser_generate_pleh
