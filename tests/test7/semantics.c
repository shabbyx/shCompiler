/*
 * Copyright (C) 2007-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shCompiler.
 *
 * shCompiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shCompiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shCompiler.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "semantics.h"

static bool _evaluate = true;

void stop_evaluation(shParser *parser)
{
	_evaluate = false;
}

void reset_semantics(shParser *parser)
{
	_evaluate = true;
}

void printlasth(shParser *parser)
{
	sh_lexer_error(parser->lexer, "matched a heading");
}

void printlastp(shParser *parser)
{
	sh_lexer_error(parser->lexer, "matched a paragraph");
}

void printlasti(shParser *parser)
{
	sh_lexer_error(parser->lexer, "matched an item");
}

void printlastc(shParser *parser)
{
	sh_lexer_error(parser->lexer, "matched a code block");
}
