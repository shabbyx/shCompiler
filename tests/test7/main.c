/*
 * Copyright (C) 2007-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shCompiler.
 *
 * shCompiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shCompiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shCompiler.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <shcompiler.h>
#include <test_settings.h>
#include <test_defaults.h>
#include "semantics.h"

#define DO_PARSE 1
#define DO_LEX 1
#define DO_LEX_DETAILED 0

static shLexer lexer;
static shParser parser;

int main(int argc, char **argv)
{
	int ret = EXIT_FAILURE;
	struct prog_settings settings = {
		.letter_file = "grammar/letters.in",
		.token_file = "grammar/tokens.in",
		.keyword_file = "grammar/keywords.in",
		.grammar_file = "grammar/grammar.in",
		.input_file = "format",
		.k = 1,
	};
	if (get_settings(argc, argv, &settings))
		return EXIT_FAILURE;

	if (sh_lexer_init(&lexer))
		return EXIT_FAILURE;
	if (test_init_lexer(&lexer, &settings))
		goto exit_cleanup_lexer;

	if (sh_parser_init(&parser))
		goto exit_cleanup_lexer;

#if DO_PARSE
	sh_parser_ambiguity_resolution(&parser, SH_PARSER_ACCEPT_FIRST);
	if (test_init_parser(&parser, &lexer, &settings, SH_PARSER_LLK))
		goto exit_cleanup_parser;

	/* give parser callbacks */
	sh_parser_set_action_routine(&parser, "@printlasth", printlasth);
	sh_parser_set_action_routine(&parser, "@printlastp", printlastp);
	sh_parser_set_action_routine(&parser, "@printlasti", printlasti);
	sh_parser_set_action_routine(&parser, "@printlastc", printlastc);
#endif

#if DO_LEX
	/* the code below tests functionality of the lexer alone */
	const char *token;
#if !DO_LEX_DETAILED
	int line = 0;
#endif
	const char *type;
	sh_lexer_peek(&lexer, &type, 50, NULL, NULL);
	for (int i = 0; i < 100; ++i)
	{
		int l, c;
		token = sh_lexer_peek(&lexer, &type, i, &l, &c);
		if (token != NULL)
		{
			printf("Peeked token: %s which is of type: %s\n", token, type);
			if (i == 20)
				sh_lexer_ignore(&lexer, "WHITE_SPACE", false);
			if (i == 60)
				sh_lexer_ignore(&lexer, "WHITE_SPACE", true);
		}
	}
	while (token = sh_lexer_next(&lexer, &type), token != NULL)
	{
#if !DO_LEX_DETAILED
		if (strcmp(type, "NEW-LINE") != 0)
			printf(" ");
		if (line != lexer.line)
		{
			line = lexer.line;
			printf("\n%d: ", line);
		}
		if (strcmp(type, "NEW-LINE") != 0)
			printf("%s", token);
#else
		char *temp_token;
		const char *temp_type;
		int l, c;
		temp_token = sh_lexer_peek(&lexer, &temp_type, 0, NULL, NULL);
		printf("%d:%d: %s is of type: %s (Next token will be: %s)\n", lexer.line, lexer.column, token, type, temp_token?temp_token:"EOF");
		sh_lexer_ignore(&lexer, "WHITE_SPACE", true);
#endif
	}
	printf("\n");
#endif

#ifdef DO_PARSE
	/* do the actual parsing */
	if (test_open_file(&lexer, &settings) == 0)
		test_parse_file(&parser);
#endif

	ret = 0;
exit_cleanup_parser:
	sh_parser_free(&parser);
exit_cleanup_lexer:
	sh_lexer_free(&lexer);

	return ret;
}

