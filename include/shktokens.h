/*
 * Copyright (C) 2007-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shCompiler.
 *
 * shCompiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shCompiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shCompiler.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SH_KTOKENS_H_BY_CYCLOPS
#define SH_KTOKENS_H_BY_CYCLOPS

#ifdef __cplusplus
extern "C" {
#endif

typedef struct shKtokens shKtokens;

/*
 * shKtokens contains at most k strings.  It is used to store the k look-ahead tokens of the parser as well as their types.
 * shKtokens keeps references to token values and types.  Therefore, the tokens in the list shouldn't outlive the tokens
 * as reported by the lexer.
 *
 * Functions of shKtokens are:
 *
 * init:		initializes the structure with a given k.  Returns 0 on success or -1 otherwise.
 * clear:		clears the list, so there wouldn't be any tokens inside
 * free:		frees structure memory
 * at:			given n, gets the nth look-ahead, or NULL if non-existent.  Returns the type of the token in
 *			the type argument if provided.
 * push:		push one token/type pair in the list, evicting oldest element if exceeding k elements.
 * replace:		replace a token/type pair at a given index.  Useful for token fixes by phrase level error handler.
 */
int sh_ktokens_init(shKtokens *l, unsigned int k);
static inline void sh_ktokens_clear(shKtokens *l);
void sh_ktokens_free(shKtokens *l);
const char *sh_ktokens_at(shKtokens *l, const char **type, unsigned int n);
void sh_ktokens_push(shKtokens *l, const char *token, const char *type);
void sh_ktokens_replace(shKtokens *l, const char *token, const char *type, unsigned int n);

struct shKtokens
{
	const char **tokens;
	const char **types;
	unsigned int start;			/* start index of the list */
	unsigned int size;			/* number of elements in the list */
	unsigned int k;				/* maximum number of elements */

#ifdef __cplusplus
	shKtokens(): tokens(NULL), types(NULL), size(0), k(0) {}
	shKtokens(unsigned int kay) { sh_ktokens_init(this, kay); }
	void clear() { sh_ktokens_clear(this); }
	~shKtokens() { sh_ktokens_free(this); }
	const char *at(const char **type, unsigned int n) { return sh_ktokens_at(this, type, n); }
	void push(const char *token, const char *type) { sh_ktokens_push(this, token, type); }
	void replace(const char *token, const char *type, unsigned int n) { sh_ktokens_replace(this, token, type, n); }
#endif
};

static inline void sh_ktokens_clear(shKtokens *l) { l->size = 0; l->start = 0; }

#ifdef __cplusplus
}
#endif

#endif
