/*
 * Copyright (C) 2007-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shCompiler.
 *
 * shCompiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shCompiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shCompiler.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SH_NFA_H_BY_CYCLOPS
#define SH_NFA_H_BY_CYCLOPS

#include <stdbool.h>
#include <shds.h>

#ifdef __cplusplus
#include <new>

extern "C" {
#endif

#define SH_NFA_MAX_LETTERS 128

typedef struct shNfa shNfa;

/*
 * shNfa contains an NFA which is directly created from regular expressions.  It is later used to create an equivalent DFA.
 * The functions here that return a value, return 0 if successful and a negative value on error.  In case of error, the NFA
 * being operated on may be invalid and no operation other than sh_nfa_free is allowed on it.
 *
 * Functions of shNfa are:
 *
 * init:		initializes an NFA.  Note that the actual letters can be set in the end so that they wouldn't
 *			need to be copied around.
 * copy:		copies an NFA.  Name and letters are shallow copied, so don't free them while nfas are alive.
 * or:			builds another NFA which is the | of both NFAs (shallow copies second, so don't free or reuse it).
 * cat:			builds another NFA which is the . of both NFAs (shallow copies second, so don't free or reuse it).
 * unary:		builds another NFA which is the same NFA '.'ed count times if count is positive, or:
 *			count == 0 means (NFA)+
 *			count == -1 means (NFA)?
 *			count < -1 means (NFA)*
 * free:		letter names and characters allocated/freed by token reader.
 * shallow_free:	frees the memory of dfa, but allows shallow copies to survive, i.e. frees the nfa->states but doesn't
 *			free the state's memories.
 */
int sh_nfa_init(shNfa *nfa, int letter, int alphabet_size);
int sh_nfa_copy(shNfa *nfa, const shNfa *other);
int sh_nfa_or(shNfa *nfa, const shNfa *second);
int sh_nfa_cat(shNfa *nfa, const shNfa *second);
int sh_nfa_unary(shNfa *nfa, int count);
void sh_nfa_free(shNfa *nfa);
void sh_nfa_shallow_free(shNfa *nfa);

void sh_nfa_dump(const shNfa *nfa);

typedef struct shLetter
{
	char *name;
	char *chars;
	int chars_count;
} shLetter;

typedef struct shNfaState
{
	bool final;
	shVector/* int */ lambdas;				/* all states it goes with a lambda */
	shVector/* int */ nexts[SH_NFA_MAX_LETTERS];		/* all states it goes with each character */
} shNfaState;

struct shNfa
{
	int start;
	int final;						/* always keep the final unique */
	shVector/* shNfaState */ states;
	shLetter alphabet[SH_NFA_MAX_LETTERS];
	int alphabet_size;					/* alphabet_size must be set, even if alphabet itself is not */

#ifdef __cplusplus
	shNfa(int letter, int as) { if (sh_nfa_init(this, letter, as)) throw std::bad_alloc(); }
	shNfa(const shNfa &other) { if (sh_nfa_copy(this, &other)) throw std::bad_alloc(); }
	shNfa &operator |=(const shNfa &second) { if (sh_nfa_or(this, &second)) throw std::bad_alloc(); return *this; }
	shNfa &operator &=(const shNfa &second) { if (sh_nfa_cat(this, &second)) throw std::bad_alloc(); return *this; }
	shNfa &operator *(int count) { if (sh_nfa_unary(this, count)) throw std::bad_alloc(); return *this; }
	void free() { sh_nfa_free(this); }
	void shallow_free() { sh_nfa_shallow_free(this); }

	void dump() { sh_nfa_dump(this); }
#endif
};

#ifdef __cplusplus
}
#endif

#endif
