/*
 * Copyright (C) 2007-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shCompiler.
 *
 * shCompiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shCompiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shCompiler.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SH_PARSER_H_BY_CYCLOPS
#define SH_PARSER_H_BY_CYCLOPS

#include <stdio.h>
#include "shktokens.h"
#include "shlexer.h"

#ifdef __cplusplus
extern "C" {
#endif

#define SH_PARSER_SUCCESS 0
#define SH_PARSER_NO_FILE -1
#define SH_PARSER_NO_MEM -2
#define SH_PARSER_BAD_LEXER -3
#define SH_PARSER_GRAMMAR_ERROR -4
#define SH_PARSER_AMBIGUOUS -5
#define SH_PARSER_LEFT_RECURSIVE -6		/* only with LL(k) */
#define SH_PARSER_PARSE_ERROR -7
#define SH_PARSER_NO_RULES -8
#define SH_PARSER_FAIL -9
#define SH_PARSER_INTERNAL_ERROR -10

#define SH_PARSER_AMBIGUITY_ERROR 0
#define SH_PARSER_ACCEPT_FIRST 0x01
#define SH_PARSER_ACCEPT_LAST 0x02
#define SH_PARSER_ACCEPT_SHIFT 0x04		/* only with LR(k) */
#define SH_PARSER_ACCEPT_REDUCE 0x08		/* only with LR(k) */

typedef enum sh_grammar_type
{
	SH_PARSER_LLK = 0,
	SH_PARSER_LRK
} sh_grammar_type;

typedef struct shParser shParser;

/*
 * Callbacks:
 *
 * ar:			action routines take as argument a reference to the parser.  Action routines are callbacks
 *			assigned to @name symbols in the grammar.  During LL(k) parse, these action routines are
 *			called as the symbols (terminals or nonterminals) in the rule of an expanded nonterminal
 *			are matched and action routine symbol is visited.  During LR(k) parse, they are called as
 *			a nonterminal is reduced, which implies the action routine can only be placed at the end
 *			of the rule.  To overcome this limitation, the grammar is automatically transformed to
 *			comply with this limitation.  For example, the following rule:
 *
 *				T -> A B @ar C ...
 *
 *			becomes:
 *
 *				T -> (A~B~@ar) C ...
 *				(A~B~@ar) -> A B @ar
 *
 *			For simplicity of common data structures, this transformation is also done for LL(k) parsers.
 *			Note: if this changes what is accepted by the LL(k) grammar, please report a bug with an
 *			example demonstrating this.
 *
 * 			The parser's bound lexer can be queried for the current token/type, which is the terminal read
 * 			from the lexer just before calling the action routine.  In the example above, if B is a terminal,
 * 			lexer->current_type and lexer->current_token return B and its matched literal respectively.  If
 * 			placed not immediately after a terminal, normally the action routine has other tasks, such as
 * 			initialization or finalization.
 *
 *			Other uses for lexer include getting the line number and column, or setting whether e.g., WHITE_SPACEs
 *			should be ignored or not.  For parser, no use yet, but later there may be options tunable during parse
 *			(so, forward compatibility).
 *
 *			NOTE: The action routine can safely call sh_lexer_ignore.  The parser look-ahead will be rebuilt
 *			if necessary.  This check/rebuild for error-production rules and phrase-level error handler doesn't
 *			exist though, because it is not needed.  With the phrase-level error handler, the job is not to alter
 *			the process of lexing, but to handle errors and allow the parser to continue.  With error-production rules,
 *			the job is to accept some **parse** errors that the parser could handle without getting confused,
 *			and again, not change the behaviour of lexer (which usually is due to semantical reasons).
 *
 * eprh:		error production rule handlers take as argument the parser.  Error production rule handlers are callbacks
 *			assigned to !name symbols in the grammar.  The error production rules are there for the parser not
 *			to fail, yet error to be generated and handled.  That is why it cannot return fail or success.
 *			Normally, a flag is set for semantics analysis to make sure compilation fails for this erroneous input,
 *			and an error given.  Error production rules are an alternative to phrase level error handlers, which
 *			are sometimes more convenient, but they are not always applicable.  For example, a grammar may require
 *			statements to end in semicolon, but not ending them in semicolon doesn't actually introduce ambiguity.
 *			In such a case, the grammar may use the following rule:
 *
 *				statement_end -> SEMICOLON | !missing_semicolon;
 *
 *			In the case of error production rules, the parser actually accepts the input according to the grammar,
 *			but it's the semantics analyzer that understands that matching a nonterminal using that particular
 *			rule was an error.
 *
 * llk_pleh, lrk_pleh:	the phrase level error handler is a callback that is called in case the parser cannot continue with
 *			parsing due to an error in the input.  This error is normally either due to a missing token, or an
 *			extra one.  However, from the point of view of the parser, there's only one possibility: an unexpected
 *			token.  The phrase level error handler is written to be able to distinguish between these cases, using
 *			the expert knowledge of the grammar designer and take the appropriate action.  Normally, taking the
 *			correct action allows the parser to continue without further problems, and taking the wrong action
 *			results in subsequent calls to the phrase level error handler (pleh) due to further unexpected input
 *			(this is the reason why one missing semicolon could cause hundreds of error messages with some compilers).
 *
 *			The pleh is given the parser (through which the lexer is accessible) and the look-ahead tokens (which
 *			are k tokens in an LL(k) or LR(k) grammar).  In the case of LL(k) parsers, the current expected token type
 *			is also given.  (TODO: In the case of LR(k), probably the state and the token type are also necessary!).
 *			The top parser stack element is already popped when this function is called, therefore simply doing
 *			nothing makes the parser lose that element and carry on.  Sometimes, this is the desired behavior, for
 *			example if an expected terminal is missing.  In such cases, for the sake of action routines, it is possible
 *			to replace the last token matched to a fake token of the type of the missing terminal (sh_lexer_insert_fake).
 *			Alternatively, the look-ahead may be inspected and accordingly symbols pushed to stack and possible input
 *			faked.  This could mean replacing the look-ahead (sh_ktokens_replace) for example to rename a keyword
 *			used as variable, skip some tokens in look-ahead, expand a nonterminal by a rule and skip parts of it (if LL(k)),
 *			or TODO: change state if LR(k)?
 *
 *			It is recommended to use sh_parser_generate_pleh (TODO: currently only for LL(k)) to generate an error handler
 *			with all possible error cases and edit the resulting file.  Most likely, many cases have the same resolution
 *			can the final file would be greatly refactored.  The automatically generated pleh provides possible solutions as
 *			comments, some of which are tailored to the specific error case.  Furthermore, the comments provide additional
 *			information such as what rules probably need to be considered (TODO: or what the current state looks like in details if LR(k)),
 *			which can be very useful in writing a complete pleh.
 *
 *			This function returns whether the error was recovered, i.e., a return value of true means the parser should continue
 *			parsing, while a return value of false means error recovery was impossible and the parser should stop immediately.
 */

typedef void (*sh_parser_ar)(shParser *);
typedef void (*sh_parser_eprh)(shParser *);
typedef bool (*sh_parser_llk_pleh)(shParser *, const char *, shKtokens *);
typedef bool (*sh_parser_lrk_pleh)(shParser *, shKtokens *);

/*
 * shParser is a generic LL(k) or CLR(k) parser that uses shLexer to tokenize input and parses it.  It uses action routines to
 * callback the semantic analyzer/code generator.  Error-production-rules can be defined in the grammar with their specific callbacks,
 * as well as a generic phrase-level error handler that would be called in case of parse errors.
 *
 * - Initialization and Cleanup:
 *
 * init:		initializes the parser internals.  A lexer needs to be bound to it and the grammar loaded.
 * free:		frees the parser.
 * bind:		bind a lexer to this parser.  The parser would use this lexer to recognize terminals, read inputs, generate errors etc.
 *			The lexer is not copied, so it should remain alive.  The lexer's error_out member should be set for the parser to
 *			be able to output errors.
 *
 * - Loading the grammar:
 *
 * ambiguity_resolution	set the behavior when rebuilding LL(k) table or LR(k) diagram and ambiguity is detected.  It takes one of
 *			SH_PARSER_AMBIGUITY_* values.  If the grammar is LL(k) but one of LR(k)-specific ambiguity resolutions is
 *			selected, SH_PARSER_AMBIGUITY_ERROR is assumed.
 * load_llk:		load grammar from strings, instead of files.  It takes a lexer to bind to, followed b an array of rules,
 *			the LL(k) table, K of grammar and a target file that if given table was invalid would contain the new table.
 * load_lrk:		similar to load_llk.
 * read_grammar:	first, retrieve the possible terminals from bound lexer (the lexer thus needs to be already initialized).  It
 *			then reads the grammar from file.  The grammar is processed and the LL(k) table or LR(k) diagram is generated
 *			Afterwards, the function stores the generated table/diagram to the cache file:
 *				{dirname grammar_file}/cache/{basename grammar_file}.L[LR]Ktable
 *			If the cache is already present, the table/diagram is not generated, but read from file (unless new_grammar
 *			is set).  This function may return SH_PARSER_NO_FILE if it couldn't open the input file, SH_PARSER_BAD_LEXER
 *			if lexer is not properly initialized, SH_PARSER_GRAMMAR_ERROR if there was an error reading the grammar,
 *			SH_PARSER_AMBIGUOUS if the grammar is ambiguous and no ambiguity resolution is set or SH_PARSER_SUCCESS if
 *			successful.  Additionally, it may return SH_PARSER_LEFT_RECURSIVE if LL(k) and the grammar is left recursive.
 *			The first nonterminal seen in the grammar is considered the start nonterminal.
 *
 * - Setting callbacks:
 *
 * set_action_routine:	sets callback for a given action routine.  Action routine names start with '@'.  This function returns NULL if
 *			no such action routine exists.  Otherwise, the previously set action routine would be returned.
 * set_epr_handler:	sets callback for a given error production rule.  Error production rule names start with '!'.  This function returns
 *			NULL of no such error production rule exists.  Otherwise, the previously set handler would be returned.
 * set_llk_pleh:	sets callback for LL(k) phrase level error handler.  Phrase level error handler is called when the parser cannot
 *			deduce what to do, e.g., when it encounters a terminal it doesn't expect to see.  This function returns the
 *			previously set handler (default handler if not previously set).
 * set_lrk_pleh:	similar to set_llk_pleh, but used for LR(k) parsing.
 *
 * - Parsing:
 *
 * parse:		parse according to the grammar, getting input from lexer.  This function may return SH_PARSER_BAD_LEXER,
 *			SH_PARSER_NO_RULES or SH_PARSER_PARSE_ERROR if error.
 * stop:		called from action routines, this function can be used to terminate the parsing process early.  This could be for
 *			various reasons, such as having read enough of the file to get all information that is needed, or because a fatal
 *			error has happened, such as getting out of memory.  A return value is given to this function, which will be used
 *			as the return value of sh_parser_parse.
 * push:		called from the phrase level error handler to insert fake symbols on the stack.  This allows the pleh to
 *			insert symbols the grammar expects, but are missing from input.  Returns non-negative on error.
 *
 * - Tools:
 *
 * generate_pleh	generate pleh_file.h and pleh_file.cpp with an automatically generated phrase level error handler for the loaded grammar.
 *			The generated function can then be used as a base for the error handler and edited for choice of actions and generation
 *			of meaningful errors.
 */

int sh_parser_init(shParser *parser);
void sh_parser_free(shParser *parser);
void sh_parser_bind(shParser *parser, shLexer *lexer);

void sh_parser_ambiguity_resolution(shParser *parser, int resolution);
int sh_parser_load_llk(shParser *parser, shLexer *lexer, const char **rules, unsigned int rules_count,
		const char *table, unsigned int k, const char *table_file);
int sh_parser_load_lrk(shParser *parser, shLexer *lexer, const char **rules, unsigned int rules_count,
		const char *diagram, unsigned int k, const char *diagram_file);
#define sh_parser_read_grammar(...) sh_parser_read_grammar(__VA_ARGS__, false)
int (sh_parser_read_grammar)(shParser *parser, const char *grammar_file, sh_grammar_type type, unsigned int k, bool new_grammar, ...);

sh_parser_ar sh_parser_set_action_routine(shParser *parser, const char *name, sh_parser_ar func);
sh_parser_eprh sh_parser_set_epr_handler(shParser *parser, const char *name, sh_parser_eprh h);
sh_parser_llk_pleh sh_parser_set_llk_pleh(shParser *parser, sh_parser_llk_pleh h);
sh_parser_lrk_pleh sh_parser_set_lrk_pleh(shParser *parser, sh_parser_lrk_pleh h);

int sh_parser_parse(shParser *parser);
void sh_parser_stop(shParser *parser, int return_value);
int sh_parser_push(shParser *parser, const char *symbol);	// TODO: when pleh for LRK is written, perhaps add an `int state` parameter?

int sh_parser_generate_pleh(shParser *parser, const char *pleh_file);

/* output a c file with strings that could be fed to sh_parser_load_* */
/* void sh_parser_generate_c_table(shParser *parser, const char *file); */

struct shParser
{
	void *internal;			/* a pointer to the internal data structures that the user doesn't need to care about */
	shLexer *lexer;			/* shLexer bound to this parser */

#ifdef __cplusplus
	shParser() { sh_parser_init(this); }
	~shParser() { sh_parser_free(this); }

	void bind(shLexer &l)
	{
		sh_parser_bind(this, &l);
	}

	void ambiguity_resolution(int resolution)
	{
		sh_parser_ambiguity_resolution(this, resolution);
	}
	int load_llk(shLexer &l, const char **rules_arr, unsigned int rules_count,
			const char *table, unsigned int k, const char *table_file)
	{
		return sh_parser_load_llk(this, &l, rules_arr, rules_count, table, k, table_file);
	}
	int load_lrk(shLexer &l, const char **rules_arr, unsigned int rules_count,
			const char *diagram, unsigned int k, const char *diagram_file)
	{
		return sh_parser_load_lrk(this, &l, rules_arr, rules_count, diagram, k, diagram_file);
	}
	int read_grammar(const char *grammar_file, sh_grammar_type type, unsigned int k, bool new_grammar = false)
	{
		return (sh_parser_read_grammar)(this, grammar_file, type, k, new_grammar);
	}

	sh_parser_ar set_action_routine(const char *name, sh_parser_ar func)
	{
		return sh_parser_set_action_routine(this, name, func);
	}
	sh_parser_eprh set_epr_handler(const char *name, sh_parser_eprh h)
	{
		return sh_parser_set_epr_handler(this, name, h);
	}
	sh_parser_llk_pleh set_llk_pleh(sh_parser_llk_pleh h)
	{
		return sh_parser_set_llk_pleh(this, h);
	}
	sh_parser_lrk_pleh set_lrk_pleh(sh_parser_lrk_pleh h)
	{
		return sh_parser_set_lrk_pleh(this, h);
	}

	int parse()
	{
		return sh_parser_parse(this);
	}
	void stop(int return_value)
	{
		sh_parser_stop(this, return_value);
	}
	int push(const char *symbol)
	{
		return sh_parser_push(this, symbol);
	}

	int generate_pleh(const char *pleh_file)
	{
		return sh_parser_generate_pleh(this, pleh_file);
	}
#endif
};

#ifdef __cplusplus
}
#endif

#endif
