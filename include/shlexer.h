/*
 * Copyright (C) 2007-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shCompiler.
 *
 * shCompiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shCompiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shCompiler.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SH_LEXER_H_BY_CYCLOPS
#define SH_LEXER_H_BY_CYCLOPS

#include <stdio.h>
#include <stdarg.h>
#include <stdbool.h>
#include "shdfa.h"
#include "shnonstd.h"

#ifdef __cplusplus
extern "C" {
#endif

#define SH_LEXER_SUCCESS 0
#define SH_LEXER_NO_FILE -1
#define SH_LEXER_NO_MEM -2
#define SH_LEXER_EXISTS -3
#define SH_LEXER_FAIL -4

#define SH_LEXER_REBUILD true
#define SH_LEXER_USE_CACHE false

typedef struct shLexer shLexer;

/*
 * Callbacks:
 *
 * match_function:	a match_function is called whenever a token is matched.  Argument is a pointer to
 *			the token matched.  A pointer is given so that it could be reallocated if necessary.
 *			If no action is set, an empty function will be called.
 *			Note: This function should NOT alter token if it's a keyword
 * error_handler:	error_handler is called when it is impossible to match a token.  Arguments are input content,
 *			lexeme beginning (up to where input was fine), and where the error occurred (all DFAs went in trap),
 *			line and column.  The error handler can either change the input contents, or move lexeme_beginning
 *			forward so as to skip the erroneous part, updating line and column variables.
 * reader:		the callback for getting more text.  The first argument is the text returned by the reader.
 *			The first time, it will point to a variable containing NULL.  The consecutive times, it will
 *			be the previous pointer set by this callback (so it could be freed for example).
 *			The second argument is a user provided data.  The last argument is always false, except after
 *			the input is finished.  In that case, the function will be called once with this argument as
 *			true and it can perform cleanup at this point.
 *			The return value of this function tells the number of bytes read and stored in the first
 *			argument.  If returned 0, it is assumed EOF has been reached or some error has occurred.
 *			Note: shLexer will convert line-endings to '\n' automatically.
 *			Note: if the last argument is true, the other two arguments _may_ be NULL.  This is due to
 *			cleanup in case of error.  Make sure you check them against NULL.
 *			Note: the returned text doesn't need to include '\0' in the end.
 */
typedef void (*sh_lexer_match_function)(shLexer *, char **);
typedef void (*sh_lexer_error_handler)(shLexer *, char *, int *, int *, int *, int *);
typedef size_t (*sh_lexer_reader)(shLexer *, char **, void *, bool);

/*
 * shLexer is a generic lexer that uses regular expressions to tokenize ASCII texts.
 *
 * Funcions of shLexer are:
 *
 * - Initialization and Cleanup:
 *
 * init:		initializes the lexer internals.  The tokens for the lexer still need to be added
 *			either by sh_lexer_tokens or calls to sh_lexer_keywords and sh_lexer_add_dfa manually.
 *			Returns SH_LEXER_SUCCESS or SH_LEXER_NO_MEM.
 * free:		frees the lexer.
 *
 * - Loading DFAs:
 *
 * tokens:		this is a wrapper for sh_lexer_add_dfa and sh_lexer_keywords.  It parses the tokens_file and
 *			for each token, either loads the DFA from cache (rebuild == false and cache exists) or
 *			builds the DFA and cache it.  Alternatively, use SH_LEXER_REBUILD or SH_LEXER_USE_CACHE.
 *			If decided to hard-code the DFAs in your program, you need to use sh_dfa_load to construct
 *			the DFAs and add them by sh_lexer_add_dfa.  You then need to use sh_lexer_keywords to load
 *			the keywords from a string or sh_lexer_keywordsf to load them from file.
 *			Returns SH_LEXER_SUCCESS, SH_LEXER_NO_MEM, SH_LEXER_NO_FILE or SH_LEXER_FAIL.
 * add_dfa:		adds a dfa to possible tokens that can be matched.
 *			Returns SH_LEXER_SUCCESS, SH_LEXER_EXISTS or SH_LEXER_FAIL.
 * keywords:		loads the tokens that should be matched as keywords from buffer.  Note that keywords should
 *			be first matched by a DFA of another type and they are later identified as keywords.
 *			returns SH_LEXER_SUCCESS, SH_LEXER_FAIL or SH_LEXER_NO_MEM.
 * keywordsf:		similar to sh_lexer_keywords, but reads the keywords from file instead of buffer.
 *			Returns SH_LEXER_SUCCESS, SH_LEXER_NO_FILE, SH_LEXER_NO_MEM or SH_LEXER_FAIL.
 *
 * - Lexer input:
 *
 * file:		opens a file to lex.  Each time PAGE_SIZE is read (4096 if not defined).
 *			The memory used by shLexer will be at most
 *			    PAGE_SIZE + max of memory needed to keep all the peeked tokens as well as ignored ones.
 *			Returns SH_LEXER_SUCCESS, SH_LEXER_NO_FILE, SH_LEXER_NO_MEM or SH_LEXER_FAIL.
 * text:		takes a copy of the array to lex, so it can be deleted or changed.  Additionally, an extra
 *			memory is needed by the lexer which is equal to that of sh_lexer_file.
 *			If `buffer_name` is not NULL, it will be copied as file name so that errors would write it
 *			in output.  Unlike `sh_lexer_file`, the global path of this name will not be computed
 *			(because it can be a fake name such as "input" from terminal and not a real file name).
 *			Returns either SH_LEXER_SUCCESS, SH_LEXER_NO_MEM or SH_LEXER_FAIL.
 * interactive:		this will call `reader` every time new data is required.  See the comments on sh_lexer_reader
 *			for more info.  The memory used by shLexer will be similar to sh_lexer_file, except instead of
 *			PAGE_SIZE, it is the memory size returned by reader callback.
 *			If `reader` is NULL, the default reader will be used.  This reader will read from stdin line
 *			by line and `data` will be ignored.
 *			If `name` is not NULL, it will be copied as file name so that errors would write it in output.
 *			Similar to `sh_lexer_text`, this name is not processed.
 *			`data` will be sent to `reader` on each call and is untouched by shLexer.
 *			Returns SH_LEXER_SUCCESS or SH_LEXER_FAIL.  If using default reader, SH_LEXER_NO_MEM may also
 *			be returned.
 *
 * - Lexing:
 *
 * next:		reads and consumes the next token.  The token will be returned and will also be available as
 *			`current_token`.  Its type, which is the name of its DFA (or itself if its a keyword) will be
 *			written in `type` (if not NULL) and will also be available as `current_type`.
 *			`file_name`, `line` and `column` will contain the position where this token was matched.
 *			By default, tokens of type WHITE_SPACE are ignored and will not be returned,
 *			but this is customizable with sh_lexer_ignore for WHITE_SPACE as well as other token types.
 *			If no tokens are left, returns NULL.
 * peek:		similar to sh_lexer_next, but will only peek at the input without consuming it.  If n is 0,
 *			it will return the same token as sh_lexer_next would.  Otherwise, it returns n tokens after it.
 *			This function will not consume the input.  That is, calling this function twice
 *			with the same n, returns the same thing.  If sh_lexer_ignore or sh_lexer_match change the tokens
 *			that will be matched/ignored, then the previous sentence may not be true.
 *			If there aren't n tokens left in the input, returns NULL.
 *			Returns the line and column where the token was matched in `*line` and `*column` respectively if
 *			they are not NULL.
 * ignore:		tells the lexer whether to ignore a token type or not.  Ignored tokens are matched, but thrown away.
 *			By default, "WHITE_SPACE" will be ignored.
 * ignores:		returns whether the lexer is ignoring a given type or not.
 * match:		tells the lexer whether to match a token type or not.  If a token is asked not to be matched,
 *			it is as if it doesn't exist, unlike ignored tokens.  By default, all tokens will be matched.
 * matches:		tells whether the lexer is matching a given type or not.
 * set_position:	set line and column.  Default is (1, 0) in the beginning.  Effective from the current token.
 *			Automatically updates line and column of peeked tokens.
 * set_column_start:	set the starting column of text.  Default is 0.  Effective from the current token.
 *			Automatically updates line and column of peeked tokens.
 *			Note: set_position and set_left_column are useful for generating correct messages for a rectangular
 *			sub text of a larger text.
 *
 * - Error handling and preprocessing:
 *
 * set_match_function:	makes the lexer call a certain function if a token of this type is matched.
 *			This is useful for example to change two consecutive characters '\\' and 'n' in an input
 *			string to a '\n' in the token or things like that so that the semantic analyzer would
 *			not get involved with it.
 *			Returns the previously assigned match function (default is a function that does nothing).
 * set_error_handler:	the lexer would call this function if no token could be matched in the input.
 *			The default function strips the erroneous part of the input silently.
 *			Returns the previously assigned error handling function (the default on first call),
 *			so it can be used for example to print a message and then execute the default handler.
 * insert_fake:		used most likely by the parser's phrase level error handler, this function sets the lexer's
 *			`current_token` to a given token (and its type).  If given token is NULL, generates a token
 *			of given type.  This is useful for error handling where a missing token can be inserted as
 *			if it is correctly read.
 *			This function generates a valid token of this type, so that the semantic analyzer doesn't get
 *			confused if processing the fake token.
 *			Note: This function doesn't affect sh_lexer_peek/next, but only the `current_token`.
 *			Returns SH_LEXER_SUCCESS, SH_LEXER_NO_MEM or SH_LEXER_FAIL.
 * error:		the sh_lexer_error* functions generate error.  If given column is <= 0, it is not printed.
 *			If given line is <= 0, both line and column are ignored.  If file_name is NULL,
 *			all three of them are ignored.  This function generates error in the following form:
 *			    lexer->file_name():lexer->line:lexer->column: fmt(...)\n
 *			Note that this function automatically adds the '\n'.
 * error_custom:	similar to sh_lexer_error function, but it generates the error in the following form:
 *			    file_name:line:column: fmt(...)
 *			This function doesn't add '\n' so it could be called multiple times (subsequent calls
 *			with file_name == NULL) to still generate 1 line of message.
 */

int sh_lexer_init(shLexer *lexer);
void sh_lexer_free(shLexer *lexer);

int sh_lexer_tokens(shLexer *lexer, const char *letters_file, const char *tokens_file, const char *keywords_file, bool rebuild);
int sh_lexer_add_dfa(shLexer *lexer, const shDfa *dfa);
int sh_lexer_keywords(shLexer *lexer, const char *buffer);
int sh_lexer_keywordsf(shLexer *lexer, const char *file);

int sh_lexer_file(shLexer *lexer, const char *file_name);
int sh_lexer_text(shLexer *lexer, const char *text, const char *buffer_name);
int sh_lexer_interactive(shLexer *lexer, sh_lexer_reader reader, const char *name, void *data);

const char *sh_lexer_next(shLexer *lexer, const char **type);
const char *sh_lexer_peek(shLexer *lexer, const char **type, unsigned int n, int *line, int *column);
void sh_lexer_ignore(shLexer *lexer, const char *type, bool ignore);
bool sh_lexer_ignores(shLexer *lexer, const char *type);
void sh_lexer_match(shLexer *lexer, const char *type, bool match);
bool sh_lexer_matches(shLexer *lexer, const char *type);
void sh_lexer_set_position(shLexer *lexer, int line, int column);
void sh_lexer_set_column_start(shLexer *lexer, int column);

sh_lexer_match_function sh_lexer_set_match_function(shLexer *lexer, const char *type, sh_lexer_match_function func);
sh_lexer_error_handler sh_lexer_set_error_handler(shLexer *lexer, sh_lexer_error_handler eh);
int sh_lexer_insert_fake(shLexer *lexer, const char *type, const char *token);
void sh_lexer_error(shLexer *lexer, const char *fmt, ...) SH_COMPILER_PRINTF_STYLE(2, 3);
void sh_lexer_error_custom(shLexer *lexer, const char *file_name, int line, int column, const char *fmt, ...) SH_COMPILER_PRINTF_STYLE(5, 6);
void sh_lexer_errorv(shLexer *lexer, const char *fmt, va_list args);
void sh_lexer_error_customv(shLexer *lexer, const char *file_name, int line, int column, const char *fmt, va_list args);
void sh_lexer_suppress_errors(shLexer *lexer, bool suppress);

struct shLexer
{
	void *internal;			/* a pointer to the internal data structures that the user doesn't need to care about */
	bool initialized;		/* whether lexer is initialized or not */
	char *file_name;		/* the file being lexed (or name of buffer) */
	FILE *error_out;		/*
					 * Set this to a FILE to write warnings and errors to, or NULL to suppress
					 * output messages.  Initially, it will be set to stderr.
					 */
	bool repeek;			/*
					 * if sh_lexer_ignore/match caused peeked tokens to be invalidated, this flag
					 * will be set.  Therefore, before calling a function that may call sh_lexer_ignore,
					 * set this to false, and check it afterwards.
					 */
	void *user_data;		/*
					 * data for various callbacks, managed by the user.  The user can set this at
					 * initialization and later use it, for example in action routines to access
					 * the compiler data
					 */

	/* information on last matched token: */
	const char *current_token;	/* the last matched token */
	const char *current_type;	/* the type of last matched token */
	int line;			/* the line where last token was matched */
	int column;			/* the column in line where last token was matched */
	bool space_met;			/* whether before matching the last token, there were tokens ignored (such as WHITE_SPACE) */

#ifdef __cplusplus
	shLexer() { sh_lexer_init(this); }
	~shLexer() { sh_lexer_free(this); }
	int tokens(const char *letters_file, const char *tokens_file, const char *keywords_file, bool rebuild)
	{
		return sh_lexer_tokens(this, letters_file, tokens_file, keywords_file, rebuild);
	}
	int add_dfa(const shDfa *dfa) { return sh_lexer_add_dfa(this, dfa); }
	int keywords(const char *buffer) { return sh_lexer_keywords(this, buffer); }
	int keywordsf(const char *f) { return sh_lexer_keywordsf(this, f); }
	int file(const char *f) { return sh_lexer_file(this, f); }
	int text(const char *t, const char *buffer_name) { return sh_lexer_text(this, t, buffer_name); }
	int interactive(sh_lexer_reader reader, const char *name, void *data)
	{
		return sh_lexer_interactive(this, reader, name, data);
	}
	const char *next(const char **type) { return sh_lexer_next(this, type); }
	const char *peek(const char **type, unsigned int n, int *l, int *c) { return sh_lexer_peek(this, type, n, l, c); }
	void ignore(const char *type, bool yes) { sh_lexer_ignore(this, type, yes); }
	bool ignores(const char *type) { return sh_lexer_ignores(this, type); }
	void match(const char *type, bool yes) { sh_lexer_match(this, type, yes); }
	bool matches(const char *type) { return sh_lexer_matches(this, type); }
	void set_position(int l, int c) { sh_lexer_set_position(this, l, c); }
	void set_column_start(int c) { sh_lexer_set_column_start(this, c); }
	sh_lexer_match_function set_match_function(const char *type, sh_lexer_match_function func)
	{
		return sh_lexer_set_match_function(this, type, func);
	}
	sh_lexer_error_handler set_error_handler(sh_lexer_error_handler eh) { return sh_lexer_set_error_handler(this, eh); }
	int insert_fake(const char *type, const char *token) { return sh_lexer_insert_fake(this, type, token); }
	void error(const char *fmt, ...) SH_COMPILER_PRINTF_STYLE(2, 3)
	{
		va_list args;
		va_start(args, fmt);
		sh_lexer_errorv(this, fmt, args);
		va_end(args);
	}
	void error_custom(const char *f, int l, int c, const char *fmt, ...) SH_COMPILER_PRINTF_STYLE(5, 6)
	{
		va_list args;
		va_start(args, fmt);
		sh_lexer_error_customv(this, f, l, c, fmt, args);
		va_end(args);
	}
	void errorv(const char *fmt, va_list args) { sh_lexer_errorv(this, fmt, args); }
	void error_customv(const char *f, int l, int c, const char *fmt, va_list args)
	{
		sh_lexer_error_customv(this, f, l, c, fmt, args);
	}
	void suppress_errors(bool suppress) { sh_lexer_suppress_errors(this, suppress); }
#endif
};

#ifdef __cplusplus
}
#endif

#endif
