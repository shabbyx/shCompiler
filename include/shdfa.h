/*
 * Copyright (C) 2007-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shCompiler.
 *
 * shCompiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shCompiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shCompiler.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SH_DFA_H_BY_CYCLOPS
#define SH_DFA_H_BY_CYCLOPS

#include "shnfa.h"

#ifdef __cplusplus
extern "C" {
#endif

#define SH_DFA_MAX_LETTERS SH_NFA_MAX_LETTERS

typedef struct shDfa shDfa;

/*
 * shDfa contains a DFA which is created from an NFA.
 *
 * Functions of shDfa are:
 *
 * init:		initializes the DFA.
 * store:		stores the DFA in file.
 * load:		loads DFA from file.
 * convert:		converts an NFA to a DFA.
 * simplify:		simplifies a DFA.
 * free:		frees a DFA.
 */
void sh_dfa_init(shDfa *dfa);
void sh_dfa_store(const shDfa *dfa, FILE *fout);
int sh_dfa_load(shDfa *dfa, const char *buffer);
int sh_dfa_convert(shDfa *dfa, const shNfa *nfa, const char *name);
int sh_dfa_simplify(shDfa *simpdfa, const shDfa *dfa);
void sh_dfa_free(shDfa *dfa);

void sh_dfa_dump(const shDfa *dfa);

struct shDfa
{
	int states_count;
	int start;
	int trap_state;		/* from trap state you can go nowhere */
	int alphabet_size;
	int to_letter[256];	/* map of actual characters to defined letters in alphabet */
	bool *final;
	int **to;
	shLetter alphabet[SH_DFA_MAX_LETTERS];
	char *name;

#ifdef __cplusplus
	shDfa() { sh_dfa_init(this); }
	void store(FILE *fout) const { sh_dfa_store(this, fout); }
	int load(const char *buffer) { return sh_dfa_load(this, buffer); }
	int convert(const shNfa &nfa, const char *n) { return sh_dfa_convert(this, &nfa, n); }
	int simplify(const shDfa &dfa) { return sh_dfa_simplify(this, &dfa); }
	void free() { sh_dfa_free(this); }

	void dump() { sh_dfa_dump(this); }
#endif
};

#ifdef __cplusplus
}
#endif

#endif
