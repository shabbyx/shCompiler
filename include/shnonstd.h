/*
 * Copyright (C) 2007-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shCompiler.
 *
 * shCompiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shCompiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shCompiler.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SH_NONSTD_H_BY_CYCLOPS
#define SH_NONSTD_H_BY_CYCLOPS

/*
 * This file contains macros that expand to different values based
 * on compiler or compilation environment.  These macros are used
 * in the library header files and therefore are visible to the user.
 */

#ifdef __GNUC__
# define SH_COMPILER_PRINTF_STYLE(format_index, args_index) __attribute__ ((format (printf, format_index, args_index)))
#else
# define SH_COMPILER_PRINTF_STYLE(format_index, args_index)
#endif

#endif
