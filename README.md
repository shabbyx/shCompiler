## shCompiler: Compiler library

Travis-CI: [![Build Status](https://travis-ci.org/ShabbyX/shCompiler.svg?branch=master)](https://travis-ci.org/ShabbyX/shCompiler)

This library contains a lexer and a parser that accepts action routines and
therefore can be used to compile input. The parser can be LL(k) or CLR(k).

This library started as an ambitious course project and over the years I have
used it many times. It has gone a long way, including a major rewrite in C
which was accompanied by many better choices of algorithms and general
improvements. Many things about this library could be improved to make it
more in line with many of the standard syntaxes (such as regexes), but that's
a task for another day. I would also document it when I finalize my document
generation program [DocThis!](https://gitlab.com/ShabbyX/DocThis).

About LL(k) and CLR(k) parsers, at the time I wrote this I had not completely
learned them for k > 1. For k = 1, they are correct. For k > 1, each parser
of k+1 is more powerful than of k, but it's not as powerful as the real
LL(k) and CLR(k) parsers. However, I didn't go through the trouble of fixing
it because I never had any need for k > 1.

Note that, with shCompiler you can parse ambiguous grammars too, so often
CLR grammars can be parsed with an LL equivalent that has ambiguities
(such as the famous if-else problem. See tests/test3/grammar/grammar.in).
