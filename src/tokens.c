/*
 * Copyright (C) 2007-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shCompiler.
 *
 * shCompiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shCompiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shCompiler.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <assert.h>
#include <shlexer.h>
#include <shparser.h>
#include "lexer_internal.h"
#include "parser_internal.h"
#include "tokens.h"
#include "utils.h"
#include "nonstd.h"

#define PARSER_INTERNAL ((parser_internal *)parser->internal)
#define LEXER_INTERNAL ((lexer_internal *)lexer->internal)

static const char *_keywords =
"lambda\n"
"new_line\n"
"space\n"
"tab\n";

static const char *_token_ID =
"start = 0\n"
"trap_state = 1\n"
"alphabet_size = 21\n"
"size = 4\n"
"states =\n"
"0 1 1 1 1 2 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"1 1 1 1 2 2 1 1 1 1 1 1 1 1 3 1 1 1 1 1 1 1\n"
"0 1 1 1 2 2 1 1 1 1 1 1 1 1 3 1 1 1 1 1 1 1\n"
"name = ID\n"
"alphabet =\n"
"space 1 32\n"
"new_line 1 10\n"
"tab 1 9\n"
"digit 10 48 49 50 51 52 53 54 55 56 57\n"
"letter 55 46 47 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90 95 97 98 99 100 101 102 103 104 105 106 107 108 109 110 111 112 113 114 115 116 117 118 119 120 121 122\n"
"left_paren 1 40\n"
"right_paren 1 41\n"
"bar 1 124\n"
"star 1 42\n"
"plus 1 43\n"
"question 1 63\n"
"equal 1 61\n"
"colon 1 58\n"
"dash 1 45\n"
"angle_right 1 62\n"
"semicolon 1 59\n"
"hash 1 35\n"
"backslash 1 92\n"
"at 1 64\n"
"exclamation 1 33\n"
"illegal 14 34 36 37 38 39 44 60 91 93 94 96 123 125 126\n";

static const char *_token_EQUAL =
"start = 0\n"
"trap_state = 1\n"
"alphabet_size = 21\n"
"size = 3\n"
"states =\n"
"0 1 1 1 1 1 1 1 1 1 1 1 2 1 1 1 1 1 1 1 1 1\n"
"0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"name = EQUAL\n"
"alphabet =\n"
"space 1 32\n"
"new_line 1 10\n"
"tab 1 9\n"
"digit 10 48 49 50 51 52 53 54 55 56 57\n"
"letter 55 46 47 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90 95 97 98 99 100 101 102 103 104 105 106 107 108 109 110 111 112 113 114 115 116 117 118 119 120 121 122\n"
"left_paren 1 40\n"
"right_paren 1 41\n"
"bar 1 124\n"
"star 1 42\n"
"plus 1 43\n"
"question 1 63\n"
"equal 1 61\n"
"colon 1 58\n"
"dash 1 45\n"
"angle_right 1 62\n"
"semicolon 1 59\n"
"hash 1 35\n"
"backslash 1 92\n"
"at 1 64\n"
"exclamation 1 33\n"
"illegal 14 34 36 37 38 39 44 60 91 93 94 96 123 125 126\n";

static const char *_token_END =
"start = 0\n"
"trap_state = 1\n"
"alphabet_size = 21\n"
"size = 3\n"
"states =\n"
"0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 2 1 1 1 1 1\n"
"0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"name = END\n"
"alphabet =\n"
"space 1 32\n"
"new_line 1 10\n"
"tab 1 9\n"
"digit 10 48 49 50 51 52 53 54 55 56 57\n"
"letter 55 46 47 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90 95 97 98 99 100 101 102 103 104 105 106 107 108 109 110 111 112 113 114 115 116 117 118 119 120 121 122\n"
"left_paren 1 40\n"
"right_paren 1 41\n"
"bar 1 124\n"
"star 1 42\n"
"plus 1 43\n"
"question 1 63\n"
"equal 1 61\n"
"colon 1 58\n"
"dash 1 45\n"
"angle_right 1 62\n"
"semicolon 1 59\n"
"hash 1 35\n"
"backslash 1 92\n"
"at 1 64\n"
"exclamation 1 33\n"
"illegal 14 34 36 37 38 39 44 60 91 93 94 96 123 125 126\n";

static const char *_token_WHITE_SPACE =
"start = 0\n"
"trap_state = 2\n"
"alphabet_size = 21\n"
"size = 7\n"
"states =\n"
"0 1 1 1 2 2 2 2 2 2 2 2 2 2 2 2 2 3 2 2 2 2\n"
"1 1 1 1 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2\n"
"0 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2\n"
"0 3 4 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 5 3 3 3\n"
"1 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2\n"
"0 3 6 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 5 3 3 3\n"
"1 3 4 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 5 3 3 3\n"
"name = WHITE_SPACE\n"
"alphabet =\n"
"space 1 32\n"
"new_line 1 10\n"
"tab 1 9\n"
"digit 10 48 49 50 51 52 53 54 55 56 57\n"
"letter 55 46 47 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90 95 97 98 99 100 101 102 103 104 105 106 107 108 109 110 111 112 113 114 115 116 117 118 119 120 121 122\n"
"left_paren 1 40\n"
"right_paren 1 41\n"
"bar 1 124\n"
"star 1 42\n"
"plus 1 43\n"
"question 1 63\n"
"equal 1 61\n"
"colon 1 58\n"
"dash 1 45\n"
"angle_right 1 62\n"
"semicolon 1 59\n"
"hash 1 35\n"
"backslash 1 92\n"
"at 1 64\n"
"exclamation 1 33\n"
"illegal 14 34 36 37 38 39 44 60 91 93 94 96 123 125 126\n";

static const char *_token_PAREN_OPEN =
"start = 0\n"
"trap_state = 1\n"
"alphabet_size = 21\n"
"size = 3\n"
"states =\n"
"0 1 1 1 1 1 2 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"name = PAREN_OPEN\n"
"alphabet =\n"
"space 1 32\n"
"new_line 1 10\n"
"tab 1 9\n"
"digit 10 48 49 50 51 52 53 54 55 56 57\n"
"letter 55 46 47 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90 95 97 98 99 100 101 102 103 104 105 106 107 108 109 110 111 112 113 114 115 116 117 118 119 120 121 122\n"
"left_paren 1 40\n"
"right_paren 1 41\n"
"bar 1 124\n"
"star 1 42\n"
"plus 1 43\n"
"question 1 63\n"
"equal 1 61\n"
"colon 1 58\n"
"dash 1 45\n"
"angle_right 1 62\n"
"semicolon 1 59\n"
"hash 1 35\n"
"backslash 1 92\n"
"at 1 64\n"
"exclamation 1 33\n"
"illegal 14 34 36 37 38 39 44 60 91 93 94 96 123 125 126\n";

static const char *_token_PAREN_CLOSE =
"start = 0\n"
"trap_state = 1\n"
"alphabet_size = 21\n"
"size = 3\n"
"states =\n"
"0 1 1 1 1 1 1 2 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"name = PAREN_CLOSE\n"
"alphabet =\n"
"space 1 32\n"
"new_line 1 10\n"
"tab 1 9\n"
"digit 10 48 49 50 51 52 53 54 55 56 57\n"
"letter 55 46 47 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90 95 97 98 99 100 101 102 103 104 105 106 107 108 109 110 111 112 113 114 115 116 117 118 119 120 121 122\n"
"left_paren 1 40\n"
"right_paren 1 41\n"
"bar 1 124\n"
"star 1 42\n"
"plus 1 43\n"
"question 1 63\n"
"equal 1 61\n"
"colon 1 58\n"
"dash 1 45\n"
"angle_right 1 62\n"
"semicolon 1 59\n"
"hash 1 35\n"
"backslash 1 92\n"
"at 1 64\n"
"exclamation 1 33\n"
"illegal 14 34 36 37 38 39 44 60 91 93 94 96 123 125 126\n";

static const char *_token_NUMBER =
"start = 0\n"
"trap_state = 1\n"
"alphabet_size = 21\n"
"size = 3\n"
"states =\n"
"0 1 1 1 2 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"1 1 1 1 2 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"name = NUMBER\n"
"alphabet =\n"
"space 1 32\n"
"new_line 1 10\n"
"tab 1 9\n"
"digit 10 48 49 50 51 52 53 54 55 56 57\n"
"letter 55 46 47 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90 95 97 98 99 100 101 102 103 104 105 106 107 108 109 110 111 112 113 114 115 116 117 118 119 120 121 122\n"
"left_paren 1 40\n"
"right_paren 1 41\n"
"bar 1 124\n"
"star 1 42\n"
"plus 1 43\n"
"question 1 63\n"
"equal 1 61\n"
"colon 1 58\n"
"dash 1 45\n"
"angle_right 1 62\n"
"semicolon 1 59\n"
"hash 1 35\n"
"backslash 1 92\n"
"at 1 64\n"
"exclamation 1 33\n"
"illegal 14 34 36 37 38 39 44 60 91 93 94 96 123 125 126\n";

static const char *_token_MAYBE =
"start = 0\n"
"trap_state = 1\n"
"alphabet_size = 21\n"
"size = 3\n"
"states =\n"
"0 1 1 1 1 1 1 1 1 1 1 2 1 1 1 1 1 1 1 1 1 1\n"
"0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"name = MAYBE\n"
"alphabet =\n"
"space 1 32\n"
"new_line 1 10\n"
"tab 1 9\n"
"digit 10 48 49 50 51 52 53 54 55 56 57\n"
"letter 55 46 47 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90 95 97 98 99 100 101 102 103 104 105 106 107 108 109 110 111 112 113 114 115 116 117 118 119 120 121 122\n"
"left_paren 1 40\n"
"right_paren 1 41\n"
"bar 1 124\n"
"star 1 42\n"
"plus 1 43\n"
"question 1 63\n"
"equal 1 61\n"
"colon 1 58\n"
"dash 1 45\n"
"angle_right 1 62\n"
"semicolon 1 59\n"
"hash 1 35\n"
"backslash 1 92\n"
"at 1 64\n"
"exclamation 1 33\n"
"illegal 14 34 36 37 38 39 44 60 91 93 94 96 123 125 126\n";

static const char *_token_KLEENE_STAR =
"start = 0\n"
"trap_state = 1\n"
"alphabet_size = 21\n"
"size = 3\n"
"states =\n"
"0 1 1 1 1 1 1 1 1 2 1 1 1 1 1 1 1 1 1 1 1 1\n"
"0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"name = KLEENE_STAR\n"
"alphabet =\n"
"space 1 32\n"
"new_line 1 10\n"
"tab 1 9\n"
"digit 10 48 49 50 51 52 53 54 55 56 57\n"
"letter 55 46 47 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90 95 97 98 99 100 101 102 103 104 105 106 107 108 109 110 111 112 113 114 115 116 117 118 119 120 121 122\n"
"left_paren 1 40\n"
"right_paren 1 41\n"
"bar 1 124\n"
"star 1 42\n"
"plus 1 43\n"
"question 1 63\n"
"equal 1 61\n"
"colon 1 58\n"
"dash 1 45\n"
"angle_right 1 62\n"
"semicolon 1 59\n"
"hash 1 35\n"
"backslash 1 92\n"
"at 1 64\n"
"exclamation 1 33\n"
"illegal 14 34 36 37 38 39 44 60 91 93 94 96 123 125 126\n";

static const char *_token_OR =
"start = 0\n"
"trap_state = 1\n"
"alphabet_size = 21\n"
"size = 3\n"
"states =\n"
"0 1 1 1 1 1 1 1 2 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"name = OR\n"
"alphabet =\n"
"space 1 32\n"
"new_line 1 10\n"
"tab 1 9\n"
"digit 10 48 49 50 51 52 53 54 55 56 57\n"
"letter 55 46 47 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90 95 97 98 99 100 101 102 103 104 105 106 107 108 109 110 111 112 113 114 115 116 117 118 119 120 121 122\n"
"left_paren 1 40\n"
"right_paren 1 41\n"
"bar 1 124\n"
"star 1 42\n"
"plus 1 43\n"
"question 1 63\n"
"equal 1 61\n"
"colon 1 58\n"
"dash 1 45\n"
"angle_right 1 62\n"
"semicolon 1 59\n"
"hash 1 35\n"
"backslash 1 92\n"
"at 1 64\n"
"exclamation 1 33\n"
"illegal 14 34 36 37 38 39 44 60 91 93 94 96 123 125 126\n";

static const char *_token_AT_LEAST_ONCE =
"start = 0\n"
"trap_state = 1\n"
"alphabet_size = 21\n"
"size = 3\n"
"states =\n"
"0 1 1 1 1 1 1 1 1 1 2 1 1 1 1 1 1 1 1 1 1 1\n"
"0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"name = AT_LEAST_ONCE\n"
"alphabet =\n"
"space 1 32\n"
"new_line 1 10\n"
"tab 1 9\n"
"digit 10 48 49 50 51 52 53 54 55 56 57\n"
"letter 55 46 47 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90 95 97 98 99 100 101 102 103 104 105 106 107 108 109 110 111 112 113 114 115 116 117 118 119 120 121 122\n"
"left_paren 1 40\n"
"right_paren 1 41\n"
"bar 1 124\n"
"star 1 42\n"
"plus 1 43\n"
"question 1 63\n"
"equal 1 61\n"
"colon 1 58\n"
"dash 1 45\n"
"angle_right 1 62\n"
"semicolon 1 59\n"
"hash 1 35\n"
"backslash 1 92\n"
"at 1 64\n"
"exclamation 1 33\n"
"illegal 14 34 36 37 38 39 44 60 91 93 94 96 123 125 126\n";

static const char *_token_ACTION_ROUTINE =
"start = 0\n"
"trap_state = 1\n"
"alphabet_size = 21\n"
"size = 5\n"
"states =\n"
"0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 2 1 1\n"
"0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"0 1 1 1 1 3 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"1 1 1 1 3 3 1 1 1 1 1 1 1 1 4 1 1 1 1 1 1 1\n"
"0 1 1 1 3 3 1 1 1 1 1 1 1 1 4 1 1 1 1 1 1 1\n"
"name = ACTION_ROUTINE\n"
"alphabet =\n"
"space 1 32\n"
"new_line 1 10\n"
"tab 1 9\n"
"digit 10 48 49 50 51 52 53 54 55 56 57\n"
"letter 55 46 47 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90 95 97 98 99 100 101 102 103 104 105 106 107 108 109 110 111 112 113 114 115 116 117 118 119 120 121 122\n"
"left_paren 1 40\n"
"right_paren 1 41\n"
"bar 1 124\n"
"star 1 42\n"
"plus 1 43\n"
"question 1 63\n"
"equal 1 61\n"
"colon 1 58\n"
"dash 1 45\n"
"angle_right 1 62\n"
"semicolon 1 59\n"
"hash 1 35\n"
"backslash 1 92\n"
"at 1 64\n"
"exclamation 1 33\n"
"illegal 14 34 36 37 38 39 44 60 91 93 94 96 123 125 126\n";

static const char *_token_EPR_HANDLER =
"start = 0\n"
"trap_state = 1\n"
"alphabet_size = 21\n"
"size = 5\n"
"states =\n"
"0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 2 1\n"
"0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"0 1 1 1 1 3 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"1 1 1 1 3 3 1 1 1 1 1 1 1 1 4 1 1 1 1 1 1 1\n"
"0 1 1 1 3 3 1 1 1 1 1 1 1 1 4 1 1 1 1 1 1 1\n"
"name = EPR_HANDLER\n"
"alphabet =\n"
"space 1 32\n"
"new_line 1 10\n"
"tab 1 9\n"
"digit 10 48 49 50 51 52 53 54 55 56 57\n"
"letter 55 46 47 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90 95 97 98 99 100 101 102 103 104 105 106 107 108 109 110 111 112 113 114 115 116 117 118 119 120 121 122\n"
"left_paren 1 40\n"
"right_paren 1 41\n"
"bar 1 124\n"
"star 1 42\n"
"plus 1 43\n"
"question 1 63\n"
"equal 1 61\n"
"colon 1 58\n"
"dash 1 45\n"
"angle_right 1 62\n"
"semicolon 1 59\n"
"hash 1 35\n"
"backslash 1 92\n"
"at 1 64\n"
"exclamation 1 33\n"
"illegal 14 34 36 37 38 39 44 60 91 93 94 96 123 125 126\n";

static const char *_token_EXPANDS =
"start = 0\n"
"trap_state = 1\n"
"alphabet_size = 21\n"
"size = 6\n"
"states =\n"
"0 1 1 1 1 1 1 1 1 1 1 1 1 2 3 1 1 1 1 1 1 1\n"
"0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"0 1 1 1 1 1 1 1 1 1 1 1 1 4 1 1 1 1 1 1 1 1\n"
"0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 5 1 1 1 1 1 1\n"
"0 1 1 1 1 1 1 1 1 1 1 1 5 1 1 1 1 1 1 1 1 1\n"
"1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"name = EXPANDS\n"
"alphabet =\n"
"space 1 32\n"
"new_line 1 10\n"
"tab 1 9\n"
"digit 10 48 49 50 51 52 53 54 55 56 57\n"
"letter 55 46 47 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90 95 97 98 99 100 101 102 103 104 105 106 107 108 109 110 111 112 113 114 115 116 117 118 119 120 121 122\n"
"left_paren 1 40\n"
"right_paren 1 41\n"
"bar 1 124\n"
"star 1 42\n"
"plus 1 43\n"
"question 1 63\n"
"equal 1 61\n"
"colon 1 58\n"
"dash 1 45\n"
"angle_right 1 62\n"
"semicolon 1 59\n"
"hash 1 35\n"
"backslash 1 92\n"
"at 1 64\n"
"exclamation 1 33\n"
"illegal 14 34 36 37 38 39 44 60 91 93 94 96 123 125 126\n";

#define RULE_COUNT 23

static const char *_rules[RULE_COUNT] =
{
	"tokens_file -> @tokensStart tokens @tokensEnd",
	"tokens -> token_def tokens",
	"tokens -> ",
	"token_def -> @newNFA ID @setName EQUAL or_expr END @endNFA",
	"or_expr -> cat_expr more_or_expr",
	"more_or_expr -> OR cat_expr @or more_or_expr",
	"more_or_expr -> ",
	"cat_expr -> unary_expr more_cat_expr",
	"more_cat_expr -> unary_expr @cat more_cat_expr",
	"more_cat_expr -> ",
	"unary_expr -> primary_expr unary_operators",
	"unary_operators -> unary_operator unary_operators",
	"unary_operators -> ",
	"unary_operator -> KLEENE_STAR @star",
	"unary_operator -> AT_LEAST_ONCE @atLeastOnce",
	"unary_operator -> MAYBE @maybe",
	"unary_operator -> NUMBER @times",
	"primary_expr -> ID @push",
	"primary_expr -> lambda @push",
	"primary_expr -> space @push",
	"primary_expr -> tab @push",
	"primary_expr -> new_line @push",
	"primary_expr -> PAREN_OPEN or_expr PAREN_CLOSE"
};

static const char *_grammar_table =
"tokens_file 1 0\n"
"tokens_file 1 1 ID\n"
"tokens 3 0\n"
"tokens 2 1 ID\n"
"(@tokensStart) 0 0\n"
"(@tokensStart) 0 1 ID\n"
"token_def 6 1 ID\n"
"or_expr 7 1 ID\n"
"or_expr 7 1 PAREN_OPEN\n"
"or_expr 7 1 lambda\n"
"or_expr 7 1 new_line\n"
"or_expr 7 1 space\n"
"or_expr 7 1 tab\n"
"(@newNFA) 4 1 ID\n"
"((@newNFA)ID~@setName) 5 1 ID\n"
"cat_expr 11 1 ID\n"
"cat_expr 11 1 PAREN_OPEN\n"
"cat_expr 11 1 lambda\n"
"cat_expr 11 1 new_line\n"
"cat_expr 11 1 space\n"
"cat_expr 11 1 tab\n"
"more_or_expr 10 1 END\n"
"more_or_expr 9 1 OR\n"
"more_or_expr 10 1 PAREN_CLOSE\n"
"(OR~cat_expr~@or) 8 1 OR\n"
"unary_expr 15 1 ID\n"
"unary_expr 15 1 PAREN_OPEN\n"
"unary_expr 15 1 lambda\n"
"unary_expr 15 1 new_line\n"
"unary_expr 15 1 space\n"
"unary_expr 15 1 tab\n"
"more_cat_expr 14 1 END\n"
"more_cat_expr 13 1 ID\n"
"more_cat_expr 14 1 OR\n"
"more_cat_expr 14 1 PAREN_CLOSE\n"
"more_cat_expr 13 1 PAREN_OPEN\n"
"more_cat_expr 13 1 lambda\n"
"more_cat_expr 13 1 new_line\n"
"more_cat_expr 13 1 space\n"
"more_cat_expr 13 1 tab\n"
"(unary_expr~@cat) 12 1 ID\n"
"(unary_expr~@cat) 12 1 PAREN_OPEN\n"
"(unary_expr~@cat) 12 1 lambda\n"
"(unary_expr~@cat) 12 1 new_line\n"
"(unary_expr~@cat) 12 1 space\n"
"(unary_expr~@cat) 12 1 tab\n"
"primary_expr 22 1 ID\n"
"primary_expr 27 1 PAREN_OPEN\n"
"primary_expr 23 1 lambda\n"
"primary_expr 26 1 new_line\n"
"primary_expr 24 1 space\n"
"primary_expr 25 1 tab\n"
"unary_operators 16 1 AT_LEAST_ONCE\n"
"unary_operators 17 1 END\n"
"unary_operators 17 1 ID\n"
"unary_operators 16 1 KLEENE_STAR\n"
"unary_operators 16 1 MAYBE\n"
"unary_operators 16 1 NUMBER\n"
"unary_operators 17 1 OR\n"
"unary_operators 17 1 PAREN_CLOSE\n"
"unary_operators 17 1 PAREN_OPEN\n"
"unary_operators 17 1 lambda\n"
"unary_operators 17 1 new_line\n"
"unary_operators 17 1 space\n"
"unary_operators 17 1 tab\n"
"unary_operator 19 1 AT_LEAST_ONCE\n"
"unary_operator 18 1 KLEENE_STAR\n"
"unary_operator 20 1 MAYBE\n"
"unary_operator 21 1 NUMBER\n";

static void _lexer_eh(shLexer *lexer, char *fc, int *lb, int *f, int *l, int *c);
static bool _pleh(shParser *parser, const char *cur, shKtokens *la);

/* action routines */
static void _tokensStart(shParser *parser);
static void _tokensEnd(shParser *parser);
static void _setName(shParser *parser);
static void _newNFA(shParser *parser);
static void _endNFA(shParser *parser);
static void _or(shParser *parser);
static void _cat(shParser *parser);
static void _star(shParser *parser);
static void _atLeastOnce(shParser *parser);
static void _maybe(shParser *parser);
static void _times(shParser *parser);
static void _push(shParser *parser);

typedef struct tokens_data
{
	shVector/* shDfa */ dfas;		/* dfas gathered */
	shVector/* shNfa */ nfa_stack;		/* semantics stack */
	bool evaluate;				/* whether current token is good or bad */
	bool rebuild;				/* whether it should load from cache if possible */
	int error;				/* error code */

	shLetter alphabet[SH_DFA_MAX_LETTERS];	/* letters */
	int alphabet_count;			/* number of letters */
	shMap/* char *->int */ alphabet_map;	/* letter name to its id in alphabet array */

	const char *cache_path;			/* path to token caches */
	bool reloaded;				/* whether current token is reloaded from cache or not */
	char *token_name;			/* name of token being processed */

	int missing_token;			/* used by phrase-level error handler to generate names for missing tokens */
	bool eof_error_given;			/* whether error given on reaching EOF */

	sh_lexer_error_handler default_eh;	/* lexer error handler */
} tokens_data;

static void _dfa_assign(void *a, void *b)
{
	*(shDfa *)a = *(shDfa *)b;
}

static void _nfa_assign(void *a, void *b)
{
	*(shNfa *)a = *(shNfa *)b;
}

static void _str_assign(void *a, void *b)
{
	*(char **)a = b;
}

static void _int_assign(void *a, void *b)
{
	*(int *)a = *(int *)b;
}

static int _str_cmp(const void *a, const void *b)
{
	return strcmp(*(char * const *)a, *(char * const *)b);
}

int sh_lexer_init_to_tokens_file(shLexer *lexer)
{
	int error;
	shDfa dfa;

	error = sh_lexer_init(lexer);
	if (error != SH_LEXER_SUCCESS)
		return error;

	tokens_data *td = malloc(sizeof *td);
	if (td == NULL)
		goto exit_no_mem;
	*td = (tokens_data){ .error = 0 };
	sh_vector_init(&td->dfas, _dfa_assign, sizeof(shDfa));
	sh_vector_init(&td->nfa_stack, _nfa_assign, sizeof(shNfa));
	sh_map_init(&td->alphabet_map, _str_assign, sizeof(char *), _int_assign, sizeof(int), _str_cmp);

	lexer->user_data = td;
	sh_lexer_keywords(lexer, _keywords);

	sh_dfa_load(&dfa, _token_ID);			sh_lexer_add_dfa(lexer, &dfa);
	sh_dfa_load(&dfa, _token_EQUAL);		sh_lexer_add_dfa(lexer, &dfa);
	sh_dfa_load(&dfa, _token_END);			sh_lexer_add_dfa(lexer, &dfa);
	sh_dfa_load(&dfa, _token_WHITE_SPACE);		sh_lexer_add_dfa(lexer, &dfa);
	sh_dfa_load(&dfa, _token_PAREN_OPEN);		sh_lexer_add_dfa(lexer, &dfa);
	sh_dfa_load(&dfa, _token_PAREN_CLOSE);		sh_lexer_add_dfa(lexer, &dfa);
	sh_dfa_load(&dfa, _token_NUMBER);		sh_lexer_add_dfa(lexer, &dfa);
	sh_dfa_load(&dfa, _token_MAYBE);		sh_lexer_add_dfa(lexer, &dfa);
	sh_dfa_load(&dfa, _token_KLEENE_STAR);		sh_lexer_add_dfa(lexer, &dfa);
	sh_dfa_load(&dfa, _token_OR);			sh_lexer_add_dfa(lexer, &dfa);
	sh_dfa_load(&dfa, _token_AT_LEAST_ONCE);	sh_lexer_add_dfa(lexer, &dfa);
	sh_dfa_load(&dfa, _token_ACTION_ROUTINE);	sh_lexer_add_dfa(lexer, &dfa);
	sh_dfa_load(&dfa, _token_EPR_HANDLER);		sh_lexer_add_dfa(lexer, &dfa);
	sh_dfa_load(&dfa, _token_EXPANDS);		sh_lexer_add_dfa(lexer, &dfa);
	td->missing_token = 0;
	td->eof_error_given = 0;
	td->default_eh = sh_lexer_set_error_handler(lexer, _lexer_eh);
	lexer->initialized = true;
	return 0;
exit_no_mem:
	printf("insufficient memory when initializing tokens reader\n");
	return -1;
}

int sh_parser_init_to_tokens_file(shParser *parser, shLexer *lexer)
{
	int error;

	error = sh_parser_init(parser);
	if (error != SH_PARSER_SUCCESS)
		return error;

	error = sh_parser_load_llk(parser, lexer, _rules, RULE_COUNT, _grammar_table, 1, NULL);
	if (error)
	{
		_message("error: could not initialize tokens reader (%d)", error);
		return error;
	}
	PARSER_INTERNAL->llk_pleh = _pleh;
	sh_parser_set_action_routine(parser, "@tokensStart", _tokensStart);
	sh_parser_set_action_routine(parser, "@tokensEnd", _tokensEnd);
	sh_parser_set_action_routine(parser, "@setName", _setName);
	sh_parser_set_action_routine(parser, "@newNFA", _newNFA);
	sh_parser_set_action_routine(parser, "@endNFA", _endNFA);
	sh_parser_set_action_routine(parser, "@or", _or);
	sh_parser_set_action_routine(parser, "@cat", _cat);
	sh_parser_set_action_routine(parser, "@star", _star);
	sh_parser_set_action_routine(parser, "@atLeastOnce", _atLeastOnce);
	sh_parser_set_action_routine(parser, "@maybe", _maybe);
	sh_parser_set_action_routine(parser, "@times", _times);
	sh_parser_set_action_routine(parser, "@push", _push);

	return 0;
}

static void _lexer_eh(shLexer *lexer, char *fc, int *lb, int *f, int *l, int *c)
{
	tokens_data *td = lexer->user_data;
	sh_lexer_error_custom(lexer, lexer->file_name, *l, *c, "warning: no token can be matched, probably because of an invalid character (is it '");
	if (fc[*f] == '\n')
		sh_lexer_error_custom(lexer, NULL, 0, 0, "\\n");
	else if (fc[*f] == '\r')
		sh_lexer_error_custom(lexer, NULL, 0, 0, "\\r");
	else if (fc[*f] == '\t')
		sh_lexer_error_custom(lexer, NULL, 0, 0, "\\t");
	else if (fc[*f] >= ' ' && fc[*f] < 127)
		sh_lexer_error_custom(lexer, NULL, 0, 0, "%c", fc[*f]);
	else
		sh_lexer_error_custom(lexer, NULL, 0, 0, "\\%o", (unsigned int)fc[*f]);
	sh_lexer_error_custom(lexer, NULL, 0, 0, "'?)\n");
	td->default_eh(lexer, fc, lb, f, l, c);
	td->error = SH_LEXER_FAIL;
}

static void _clear_nfa_stack(shVector/* shNfa */ *s)
{
	shNfa *nfas = sh_vector_data(s);

	for (size_t i = 0; i < s->size; ++i)
		sh_nfa_free(&nfas[i]);

	sh_vector_clear(s);
}

/* return offset'th top nfa */
static shNfa *_get_top_nfa(shVector/* shNfa */ *s, size_t offset)
{
	assert(s->size > offset);
	return sh_vector_at(s, s->size - 1 - offset);
}

static int _letter_init(shLetter *letter, const char *name, const char *chars, int chars_count)
{
	letter->name = strdup(name);
	letter->chars = malloc(chars_count * sizeof *letter->chars);
	letter->chars_count = chars_count;
	if (letter->name == NULL || letter->chars == NULL)
		return -1;
	memcpy(letter->chars, chars, chars_count * sizeof *letter->chars);
	return 0;
}

int sh_compiler_TF_init(shLexer *l, bool rebuild, const char *letters, const char *cache_path)
{
#define READ_INT(x) \
	do {\
		char *end_position;\
		errno = 0;\
		x = strtol(buffer + position, &end_position, 10);\
		chars_read = end_position - (buffer + position);\
		if (errno || chars_read == 0)\
			goto exit_bad_format;\
		position += chars_read;\
	} while (0)
#define READ_CHAR(x) \
	do {\
		/* skip white-space */\
		while (buffer[position] != '\0' && isspace(buffer[position]))\
			++position;\
		if (buffer[position] == '\0')\
			goto exit_bad_format;\
		x = buffer[position];\
		++position;\
	} while (0)
	const char *buffer = letters;
	size_t position = 0;
	int chars_read;
	tokens_data *td = l->user_data;

	td->rebuild = rebuild;
	td->error = SH_LEXER_SUCCESS;
	td->missing_token = 0;
	td->eof_error_given = false;
	td->cache_path = cache_path;
	td->alphabet_count = 0;
	td->token_name = NULL;
	memset(td->alphabet, 0, sizeof(td->alphabet));
	_letter_init(&td->alphabet[0], "space"   , " " , 1);
	_letter_init(&td->alphabet[1], "new_line", "\n", 1);
	_letter_init(&td->alphabet[2], "tab"     , "\t", 1);
	sh_map_insert(&td->alphabet_map, td->alphabet[0].name, &td->alphabet_count);
	++td->alphabet_count;
	sh_map_insert(&td->alphabet_map, td->alphabet[1].name, &td->alphabet_count);
	++td->alphabet_count;
	sh_map_insert(&td->alphabet_map, td->alphabet[2].name, &td->alphabet_count);
	++td->alphabet_count;
	while ((td->alphabet[td->alphabet_count].name = sh_compiler_read_str(buffer + position, &chars_read)) != NULL)
	{
		bool ignore_this;
		char *name;
		position += chars_read;
		ignore_this = false;
		shLetter *letter = &td->alphabet[td->alphabet_count];
		name = letter->name;
		if (strcmp(name, "lambda") == 0 || sh_map_exists(&td->alphabet_map, &name))
		{
			_message("error: letter redifinition (letter %s)", name);
			_message("note:  letter ignored");
			ignore_this = true;
		}
		else
		{
			for (size_t i = 0, size = strlen(name); i < size; ++i)
			{
				char c = name[i];
				if (c == '+' || c == '|' || c== '*' || c== '?' || c == '(' || c == ')'
					|| c == '=' || c == '@' || c == ';' || c== '#' || c == '\\'
					|| (i == 0 && isdigit(c)))
				{
					_message("error: letter name cannot contain +, *, ?, |, (, ), =, @, ;, # or \\ and"
						" cannot start with a digit (letter %s), letter ignored", name);
					ignore_this = true;
				}
			}
		}
		if (!ignore_this)
			sh_map_insert(&td->alphabet_map, name, &td->alphabet_count);
		READ_INT(letter->chars_count);
		letter->chars = malloc(letter->chars_count * sizeof *letter->chars);
		if (letter->chars == NULL)
		{
			CLEAN_FREE(letter->name);
			goto exit_no_mem;
		}
		for (int i = 0, size = letter->chars_count; i < size; ++i)
		{
			char c;
			READ_CHAR(c);
			if (!ignore_this)
				letter->chars[i] = c;
		}
		if (!ignore_this)
			++td->alphabet_count;
		else
		{
			CLEAN_FREE(letter->name);
			CLEAN_FREE(letter->chars);
		}
	}
	return 0;
exit_no_mem:
	printf("insufficient memory when initializing tokens reader\n");
	goto exit_fail;
exit_bad_format:
	printf("error in letters file\n");
exit_fail:
	sh_compiler_TF_cleanup(l);
	return -1;
}

void sh_compiler_TF_cleanup(shLexer *l)
{
	tokens_data *td = l->user_data;
	if (td == NULL)
		return;
	for (int i = 0; i < SH_DFA_MAX_LETTERS; ++i)
	{
		CLEAN_FREE(td->alphabet[i].name);
		CLEAN_FREE(td->alphabet[i].chars);
	}
	sh_map_free(&td->alphabet_map);
	sh_vector_free(&td->dfas);
	_clear_nfa_stack(&td->nfa_stack);
	CLEAN_FREE(td->token_name);
	free(td);
}

shVector/* shDfa */ sh_compiler_TF_get_dfas(shLexer *l)
{
	tokens_data *td = l->user_data;
	return td->dfas;
}

int sh_compiler_TF_error(shLexer *l)
{
	tokens_data *td = l->user_data;
	return td->error;
}

static void _stop_evaluation(tokens_data *td, int error_value)
{
	td->evaluate = false;
	td->error = error_value;
}

static void _tokensStart(shParser *parser)
{
	tokens_data *td = parser->lexer->user_data;
	sh_vector_clear(&td->dfas);
	_clear_nfa_stack(&td->nfa_stack);
}

static void _tokensEnd(shParser *parser)
{
	tokens_data *td = parser->lexer->user_data;
	_clear_nfa_stack(&td->nfa_stack);
}

static void _newNFA(shParser *parser)
{
	tokens_data *td = parser->lexer->user_data;
	td->reloaded = false;
	td->evaluate = true;
}

static void _endNFA(shParser *parser)
{
	tokens_data *td = parser->lexer->user_data;
	if (td->reloaded)
		return;
	if (!td->evaluate && td->nfa_stack.size > 0)
	{
		shNfa *nfa = _get_top_nfa(&td->nfa_stack, 0);
		sh_nfa_free(nfa);
		sh_vector_remove_last(&td->nfa_stack);
		return;
	}
	if (td->nfa_stack.size == 0)
	{
		_stop_evaluation(td, SH_LEXER_FAIL);
		return;
	}

	shDfa dfa, simpdfa;
	shNfa *nfa;
	char *cache_file;
	FILE *dfa_cache;

	nfa = _get_top_nfa(&td->nfa_stack, 0);
	nfa->alphabet_size = td->alphabet_count;
	for (int i = 0; i < td->alphabet_count; ++i)
	{
		nfa->alphabet[i].name = td->alphabet[i].name;
		nfa->alphabet[i].chars = td->alphabet[i].chars;
		nfa->alphabet[i].chars_count = td->alphabet[i].chars_count;
	}

	if (sh_dfa_convert(&dfa, nfa, td->token_name))
		goto cleanup;
	if (sh_dfa_simplify(&simpdfa, &dfa))
		goto cleanup;
	sh_dfa_free(&dfa);

	cache_file = sh_compiler_string("%s%s.dfa", td->cache_path, td->token_name);
	if (cache_file == NULL)
		goto cleanup;
	dfa_cache = fopen(cache_file, "w");
	free(cache_file);
	if (dfa_cache)
	{
		sh_dfa_store(&simpdfa, dfa_cache);
		fclose(dfa_cache);
	}
	sh_vector_append(&td->dfas, &simpdfa);
cleanup:
	sh_nfa_free(nfa);
	sh_vector_remove_last(&td->nfa_stack);
}

static void _setName(shParser *parser)
{
	tokens_data *td = parser->lexer->user_data;
	FILE *dfa_file = NULL;

	free(td->token_name);
	td->token_name = strdup(parser->lexer->current_token);
	if (td->token_name == NULL)
		goto exit_no_mem;

	char *cache_file = sh_compiler_string("%s%s.dfa", td->cache_path, td->token_name);
	if (cache_file != NULL)
		dfa_file = fopen(cache_file, "r");
	free(cache_file);

	if (!td->rebuild && dfa_file)
	{
		shDfa dfa;
		char *fc;
		unsigned int length;
		fseek(dfa_file, 0, SEEK_END);
		length = ftell(dfa_file);
		fc = malloc((length + 1) * sizeof *fc);
		if (fc == NULL)
			goto exit_no_mem;
		fseek(dfa_file, 0, SEEK_SET);
		if (fread(fc, 1, length, dfa_file) != length)
			goto exit_fail;
		fc[length] = '\0';
		if (sh_dfa_load(&dfa, fc))
			goto exit_fail;
		td->reloaded = true;
		sh_vector_append(&td->dfas, &dfa);
	exit_fail:
		free(fc);
	}
exit_cleanup:
	if (dfa_file)
		fclose(dfa_file);
	return;
exit_no_mem:
	sh_parser_stop(parser, SH_PARSER_NO_MEM);
	goto exit_cleanup;
}

static void _or(shParser *parser)
{
	tokens_data *td = parser->lexer->user_data;
	if (td->reloaded)
		return;

	if (td->nfa_stack.size < 2)
	{
		_stop_evaluation(td, SH_LEXER_FAIL);
		return;
	}

	shNfa *nfa1, *nfa2;

	nfa2 = _get_top_nfa(&td->nfa_stack, 0);
	nfa1 = _get_top_nfa(&td->nfa_stack, 1);
	if (sh_nfa_or(nfa1, nfa2))
	{
		sh_nfa_free(nfa2);
		sh_parser_stop(parser, SH_PARSER_NO_MEM);
		_stop_evaluation(td, SH_PARSER_NO_MEM);
	}
	else
		sh_nfa_shallow_free(nfa2);
	sh_vector_remove_last(&td->nfa_stack);
}

static void _cat(shParser *parser)
{
	tokens_data *td = parser->lexer->user_data;
	if (td->reloaded)
		return;

	if (td->nfa_stack.size < 2)
	{
		_stop_evaluation(td, SH_LEXER_FAIL);
		return;
	}

	shNfa *nfa1, *nfa2;

	nfa2 = _get_top_nfa(&td->nfa_stack, 0);
	nfa1 = _get_top_nfa(&td->nfa_stack, 1);
	if (sh_nfa_cat(nfa1, nfa2))
	{
		sh_nfa_free(nfa2);
		sh_parser_stop(parser, SH_PARSER_NO_MEM);
		_stop_evaluation(td, SH_PARSER_NO_MEM);
	}
	else
		sh_nfa_shallow_free(nfa2);
	sh_vector_remove_last(&td->nfa_stack);
}

static void _star(shParser *parser)
{
	tokens_data *td = parser->lexer->user_data;
	if (td->reloaded)
		return;

	if (td->nfa_stack.size == 0)
	{
		_stop_evaluation(td, SH_LEXER_FAIL);
		return;
	}

	shNfa *nfa = _get_top_nfa(&td->nfa_stack, 0);
	if (sh_nfa_unary(nfa, -2))
		sh_parser_stop(parser, SH_PARSER_NO_MEM);
}

static void _atLeastOnce(shParser *parser)
{
	tokens_data *td = parser->lexer->user_data;
	if (td->reloaded)
		return;

	if (td->nfa_stack.size == 0)
	{
		_stop_evaluation(td, SH_LEXER_FAIL);
		return;
	}

	shNfa *nfa = _get_top_nfa(&td->nfa_stack, 0);
	if (sh_nfa_unary(nfa, 0))
		sh_parser_stop(parser, SH_PARSER_NO_MEM);
}

static void _maybe(shParser *parser)
{
	tokens_data *td = parser->lexer->user_data;
	if (td->reloaded)
		return;

	if (td->nfa_stack.size == 0)
	{
		_stop_evaluation(td, SH_LEXER_FAIL);
		return;
	}

	shNfa *nfa = _get_top_nfa(&td->nfa_stack, 0);
	if (sh_nfa_unary(nfa, -1))
		sh_parser_stop(parser, SH_PARSER_NO_MEM);
}

static void _times(shParser *parser)
{
	tokens_data *td = parser->lexer->user_data;
	if (td->reloaded)
		return;

	if (td->nfa_stack.size == 0)
	{
		_stop_evaluation(td, SH_LEXER_FAIL);
		return;
	}

	shNfa *nfa;
	int count = 0;

	nfa = _get_top_nfa(&td->nfa_stack, 0);
	count = strtol(parser->lexer->current_token, NULL, 10);
	if (count != 0)
	{
		if (sh_nfa_unary(nfa, count))
			sh_parser_stop(parser, SH_PARSER_NO_MEM);
	}
	else
	{
		/* if count is 0, just make a lambda NFA */
		sh_nfa_free(nfa);
		if (sh_nfa_init(nfa, -1, td->alphabet_count))
			sh_parser_stop(parser, SH_PARSER_NO_MEM);
	}
}

static void _push(shParser *parser)
{
	shNfa nfa;
	shNode n;
	const char *letter;
	tokens_data *td = parser->lexer->user_data;
	if (td->reloaded)
		return;

	int letter_id = -1;

	letter = parser->lexer->current_token;
	if (strcmp(letter, "lambda") != 0)
	{
		if (!sh_map_find(&td->alphabet_map, &letter, &n))
		{
			sh_lexer_error(parser->lexer, "error: unknown letter name '%s'", letter);
			_stop_evaluation(td, SH_LEXER_FAIL);

			/* push a lambda instead of this unknown letter */
		}
		else
			letter_id = *(int *)n.data;
	}

	if (sh_nfa_init(&nfa, letter_id, td->alphabet_count))
	{
		sh_parser_stop(parser, SH_PARSER_NO_MEM);
		_stop_evaluation(td, SH_PARSER_NO_MEM);
	}
	else
		sh_vector_append(&td->nfa_stack, &nfa);
}

static bool _pleh(shParser *parser, const char *cur, shKtokens *la)
{
	bool recovered = true;
	shLexer *l = parser->lexer;
	tokens_data *td = l->user_data;
	if (strcmp(cur, "END") == 0)
	{
		sh_lexer_insert_fake(l, cur, NULL);
		sh_lexer_error(l, "error: missing semicolon, inserted ';'");
	}
	else if (strcmp(cur, "EQUAL") == 0)
	{
		sh_lexer_insert_fake(l, cur, NULL);
		sh_lexer_error(l, "error: missing assignment operator, inserted '='");
	}
	else if (strcmp(cur, "ID") == 0)
	{
		char id[100];
		sprintf(id, "__missing_%d", td->missing_token++);
		sh_lexer_insert_fake(l, cur, id);
		sh_lexer_error(l, "error: missing identifier, inserted '%s'", l->current_token);
	}
	else if (strcmp(cur, "PAREN_CLOSE") == 0)
	{
		sh_lexer_insert_fake(l, cur, NULL);
		sh_lexer_error(l, "error: missing closing parenthesis, inserted ')'");
	}
	else
	{
		const char *type;
		const char *token;
		token = sh_ktokens_at(la, &type, 0);
		if (type == NULL)
		{
			type = "";
			token = "";
		}
		if (strcmp(type, "EXPANDS") == 0)
		{
			/* since lookaheads are not yet read, then to get the correct line and column, I shall peek one token ahead */
			int line, column;
			const char *ignore;
			sh_lexer_peek(l, &ignore, 0, &line, &column);
			sh_lexer_error_custom(l, l->file_name, line, column, "error: '%s' cannot appear in this file\n", token);
			sh_parser_push(parser, cur);
			sh_parser_push(parser, token);
		}
		else if (strcmp(type, "ACTION_ROUTINE") == 0)
		{
			int line, column;
			const char *ignore;
			sh_lexer_peek(l, &ignore, 0, &line, &column);
			sh_lexer_error_custom(l, l->file_name, line, column, "error: names starting with '@' are reserved for action routines\n");
			sh_parser_push(parser, cur);
			sh_parser_push(parser, type);
		}
		else if (strcmp(type, "EPR_HANDLER") == 0)
		{
			int line, column;
			const char *ignore;
			sh_lexer_peek(l, &ignore, 0, &line, &column);
			sh_lexer_error_custom(l, l->file_name, line, column, "error: names starting with '!' are reserved for error production rule handlers\n");
			sh_parser_push(parser, cur);
			sh_parser_push(parser, type);
		}
		else if (strcmp(type, "") == 0)
		{
			if (!td->eof_error_given)
			{
				assert(0);	/* Note: this shouldn't happen */
				sh_lexer_error(l, "error: unexpected end of file before end of token defintion");
			}
			td->eof_error_given = true;
			sh_parser_push(parser, "@tokensEnd");
			sh_parser_push(parser, "@endNFA");
			if (strcmp(cur, "or_expr") == 0 || strcmp(cur, "cat_expr") == 0 || strcmp(cur, "unary_expr") == 0 || strcmp(cur, "primary_expr") == 0)
			{
				sh_lexer_insert_fake(l, "lambda", NULL);
				sh_parser_push(parser, "@push");
			}
		}
		else if (strcmp(cur, "tokens_file") == 0 || strcmp(cur, "tokens") == 0 || strcmp(cur, "token_def") == 0)
		{
			if (strcmp(type, "AT_LEAST_ONCE") == 0)
			{
				sh_lexer_error(l, "error: unexpected '+' at start of token definition");
				sh_parser_push(parser, cur);
				sh_parser_push(parser, type);
			}
			else if (strcmp(type, "END") == 0)		/* just a single semicolon, ignore it */
			{
				sh_parser_push(parser, cur);
				sh_parser_push(parser, type);
			}
			else if (strcmp(type, "EQUAL") == 0)
			{
				sh_lexer_error(l, "error: expected token name before '='");
				char id[100];
				sprintf(id, "__missing_%d", td->missing_token++);
				sh_lexer_insert_fake(l, "ID", id);
				if (strcmp(cur, "tokens_file") == 0)
					sh_parser_push(parser, "@tokensEnd");
				if (strcmp(cur, "token_def") != 0)
					sh_parser_push(parser, "tokens");
				sh_parser_push(parser, "@endNFA");
				sh_parser_push(parser, "END");
				sh_parser_push(parser, "or_expr");
				sh_parser_push(parser, "EQUAL");
				sh_parser_push(parser, "@setName");
				/* sh_parser_push(parser, "ID");	** missing ID faked */
				sh_parser_push(parser, "@newNFA");
				if (strcmp(cur, "tokens_file") == 0)
					sh_parser_push(parser, "@tokensStart");
			}
			else if (strcmp(type, "KLEENE_STAR") == 0)
			{
				sh_lexer_error(l, "error: unexpected '*' at start of token definition");
				sh_parser_push(parser, cur);
				sh_parser_push(parser, type);
			}
			else if (strcmp(type, "MAYBE") == 0)
			{
				sh_lexer_error(l, "error: unexpected '?' at start of token definition");
				sh_parser_push(parser, cur);
				sh_parser_push(parser, type);
			}
			else if (strcmp(type, "NUMBER") == 0)
			{
				sh_lexer_error(l, "error: unexpected number at start of token definition");
				sh_lexer_error(l, "note:  identifiers cannot start with a digit");
				sh_parser_push(parser, cur);
				sh_parser_push(parser, type);
			}
			else if (strcmp(type, "OR") == 0)
			{
				sh_lexer_error(l, "error: unexpected '|' at start of token definition");
				sh_parser_push(parser, cur);
				sh_parser_push(parser, type);
			}
			else if (strcmp(type, "PAREN_CLOSE") == 0)
			{
				sh_lexer_error(l, "error: unexpected ')' at start of token definition");
				sh_parser_push(parser, cur);
				sh_parser_push(parser, type);
			}
			else if (strcmp(type, "PAREN_OPEN") == 0)
			{
				sh_lexer_error(l, "error: unexpected '(' at start of token definition");
				sh_parser_push(parser, cur);
				sh_parser_push(parser, type);
			}
			else if (strcmp(type, "lambda") == 0 || strcmp(type, "new_line") == 0 || strcmp(type, "space") == 0 || strcmp(type, "tab") == 0)
			{
				char id[100];
				sprintf(id, "__missing_%d", td->missing_token++);
				sh_lexer_insert_fake(l, "ID", id);
				sh_parser_push(parser, cur);
				sh_ktokens_replace(la, l->current_token, l->current_type, 0);
				sh_lexer_error(l, "error: '%s' is a reserved word and cannot be used as a token name, replaced by %s",
						type, l->current_token);
			}
			else
				goto exit_unhandled;
		}
		else if (strcmp(cur, "or_expr") == 0 || strcmp(cur, "cat_expr") == 0 || strcmp(cur, "unary_expr") == 0 || strcmp(cur, "primary_expr") == 0)
		{
			if (strcmp(type, "AT_LEAST_ONCE") == 0)
			{
				sh_lexer_error(l, "error: expected expression before '+'");
				sh_parser_push(parser, cur);
				sh_parser_push(parser, type);
			}
			else if (strcmp(type, "END") == 0)
			{
				sh_lexer_insert_fake(l, "lambda", NULL);
				sh_lexer_error(l, "error: missing expression in token definition or unclosed '('");
				sh_parser_push(parser, "@push");
			}
			else if (strcmp(type, "EQUAL") == 0)
			{
				sh_lexer_error(l, "error: missing semicolon at the end of previous token definition");
			}
			else if (strcmp(type, "KLEENE_STAR") == 0)
			{
				sh_lexer_error(l, "error: expected expression before '*'");
				sh_parser_push(parser, cur);
				sh_parser_push(parser, type);
			}
			else if (strcmp(type, "MAYBE") == 0)
			{
				sh_lexer_error(l, "error: expected expression before '?'");
				sh_parser_push(parser, cur);
				sh_parser_push(parser, type);
			}
			else if (strcmp(type, "NUMBER") == 0)
			{
				sh_lexer_error(l, "error: expected expression before repetition");
				sh_parser_push(parser, cur);
				sh_parser_push(parser, type);
			}
			else if (strcmp(type, "OR") == 0)
			{
				sh_lexer_error(l, "error: expected expression before '|'");
				sh_parser_push(parser, cur);
				sh_parser_push(parser, type);
			}
			else if (strcmp(type, "PAREN_CLOSE") == 0)
			{
				sh_lexer_error(l, "error: missing expression before ')'");
				sh_parser_push(parser, cur);
				sh_parser_push(parser, type);
			}
			else
				goto exit_unhandled;
		}
		else if (strcmp(cur, "more_or_expr") == 0)
		{
			if (strcmp(type, "AT_LEAST_ONCE") == 0 || strcmp(type, "ID") == 0 || strcmp(type, "KLEENE_STAR") == 0 || strcmp(type, "MAYBE") == 0
					|| strcmp(type, "NUMBER") == 0 || strcmp(type, "PAREN_OPEN") == 0 || strcmp(type, "lambda") == 0
					|| strcmp(type, "new_line") == 0 || strcmp(type, "space") == 0 || strcmp(type, "tab") == 0)
			{
				assert(0);	/* Note: this shouldn't happen */
				sh_lexer_error(l, "error: expected '|'");
				sh_parser_push(parser, "more_or_expr");
				sh_parser_push(parser, "@or");
				sh_parser_push(parser, "cat_expr");
				/* sh_parser_push(parser, "OR");	** missing '|' ignored */
			}
			else if (strcmp(type, "EQUAL") == 0)
			{
				sh_lexer_error(l, "error: missing semicolon at the end of previous token definition");
			}
			else
				goto exit_unhandled;
		}
		else if (strcmp(cur, "more_cat_expr") == 0)
		{
			if (strcmp(type, "AT_LEAST_ONCE") == 0)
			{
				assert(0);	/* Note: this shouldn't happen */
				sh_lexer_error(l, "error: expected expression before '+'");
				sh_parser_push(parser, cur);
				sh_parser_push(parser, type);
			}
			else if (strcmp(type, "EQUAL") == 0)
			{
				/* error will be given with more_or_expr */
				/* sh_lexer_error(l, "error: missing semicolon at the end of previous token definition"); */
			}
			else if (strcmp(type, "KLEENE_STAR") == 0)
			{
				assert(0);	/* Note: this shouldn't happen */
				sh_lexer_error(l, "error: expected expression before '*'");
				sh_parser_push(parser, cur);
				sh_parser_push(parser, type);
			}
			else if (strcmp(type, "MAYBE") == 0)
			{
				assert(0);	/* Note: this shouldn't happen */
				sh_lexer_error(l, "error: expected expression before '?'");
				sh_parser_push(parser, cur);
				sh_parser_push(parser, type);
			}
			else if (strcmp(type, "NUMBER") == 0)
			{
				assert(0);	/* Note: this shouldn't happen */
				sh_lexer_error(l, "error: expected expression before repetition");
				sh_parser_push(parser, cur);
				sh_parser_push(parser, type);
			}
			else
				goto exit_unhandled;
		}
		else if (strcmp(cur, "unary_operators") == 0)
		{
			if (strcmp(type, "EQUAL") == 0)
			{
				/* error will be given with more_or_expr */
				/* sh_lexer_error(l, "error: missing semicolon at the end of previous token definition"); */
			}
			else
				goto exit_unhandled;
		}
		else if (strcmp(cur, "unary_operator") == 0)
		{
			if (strcmp(type, "END") == 0 || strcmp(type, "EQUAL") == 0 || strcmp(type, "ID") == 0 || strcmp(type, "OR") == 0
					|| strcmp(type, "PAREN_CLOSE") == 0 || strcmp(type, "PAREN_OPEN") == 0 || strcmp(type, "lambda") == 0
					|| strcmp(type, "new_line") == 0 || strcmp(type, "space") == 0 || strcmp(type, "tab") == 0)
			{
				assert(0);	/* Note: this shouldn't happen */
				sh_lexer_error(l, "error: expected unary operator: '*', '+', '?' or a number");
			}
			else
				goto exit_unhandled;
		}
		else
			goto exit_unhandled;
		goto exit_handled;
	exit_unhandled:
		sh_lexer_error(l, "error: unhandled error, expected to match '%s', but found '%s'", cur, type);
	}
exit_handled:
	_stop_evaluation(td, SH_LEXER_FAIL);
	return recovered;
}
