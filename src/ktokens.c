/*
 * Copyright (C) 2007-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shCompiler.
 *
 * shCompiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shCompiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shCompiler.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include "shktokens.h"

int sh_ktokens_init(shKtokens *l, unsigned int k)
{
	if (k == 0 || l == NULL)
		return -1;

	*l = (shKtokens){
		.tokens = malloc(k * sizeof *l->tokens),
		.types = malloc(k * sizeof *l->types),
		.start = 0,
		.size = 0,
		.k = k
	};

	if (l->tokens == NULL || l->types == NULL)
		goto exit_no_mem;

	return 0;
exit_no_mem:
	free(l->tokens);
	free(l->types);
	*l = (shKtokens){0};
	return -1;
}

void sh_ktokens_free(shKtokens *l)
{
	if (l == NULL)
		return;

	free(l->tokens);
	free(l->types);

	*l = (shKtokens){0};
}

const char *sh_ktokens_at(shKtokens *l, const char **type, unsigned int n)
{
	if (n < l->size)
	{
		unsigned int index = l->start + n;

		if (index >= l->k)
			index -= l->k;

		if (type)
			*type = l->types[index];

		return l->tokens[index];
	}

	if (type)
		*type = NULL;

	return NULL;
}

void sh_ktokens_push(shKtokens *l, const char *token, const char *type)
{
	unsigned int index = l->start + l->size;

	if (index >= l->k)
		index -= l->k;

	l->tokens[index] = token;
	l->types[index] = type;

	if (l->size < l->k)
		++l->size;
	else
	{
		++l->start;
		if (l->start == l->k)
			l->start = 0;
	}
}

void sh_ktokens_replace(shKtokens *l, const char *token, const char *type, unsigned int n)
{
	unsigned int index;

	if (l == NULL || n >= l->size)
		return;

	index = l->start + n;
	if (index >= l->k)
		index -= l->k;

	l->tokens[index] = token;
	l->types[index] = type;
}
