/*
 * Copyright (C) 2007-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shCompiler.
 *
 * shCompiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shCompiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shCompiler.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PARSER_INTERNAL_H_BY_CYCLOPS
#define PARSER_INTERNAL_H_BY_CYCLOPS

#include <shparser.h>

/* TODO: change _data to _lookup? */

typedef enum symbol_type
{
	TERMINAL = 0,
	NONTERMINAL,
	ACTION_ROUTINE,
	EPR_HANDLER
} symbol_type;

typedef struct grammar_symbol
{
	symbol_type type;		/* whether it's a terminal, non-terminal or action routine */
	int id;				/* The index in terminals, nonterminals or action routines.  -1 for end of input ($) */
} grammar_symbol;

typedef struct grammar_rule
{
	grammar_symbol head;		/* The nonterminal (T) on the left of rule: T -> ... */
	shVector/* grammar_symbol */ body;
					/*
					 * The sequence of terminals, nonterminals, action routines and error production rule handlers
					 * on the right of rule: T -> ...
					 * If body is empty, it shows T -> lambda
					 */
} grammar_rule;

typedef struct find_first_data
{
	grammar_symbol *sentence;	/* the sentence for which first is begin calculated */
	unsigned int k;			/* the number of tokens of first */

	bool sentence_malloced;		/* whether sentence has been malloced */
	size_t sentence_len;		/* length of sentence */
} find_first_data;

typedef struct sentence_first
{
	shVector/* int */ first;	/* one first of a sentence with length k of find_first_data */
	bool partial;			/*
					 * whether this is a partial or complete generation of sentence.
					 * A first could both be a partial and nonpartial generation of a
					 * sentence.  In that case, it is stored only once and as nonpartial.
					 */
	bool needs_free;		/* whether the vector is shallow copied from another one or should be freed */
} sentence_first;

typedef struct find_first_result
{
	shSet/* sentence_first */ firsts;
	bool complete;			/* whether all firsts have been calculated */
	bool dfs_visited;		/* visited mark for DFS */
} find_first_result;

typedef struct parent_and_after_data
{
	int parent;			/*
					 * in a rule such as A -> <before> B <after>, parent of B is A,
					 * meaning that what comes after B may depend on what comes after A
					 */
	grammar_symbol *after;		/* <after> in the example above */
	size_t after_len;		/* length of after */
} parent_and_after_data;

typedef struct find_follow_data
{
	int nonterminal;		/*
					 * the nonterminal for which the follow is being calculated
					 * Note: parent_and_after uses a negative value to indicate no parent, so this must remain
					 * an int (not unsigned).  Take care therefore to index parent_and_after with ints.
					 */
	unsigned int s;			/* the length of the follow */
} find_follow_data;

typedef struct nonterminal_follow
{
	shVector/* int */ follow;	/* one follow of a nonterminal with length s of find_follow_data */
	bool needs_free;		/* whether the vector is shallow copied from another one or should be freed */
} nonterminal_follow;

typedef struct find_follow_result
{
	shSet/* nonterminal_follow */ follows;
} find_follow_result;

typedef struct llk_table_data
{
	int nonterminal;		/* the nonterminal being expanded */
	shVector/* int */ look_ahead;	/* the k look-ahead tokens determining by which rule the nonterminal would be expanded */
} llk_table_data;

typedef struct llk_table_result
{
	int rule;			/* the rule which would be used to expand a nonterminal based on the given look_ahead */
					/* TODO: change to unsigned int */
} llk_table_result;

typedef struct lrk_diagram_data
{
	int state;			/* state in the diagram */
	bool with_nonterminal;		/* whether nonterminal or look_ahead should be used */
	int nonterminal;		/* nonterminal determining state transition */
	shVector/* int */ look_ahead;	/* k look-ahead tokens determining the state transition/action */
} lrk_diagram_data;

typedef enum lrk_transition
{
	SHIFT = 0,
	REDUCE,
	GOTO,
	ACCEPT,
} lrk_transition;

typedef struct lrk_diagram_result
{
	lrk_transition transition;	/* the transition from the state with nonterminal/look-ahead */
	union
	{
		int state;		/* if SHIFT or GOTO, to which state the diagram should move to */
		int rule;		/* if REDUCE, which rule to use for reduction */
	};
} lrk_diagram_result;

typedef struct llk_stack_element
{
	grammar_symbol symbol;		/* either a terminal, nonterminal that needs to be matched */
} llk_stack_element;

typedef struct lrk_stack_element
{
	grammar_symbol symbol;		/* either a terminal, nonterminal that needs to be matched */
	int state;			/* the current state in LRK diagram */
} lrk_stack_element;

typedef struct parser_internal
{
	/* entities involved in grammar description */

	unsigned int k_in_grammar;				/* K of the grammer, for example 1 if LL(1) or 3 if LR(3) */
	sh_grammar_type grammar_type;				/* type of grammar being parsed */

	shVector/* const char * */ terminals;			/* list of terminals in the grammar, i.e. token types + keywords, shared pointers with bound lexer */
	shVector/* char * */ nonterminals;			/* list of nonterminals in the grammar */
	shVector/* char * */ action_routines;			/* list of action routines in the grammar */
	shVector/* char * */ epr_handlers;			/* list of error production rule handlers in the grammar */
	shMap/* const char *->int */ terminal_codes;		/* a map from terminal name to index in terminals */
	shMap/* const char *->int */ nonterminal_codes;		/* a map from nonterminal name to index in nonterminals */
	shMap/* const char *->int */ action_routine_codes;	/* a map from action routine name to index in action_routines */
	shMap/* const char *->int */ epr_handler_codes;		/* a map from error production rule handlers to index in epr_handlers */
	shVector/* sh_parser_ar */ action_routine_functions;	/* action routine callbacks indexed by id of action routine */
	shVector/* sh_parser_eprh */ epr_handler_functions;	/* error production rule handler callbacks indexed by id of epr handler */
	shVector/* int */ rule_epr_handler;			/* index to epr_handler_functions for each rule.  -1 represents not error production rule */
	shVector/* grammar_rule */ rules;			/* rules of the grammar */
	shVector/* int */ rule_action_routines;			/*
								 * id of action routine of each rule. The action routine is
								 * always placed last, since CLRK needs it that way. -1 if no action routine
								 */
	shVector/* shVector of int */ nonterminal_rules;	/* list of rules per nonterminal (TODO: change to unsigned int) */

	/* items required for parsing */

	shStack/* l[lr]k_stack_element */ parse_stack;		/* parse stack for both LL(k) and LR(k) */

	bool llk_table_built;					/* if false, LL(k) table is not yet built */
	bool lrk_diagram_built;					/* if false, LR(k) diagram is not yet built */
	shMap/* llk_table_data->llk_table_result */ llk_table;	/* a table showing which nonterminal with which look-ahead (k terminals) must use which rule */
	shMap/* lrk_diagram_data->lrk_diagram_result */ lrk_diagram;
								/* a diagram showing which state with which look-ahead (k terminals) or nonterminal should take which action */

	sh_parser_llk_pleh llk_pleh;				/* phrase level error handler for an LL(k) grammar */
	sh_parser_lrk_pleh lrk_pleh;				/* phrase level error handler for an LR(k) grammar */

	bool terminate_early;					/* set by sh_parser_stop, to indicate the parser should terminate early */
	int terminate_return_value;				/* value to return from sh_parser_parse if terminate_only */

	/* for parse table/diagram creation */

	const char *grammar_file_name;				/* keep the grammar file name for logging grammar errors during processing */
	int ambiguity_resolution;				/* the method of ambiguity resolution. One of SH_PARSER_AMBIGUITY_* */
	shMap/* find_first_data->find_first_result */ firsts;	/* a map from a sentence to its possible firsts, used with both LL(k) and LR(k) */
	shMap/* int -> shSet of parent_and_after_data */ parent_and_after;
								/*
								 * a map from a nonterminal to a set.  Each element of this set shows that this nonterminal
								 * can be derived from which nonterminal and what sentence would come after it.  When follow
								 * dependencies are found and cycles removed, some of the (nonterminal, number_of_tokens_to_follow)
								 * may be grouped together.  This map then accumulates "after"s of each strongly connected component
								 * in its representative.  Beyond this point, the representative is what is queried for "after"s and
								 * never the others.  This is used for LL(k) follow generation.
								 */
	shMap/* find_follow_data->shSet of find_follow_data */ follow_graph;
								/*
								 * an adjacency list of follow dependencies: "nonterminal_a_wanting_i_tokens" is dependent on
								 * "nonterminal_b_wanting_j_tokens"s.  This is used for LL(k) follow generation.
								 */
	shMap/* find_follow_data->find_follow_data */follow_representative;
								/* representative of strongly connected components of follow_graph */
	shMap/* find_follow_data->find_follow_result */ follows;/* get a set of follows of length k of a nonterminal.  Used in LL(k) table generation. */

} parser_internal;

/*
 * Startup and Cleanup:
 * load_tokens_from_lexer read tokens from the lexer
 * load_grammar		parse the grammar file and load grammar
 *
 * Loading Rules:
 * add_terminal		stores terminal in the parser.  Returns id (>= 0) if successful or -1 if out of memory.
 * add_nonterminal	checks whether nonterminal is new.  If so, stores the string.  Returns if (>= 0) if successful or -1 if out of memory.
 * add_action_routine	similar to add_nonterminal but for action routine.
 * add_epr_handler	similar to add_nonterminal but for EPR handlers.
 * add_symbol		adds/retrieves a symbol, automatically detecting its type.  Returns the type and id.  In case of error,
 *			the id of the returned symbol would be -1.
 * generate_and_add_rule read a rule from a string
 * rules_sanity_check	does some common checks after loading rules, such as if all nonterminals have at least one expansion rule.
 *
 * Calculating Parse Tables
 * is_left_recursive	called for LL(k) grammars to check if it is left recursive (left recursive is not accepted).
 *			returns SH_PARSER_NO_MEM if out of memory, SH_PARSER_SUCCESS if everything alright or
 *			1+id of offending nonterminal.
 * find_all_firsts	calculates the firsts of all nonterminals with lengths 0 to k and some sentences as side effect.
 * find_firsts		calculates the firsts of a given non-empty sentence and returns the node that corresponds to
 *			to the find_first_data/result key-data pair.
 * find_all_follows	calculates the follows of all nonterminals with lengths 0 to k.
 * find_follows		calculates the follows of a given length of a given nonterminal and returns the node that
 *			corresponds to the find_follow_data/result key/data pair.
 * prepare_llk_parser	reload or if forced or impossible, build and store the LL(k) table.
 * prepare_lrk_parser	reload or if forced or impossible, build and store the LR(k) diagram.
 * cleanup		cleanup structures used to calculate LL(k) and LR(k) tables, such as firsts, follows etc.
 *
 * Parsing
 * parse_sanity_check	does some common checks before parsing, such as making sure all callbacks are set.
 * parse_llk		LL(k) parse
 * parse_lrk		LR(k) parse
 *
 * Miscellaneous
 * symbol_name		taking a symbol, it will return its name.  Returns NULL if non-existent
 * terminal_name	shortcut for symbol name, for terminals.
 * nonterminal_name	shortcut for symbol name, for nonterminals.
 * print_rule		print a single rule by sh_lexer_error, useful for error messages containing the rule
 *
 * Tools
 * generate_llk_pleh	generate phrase level error handler for LL(k)
 * generate_lrk_pleh	generate phrase level error handler for LR(k)
 *
 * Debug
 * print_rules		print information regarding rules
 *
 */
int sh_parser_load_tokens_from_lexer(shParser *parser);
int sh_parser_load_grammar(shParser *parser, const char *grammar_file, unsigned int k, sh_grammar_type type);

int sh_parser_add_terminal(shParser *parser, char *name);
int sh_parser_add_nonterminal(shParser *parser, char *name);
int sh_parser_add_action_routine(shParser *parser, char *name);
int sh_parser_add_epr_handler(shParser *parser, char *name);
grammar_symbol sh_parser_add_symbol(shParser *parser, char *name);
int sh_parser_generate_and_add_rule(shParser *parser, const char *str);
int sh_parser_rules_sanity_check(shParser *parser);

int sh_parser_is_left_recursive(shParser *parser);
int sh_parser_find_all_firsts(shParser *parser);
int sh_parser_find_firsts(shParser *parser, find_first_data *d, shNode *result);
int sh_parser_find_all_follows(shParser *parser);
int sh_parser_find_follows(shParser *parser, find_follow_data *d, shNode *result);
int sh_parser_prepare_llk_parser(shParser *parser, const char *table_file, bool load);
int sh_parser_prepare_lrk_parser(shParser *parser, const char *diagram_file, bool load);
void sh_parser_cleanup(shParser *parser);

int sh_parser_parse_sanity_check(shParser *parser);
int sh_parser_parse_llk(shParser *parser);
int sh_parser_parse_lrk(shParser *parser);

const char *sh_parser_symbol_name(shParser *parser, grammar_symbol s);
static inline const char *sh_parser_terminal_name(shParser *parser, int terminal)
{
	return sh_parser_symbol_name(parser, (grammar_symbol){ .type = TERMINAL, .id = terminal});
}
static inline const char *sh_parser_nonterminal_name(shParser *parser, int nonterminal)
{
	return sh_parser_symbol_name(parser, (grammar_symbol){ .type = NONTERMINAL, .id = nonterminal});
}

int sh_parser_generate_llk_pleh(shParser *parser, FILE *src, FILE *hdr);
int sh_parser_generate_lrk_pleh(shParser *parser, FILE *src, FILE *hdr);

void sh_parser_print_rule(shParser *parser, int r);

int sh_parser_init_to_tokens_file(shParser *parser, shLexer *lexer);
int sh_parser_init_to_grammar_file(shParser *parser, shLexer *lexer);

#ifndef NDEBUG
void sh_parser_print_rules(shParser *parser);
#endif

#endif
