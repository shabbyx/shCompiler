/*
 * Copyright (C) 2007-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shCompiler.
 *
 * shCompiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shCompiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shCompiler.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TOKENS_H_BY_CYCLOPS
#define TOKENS_H_BY_CYCLOPS

#include <shds.h>

/*
 * These functions should not be visible by user. They are not static because they are implemented and used in
 * different files, therefore I have prefixed them with something strange so they wouldn't accidentally be used by a user
 */
int sh_compiler_TF_init(shLexer *l, bool rebuild, const char *letters, const char *cachePath);
void sh_compiler_TF_cleanup(shLexer *l);
shVector/* shDfa */ sh_compiler_TF_get_dfas(shLexer *l);
int sh_compiler_TF_error(shLexer *l);

#endif
