/*
 * Copyright (C) 2007-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shCompiler.
 *
 * shCompiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shCompiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shCompiler.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include "utils.h"
#include "nonstd.h"

char *sh_compiler_read_str(const char *buffer, int *chars_read)
{
	char *result;
	size_t str_length = 0;

	/* skip white space in beginning */
	while (isspace(buffer[str_length]))
		++str_length;

	*chars_read = str_length;
	buffer += str_length;
	str_length = 0;
	while (buffer[str_length] != '\0' && !isspace(buffer[str_length]))
		++str_length;
	if (str_length == 0)
		return NULL;

	result = malloc((str_length + 1) * sizeof *result);
	if (result == NULL)
		return NULL;

	memcpy(result, buffer, str_length * sizeof *buffer);
	result[str_length] = '\0';
	*chars_read += str_length;

	return result;
}

int sh_compiler_skip_str(const char *buffer)
{
	size_t str_length = 0;

	/* skip white space in beginning */
	while (isspace(buffer[str_length]))
		++str_length;

	/* skip non white space until whitespace, or end of string */
	while (buffer[str_length] != '\0' && !isspace(buffer[str_length]))
		++str_length;

	return str_length;
}

char *sh_compiler_read_whole_file(const char *name, int *error)
{
	FILE *fin;
	long int length;
	char *fin_fc = NULL;

	fin = fopen(name, "r");
	if (fin == NULL)
	{
		*error = -1;
		return NULL;
	}

	fseek(fin, 0, SEEK_END);
	length = ftell(fin);
	if (length < 0)
	{
		*error = -1;
		goto exit_no_mem;
	}

	fin_fc = malloc((length + 1) * sizeof *fin_fc);
	if (fin_fc == NULL)
	{
		*error = -2;
		goto exit_no_mem;
	}

	fseek(fin, 0, SEEK_SET);
	if (fread(fin_fc, 1, length, fin) != (size_t)length)
	{
		*error = -1;
		goto exit_bad_file;
	}
	fin_fc[length] = '\0';
	*error = 0;

	goto exit_normal;
exit_bad_file:
	CLEAN_FREE(fin_fc);
exit_no_mem:
exit_normal:
	fclose(fin);
	return fin_fc;
}

char *sh_compiler_get_path_and_add(const char *path_to_file, const char **file, const char *fmt, ...)
{
	char *path = NULL;
	size_t path_length = 0;
	size_t extra_length;
	va_list args;

	va_start(args, fmt);
	extra_length = vsnprintf(NULL, 0, fmt, args) + 1;	/* +1 for the NUL */
	va_end(args);

	for (path_length = strlen(path_to_file); path_length > 0; --path_length)
		if (_is_path_separator(path_to_file[path_length - 1]))
			break;

	if (file != NULL)
		*file = path_to_file + path_length;

	if (path_length == 0)
	{
		/* there was no / at all */
		path = malloc((extra_length + 2) * sizeof *path);
		if (path == NULL)
			goto exit_no_mem;
		strcpy(path, "./");
		path_length = 2;
	}
	else
	{
		path = malloc((path_length + extra_length) * sizeof *path);
		if (path == NULL)
			goto exit_no_mem;
		memcpy(path, path_to_file, path_length * sizeof *path);
	}

	va_start(args, fmt);
	vsprintf(path + path_length, fmt, args);
	va_end(args);

	return path;
exit_no_mem:
	return NULL;
}

char *sh_compiler_get_global_path(const char *file)
{
	size_t file_length = strlen(file);
	char *current_path;
	size_t path_length;
	size_t total_length;
	char *global_path;
	char additional = '\0';

	if (_is_absolute(file, file_length))
		return strdup(file);

	current_path = malloc((PATH_MAX + 1) * sizeof *current_path);
	if (current_path == NULL)
		return NULL;

	if (!_get_current_dir(current_path, PATH_MAX))
		/* if couldn't retrieve the path, just ignore the path */
		current_path[0] = '\0';

	path_length = strlen(current_path);
	total_length = path_length + file_length;
	if (path_length > 0 && !_is_path_separator(current_path[path_length - 1]))
	{
		additional = PATH_SEPARATOR;
		++total_length;
	}

	global_path = malloc((total_length + 1) * sizeof *global_path);
	if (global_path == NULL)
	{
		/* If couldn't allocate, at least try the local name */
		global_path = strdup(file);
		goto exit_no_mem;
	}

	strcpy(global_path, current_path);
	if (additional != '\0')
		global_path[path_length++] = additional;
	strcpy(global_path + path_length, file);
exit_no_mem:
	free(current_path);
	return global_path;
}

char *sh_compiler_string(const char *fmt, ...)
{
	size_t len;
	va_list args;
	char *res;

	va_start(args, fmt);
	len = vsnprintf(NULL, 0, fmt, args) + 1;	/* +1 for the NUL */
	va_end(args);

	res = malloc(len * sizeof *res);
	if (res == NULL)
		return NULL;

	va_start(args, fmt);
	len = vsprintf(res, fmt, args);
	va_end(args);

	return res;
}
