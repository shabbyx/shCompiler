/*
 * Copyright (C) 2007-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shCompiler.
 *
 * shCompiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shCompiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shCompiler.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UTILS_H_BY_CYCLOPS
#define UTILS_H_BY_CYCLOPS

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <shnonstd.h>

#define CLEAN_FREE(x) do { free(x); x = NULL; } while (0)

/* read a space-separated string from buffer */
char *sh_compiler_read_str(const char *buffer, int *chars_read);

/* skip a space-separated string, returning number of characters skipped */
int sh_compiler_skip_str(const char *buffer);

/* completely load file in memory */
char *sh_compiler_read_whole_file(const char *name, int *error);

/*
 * get and return path to file (not including file name), adding a suffix
 * Returns file name itself in `*file`.
 */
char *sh_compiler_get_path_and_add(const char *path_to_file, const char **file,
		const char *fmt, ...) SH_COMPILER_PRINTF_STYLE(3, 4);

/* get the global path to a file */
char *sh_compiler_get_global_path(const char *file);

/* to generate a dynamic string */
char *sh_compiler_string(const char *fmt, ...) SH_COMPILER_PRINTF_STYLE(1, 2);

/* This is for when shLexer is not initialized, so its error_out cannot be used */
#define _message(...) do { fprintf(stderr, __VA_ARGS__); fprintf(stderr, "\n"); } while (0)

#endif
