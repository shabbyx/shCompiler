/*
 * Copyright (C) 2007-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shCompiler.
 *
 * shCompiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shCompiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shCompiler.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <errno.h>
#include <shparser.h>
#include "parser_internal.h"
#include "utils.h"

//#define SH_DEBUG
//#define SH_DETAILED_DEBUG

#define INTERNAL ((parser_internal *)parser->internal)
#define LEXER (parser->lexer)

static bool default_pleh(shParser *p, const char *cur, shKtokens *la)
{
	const char *token, *type;
	shLexer *l = p->lexer;
	sh_lexer_error_custom(l, l->file_name, l->line, l->column, "error: unhandled error, expected to match '%s', but found", cur);
	for (unsigned int i = 0; i < la->size; ++i)
	{
		token = sh_ktokens_at(la, &type, i);
		if (token == NULL)
			sh_lexer_error_custom(l, NULL, 0, 0, " $");
		else
			sh_lexer_error_custom(l, NULL, 0, 0, " '%s'", type);
	}
	sh_lexer_error_custom(l, NULL, 0, 0, "\n");
	sh_parser_push(p, cur);
	token = sh_ktokens_at(la, &type, 0);
	sh_parser_push(p, type);
	return token != NULL;
}

static void _intcpy(void *to, void *from)
{
	*(int *)to = *(int *)from;
}

static int _intcmp(const void *a, const void *b)
{
	int first = *(const int *)a;
	int second = *(const int *)b;

	return (first > second) - (first < second);
}

static void _llk_table_data_cpy(void *to, void *from)
{
	*(llk_table_data *)to = *(llk_table_data *)from;
}

static int _llk_table_data_cmp(const void *a, const void *b)
{
	const llk_table_data *first = a;
	const llk_table_data *second = b;

	if (first->nonterminal == second->nonterminal)
		return sh_vector_compare(&first->look_ahead, &second->look_ahead);
	return (first->nonterminal > second->nonterminal) - (first->nonterminal < second->nonterminal);
}

static void _llk_table_result_cpy(void *to, void *from)
{
	*(llk_table_result *)to = *(llk_table_result *)from;
}

#ifdef SH_DEBUG
static bool _print_llk_table_entry(shNode *n, void *extra)
{
	shParser *parser = extra;
	llk_table_data *d = (llk_table_data *)n->key;
	llk_table_result *r = (llk_table_result *)n->data;
	int *look_aheads = sh_vector_data(&d->look_ahead);

	printf("%s with", sh_parser_nonterminal_name(parser, d->nonterminal));
	for (size_t i = 0; i < d->look_ahead.size; ++i)
	{
		int t = look_aheads[i];
		printf(" %s", t == -1?"$":sh_parser_terminal_name(parser, t));
	}
	printf(" implies use of rule %d\n", r->rule);
	return true;
}

static void _print_llk_table(shParser *parser)
{
	printf("LL(k) table:\n");
	sh_map_inorder(&INTERNAL->llk_table, _print_llk_table_entry, parser);
}
#endif

static bool _free_llk_table(shNode *n, void *extra)
{
	sh_vector_free(&((llk_table_data *)n->key)->look_ahead);
	return true;
}

static int _add_to_llk_table(shParser *parser, size_t rule_id, int nont, const char *nont_symbol, shVector *la)
{
	int ret = 0;

	llk_table_data table_entry = {
		.nonterminal = nont,
		.look_ahead = *la,
	};
	llk_table_result *old;
	shNode n;

	int insertret = sh_map_insert(&INTERNAL->llk_table, &table_entry, &(llk_table_result){ .rule = rule_id }, &n);
	if (insertret < 0)
		return -1;
	bool exists = insertret > 0;

	old = n.data;
	if (exists && old->rule != (int)rule_id)
	{
		int *la_tokens = sh_vector_data(la);
		sh_lexer_error_custom(LEXER, INTERNAL->grammar_file_name, 0, 0, "warning: ambiguity in grammar: ");
		sh_lexer_error_custom(LEXER, NULL, 0, 0, "%s with", nont_symbol);
		for (size_t i = 0; i < la->size; ++i)
		{
			int t = la_tokens[i];
			sh_lexer_error_custom(LEXER, NULL, 0, 0, " %s", t == -1?"$":sh_parser_terminal_name(parser, t));
		}
		sh_lexer_error_custom(LEXER, NULL, 0, 0, " implies use of rule %d as well as %zu\n", old->rule, rule_id);
		sh_parser_print_rule(parser, old->rule);
		sh_parser_print_rule(parser, rule_id);
		if ((INTERNAL->ambiguity_resolution & SH_PARSER_ACCEPT_FIRST))
			sh_lexer_error_custom(LEXER, INTERNAL->grammar_file_name, 0, 0, "note: ambiguity resolved by prioritizing the former action\n");
		else if ((INTERNAL->ambiguity_resolution & SH_PARSER_ACCEPT_LAST))
		{
			sh_lexer_error_custom(LEXER, INTERNAL->grammar_file_name, 0, 0, "note: ambiguity resolved by prioritizing the later action\n");
			old->rule = rule_id;
		}
		else
			ret = 1;
	}

	if (exists)
		sh_vector_free(la);

	return ret;
}

static void _save_table(shParser *parser, const char *file)
{
	FILE *tout = fopen(file, "w");
	if (tout == NULL)
		return;

	const char **terminals = sh_vector_data(&INTERNAL->terminals);

	shNode e;
	for (int res = sh_map_get_first(&INTERNAL->llk_table, &e); res == 0; res = sh_map_get_next(&e, &e))
	{
		size_t size = 0;
		llk_table_data *td = e.key;
		llk_table_result *tr = e.data;
		int *look_aheads = sh_vector_data(&td->look_ahead);

		for (; size < td->look_ahead.size; ++size)
			if (look_aheads[size] == -1)
				break;
		fprintf(tout, "%s %d %zu", *(const char **)sh_vector_at(&INTERNAL->nonterminals, td->nonterminal), tr->rule, size);

		/* if it is $, do not write it, it will be obvious by the size of lookahead */
		for (size_t i = 0; i < size; ++i)
			fprintf(tout, " %s", terminals[look_aheads[i]]);
		fprintf(tout, "\n");
	}

	fclose(tout);
}

static int _calculate_llk_table(shParser *parser, const char *table_file)
{
	int error;

#ifdef SH_DEBUG
	printf("Rebuilding LLK table from grammar\n");
#endif

	error = sh_parser_is_left_recursive(parser);
	if (error)
	{
		if (error > 0)
		{
			sh_lexer_error_custom(LEXER, INTERNAL->grammar_file_name, 0, 0, "error: grammar is left recursive (nonterminal: %s)\n",
					sh_parser_nonterminal_name(parser, error - 1));
			return SH_PARSER_LEFT_RECURSIVE;
		}
		goto exit_no_mem;
	}

	if (sh_parser_find_all_firsts(parser))
		goto exit_no_mem;

	if (sh_parser_find_all_follows(parser))
		goto exit_no_mem;

	bool ambiguous = false;
	grammar_rule *rules = sh_vector_data(&INTERNAL->rules);
	for (size_t rule_id = 0; rule_id < INTERNAL->rules.size; ++rule_id)
	{
		grammar_rule *rule = &rules[rule_id];
		const char *head_symbol = sh_parser_symbol_name(parser, rule->head);
		/*
		 * In LL(k), the look-ahead is of length k.  This means given a nonterminal, the look-ahead is
		 * either length k firsts of the nonterminal's rules' bodies, or length (k-i) nonpartial firsts of
		 * the rules' bodies concatenated with length i follows of the nonterminal.
		 */
		for (unsigned int k = 0; k < INTERNAL->k_in_grammar; ++k)
		{
			find_follow_data second_part_data = {
				.nonterminal = rule->head.id,
				.s = INTERNAL->k_in_grammar - k,
			};
			shNode sp;
			shSet/* nonterminal_follow */ *second_part;

			if (sh_parser_find_follows(parser, &second_part_data, &sp) < 0)
				goto exit_no_mem;
			second_part = &((find_follow_result *)sp.data)->follows;

			if (rule->body.size > 0)
			{
				shNode fp;
				shSet/* sentence_first */ *first_part;
				find_first_data first_part_data = {
					.sentence = sh_vector_data(&rule->body),
					.k = k,
					.sentence_len = rule->body.size
				};
				sh_parser_find_firsts(parser, &first_part_data, &fp);
				first_part = &((find_first_result *)fp.data)->firsts;

				/* concatenate nonpartial firsts and follows and add results to LL(k) table */
				for (int res = sh_set_get_first(first_part, &fp); res == 0; res = sh_set_get_next(&fp, &fp))
				{
					sentence_first *sf = fp.key;
					if (sf->partial)
						continue;

					for (res = sh_set_get_first(second_part, &sp); res == 0; res = sh_set_get_next(&sp, &sp))
					{
						shVector look_ahead;
						nonterminal_follow *nf = sp.key;

						sh_vector_duplicate(&sf->first, &look_ahead);
						sh_vector_concat(&look_ahead, &nf->follow);

						res = _add_to_llk_table(parser, rule_id, rule->head.id, head_symbol, &look_ahead);
						if (res < 0)
						{
							sh_vector_free(&look_ahead);
							goto exit_no_mem;
						}
						if (res > 0)
							ambiguous = true;
					}
				}
			}
			else if (k == 0)
			{
				/*
				 * if sentence length is 0 and k > 0, no firsts are possible.  If k == 0, only follow needs to
				 * be taken into account, as there is only one first and it has zero length
				 */
				for (int res = sh_set_get_first(second_part, &sp); res == 0; res = sh_set_get_next(&sp, &sp))
				{
					shVector look_ahead;

					sh_vector_duplicate(&((nonterminal_follow *)sp.key)->follow, &look_ahead);

					res = _add_to_llk_table(parser, rule_id, rule->head.id, head_symbol, &look_ahead);
					if (res < 0)
					{
						sh_vector_free(&look_ahead);
						goto exit_no_mem;
					}
					if (res > 0)
						ambiguous = true;
				}
			}
		}

		/*
		 * for i == k, only firsts matter.  It is clear that zero sized body cannot produce k > 0 sized firsts.
		 * In this case, partial firsts are also acceptable.
		 */
		if (rule->body.size > 0)
		{
			shNode wp;
			shSet/* sentence_first */ *whole_part;
			find_first_data whole_part_data = {
				.sentence = sh_vector_data(&rule->body),
				.k = INTERNAL->k_in_grammar,
				.sentence_len = rule->body.size
			};
			sh_parser_find_firsts(parser, &whole_part_data, &wp);
			whole_part = &((find_first_result *)wp.data)->firsts;

			for (int res = sh_set_get_first(whole_part, &wp); res == 0; res = sh_set_get_next(&wp, &wp))
			{
				shVector look_ahead;

				sh_vector_duplicate(&((sentence_first *)wp.key)->first, &look_ahead);

				res = _add_to_llk_table(parser, rule_id, rule->head.id, head_symbol, &look_ahead);
				if (res < 0)
					goto exit_no_mem;
				if (res > 0)
					ambiguous = true;
			}
		}
	}
	sh_parser_cleanup(parser);
	if (ambiguous)
		return SH_PARSER_AMBIGUOUS;
#ifdef SH_DEBUG
	_print_llk_table(parser);
#endif
	_save_table(parser, table_file);
	return SH_PARSER_SUCCESS;
exit_no_mem:
	sh_lexer_error_custom(LEXER, INTERNAL->grammar_file_name, 0, 0, "error: out of memory\n");
	sh_parser_cleanup(parser);
	return SH_PARSER_NO_MEM;
}

static int _load_llk_table(shParser *parser, const char *table)
{
	llk_table_data tdata = {0};
	llk_table_result tresult;
	char *nonterminal = NULL;
	char *terminal = NULL;
	int chars_read = 0;
	int temp;
	unsigned int la_size, i;
	int dollar = -1;

	while ((nonterminal = sh_compiler_read_str(table + chars_read, &temp)) != NULL)
	{
		char *end;

		int *nont = sh_map_at(&INTERNAL->nonterminal_codes, &nonterminal);
		if (nont == NULL)
			goto exit_unknown_nonterminal;
		tdata.nonterminal = *nont;

		chars_read += temp;
		errno = 0;
		tresult.rule = strtol(table + chars_read, &end, 10);
		la_size = strtoul(end, &end, 10);
		temp = end - (table + chars_read);
		if (errno || temp == 0)
			goto exit_bad_format;
		chars_read += temp;

		sh_vector_init(&tdata.look_ahead, _intcpy, sizeof(int), _intcmp);
		sh_vector_reserve(&tdata.look_ahead, INTERNAL->k_in_grammar);
		for (i = 0; i < la_size; ++i)
		{
			terminal = sh_compiler_read_str(table + chars_read, &temp);
			chars_read += temp;

			if (terminal == NULL)
				goto exit_incomplete;

			/* Note: when the table also stores numbers, this step is unnecessary */
			int *t = sh_map_at(&INTERNAL->terminal_codes, &terminal);
			if (t == NULL)
				goto exit_unknown_terminal;
			sh_vector_append(&tdata.look_ahead, t);
			free(terminal);
			terminal = NULL;
		}
		for (; i < INTERNAL->k_in_grammar; ++i)
			sh_vector_append(&tdata.look_ahead, &dollar);
		int ret = sh_map_insert(&INTERNAL->llk_table, &tdata, &tresult);
		if (ret > 0)
		{
			sh_vector_free(&tdata.look_ahead);
			sh_lexer_error_custom(LEXER, INTERNAL->grammar_file_name, 0, 0, "warning: duplicate table entry at byte %d\n", chars_read);
		}
		else if (ret < 0)
			goto exit_no_mem;
		free(nonterminal);
	}

	return SH_PARSER_SUCCESS;
exit_bad_format:
	if (table[chars_read] == '\0')
		goto exit_incomplete;
	sh_lexer_error_custom(LEXER, INTERNAL->grammar_file_name, 0, 0, "error: bad table format at byte %d\n", chars_read);
	goto exit_fail;
exit_incomplete:
	sh_vector_free(&tdata.look_ahead);
	sh_lexer_error_custom(LEXER, INTERNAL->grammar_file_name, 0, 0, "error: incomplete table at byte %d\n", chars_read);
	goto exit_fail;
exit_unknown_nonterminal:
	sh_vector_free(&tdata.look_ahead);
	sh_lexer_error_custom(LEXER, INTERNAL->grammar_file_name, 0, 0, "error: unknown nonterminal '%s' in table at byte %d\n", nonterminal, chars_read);
	goto exit_fail;
exit_unknown_terminal:
	sh_lexer_error_custom(LEXER, INTERNAL->grammar_file_name, 0, 0, "error: unknown terminal '%s' in table at byte %d\n", terminal, chars_read);
	goto exit_fail;
exit_no_mem:
	sh_lexer_error_custom(LEXER, INTERNAL->grammar_file_name, 0, 0, "error: out of memory at byte %d\n", chars_read);
	goto exit_fail;
exit_fail:
	free(terminal);
	free(nonterminal);
	return SH_PARSER_FAIL;
}

int sh_parser_prepare_llk_parser(shParser *parser, const char *table_file, bool load)
{
	char *table = NULL;
	int error = SH_PARSER_FAIL;
	const char *grammar_file = INTERNAL->grammar_file_name;

	INTERNAL->llk_table_built = false;
	INTERNAL->llk_pleh = default_pleh;

	sh_map_init(&INTERNAL->llk_table, _llk_table_data_cpy, sizeof(llk_table_data),
			_llk_table_result_cpy, sizeof(llk_table_result), _llk_table_data_cmp);
	if (load)
	{
		table = sh_compiler_read_whole_file(table_file, &error);
		INTERNAL->grammar_file_name = table_file;
		if (table != NULL)
			error = _load_llk_table(parser, table);
	}
	if (error)
	{
		sh_map_for_each(&INTERNAL->llk_table, _free_llk_table);
		sh_map_clear(&INTERNAL->llk_table);

		/* then either forced to recalculate, or load failed */
		INTERNAL->grammar_file_name = grammar_file;
		error = _calculate_llk_table(parser, table_file);
	}

	free(table);
	INTERNAL->llk_table_built = !error;

	return error;
}

int sh_parser_load_llk(shParser *parser, shLexer *lexer, const char **rules, unsigned int rules_count,
		const char *table, unsigned int k, const char *table_file)
{
	int error;

	INTERNAL->grammar_type = SH_PARSER_LLK;
	INTERNAL->grammar_file_name = "<function argument>";

	sh_parser_bind(parser, lexer);
	error = sh_parser_load_tokens_from_lexer(parser);
	if (error)
		return error;

	INTERNAL->k_in_grammar = k;
	for (unsigned int i = 0; i < rules_count; ++i)
	{
		error = sh_parser_generate_and_add_rule(parser, rules[i]);
		if (error)
			return error;
	}
	error = sh_parser_rules_sanity_check(parser);
	if (error)
		return error;

#ifdef SH_DEBUG
	sh_parser_print_rules(parser);
#endif

	sh_map_init(&INTERNAL->llk_table, _llk_table_data_cpy, sizeof(llk_table_data),
			_llk_table_result_cpy, sizeof(llk_table_result), _llk_table_data_cmp);
	if (table)
		error = _load_llk_table(parser, table);
	else
		/* this is for when the format of table file changes and they want to rewrite the table file */
		error = _calculate_llk_table(parser, table_file);
	if (error)
		return error;
	INTERNAL->llk_table_built = true;

#ifdef SH_DEBUG
	_print_llk_table(parser);
#endif

	return SH_PARSER_SUCCESS;
}

static shVector _k_elements(shParser *parser, shKtokens *la)
{
	shVector res;
	int dollar = -1;

	sh_vector_init(&res, _intcpy, sizeof(int), _intcmp);
	shMap *terminal_codes = &INTERNAL->terminal_codes;

	for (unsigned int i = 0; i < la->k; ++i)
	{
		const char *type;
		sh_ktokens_at(la, &type, i);
		if (type)
		{
			int *t = sh_map_at(terminal_codes, &type);
			if (t == NULL)
				sh_lexer_error(LEXER, "internal error: unknown token type %s", type);
			sh_vector_append(&res, t);
		}
		else
			sh_vector_append(&res, &dollar);
	}

	return res;
}

static void _llk_stack_element_cpy(void *to, void *from)
{
	*(llk_stack_element *)to = *(llk_stack_element *)from;
}

int sh_parser_parse_llk(shParser *parser)
{
	int error;
	shKtokens look_ahead;
	shLexer *lexer = parser->lexer;
	shMap *llk_table = &INTERNAL->llk_table;
	grammar_rule *rules = sh_vector_data(&INTERNAL->rules);
	int *rule_action_routines = sh_vector_data(&INTERNAL->rule_action_routines);
	sh_parser_ar *action_routine_functions = sh_vector_data(&INTERNAL->action_routine_functions);
	int *rule_epr_handler = sh_vector_data(&INTERNAL->rule_epr_handler);
	sh_parser_eprh *epr_handler_functions = sh_vector_data(&INTERNAL->epr_handler_functions);
	shStack *parse_stack = &INTERNAL->parse_stack;
	unsigned int k_in_grammar = INTERNAL->k_in_grammar;

	if (!INTERNAL->llk_table_built)
		return SH_PARSER_NO_RULES;

	error = sh_parser_parse_sanity_check(parser);
	if (error)
		return error;

	if (sh_ktokens_init(&look_ahead, k_in_grammar))
		return SH_PARSER_NO_MEM;

	sh_stack_init(parse_stack, _llk_stack_element_cpy, sizeof(llk_stack_element));

	const char *type, *token;
	llk_stack_element e;
	for (unsigned int i = 0; i < k_in_grammar; ++i)
	{
		e.symbol.type = TERMINAL;
		e.symbol.id = -1;
		sh_stack_push(parse_stack, &e);
		token = sh_lexer_peek(lexer, &type, i, NULL, NULL);
#ifdef SH_DEBUG
		printf("read %s from input which is of type: %s\n", token?token:"<null>", type?type:"<null>");
#endif
		sh_ktokens_push(&look_ahead, token, type);
	}
	e.symbol.type = NONTERMINAL;
	e.symbol.id = 0;
	sh_stack_push(parse_stack, &e);
	while (parse_stack->size && !INTERNAL->terminate_early)
	{
		grammar_symbol current = ((llk_stack_element *)sh_stack_top(parse_stack))->symbol;
#ifdef SH_DEBUG
		printf("Reading %s from stack\n", sh_parser_symbol_name(parser, current));
#endif
		token = sh_ktokens_at(&look_ahead, &type, 0);
		if (current.id == -1 && token != NULL)
		{
			sh_lexer_error(lexer, "warning: extra characters in file after termination of parse");
			break;
		}
		sh_stack_pop(parse_stack);
		if (current.type == ACTION_ROUTINE)
		{
			lexer->repeek = false;
			action_routine_functions[current.id](parser);
			if (lexer->repeek)
			{
				/* the action routine has issued sh_lexer_ignore and thus the look-aheads must be retaken */
				sh_ktokens_clear(&look_ahead);
				for (unsigned int i = 0; i < k_in_grammar; ++i)
				{
					token = sh_lexer_peek(lexer, &type, i, NULL, NULL);
#ifdef SH_DEBUG
					printf("read %s from input which is of type: %s\n", token?token:"<null>", type?type:"<null>");
#endif
					sh_ktokens_push(&look_ahead, token, type);
				}
			}
		}
		else if (current.type == TERMINAL)
		{
			/* a terminal must be matched */
			const char *current_name = sh_parser_symbol_name(parser, current);
			if (type?strcmp(type, current_name) != 0:current.id != -1)
			{
				if (!(*INTERNAL->llk_pleh)(parser, current_name, &look_ahead))
				{
					sh_ktokens_free(&look_ahead);
					sh_stack_free(parse_stack);
					return SH_PARSER_PARSE_ERROR;
				}
				else
					continue;
			}

			/* this is the token just matched */
			sh_lexer_next(lexer, &type);
			token = sh_lexer_peek(lexer, &type, k_in_grammar - 1, NULL, NULL);
#ifdef SH_DEBUG
			printf("read %s from input which is of type: %s\n", token?token:"<null>", type?type:"<null>");
#endif
			sh_ktokens_push(&look_ahead, token, type);
		}
		else if (current.type == NONTERMINAL)
		{
			llk_table_data lookup = {
				.nonterminal = current.id,
				.look_ahead = _k_elements(parser, &look_ahead),
			};
			llk_table_result *entry = sh_map_at(llk_table, &lookup);
			if (entry != NULL)
			{
				int ruleToUse = entry->rule;
				sh_vector_free(&lookup.look_ahead);
				if (ruleToUse >= (int)INTERNAL->rules.size)
				{
					sh_lexer_error(lexer, "internal error: bug in LLK table");
					sh_lexer_error(lexer, "could not recover from last error");
					sh_ktokens_free(&look_ahead);
					sh_stack_free(parse_stack);
					return SH_PARSER_INTERNAL_ERROR;
				}
				grammar_rule *rule = &rules[ruleToUse];
				int eprh = rule_epr_handler[ruleToUse];
				if (eprh != -1)
					epr_handler_functions[eprh](parser);
				int rule_ar = rule_action_routines[ruleToUse];
				/* if the rule has action routine, put it first */
				if (rule_ar >= 0)
				{
					grammar_symbol s;
					s.type = ACTION_ROUTINE;
					s.id = rule_ar;
					e.symbol = s;
					sh_stack_push(parse_stack, &e);
				}
				grammar_symbol *rule_body = sh_vector_data(&rule->body);
				for (size_t i = rule->body.size; i > 0; --i)
				{
					e.symbol = rule_body[i - 1];
					sh_stack_push(parse_stack, &e);
				}
			}
			else
			{
				sh_vector_free(&lookup.look_ahead);
				if (!(*INTERNAL->llk_pleh)(parser, sh_parser_symbol_name(parser, current), &look_ahead))
				{
					sh_ktokens_free(&look_ahead);
					sh_stack_free(parse_stack);
					return SH_PARSER_PARSE_ERROR;
				}
				else
					continue;
			}
		}
		else
		{
			if (current.id != -1)
			{
				sh_lexer_error(lexer, "internal error: found '%s' on parse stack which is invalid", sh_parser_symbol_name(parser, current));
				sh_lexer_error(lexer, "could not recover from last error");
				sh_ktokens_free(&look_ahead);
				sh_stack_free(parse_stack);
				return SH_PARSER_INTERNAL_ERROR;
			}
		}
	}
	sh_ktokens_free(&look_ahead);
	sh_stack_free(parse_stack);

	return INTERNAL->terminate_early?INTERNAL->terminate_return_value:SH_PARSER_SUCCESS;
}

sh_parser_llk_pleh sh_parser_set_llk_pleh(shParser *parser, sh_parser_llk_pleh h)
{
	sh_parser_llk_pleh prev;
	prev = INTERNAL->llk_pleh;
	INTERNAL->llk_pleh = h;
	return prev;
}

static void _print_as_c_string(FILE *src, const char *str)
{
	fprintf(src, "\"");
	for (const char *p = str; *p; ++p)
	{
		assert(isgraph(*p));		/* the tokens and grammar reader should only read printable names */
		fprintf(src, "%s%c", *p == '\\' || *p == '"'?"\\":"", *p);
	}
	fprintf(src, "\"");
}

static void _print_indent(FILE *src, int k)
{
	fprintf(src, "\t\t\t");
	for (int i = 0; i < k; ++i)
		fprintf(src, "\t");
}

int sh_parser_generate_llk_pleh(shParser *parser, FILE *src, FILE *hdr)
{
	const char **terminals = sh_vector_data(&INTERNAL->terminals);
	char **nonterminals = sh_vector_data(&INTERNAL->nonterminals);
	char **action_routines = sh_vector_data(&INTERNAL->action_routines);
	grammar_rule *rules = sh_vector_data(&INTERNAL->rules);
	int *rule_action_routines = sh_vector_data(&INTERNAL->rule_action_routines);
	shVector *nonterminal_rules = sh_vector_data(&INTERNAL->nonterminal_rules);
	unsigned int k_in_grammar = INTERNAL->k_in_grammar;

	fprintf(hdr,	"bool llkpleh(shParser *parser, const char *cur, shKtokens *la);\n");
	fprintf(src,	"static void _char_cpy(void *to, void *from)\n"
			"{\n"
			"\t*(char *)to = *(char *)from;\n"
			"}\n"
			"\n");
	fprintf(src,	"bool llkpleh(shParser *parser,       /* parser */\n"
			"             const char *cur,        /* the token to be matched (could be terminal or non-terminal) */\n"
			"             shKtokens *la)          /* lookahead (containing tokens and their types) */\n"
			"{\n"
			"\t/*\n"
			"\t * Note: this function needs to notify the semantics analyzer that there is an error.\n"
			"\t * Therefore, before returning from the function, replace STOP_EVALUATION with your own method.\n"
			"\t */\n"
			"\tbool is_warning = false;\t\t\t/* if only a warning is generated, set this to true */\n"
			"\tbool recovered = true;\n"
			"\tshLexer *l = parser->lexer;\n"
			"\tchar *la_str = NULL;\n");
	for (unsigned i = 0, size = INTERNAL->terminals.size; i < size; ++i)
	{
		fprintf(src,	"\t%sif (strcmp(cur, ", i?"else ":"");
		_print_as_c_string(src, terminals[i]);
		fprintf(src,	") == 0)\n"
				"\t{\n"
				"\t\tsh_lexer_insert_fake(l, cur, NULL);\n"
				"\t\tsh_lexer_error(l, \"error: missing '%%s', inserted '%%s'\", cur, l->current_token);\n"
				"\t}\n");
	}
	fprintf(src,	"\telse\n"
			"\t{\n"
			"\t\t/*\n"
			"\t\t * Note: usually in the phrase level error handler, you work with token types,\n"
			"\t\t * therefore, the lookahead token types are extracted for you below where\n"
			"\t\t * tokens[N] refers to the type of the Nth lookahead.\n"
			"\t\t */\n"
			"\t\tconst char *tokens[%u];\n", k_in_grammar);
	for (unsigned int i = 0; i < k_in_grammar; ++i)
		fprintf(src, "\t\tsh_ktokens_at(la, &tokens[%u], %u);\n", i, i);

	fprintf(src,	"\t\tshVector la_str_helper;\n"
			"\t\tchar nul = '\\0';\n"
			"\t\tbool no_mem = false;\n"
			"\t\tsh_vector_init(&la_str_helper, _char_cpy, sizeof(char));\n"
			"\t\tfor (unsigned int i = 0; i < %u; ++i)\n"
			"\t\t{\n"
			"\t\t\tchar c = ' ';\n"
			"\t\t\tif (i)\n"
			"\t\t\t\tif (sh_vector_append(&la_str_helper, &c))\n"
			"\t\t\t\t\tno_mem = true;\n"
			"\t\t\tc = '\\'';\n"
			"\t\t\tif (sh_vector_append(&la_str_helper, &c)\n"
			"\t\t\t\t|| sh_vector_concat_array(&la_str_helper, tokens[i] == NULL?\"$\":tokens[i], tokens[i] == NULL?1:strlen(tokens[i]))\n"
			"\t\t\t\t|| sh_vector_append(&la_str_helper, &c))\n"
			"\t\t\t\tno_mem = true;\n"
			"\t\t}\n"
			"\t\tif (sh_vector_append(&la_str_helper, &nul))\n"
			"\t\t\tno_mem = true;\n"
			"\t\tla_str = sh_vector_uninline(&la_str_helper);\n"
			"\t\tif (no_mem || la_str == NULL)\n"
			"\t\t\tgoto exit_no_mem;\n",
			k_in_grammar);

	int first_terminal = INTERNAL->terminals.size?0:-1;
	bool thereWasNone = true;
	for (unsigned int i = 0, size = INTERNAL->nonterminals.size; i < size; ++i)
	{
		llk_table_data possible_lookup;
		sh_vector_init(&possible_lookup.look_ahead, _intcpy, sizeof(int), _intcmp);
		possible_lookup.nonterminal = i;
		unsigned int *look_ahead_indices = malloc(k_in_grammar * sizeof *look_ahead_indices);
		if (look_ahead_indices == NULL)
			goto exit_no_mem;
		for (unsigned int j = 0; j < k_in_grammar; ++j)
		{
			sh_vector_append(&possible_lookup.look_ahead, &first_terminal);
			look_ahead_indices[j] = 0;
		}
		bool first = true;
		int lastOpen = -1;
		while (*(int *)sh_vector_at(&possible_lookup.look_ahead, 0) != -1)
		{
			if (!sh_map_exists(&INTERNAL->llk_table, &possible_lookup))
			{
#define OPEN_IFS \
	do {\
		first = false;\
		thereWasNone = false;\
		fprintf(src,	"\t\t%sif (strcmp(cur, ", i?"else ":"");\
		_print_as_c_string(src, nonterminals[possible_lookup.nonterminal]);\
		fprintf(src,	") == 0)\n"\
				"\t\t{\n");\
		int k = 0;\
		shNode n;\
		for (int res = sh_vector_get_first(&possible_lookup.look_ahead, &n); res == 0; res = sh_vector_get_next(&n, &n), ++k) \
		{\
			_print_indent(src, k);	fprintf(src, "if (strcmp(tokens[%d], ", k);\
						_print_as_c_string(src, *(int *)n.data == -1?"":terminals[*(int *)n.data]);\
						fprintf(src, ") == 0)\n");\
			_print_indent(src, k);	fprintf(src, "{\n");\
		}\
	} while (0)
#define CLOSE_IFS \
	do {\
		shNode n;\
		int res;\
		res = sh_vector_get_first(&possible_lookup.look_ahead, &n); \
		for (int k = k_in_grammar - 1; k > lastOpen + 1; --k)\
		{\
			_print_indent(src, k); fprintf(src, "}\n");\
			_print_indent(src, k); fprintf(src, "else\n");\
			_print_indent(src, k); fprintf(src, "\tgoto exit_unhandled;\n");\
		}\
		_print_indent(src, lastOpen + 1);\
		fprintf(src, "}\n");\
		int k = 0;\
		for (; res == 0 && k <= lastOpen; res = sh_vector_get_next(&n, &n), ++k);\
		for (; res == 0; res = sh_vector_get_next(&n, &n), ++k)\
		{\
			_print_indent(src, k);\
			if (k == lastOpen + 1)\
				fprintf(src,	"else ");\
			fprintf(src,	"if (strcmp(tokens[%d], ", k);\
			_print_as_c_string(src, *(int *)n.data == -1?"":terminals[*(int *)n.data]);\
			fprintf(src,	") == 0)\n");\
			_print_indent(src, k);\
			fprintf(src,	"{\n");\
		}\
	} while (0)
				if (first)
					OPEN_IFS;
				else
					CLOSE_IFS;
				lastOpen = k_in_grammar - 1;
				_print_indent(src, k_in_grammar); fprintf(src, "sh_lexer_error(l, \"error: expected to match '%%s', but found %%s\", cur, la_str);\n");
				_print_indent(src, k_in_grammar); fprintf(src, "/*\n");
				_print_indent(src, k_in_grammar); fprintf(src, " * TODO: Your choice of actions are:\n");
				_print_indent(src, k_in_grammar); fprintf(src, " * 1. stop parsing */\n");
				_print_indent(src, k_in_grammar); fprintf(src, "      sh_lexer_error(l, \"could not recover from last error\");    // Note: The default is to fail, so if you handle\n");
				_print_indent(src, k_in_grammar); fprintf(src, "      recovered = false;                                         // the error properly, remove these two lines\n");
				_print_indent(src, k_in_grammar); fprintf(src, "/* or handle the case by a combination of some of these actions:\n");
				_print_indent(src, k_in_grammar); fprintf(src, " * 2. skip the nonterminal (simply keep recovered true), and possibly fake some input\n");
				_print_indent(src, k_in_grammar); fprintf(src, " *    sh_lexer_insert_fake(l, \"some_type\", NULL);                // add a \"value\" parameter if you want to generate the token yourself\n");
				_print_indent(src, k_in_grammar); fprintf(src, " * 3. change the lookahead tokens (for example 1 token). Useful for example to rename an identifier which was given a reserved name\n");
				_print_indent(src, k_in_grammar); fprintf(src, " *    sh_lexer_insert_fake(l, \"some_type\", NULL);\n");
				_print_indent(src, k_in_grammar); fprintf(src, " *    sh_parser_push(parser, cur);\n");
				_print_indent(src, k_in_grammar); fprintf(src, " *    sh_ktokens_replace(la, l->current_token, l->current_type, 0);\n");
				_print_indent(src, k_in_grammar); fprintf(src, " * 4. skip some of the tokens (for example 3 tokens) and retry nonterminal\n");
				_print_indent(src, k_in_grammar); fprintf(src, " *    sh_parser_push(parser, cur);\n");
				_print_indent(src, k_in_grammar); fprintf(src, " *    sh_parser_push(parser, tokens[2]);\n");
				_print_indent(src, k_in_grammar); fprintf(src, " *    sh_parser_push(parser, tokens[1]);\n");
				_print_indent(src, k_in_grammar); fprintf(src, " *    sh_parser_push(parser, tokens[0]);\n");
				_print_indent(src, k_in_grammar); fprintf(src, " * 5. expand nonterminal and remove a few of its first elements (possibly fake input, see second choice). Rules associated with this nonterminal are:\n");
				int *nont_rules = sh_vector_data(&nonterminal_rules[i]);
				for (size_t r = 0; r < nonterminal_rules[i].size; ++r)
				{
					grammar_rule *rule = &rules[nont_rules[r]];
					grammar_symbol *body = sh_vector_data(&rule->body);
					_print_indent(src, k_in_grammar); fprintf(src, " *  %s ->", sh_parser_symbol_name(parser, rule->head));
					for (size_t j = 0; j < rule->body.size; ++j)
						fprintf(src, " %s", sh_parser_symbol_name(parser, body[j]));
					if (rule_action_routines[nont_rules[r]] >= 0)
						fprintf(src, " %s", action_routines[rule_action_routines[nont_rules[r]]]);
					fprintf(src, "\n");
					if (rule_action_routines[nont_rules[r]] >= 0)
					{
						_print_indent(src, k_in_grammar);
						fprintf(src, " *    sh_parser_push(parser, \"%s\");\n",
							action_routines[rule_action_routines[nont_rules[r]]]);
					}
					for (size_t j = rule->body.size; j > 0; --j)
					{
						_print_indent(src, k_in_grammar);
						fprintf(src, " *    sh_parser_push(parser, \"%s\");\n",
							sh_parser_symbol_name(parser, body[j - 1]));
					}
				}
				_print_indent(src, k_in_grammar); fprintf(src, " */\n");
			}
			int k = k_in_grammar - 1;
			shNode n;
			for (int res = sh_vector_get_last(&possible_lookup.look_ahead, &n); res == 0; res = sh_vector_get_prev(&n, &n), --k)
			{
				++look_ahead_indices[k];
				if (look_ahead_indices[k] < INTERNAL->terminals.size)
				{
					*(int *)n.data = look_ahead_indices[k];
					if (lastOpen > k - 1)
						lastOpen = k - 1;
					shNode nn;
					for (res = sh_vector_get_next(&n, &nn), ++k; res == 0; res = sh_vector_get_next(&nn, &nn), ++k)
					{
						*(int *)nn.data = 0;
						look_ahead_indices[k] = 0;
					}
					break;
				}
				else if (look_ahead_indices[k] == INTERNAL->terminals.size)
				{
					*(int *)n.data = -1;		/* note: -1 is $ */
					if (lastOpen > k - 1)
						lastOpen = k - 1;
					shNode nn;
					for (res = sh_vector_get_next(&n, &nn), ++k; res == 0; res = sh_vector_get_next(&nn, &nn), ++k)
					{
						*(int *)nn.data = -1;	/* after $, all the rest can only be $ */
						look_ahead_indices[k] = INTERNAL->terminals.size;
					}
					break;
				}
				/* else, overflowed. Go to the next digit and increment it */
			}
		}

		if (!sh_map_exists(&INTERNAL->llk_table, &possible_lookup))
		{
			/* now possible_lookup contains $s, so this is the case of unexpected end of file */
			if (first)
				OPEN_IFS;
			else
				CLOSE_IFS;
			lastOpen = k_in_grammar - 1;
			_print_indent(src, k_in_grammar); fprintf(src, "sh_lexer_error(l, \"error: expected to match '%%s', but encountered end of file\", cur);\n");
			_print_indent(src, k_in_grammar); fprintf(src, "/*\n");
			_print_indent(src, k_in_grammar); fprintf(src, " * TODO: Your choice of actions are:\n");
			_print_indent(src, k_in_grammar); fprintf(src, " * 1. stop parsing */\n");
			_print_indent(src, k_in_grammar); fprintf(src, "      sh_lexer_error(l, \"could not recover from last error\");    // Note: The default is to fail, so if you handle\n");
			_print_indent(src, k_in_grammar); fprintf(src, "      recovered = false;                                         // the error properly, remove these two lines\n");
			_print_indent(src, k_in_grammar); fprintf(src, "/* or handle the case by a combination of some of these actions:\n");
			_print_indent(src, k_in_grammar); fprintf(src, " * 2. skip the nonterminal (simply keep recovered true), and possibly fake some input\n");
			_print_indent(src, k_in_grammar); fprintf(src, " *    sh_lexer_insert_fake(l, \"some_type\", NULL);                // add a \"value\" parameter if you want to generate the token yourself\n");
			_print_indent(src, k_in_grammar); fprintf(src, " */\n");
		}
		if (!first)
		{
			/* this means there was no gaps in the LL(k) table at all */
			for (int k = k_in_grammar-1; k >= 0; --k)
			{
				_print_indent(src, k); fprintf(src, "}\n");
				_print_indent(src, k); fprintf(src, "else\n");
				_print_indent(src, k); fprintf(src, "\tgoto exit_unhandled;\n");
			}
			fprintf(src, "\t\t}\n");
		}
		sh_vector_free(&possible_lookup.look_ahead);
		free(look_ahead_indices);
	}
	if (thereWasNone)
		fprintf(src, "\t\tgoto exit_unhandled;\n");
	else
		fprintf(src, "\t\telse\n"
			"\t\t\tgoto exit_unhandled;\n");
	fprintf(src,	"\t}\n"
			"exit_handled:\n"
			"\tif (!is_warning)\n"
			"\t\tSTOP_EVALUATION;\n"
			"\tfree(la_str);\n"
			"\treturn recovered;\n"
			"exit_unhandled:\n"
			"\tsh_lexer_error(l, \"error: unhandled error, expected to match '%%s', but found %%s\", cur, la_str);\n"
			"\tgoto exit_handled;\n"
			"exit_no_mem:\n"
			"\tsh_lexer_error(l, \"internal error: out of memory when handling error\");\n"
			"\trecovered = false;\n"
			"\tgoto exit_handled;\n"
			"}\n");
	return 0;
exit_no_mem:
	return SH_PARSER_NO_MEM;
}
