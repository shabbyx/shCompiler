/*
 * Copyright (C) 2007-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shCompiler.
 *
 * shCompiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shCompiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shCompiler.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <shparser.h>
#include "parser_internal.h"

//#define SH_DEBUG
//#define SH_DETAILED_DEBUG

#define INTERNAL ((parser_internal *)parser->internal)
#define LEXER (parser->lexer)
#define LEXER_INTERNAL ((lexer_internal *)LEXER->internal)

#if 0
static void _symbol_cpy(void *to, void *from)
{
	*(grammar_symbol *)to = *(grammar_symbol *)from;
}
#endif

static void _intcpy(void *a, void *b)
{
	*(int *)a = *(int *)b;
}

static void _setcpy(void *a, void *b)
{
	*(shSet *)a = *(shSet *)b;
}

static void _parent_and_after_data_cpy(void *to, void *from)
{
	*(parent_and_after_data *)to = *(parent_and_after_data *)from;
}

static int _intcmp(const void *a, const void *b)
{
	int first = *(const int *)a;
	int second = *(const int *)b;

	return (first > second) - (first < second);
}

static int _symbol_cmp(const void *a, const void *b)
{ 
	const grammar_symbol *first = a;
	const grammar_symbol *second = b;

	if (first->type == second->type)
		return (first->id > second->id) - (first->id < second->id);
	return (first->type > second->type) - (first->type < second->type);
}

static int _parent_and_after_data_cmp(const void *a, const void *b)
{
	const parent_and_after_data *first = a;
	const parent_and_after_data *second = b;

	if (first->parent != second->parent)
		return first->parent < second->parent?-1:1;
	if (first->after_len != second->after_len)
		return first->after_len < second->after_len?-1:1;
	for (size_t i = 0; i < first->after_len; ++i)
	{
		int cmp = _symbol_cmp(&first->after[i], &second->after[i]);
		if (cmp)
			return cmp;
	}
	return 0;
}

static void _find_follow_data_cpy(void *to, void *from)
{
	*(find_follow_data *)to = *(find_follow_data *)from;
}

static int _find_follow_data_cmp(const void *a, const void *b)
{
	const find_follow_data *first = a;
	const find_follow_data *second = b;

	if (first->nonterminal == second->nonterminal)
		return (first->s > second->s) - (first->s < second->s);
	return (first->nonterminal > second->nonterminal) - (first->nonterminal < second->nonterminal);
}

static void _find_follow_result_cpy(void *to, void *from)
{
	*(find_follow_result *)to = *(find_follow_result *)from;
}

static void _nonterminal_follow_cpy(void *to, void *from)
{
	*(nonterminal_follow *)to = *(nonterminal_follow *)from;
}

static int _nonterminal_follow_cmp(const void *a, const void *b)
{
	const nonterminal_follow *first = a;
	const nonterminal_follow *second = b;

	/* this compares follows of length s of a nonterminal, so they should have the same size */
	assert(first->follow.size == second->follow.size);

	return sh_vector_compare_lexic(&first->follow, &second->follow);
}

#ifdef SH_DEBUG
static void _print_after(shParser *parser, grammar_symbol *after, size_t after_len)
{
	for (size_t i = 0; i < after_len; ++i)
		printf(" %s", after[i].id == -1?"$":sh_parser_symbol_name(parser, after[i]));
}

static bool _print_parent_and_after(shNode *n, void *extra)
{
	parent_and_after_data *pa = n->key;

	printf("  %s and after it is:", pa->parent >= 0?sh_parser_nonterminal_name(extra, pa->parent):"''");
	_print_after(extra, pa->after, pa->after_len);
	printf("\n");

	return true;
}

static bool _print_parent_and_afters(shNode *n, void *extra)
{
	printf("%s is derived from:\n", sh_parser_nonterminal_name(extra, *(int *)n->key));
	sh_set_for_each(n->data, _print_parent_and_after, extra);

	return true;
}

static bool _print_follow_graph_adj(shNode *n, void *extra)
{
	find_follow_data *d = n->key;
	printf("    (%s, %u)\n", sh_parser_nonterminal_name(extra, d->nonterminal), d->s);
	return true;
}

static bool _print_follow_graph(shNode *n, void *extra)
{
	find_follow_data *d = n->key;

	printf("(%s, %u) is dependent on:\n", sh_parser_nonterminal_name(extra, d->nonterminal), d->s);
	sh_set_for_each(n->data, _print_follow_graph_adj, extra);

	return true;
}

static bool _print_follow_representative(shNode *n, void *extra)
{
	find_follow_data *d = n->key;
	find_follow_data *r = n->data;

	printf("(%s, %u) is represented by (%s, %u)\n",
		sh_parser_nonterminal_name(extra, d->nonterminal), d->s,
		sh_parser_nonterminal_name(extra, r->nonterminal), r->s);

	return true;
}

static bool _print_follow(shNode *n, void *extra)
{
	nonterminal_follow *nf = n->key;
	shParser *parser = extra;
	int *symbols = sh_vector_data(&nf->follow);

	printf("    ");
	for (size_t i = 0; i < nf->follow.size; ++i)
	{
		int t = symbols[i];
		printf("%s%s", i?", ":"", t == -1?"$":sh_parser_terminal_name(parser, t));
	}
	printf("\n");

	return true;
}

static find_follow_data _get_representative(shParser *, find_follow_data *);
static bool _print_follow_results(shNode *n, void *extra)
{
	find_follow_data *d = n->key;
	find_follow_result *r = n->data;
	shParser *parser = extra;

	printf("%s follows of length %d:\n", sh_parser_nonterminal_name(parser, d->nonterminal), d->s);

	find_follow_data rep = _get_representative(parser, d);
	if (_find_follow_data_cmp(d, &rep) == 0)
		sh_set_inorder(&r->follows, _print_follow, parser);
	else
		printf("    same as %s with length %d\n", sh_parser_nonterminal_name(parser, rep.nonterminal), rep.s);

	return true;
}
#endif

static bool _check_first_has_nonpartial(shNode *n, void *extra)
{
	bool *has_nonpartial = extra;
	sentence_first *sf = n->key;

	if (!sf->partial)
		*has_nonpartial = true;

	/* continue searching if partial */
	return sf->partial;
}

static bool _firsts_has_nonpartial(shNode *n)
{
	bool has_nonpartial = false;

	find_first_result *r = n->data;
	sh_set_for_each(&r->firsts, _check_first_has_nonpartial, &has_nonpartial);

	return has_nonpartial;
}

static int _add_zero_sized_follow(shSet *res)
{
	nonterminal_follow nothing = {
		.needs_free = false,
	};

	sh_vector_init(&nothing.follow, _intcpy, sizeof(int), _intcmp);
	return sh_set_insert(res, &nothing);
}

static int _add_zero_sized_first(shSet *res)
{
	sentence_first nothing = {
		.needs_free = false,
		.partial = false,
	};

	sh_vector_init(&nothing.first, _intcpy, sizeof(int), _intcmp);
	return sh_set_insert(res, &nothing);
}

static int _add_all_dollar_follow(shSet *res, int s)
{
	nonterminal_follow all_dollar = {
		.needs_free = true,
	};

	sh_vector_init(&all_dollar.follow, _intcpy, sizeof(int), _intcmp);
	sh_vector_reserve(&all_dollar.follow, s);
	for (int i = 0; i < s; ++i)
	{
		int dollar = -1;
		if (sh_vector_append(&all_dollar.follow, &dollar))
			goto exit_no_mem;
	}

	return sh_set_insert(res, &all_dollar);
exit_no_mem:
	sh_vector_free(&all_dollar.follow);
	return -1;
}

static int _get_after_first(shParser *parser, find_first_data *d, shSet *results, bool nonpartial_only)
{
	shNode n;
	sh_set_get_first(&INTERNAL->firsts, &n);
	sh_set_init_similar(results, &((find_first_result *)n.data)->firsts);

	if (d->sentence_len > 0)
	{
		if (sh_parser_find_firsts(parser, d, &n))
			return -1;
		if (nonpartial_only && !_firsts_has_nonpartial(&n))
			return 1;
		*results = ((find_first_result *)n.data)->firsts;
	}
	else if (d->k == 0)
		return _add_zero_sized_first(results);
	else if (nonpartial_only)
		return 1;

	return 0;
}

typedef struct concat_and_union_acc
{
	shSet *res;
	sentence_first *pre;
	shSet *post;
	int error;
} concat_and_union_acc;

static bool _concat_and_union_with_post(shNode *n, void *extra)
{
	concat_and_union_acc *caua = extra;
	nonterminal_follow complete;
	int ret;

	/* concat pre and post together to make the complete follow */
	ret = sh_vector_duplicate(&caua->pre->first, &complete.follow);
	ret += sh_vector_concat(&complete.follow, &((nonterminal_follow *)n->key)->follow);

	if (ret < 0)
		caua->error = -1;

	/* add it to accumulator of follows */
	complete.needs_free = true;
	ret = sh_set_insert(caua->res, &complete);
	if (ret < 0)
		caua->error = -1;
	if (ret)
		sh_vector_free(&complete.follow);

	return true;
}

static bool _concat_and_union_pre(shNode *n, void *extra)
{
	concat_and_union_acc *caua = extra;

	caua->pre = n->key;
	sh_set_for_each(caua->post, _concat_and_union_with_post, extra);

	return true;
}

static int _concat_and_union(shSet *res, shSet/* sentence_first */ *pre, shSet/* nonterminal_follow */ *post)
{
	concat_and_union_acc caua = {
		.res = res,
		.post = post,
	};

	if (pre->size == 0 || post->size == 0)
		return 0;

	sh_set_for_each(pre, _concat_and_union_pre, &caua);

	return caua.error;
}

typedef struct union_firsts_as_follows_acc
{
	shSet *res;
	int error;
} firsts_as_follows_acc;

static bool _union_first_as_follow(shNode *n, void *extra)
{
	firsts_as_follows_acc *fafa = extra;
	sentence_first *sf = n->key;

	int ret = sh_set_insert(fafa->res, &(nonterminal_follow){ .follow = sf->first });
	if (ret < 0)
		fafa->error = ret;

	return true;
}

/* take result of find first and add to given set */
static int _union_firsts_as_follows(shSet *firsts, shSet *result)
{
	firsts_as_follows_acc fafa = {
		.res = result,
	};

	sh_set_for_each(firsts, _union_first_as_follow, &fafa);

	return fafa.error;
}

/*
 * if there is a rule such as T -> .. R, then follows of R with length s are dependent
 * on follows of T with length s.  If the rule was T -> .. R sentence, then follows of
 * R with length s are dependent on follows of T with length k if sentence can produce
 * list of terminals with length s - k.
 */
static void _find_follow_dependency(shParser *parser, int nonterminal, unsigned int s)
{
	shNode n;
	shSet *pa;
	find_follow_data nont_s = {
		.nonterminal = nonterminal,
		.s = s,
	};
	shSet/* find_follow_data */ result;

	sh_set_init(&result, _find_follow_data_cpy, sizeof(find_follow_data), _find_follow_data_cmp);

#ifdef SH_DETAILED_DEBUG
	printf("Finding follow dependency of %s with length %u\n", sh_parser_nonterminal_name(parser, nonterminal), s);
#endif

	assert(!sh_set_exists(&INTERNAL->follow_graph, &nont_s));

	/* if length zero, it doesn't depend on anything, so set its adjacency list to an empty set */
	if (s == 0)
		goto exit_done;

#ifdef SH_DETAILED_DEBUG
	printf("\twhich was not computed before\n");
#endif
	pa = sh_map_at(&INTERNAL->parent_and_after, &nonterminal);
	assert(pa != NULL);

	for (int res = sh_set_get_first(pa, &n); res == 0; res = sh_set_get_next(&n, &n))
	{
		parent_and_after_data *after_data = n.key;
		if (after_data->after_len == 0)
		{
			for (unsigned int k = 0; k < s; ++k)
				sh_set_insert(&result, &(find_follow_data){ .nonterminal = after_data->parent, .s = s - k });
			continue;
		}

		shNode after_firsts;
		find_first_data after_firsts_data = {
			.sentence = after_data->after,
			.k = 0,
			.sentence_malloced = false,
			.sentence_len = after_data->after_len
		};

		for (unsigned int k = 0; k < s; ++k)
		{
			after_firsts_data.k = k;

			sh_parser_find_firsts(parser, &after_firsts_data, &after_firsts);

			if (_firsts_has_nonpartial(&after_firsts))
				sh_set_insert(&result, &(find_follow_data){ .nonterminal = after_data->parent, .s = s - k });
		}

		/*
		 * for firsts of size s, no dependency would be created, but when calculating follows, those firsts would be required.
		 * That's why I'm calculating them here so that while the sentence is already created it would be added to the firsts
		 * as a shallow copy.
		 */
		after_firsts_data.k = s;
		sh_parser_find_firsts(parser, &after_firsts_data, &after_firsts);
	}
exit_done:
	sh_map_insert(&INTERNAL->follow_graph, &nont_s, &result);
}

static find_follow_data _get_representative(shParser *parser, find_follow_data *node)
{
	find_follow_data *rep = sh_map_at(&INTERNAL->follow_representative, node);
	assert(rep != NULL);
	return *rep;
}

struct scc_mark
{
	int index;
	int lowlink;
	bool in_stack;
};

static int _scc(shParser *parser, shStack *S, int v, int *index, struct scc_mark *marks)
{
	int nont_count = INTERNAL->nonterminals.size;
	find_follow_data vnode = {
		.nonterminal = v % nont_count,
		.s = v / nont_count,
	};
	shSet *adj;
	shNode n;

	if (sh_stack_push(S, &v))
		return -1;
	marks[v] = (struct scc_mark){
		.index = *index,
		.lowlink = *index,
		.in_stack = true,
	};
	++*index;

	adj = sh_map_at(&INTERNAL->follow_graph, &vnode);
	assert(adj != NULL);

	for (int res = sh_set_get_first(adj, &n); res == 0; res = sh_set_get_next(&n, &n))
	{
		find_follow_data *a = n.key;
		int w = a->s * nont_count + a->nonterminal;

		/* if not visited, recurse on it */
		if (marks[w].index < 0)
		{
			if (_scc(parser, S, w, index, marks))
				return -1;
			if (marks[w].lowlink < marks[v].lowlink)
				marks[v].lowlink = marks[w].lowlink;
		}
		/* otherwise if already in stack, it's in the current SCC */
		else if (marks[w].in_stack)
			if (marks[w].index < marks[v].lowlink)
				marks[v].lowlink = marks[w].index;
	}

	/* if v is root of SCC, pop from S until v is reached and make them all one SCC (with v is representative) */
	if (marks[v].lowlink == marks[v].index)
	{
#ifdef SH_DEBUG
		printf("Found a new connected component with root (%s, %d) containing:\n",
				sh_parser_nonterminal_name(parser, v % nont_count), v / nont_count);
#endif
		int *w;
		do
		{
			assert(S->size > 0);
			w = sh_stack_top(S);
			sh_stack_pop(S);
			marks[*w].in_stack = false;

			sh_map_insert(&INTERNAL->follow_representative, &(find_follow_data){
										.nonterminal = *w % nont_count,
										.s = *w / nont_count,
									}, &vnode);
#ifdef SH_DEBUG
			printf("  (%s, %d)\n", sh_parser_nonterminal_name(parser, *w % nont_count), *w / nont_count);
#endif
		} while (*w != v);
	}

	return 0;
}

/*
 * find strongly connected components (SCC) of the follow dependency graph.  The components show
 * (nonterminal, k)'s that have the same follow and iterating over all but one of them is useless
 * when calculating the follows.
 *
 * This function uses Tarjan's algorithm.  Its result is follow_representative.
 */
static int _find_strongly_connected_components(shParser *parser)
{
	/* find_follow_data is converted to s * nonterminal_count + nonterminal and used as the vertex index */
	int index = 0;
	struct scc_mark *marks = NULL;
	shStack/* int */ S;
	int vcount;
	int ret = 0;

	sh_map_init(&INTERNAL->follow_representative, _find_follow_data_cpy, sizeof(find_follow_data),
			_find_follow_data_cpy, sizeof(find_follow_data), _find_follow_data_cmp);

	sh_stack_init(&S, _intcpy, sizeof(int));

	vcount = (INTERNAL->k_in_grammar + 1) * INTERNAL->nonterminals.size;
	marks = malloc(vcount * sizeof *marks);
	if (marks == NULL)
		goto exit_no_mem;

	for (int i = 0; i < vcount; ++i)
		marks[i] = (struct scc_mark){
			.index = -1,
			.lowlink = -1,
		};

	for (int i = 0; i < vcount; ++i)
		if (marks[i].index < 0)
			if (_scc(parser, &S, i, &index, marks))
				goto exit_no_mem;

exit_normal:
	sh_stack_free(&S);
	free(marks);
	return ret;
exit_no_mem:
	ret = -1;
	goto exit_normal;
}

/*
 * accumulate "after" data from parent_and_after once the follow adjacency graph is dagified
 * by having its strongly connected components (SCC) identified.  The SCCs are represented
 * by one of their members and the follows of all members of an SCC are accumulated in that
 * representative.
 */
static int _accumulate_parent_and_after(shParser *parser)
{
	for (int i = 0, size = INTERNAL->nonterminals.size; i < size; ++i)
	{
		shSet *s = sh_map_at(&INTERNAL->parent_and_after, &i);
		assert(s != NULL);

		shSet aggregate_over_k;
		sh_set_init_similar(&aggregate_over_k, s);

		shNode n;
		for (int res = sh_set_get_first(s, &n); res == 0; res = sh_set_get_next(&n, &n))
		{
			parent_and_after_data after_data = *(parent_and_after_data *)n.key;
			for (unsigned int k = 0; k <= INTERNAL->k_in_grammar; ++k)
			{
				after_data.parent = _get_representative(parser, &(find_follow_data){
											.nonterminal = after_data.parent,
											.s = k,
										}).nonterminal;
				if (sh_set_insert(&aggregate_over_k, &after_data) < 0)
				{
					sh_set_free(&aggregate_over_k);
					return -1;
				}
			}
		}
		sh_set_clear(s);
		*s = aggregate_over_k;
	}

	/* accumulate parent_and_after_data in parent */
	for (unsigned int k = 0; k <= INTERNAL->k_in_grammar; ++k)
		for (size_t i = 0, size = INTERNAL->nonterminals.size; i < size; ++i)
		{
			find_follow_data node = _get_representative(parser, &(find_follow_data){ .nonterminal = i, .s = k });
			if (node.nonterminal == (int)i)
				continue;

			shSet *s = sh_map_at(&INTERNAL->parent_and_after, &i);
			shSet *p = sh_map_at(&INTERNAL->parent_and_after, &node.nonterminal);
			assert(s != NULL);
			assert(p != NULL);

			shNode n;
			for (int res = sh_set_get_first(s, &n); res == 0; res = sh_set_get_next(&n, &n))
				if (sh_set_insert(p, n.key) < 0)
					return -1;
		}
	return 0;
}

/*
 * calculates the follows of d->nonterminal of length d->s.  Returns the follows node containing
 * these results in result.  It returns 0 if successful and a negative value if error.
 *
 * Follow dependencies must be found first and its graph must be DAGified (if not, the function will
 * get stuck in a loop).
 *
 * The algorithm is as follows.  Assume we are looking for follows of nonterminal T with length s.
 * Corresponding to T, there is a set of parent_and_after_data.  For each of those, assuming parent is
 * P and after is a sentence X, we must have had a rule such as P -> Y T X.  The follows are the
 * union of the following for each node of parent_and_after[T].
 *
 *  s      result
 * = 0    {[]}
 * > 0    union(nonpartial_firsts(X, i) concat with follows(P, s-k)) for 0<=i<s
 *        union with firsts(X, s)
 *
 * A special case is follow of the start nonterminal, which at least always contains s times $.
 */
int sh_parser_find_follows(shParser *parser, find_follow_data *d, shNode *result)
{
	shMap *follows = &INTERNAL->follows;
	shSet res_follows;
	shSet *pa;
	shNode n;
	int ret;

#ifdef SH_DETAILED_DEBUG
	printf("Finding follow of %s with length %d\n", sh_parser_nonterminal_name(parser, d->nonterminal), d->s);
#endif

	sh_set_init(&res_follows, _nonterminal_follow_cpy, sizeof(nonterminal_follow), _nonterminal_follow_cmp);

	/* find SCC representative of the node in follow graph */
	find_follow_data rep = _get_representative(parser, d);

	/* if computed before, return return with result directly */
	if (sh_map_find(follows, &rep, result))
		return 0;

	/* in the special case of s = 0, there is always a follow which is nothing */
	if (rep.s == 0)
	{
		if (_add_zero_sized_follow(&res_follows) < 0)
			goto exit_no_mem;
		goto exit_done;
	}

	/*
	 * in the special case of follow of start nonterminal, the follow always has s times $.
	 *
	 * Note: the way the SCCs are calculated, at least the SCC containing start will have the start
	 * nonterminal as representative.
	 */
	if (rep.nonterminal == 0)
		if (_add_all_dollar_follow(&res_follows, rep.s) < 0)
			goto exit_no_mem;

#ifdef SH_DETAILED_DEBUG
	printf("\twhich was not computed before\n");
#endif

	pa = sh_map_at(&INTERNAL->parent_and_after, &rep.nonterminal);
	assert(pa != NULL);

	for (int res = sh_set_get_first(pa, &n); res == 0; res = sh_set_get_next(&n, &n))
	{
		parent_and_after_data *after_data = n.key;
		shSet/* sentence_first */ after_firsts;
		shNode parent_follows;
		find_first_data after_firsts_data = {
			.sentence = after_data->after,
			.k = 0,
			.sentence_malloced = false,
			.sentence_len = after_data->after_len
		};

		for (unsigned int k = 0; k < rep.s; ++k)
		{
			find_follow_data rep_after = _get_representative(parser, &(find_follow_data){ .nonterminal = after_data->parent, .s = rep.s - k });

			/* prevent self loop */
			if (k == 0 && _find_follow_data_cmp(&rep, &rep_after) == 0)
				continue;

			after_firsts_data.k = k;

			ret = _get_after_first(parser, &after_firsts_data, &after_firsts, true);
			if (ret > 0)
				continue;
			if (ret == 0)
			{
				sh_parser_find_follows(parser, &rep_after, &parent_follows);
				ret = _concat_and_union(&res_follows, &after_firsts, &((find_follow_result *)parent_follows.data)->follows);
			}

			/* if after_firsts was made with _add_zero_sized_first, clean it up */
			if (after_firsts_data.sentence_len == 0 && after_firsts_data.k == 0)
				sh_set_free(&after_firsts);

			if (ret < 0)
				goto exit_no_mem;
		}

		after_firsts_data.k = rep.s;

		if (_get_after_first(parser, &after_firsts_data, &after_firsts, false) < 0)
			goto exit_no_mem;
		if (_union_firsts_as_follows(&after_firsts, &res_follows) < 0)
			goto exit_no_mem;
	}

exit_done:
	ret = sh_map_insert(follows, &rep, &(find_follow_result){ .follows = res_follows }, result);
	assert(ret <= 0);
	return ret;
exit_no_mem:
	sh_set_free(&res_follows);
	return SH_PARSER_NO_MEM;
}

int sh_parser_find_all_follows(shParser *parser)
{
	grammar_rule *rules = sh_vector_data(&INTERNAL->rules);
	shSet empty;
	sh_set_init(&empty, _parent_and_after_data_cpy, sizeof(parent_and_after_data), _parent_and_after_data_cmp);

	sh_map_init(&INTERNAL->parent_and_after, _intcpy, sizeof(int), _setcpy, sizeof(shSet), _intcmp);
	for (int nont = 0; nont < (int)INTERNAL->nonterminals.size; ++nont)
		if (sh_map_insert(&INTERNAL->parent_and_after, &nont, &empty) < 0)
			goto exit_no_mem;
	for (size_t i = 0; i < INTERNAL->rules.size; ++i)
	{
		/* in CLR(k), there is an added lambda -> start rule that needs to be ignored */
		if (rules[i].head.id == -1)
			continue;
		grammar_rule *rule = &rules[i];
		grammar_symbol *follow_body = sh_vector_data(&rule->body);
		for (size_t j = 0; j < rule->body.size; ++j)
		{
			if (follow_body[j].type != NONTERMINAL)
				continue;

			shSet *pa = sh_map_at(&INTERNAL->parent_and_after, &follow_body[j].id);
			assert(pa != NULL);	/* it shouldn't fail because I inserted all nonterminals before */

			parent_and_after_data after_data = {
				.parent = rule->head.id,
				.after = follow_body + j + 1,
				.after_len = rule->body.size - j - 1
			};
			if (sh_set_insert(pa, &after_data) < 0)
				goto exit_no_mem;
		}
	}
#ifdef SH_DEBUG
	printf("Parent and after:\n");
	sh_map_inorder(&INTERNAL->parent_and_after, _print_parent_and_afters, parser);
#endif

	sh_map_init(&INTERNAL->follow_graph, _find_follow_data_cpy, sizeof(find_follow_data), _setcpy, sizeof(shSet), _find_follow_data_cmp);
	for (unsigned int k = 0; k <= INTERNAL->k_in_grammar; ++k)
		for (size_t i = 0; i < INTERNAL->nonterminals.size; ++i)
			_find_follow_dependency(parser, i, k);
#ifdef SH_DEBUG
	printf("Follow dependencies (before detecting loops):\n");
	sh_map_inorder(&INTERNAL->follow_graph, _print_follow_graph, parser);
#endif

	_find_strongly_connected_components(parser);
#ifdef SH_DEBUG
	printf("Follow graph SCC representatives:\n");
	sh_map_inorder(&INTERNAL->follow_representative, _print_follow_representative, parser);
#endif

	if (_accumulate_parent_and_after(parser))
		goto exit_no_mem;
#ifdef SH_DEBUG
	printf("Parent and after aggregated:\n");
	sh_map_inorder(&INTERNAL->parent_and_after, _print_parent_and_afters, parser);
#endif

	/* compute and store follows */
	sh_map_init(&INTERNAL->follows, _find_follow_data_cpy, sizeof(find_follow_data),
			_find_follow_result_cpy, sizeof(find_follow_result), _find_follow_data_cmp);
	for (unsigned int k = 1; k <= INTERNAL->k_in_grammar; ++k)
		for (size_t i = 0; i < INTERNAL->nonterminals.size; ++i)
		{
			find_follow_data ffd = {
				.nonterminal = (int)i,
				.s = k,
			};
			if (sh_parser_find_follows(parser, &ffd, NULL) < 0)
				goto exit_no_mem;
		}
#ifdef SH_DEBUG
	printf("Follows:\n");
	sh_map_inorder(&INTERNAL->follows, _print_follow_results, parser);
#endif

	return 0;
exit_no_mem:
	return -1;
}
