/*
 * Copyright (C) 2007-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shCompiler.
 *
 * shCompiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shCompiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shCompiler.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <shparser.h>
#include "parser_internal.h"

//#define SH_DEBUG
//#define SH_DETAILED_DEBUG

#define INTERNAL ((parser_internal *)parser->internal)

static void _find_first_data_cpy(void *to, void *from)
{
	*(find_first_data *)to = *(find_first_data *)from;
}

static int _symbol_cmp(const void *a, const void *b)
{
	const grammar_symbol *first = a;
	const grammar_symbol *second = b;

	if (first->type == second->type)
		return (first->id > second->id) - (first->id < second->id);
	return (first->type > second->type) - (first->type < second->type);
}

static int _find_first_data_cmp(const void *a, const void *b)
{
	const find_first_data *first = a;
	const find_first_data *second = b;

	if (first->k != second->k)
		return first->k < second->k?-1:1;
	if (first->sentence_len != second->sentence_len)
		return first->sentence_len < second->sentence_len?-1:1;
	for (size_t i = 0; i < first->sentence_len; ++i)
	{
		int cmp = _symbol_cmp(&first->sentence[i], &second->sentence[i]);
		if (cmp)
			return cmp;
	}
	return 0;
}

static void _find_first_result_cpy(void *to, void *from)
{
	*(find_first_result *)to = *(find_first_result *)from;
}

static void _sentence_first_cpy(void *to, void *from)
{
	*(sentence_first *)to = *(sentence_first *)from;
}

static int _sentence_first_cmp(const void *a, const void *b)
{
	const sentence_first *first = a;
	const sentence_first *second = b;

	/* this compares firsts of length k of a sentence, so they should have the same size */
	assert(first->first.size == second->first.size);

	/*
	 * partial field is not compared because it doesn't introduce distinction.
	 * We don't want two results that only differ in partial
	 */

	return sh_vector_compare(&first->first, &second->first);
}

static void _intcpy(void *to, void *from)
{
	*(int *)to = *(int *)from;
}

static int _intcmp(const void *a, const void *b)
{
	int first = *(const int *)a;
	int second = *(const int *)b;

	return (first > second) - (first < second);
}

#if defined(SH_DEBUG) || defined(SH_DETAILED_DEBUG)
static void _print_sentence(shParser *parser, find_first_data *d)
{
	printf("<");
	for (size_t i = 0; i < d->sentence_len; ++i)
		printf("%s%s", i?", ":"", sh_parser_symbol_name(parser, d->sentence[i]));
	printf("> with length %u%s", d->k, d->sentence_malloced?" [malloc]":"");
}

static void _print_first(shParser *parser, sentence_first *sf)
{
	int *symbols = sh_vector_data(&sf->first);
	printf("<");
	for (size_t i = 0; i < sf->first.size; ++i)
		printf("%s%s", i?", ":"", sh_parser_symbol_name(parser, (grammar_symbol){ .type = TERMINAL, .id = symbols[i], }));
	printf("%s>", sf->partial?" ...":"");
}

static bool _print_firsts(shNode *n, void *extra)
{
	printf("    ");
	_print_first(extra, n->key);
	printf("\n");
	return true;
}

static void _print_first_results(shParser *parser, find_first_result *r)
{
	if (r->complete || r->dfs_visited)
		printf("%s%s\n", r->complete?" [complete]":"", r->dfs_visited?" [visited]":"");
	sh_set_inorder(&r->firsts, _print_firsts, parser);
	printf("\n");
}
#endif

#ifdef SH_DEBUG
static bool _print_first_result(shNode *n, void *extra)
{
	find_first_data *d = n->key;
	find_first_result *r = n->data;

	_print_sentence(extra, d);
	printf(":\n");
	_print_first_results(extra, r);

	return true;
}
#endif

static sentence_first _build_sentence_first(int *init_value, size_t init_value_len, bool partial)
{
	sentence_first sf = {
		.partial = partial,
		.needs_free = true,
        };

	sh_vector_init(&sf.first, _intcpy, sizeof(int), _intcmp);
	sh_vector_concat_array(&sf.first, init_value, init_value_len);

	return sf;
}

typedef struct union_acc
{
	find_first_result *acc;
	int error;
	bool partial_only;
} union_acc;

typedef struct merge_acc
{
	find_first_result *acc;
	sentence_first *pre_sf;
	find_first_result *post;
	int error;
} merge_acc;

static bool _first_result_union(shNode *n, void *extra)
{
	union_acc *ua = extra;
	sentence_first sf = *(sentence_first *)n->key;
	shNode m;
	int ret;

	/* if unioning only partials, then skip nonpartial ones */
	if (ua->partial_only && !sf.partial)
		return true;

	/* since it's a shallow copy, it doesn't need to be freed */
	sf.needs_free = false;

	ret = sh_set_insert(&ua->acc->firsts, &sf, &m);
	if (ret < 0)
		goto exit_no_mem;
	if (ret == 0)
		ua->error = 1;
	/* if not a new entry, then it could be that partial flag needs to be updated */
	else if (sf.partial == false)
	{
		sentence_first *orig = m.key;
		if (orig->partial)
			ua->error = 1;
		orig->partial = false;
	}

	return true;
exit_no_mem:
	ua->error = ret;
	return false;
}

static int _update_firsts_by_union(find_first_result *acc, find_first_result *r, bool partial_only)
{
	union_acc ua = {
		.acc = acc,
		.partial_only = partial_only,
	};
	sh_set_for_each(&r->firsts, _first_result_union, &ua);
	return ua.error;
}

static bool _first_result_merge_post(shNode *n, void *extra)
{
	merge_acc *ma = extra;
	sentence_first *pre_sf = ma->pre_sf;
	sentence_first *post_sf = n->key;
	sentence_first sf = _build_sentence_first(sh_vector_data(&pre_sf->first), pre_sf->first.size, post_sf->partial);
	shNode m;
	int ret = -1;

	if (sf.first.size != pre_sf->first.size)
		goto exit_no_mem;

	/* make the sentence complete by concatenating the post to the already copied pre */
	/* Note that the partial field is inherited from post, which is taken care of on initialization of sf */
	if (sh_vector_concat(&sf.first, &post_sf->first))
		goto exit_no_mem;

	ret = sh_set_insert(&ma->acc->firsts, &sf, &m);
	if (ret < 0)
		goto exit_no_mem;
	if (ret == 0)
		ma->error = 1;
	else
	{
		sh_vector_free(&sf.first);
		/* if not a new entry, then it could be that partial flag needs to be updated */
		if (sf.partial == false)
		{
			sentence_first *orig = m.key;
			if (orig->partial)
				ma->error = 1;
			orig->partial = false;
		}
	}

	return true;
exit_no_mem:
	sh_vector_free(&sf.first);
	ma->error = ret;
	return false;
}

static bool _first_result_merge(shNode *n, void *extra)
{
	merge_acc *ma = extra;
	ma->pre_sf = n->key;

	/* if pre is partial, skip it */
	if (!ma->pre_sf->partial)
		sh_set_for_each(&ma->post->firsts, _first_result_merge_post, ma);

	return ma->error >= 0;
}

static int _update_firsts_by_merge(find_first_result *acc, find_first_result *pre, find_first_result *post)
{
	merge_acc ma = {
		.acc = acc,
		.pre_sf = NULL,
		.post = post,
	};
	sh_set_for_each(&pre->firsts, _first_result_merge, &ma);
	return ma.error;
}

static int _insert_empty_result(shMap *firsts, find_first_data *d, shNode *n)
{
	find_first_result empty = {
		.complete = false,
		.dfs_visited = false,
	};
	sh_set_init(&empty.firsts, _sentence_first_cpy, sizeof(sentence_first), _sentence_first_cmp);

	/* in the special case of k = 0, there is always a partial expansion */
	if (d->k == 0)
	{
		sentence_first f = { .partial = true };
		sh_vector_init(&f.first, _intcpy, sizeof(int), _intcmp);
		if (sh_set_insert(&empty.firsts, &f))
			goto exit_no_mem;
	}

	return sh_map_insert(firsts, d, &empty, n);
exit_no_mem:
	sh_set_free(&empty.firsts);
	return -1;
}

static int _update_first_of_terminal(find_first_data *d, find_first_result *r)
{
	sentence_first sf;
	if (d->k != 1)
		goto exit_skip;

	/* all that a terminal generates is itself and this is immediately known */
	sf = _build_sentence_first(&d->sentence[0].id, 1, d->k != 1);

	if (sf.first.size != 1)
		goto exit_no_mem;

	if (sh_set_insert(&r->firsts, &sf) < 0)
		goto exit_no_mem;
	r->complete = true;

exit_skip:
#ifdef SH_DETAILED_DEBUG
	printf("Updated first of terminal\n");
#endif

	return 0;
exit_no_mem:
	sh_vector_free(&sf.first);
	return -1;
}

static int _add_empty_first_to_nonterminal(find_first_data *d, find_first_result *r)
{
	sentence_first sf = _build_sentence_first(NULL, 0, false);
	sentence_first *sf_ref;
	shNode n;
	int ret = 0;

	if (!sh_set_find(&r->firsts, &sf, &n))
		return -1;

	/* say that it's not partial anymore */
	sf_ref = n.key;
	if (sf_ref->partial)
		ret = 1;
	sf_ref->partial = false;

#ifdef SH_DETAILED_DEBUG
	printf("Updated first of nonterminal (by making it nullable)\n");
#endif

	return ret;
}

int sh_parser_find_firsts(shParser *parser, find_first_data *d, shNode *result);

static int _update_firsts_of_nonterminal(shParser *parser, find_first_data *d, find_first_result *r)
{
	shVector *nonterminal_rules = sh_vector_data(&INTERNAL->nonterminal_rules);
	int nont = d->sentence[0].id;
	int *this_nont_rules = sh_vector_data(&nonterminal_rules[nont]);
	size_t this_nont_rules_count = nonterminal_rules[nont].size;
	grammar_rule *rules = sh_vector_data(&INTERNAL->rules);
	int ret = 0, recret;
	bool complete = true;

	for (size_t i = 0; i < this_nont_rules_count; ++i)
	{
		grammar_rule *rule = &rules[this_nont_rules[i]];
		shNode n;

		/* if seeing an empty rule, say that the nonterminal can generate lambda */
		if (rule->body.size == 0)
		{
			if (d->k == 0)
			{
				recret = _add_empty_first_to_nonterminal(d, r);
				if (recret < 0)
					goto exit_no_mem;
				if (recret == 1)
					ret = 1;
			}
		}
		/* else, recursively call sh_parser_find_firsts on the generated rule */
		else
		{
			recret = sh_parser_find_firsts(parser, &(find_first_data){
									.sentence = sh_vector_data(&rule->body),
									.k = d->k,
									.sentence_len = rule->body.size
								}, &n);
			if (recret < 0)
				goto exit_no_mem;
			if (recret == 1)
				ret = 1;
			/* union all results */
			recret = _update_firsts_by_union(r, n.data, false);
			if (recret < 0)
				goto exit_no_mem;
			if (recret == 1)
				ret = 1;
			if (!((find_first_result *)n.data)->complete)
				complete = false;
		}
	}

	if (complete)
		r->complete = true;

#ifdef SH_DETAILED_DEBUG
	printf("Updated first of nonterminal ");
	_print_sentence(parser, d);
	printf(" to:");
	_print_first_results(parser, r);
#endif

	return ret;
exit_no_mem:
	return -1;
}

static bool _check_pre_is_eligible(shNode *n, void *extra)
{
	sentence_first *sf = n->key;

	if (sf->partial)
		return true;

	*(bool *)extra = true;
	return false;
}

static inline bool _pre_is_eligible(shNode *pre)
{
	bool eligible = false;
	find_first_result *r = pre->data;

	/*
	 * check if it has any nonpartial results at all
	 *
	 * Note: checking all is too much.  I check on size > 1 || full check.  This would not be precise, but would at least eliminate
	 * the common case of "no result at all" which is represented by a set with a partial lambda.  This happens with sentences
	 * containing only terminals.  For example, in a sentence like "Terminal1 Terminal2 Very Large Sentence After The Terminals" with k = 1
	 * it is immediately obvious that there is no point in calculating firsts of "Very Large Sentence After the Terminals" because
	 * "Terminal1 Terminal2" won't give any nonpartial results with k = 1.
	 */
	if (r->firsts.size > 1)
		return true;
	sh_set_for_each(&r->firsts, _check_pre_is_eligible, &eligible);

	return eligible;
}

static int _update_firsts_of_sentence(shParser *parser, find_first_data *d, find_first_result *r)
{
	find_first_data recurse_pre = *d, recurse_post = *d;
	shNode pre, post;
	bool complete = true;
	int ret = 0, recret;

	assert(d->sentence_len > 1);

	/*
	 * break the sentence up in a point that helps reduce redundant calculations:
	 *
	 * 1 If the sentence ends in terminals, break the sentence to the part before
	 *   the terminating terminals and the part after.
	 * 2 Otherwise, if the sentence starts in terminals, break the sentence to the
	 *   part containing initial terminals and the rest.
	 * 3 Otherwise, break the sentence to its first element and the rest.
	 */
	while (recurse_pre.sentence_len > 1 && recurse_pre.sentence[recurse_pre.sentence_len - 1].type == TERMINAL)
		--recurse_pre.sentence_len;
	if (recurse_pre.sentence_len == d->sentence_len) /* if not case 1 */
	{
		recurse_pre.sentence_len = 1;
		/* covers both cases 2 and 3 */
		while (recurse_pre.sentence_len < d->sentence_len - 1 && recurse_pre.sentence[recurse_pre.sentence_len].type == TERMINAL)
			++recurse_pre.sentence_len;
	}
	recurse_post.sentence += recurse_pre.sentence_len;
	recurse_post.sentence_len = d->sentence_len - recurse_pre.sentence_len;
	recurse_pre.sentence_malloced = recurse_post.sentence_malloced = false;

	/* try combinations of pre and post firsts with different k distribution */
	for (unsigned int i = 0; i <= d->k; ++i)
	{
		int ret_pre, ret_post = 0;

		recurse_pre.k = i;
		recurse_post.k = d->k - i;

		ret_pre = sh_parser_find_firsts(parser, &recurse_pre, &pre);
		if (ret_pre < 0)
			goto exit_no_mem;
		if (ret_pre == 1)
			ret = 1;
		if (!((find_first_result *)pre.data)->complete)
			complete = false;
		/* quickly determine if there is any point in moving on with this division of k */
		if (!_pre_is_eligible(&pre))
			continue;
		ret_post = sh_parser_find_firsts(parser, &recurse_post, &post);
		if (ret_post < 0)
			goto exit_no_mem;
		if (ret_post == 1)
			ret = 1;
		if (!((find_first_result *)post.data)->complete)
			complete = false;
		/* merge the results, which take from pre only nonpartial firsts */
		recret = _update_firsts_by_merge(r, pre.data, post.data);
		if (recret < 0)
			goto exit_no_mem;
		if (recret == 1)
			ret = 1;
	}
	/* finally, take from pre only partial firsts of length k, which were calculated in the last iteration */
	recret = _update_firsts_by_union(r, pre.data, true);
	if (recret < 0)
		goto exit_no_mem;
	if (recret == 1)
		ret = 1;

	if (complete)
		r->complete = true;

#ifdef SH_DETAILED_DEBUG
	printf("Updated first of sentence ");
	_print_sentence(parser, d);
	printf(" to:");
	_print_first_results(parser, r);
#endif

	return ret;
exit_no_mem:
	return -1;
}

/*
 * calculates firsts of d->sentence and updates it in firsts.  Returns the firsts node containing
 * these results in result.  It returns 1 if result has grown in size due to a call to this function,
 * and returns 0 if no change in firsts were observed.  If error, a negative value is returned.
 *
 * The algorithm is as follows:
 *
 * sentence              k      result
 *
 * t: terminal          = 0    no addition
 *                      = 1    add: (t, not partial)
 *                      > 1    add: (t, partial)
 * T: nonterminal       any    add: union(firsts(rule, k)) for rule in in rules[T]
 * SX: Symbol + Rest    any    add: union(nonpartial_firsts(S, i) concat with firsts(S, k-i)) for 0<=i<=k
 *                                  union with partial_firsts(S, k)
 *
 * $ is expected to be given a TERMINAL type and is treated as such.
 */
int sh_parser_find_firsts(shParser *parser, find_first_data *d, shNode *result)
{
	shMap *firsts = &INTERNAL->firsts;
	shNode n;
	find_first_result *r;
	size_t prev_result_size;
	int ret = 0;

#ifdef SH_DETAILED_DEBUG
	printf("Calculating first for: ");
	_print_sentence(parser, d);
	printf("\n");
#endif

	/* it is assumed that an empty sentence is not given to sh_parser_find_firsts */
	assert(d->sentence_len > 0);

	/* first, make sure the map contains an entry for d */
	if (!sh_map_find(firsts, d, &n))
		if (_insert_empty_result(firsts, d, &n))
			goto exit_no_mem;

	r = n.data;

	/* check if firsts for this sentence is already calculated completely */
	if (r->complete)
		goto exit_skip;

	/* check if looped (this is an error for LL(k) grammars) */
	if (r->dfs_visited)
		goto exit_skip;
	r->dfs_visited = true;

	prev_result_size = r->firsts.size;

	if (d->sentence_len == 1)
	{
		if (d->sentence[0].type == TERMINAL)
		{
			if ((ret = _update_first_of_terminal(d, r)) < 0)
				goto exit_no_mem;
		}
		else
		{
			if ((ret = _update_firsts_of_nonterminal(parser, d, r)) < 0)
				goto exit_no_mem;
		}
	}
	else
	{
		if ((ret = _update_firsts_of_sentence(parser, d, r)) < 0)
			goto exit_no_mem;
	}

	/* if an increase in size is observed, notify the caller */
	if (prev_result_size != r->firsts.size)
		ret = 1;

exit_skip:
	if (result)
		*result = n;

#ifdef SH_DETAILED_DEBUG
	printf("Firsts for: ");
	_print_sentence(parser, d);
	printf("\n");
	_print_first_results(parser, r);
#endif

	return ret;
exit_no_mem:
	return -1;
}

static bool _clear_dfs_flag(shNode *n, void *extra)
{
	((find_first_result *)n->data)->dfs_visited = false;
	return true;
}

static bool _mark_as_complete(shNode *n, void *extra)
{
	((find_first_result *)n->data)->complete = true;
	return true;
}

static find_first_data _build_first_data_for_nonterminal(shParser *parser, size_t nont, unsigned int k)
{
	shVector *nonterminal_rules = sh_vector_data(&INTERNAL->nonterminal_rules);
	int *this_nont_rules = sh_vector_data(&nonterminal_rules[nont]);
	grammar_rule *rules = sh_vector_data(&INTERNAL->rules);

	/* create a sentence with only this nonterminal (any rule of this nonterminal already has this symbol as head) */
	find_first_data d = {
		.sentence = &rules[this_nont_rules[0]].head,
		.sentence_len = 1,
		.sentence_malloced = false,
		.k = k,
	};

	return d;
}

int sh_parser_find_all_firsts(shParser *parser)
{
	sh_map_init(&INTERNAL->firsts, _find_first_data_cpy, sizeof(find_first_data),
			_find_first_result_cpy, sizeof(find_first_result), _find_first_data_cmp);

	for (unsigned int k = 0; k <= INTERNAL->k_in_grammar; ++k)
	{
		int ret;
		do
		{
			for (size_t nont = 0; nont < INTERNAL->nonterminals.size; ++nont)
			{
				find_first_data d = _build_first_data_for_nonterminal(parser, nont, k);
				sh_map_for_each(&INTERNAL->firsts, _clear_dfs_flag);
				ret = sh_parser_find_firsts(parser, &d, NULL);
				if (ret < 0)
					goto exit_error;
			}
		} while (ret);

		sh_map_for_each(&INTERNAL->firsts, _mark_as_complete);

#ifdef SH_DEBUG
		sh_map_inorder(&INTERNAL->firsts, _print_first_result, parser);
#endif
	}
	return 0;
exit_error:
	return -1;
}
