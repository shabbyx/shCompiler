/*
 * Copyright (C) 2007-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shCompiler.
 *
 * shCompiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shCompiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shCompiler.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <errno.h>
#include <assert.h>
#include <shlexer.h>
#include <shparser.h>
#include "lexer_internal.h"
#include "parser_internal.h"
#include "grammar.h"
#include "utils.h"
#include "nonstd.h"

#define PARSER_INTERNAL ((parser_internal *)parser->internal)
#define LEXER_INTERNAL ((lexer_internal *)parser->lexer->internal)

static const char *_keywords =
"\n";

static const char *_token_ID =
"start = 0\n"
"trap_state = 1\n"
"alphabet_size = 21\n"
"size = 4\n"
"states =\n"
"0 1 1 1 1 2 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"1 1 1 1 2 2 1 1 1 1 1 1 1 1 3 1 1 1 1 1 1 1\n"
"0 1 1 1 2 2 1 1 1 1 1 1 1 1 3 1 1 1 1 1 1 1\n"
"name = ID\n"
"alphabet =\n"
"space 1 32\n"
"new_line 1 10\n"
"tab 1 9\n"
"digit 10 48 49 50 51 52 53 54 55 56 57\n"
"letter 55 46 47 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90 95 97 98 99 100 101 102 103 104 105 106 107 108 109 110 111 112 113 114 115 116 117 118 119 120 121 122\n"
"left_paren 1 40\n"
"right_paren 1 41\n"
"bar 1 124\n"
"star 1 42\n"
"plus 1 43\n"
"question 1 63\n"
"equal 1 61\n"
"colon 1 58\n"
"dash 1 45\n"
"angle_right 1 62\n"
"semicolon 1 59\n"
"hash 1 35\n"
"backslash 1 92\n"
"at 1 64\n"
"exclamation 1 33\n"
"illegal 14 34 36 37 38 39 44 60 91 93 94 96 123 125 126\n";

static const char *_token_EQUAL =
"start = 0\n"
"trap_state = 1\n"
"alphabet_size = 21\n"
"size = 3\n"
"states =\n"
"0 1 1 1 1 1 1 1 1 1 1 1 2 1 1 1 1 1 1 1 1 1\n"
"0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"name = EQUAL\n"
"alphabet =\n"
"space 1 32\n"
"new_line 1 10\n"
"tab 1 9\n"
"digit 10 48 49 50 51 52 53 54 55 56 57\n"
"letter 55 46 47 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90 95 97 98 99 100 101 102 103 104 105 106 107 108 109 110 111 112 113 114 115 116 117 118 119 120 121 122\n"
"left_paren 1 40\n"
"right_paren 1 41\n"
"bar 1 124\n"
"star 1 42\n"
"plus 1 43\n"
"question 1 63\n"
"equal 1 61\n"
"colon 1 58\n"
"dash 1 45\n"
"angle_right 1 62\n"
"semicolon 1 59\n"
"hash 1 35\n"
"backslash 1 92\n"
"at 1 64\n"
"exclamation 1 33\n"
"illegal 14 34 36 37 38 39 44 60 91 93 94 96 123 125 126\n";

static const char *_token_END =
"start = 0\n"
"trap_state = 1\n"
"alphabet_size = 21\n"
"size = 3\n"
"states =\n"
"0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 2 1 1 1 1 1\n"
"0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"name = END\n"
"alphabet =\n"
"space 1 32\n"
"new_line 1 10\n"
"tab 1 9\n"
"digit 10 48 49 50 51 52 53 54 55 56 57\n"
"letter 55 46 47 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90 95 97 98 99 100 101 102 103 104 105 106 107 108 109 110 111 112 113 114 115 116 117 118 119 120 121 122\n"
"left_paren 1 40\n"
"right_paren 1 41\n"
"bar 1 124\n"
"star 1 42\n"
"plus 1 43\n"
"question 1 63\n"
"equal 1 61\n"
"colon 1 58\n"
"dash 1 45\n"
"angle_right 1 62\n"
"semicolon 1 59\n"
"hash 1 35\n"
"backslash 1 92\n"
"at 1 64\n"
"exclamation 1 33\n"
"illegal 14 34 36 37 38 39 44 60 91 93 94 96 123 125 126\n";

static const char *_token_WHITE_SPACE =
"start = 0\n"
"trap_state = 2\n"
"alphabet_size = 21\n"
"size = 7\n"
"states =\n"
"0 1 1 1 2 2 2 2 2 2 2 2 2 2 2 2 2 3 2 2 2 2\n"
"1 1 1 1 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2\n"
"0 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2\n"
"0 3 4 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 5 3 3 3\n"
"1 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2\n"
"0 3 6 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 5 3 3 3\n"
"1 3 4 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 5 3 3 3\n"
"name = WHITE_SPACE\n"
"alphabet =\n"
"space 1 32\n"
"new_line 1 10\n"
"tab 1 9\n"
"digit 10 48 49 50 51 52 53 54 55 56 57\n"
"letter 55 46 47 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90 95 97 98 99 100 101 102 103 104 105 106 107 108 109 110 111 112 113 114 115 116 117 118 119 120 121 122\n"
"left_paren 1 40\n"
"right_paren 1 41\n"
"bar 1 124\n"
"star 1 42\n"
"plus 1 43\n"
"question 1 63\n"
"equal 1 61\n"
"colon 1 58\n"
"dash 1 45\n"
"angle_right 1 62\n"
"semicolon 1 59\n"
"hash 1 35\n"
"backslash 1 92\n"
"at 1 64\n"
"exclamation 1 33\n"
"illegal 14 34 36 37 38 39 44 60 91 93 94 96 123 125 126\n";

static const char *_token_PAREN_OPEN =
"start = 0\n"
"trap_state = 1\n"
"alphabet_size = 21\n"
"size = 3\n"
"states =\n"
"0 1 1 1 1 1 2 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"name = PAREN_OPEN\n"
"alphabet =\n"
"space 1 32\n"
"new_line 1 10\n"
"tab 1 9\n"
"digit 10 48 49 50 51 52 53 54 55 56 57\n"
"letter 55 46 47 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90 95 97 98 99 100 101 102 103 104 105 106 107 108 109 110 111 112 113 114 115 116 117 118 119 120 121 122\n"
"left_paren 1 40\n"
"right_paren 1 41\n"
"bar 1 124\n"
"star 1 42\n"
"plus 1 43\n"
"question 1 63\n"
"equal 1 61\n"
"colon 1 58\n"
"dash 1 45\n"
"angle_right 1 62\n"
"semicolon 1 59\n"
"hash 1 35\n"
"backslash 1 92\n"
"at 1 64\n"
"exclamation 1 33\n"
"illegal 14 34 36 37 38 39 44 60 91 93 94 96 123 125 126\n";

static const char *_token_PAREN_CLOSE =
"start = 0\n"
"trap_state = 1\n"
"alphabet_size = 21\n"
"size = 3\n"
"states =\n"
"0 1 1 1 1 1 1 2 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"name = PAREN_CLOSE\n"
"alphabet =\n"
"space 1 32\n"
"new_line 1 10\n"
"tab 1 9\n"
"digit 10 48 49 50 51 52 53 54 55 56 57\n"
"letter 55 46 47 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90 95 97 98 99 100 101 102 103 104 105 106 107 108 109 110 111 112 113 114 115 116 117 118 119 120 121 122\n"
"left_paren 1 40\n"
"right_paren 1 41\n"
"bar 1 124\n"
"star 1 42\n"
"plus 1 43\n"
"question 1 63\n"
"equal 1 61\n"
"colon 1 58\n"
"dash 1 45\n"
"angle_right 1 62\n"
"semicolon 1 59\n"
"hash 1 35\n"
"backslash 1 92\n"
"at 1 64\n"
"exclamation 1 33\n"
"illegal 14 34 36 37 38 39 44 60 91 93 94 96 123 125 126\n";

static const char *_token_NUMBER =
"start = 0\n"
"trap_state = 1\n"
"alphabet_size = 21\n"
"size = 3\n"
"states =\n"
"0 1 1 1 2 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"1 1 1 1 2 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"name = NUMBER\n"
"alphabet =\n"
"space 1 32\n"
"new_line 1 10\n"
"tab 1 9\n"
"digit 10 48 49 50 51 52 53 54 55 56 57\n"
"letter 55 46 47 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90 95 97 98 99 100 101 102 103 104 105 106 107 108 109 110 111 112 113 114 115 116 117 118 119 120 121 122\n"
"left_paren 1 40\n"
"right_paren 1 41\n"
"bar 1 124\n"
"star 1 42\n"
"plus 1 43\n"
"question 1 63\n"
"equal 1 61\n"
"colon 1 58\n"
"dash 1 45\n"
"angle_right 1 62\n"
"semicolon 1 59\n"
"hash 1 35\n"
"backslash 1 92\n"
"at 1 64\n"
"exclamation 1 33\n"
"illegal 14 34 36 37 38 39 44 60 91 93 94 96 123 125 126\n";

static const char *_token_MAYBE =
"start = 0\n"
"trap_state = 1\n"
"alphabet_size = 21\n"
"size = 3\n"
"states =\n"
"0 1 1 1 1 1 1 1 1 1 1 2 1 1 1 1 1 1 1 1 1 1\n"
"0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"name = MAYBE\n"
"alphabet =\n"
"space 1 32\n"
"new_line 1 10\n"
"tab 1 9\n"
"digit 10 48 49 50 51 52 53 54 55 56 57\n"
"letter 55 46 47 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90 95 97 98 99 100 101 102 103 104 105 106 107 108 109 110 111 112 113 114 115 116 117 118 119 120 121 122\n"
"left_paren 1 40\n"
"right_paren 1 41\n"
"bar 1 124\n"
"star 1 42\n"
"plus 1 43\n"
"question 1 63\n"
"equal 1 61\n"
"colon 1 58\n"
"dash 1 45\n"
"angle_right 1 62\n"
"semicolon 1 59\n"
"hash 1 35\n"
"backslash 1 92\n"
"at 1 64\n"
"exclamation 1 33\n"
"illegal 14 34 36 37 38 39 44 60 91 93 94 96 123 125 126\n";

static const char *_token_KLEENE_STAR =
"start = 0\n"
"trap_state = 1\n"
"alphabet_size = 21\n"
"size = 3\n"
"states =\n"
"0 1 1 1 1 1 1 1 1 2 1 1 1 1 1 1 1 1 1 1 1 1\n"
"0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"name = KLEENE_STAR\n"
"alphabet =\n"
"space 1 32\n"
"new_line 1 10\n"
"tab 1 9\n"
"digit 10 48 49 50 51 52 53 54 55 56 57\n"
"letter 55 46 47 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90 95 97 98 99 100 101 102 103 104 105 106 107 108 109 110 111 112 113 114 115 116 117 118 119 120 121 122\n"
"left_paren 1 40\n"
"right_paren 1 41\n"
"bar 1 124\n"
"star 1 42\n"
"plus 1 43\n"
"question 1 63\n"
"equal 1 61\n"
"colon 1 58\n"
"dash 1 45\n"
"angle_right 1 62\n"
"semicolon 1 59\n"
"hash 1 35\n"
"backslash 1 92\n"
"at 1 64\n"
"exclamation 1 33\n"
"illegal 14 34 36 37 38 39 44 60 91 93 94 96 123 125 126\n";

static const char *_token_OR =
"start = 0\n"
"trap_state = 1\n"
"alphabet_size = 21\n"
"size = 3\n"
"states =\n"
"0 1 1 1 1 1 1 1 2 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"name = OR\n"
"alphabet =\n"
"space 1 32\n"
"new_line 1 10\n"
"tab 1 9\n"
"digit 10 48 49 50 51 52 53 54 55 56 57\n"
"letter 55 46 47 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90 95 97 98 99 100 101 102 103 104 105 106 107 108 109 110 111 112 113 114 115 116 117 118 119 120 121 122\n"
"left_paren 1 40\n"
"right_paren 1 41\n"
"bar 1 124\n"
"star 1 42\n"
"plus 1 43\n"
"question 1 63\n"
"equal 1 61\n"
"colon 1 58\n"
"dash 1 45\n"
"angle_right 1 62\n"
"semicolon 1 59\n"
"hash 1 35\n"
"backslash 1 92\n"
"at 1 64\n"
"exclamation 1 33\n"
"illegal 14 34 36 37 38 39 44 60 91 93 94 96 123 125 126\n";

static const char *_token_AT_LEAST_ONCE =
"start = 0\n"
"trap_state = 1\n"
"alphabet_size = 21\n"
"size = 3\n"
"states =\n"
"0 1 1 1 1 1 1 1 1 1 2 1 1 1 1 1 1 1 1 1 1 1\n"
"0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"name = AT_LEAST_ONCE\n"
"alphabet =\n"
"space 1 32\n"
"new_line 1 10\n"
"tab 1 9\n"
"digit 10 48 49 50 51 52 53 54 55 56 57\n"
"letter 55 46 47 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90 95 97 98 99 100 101 102 103 104 105 106 107 108 109 110 111 112 113 114 115 116 117 118 119 120 121 122\n"
"left_paren 1 40\n"
"right_paren 1 41\n"
"bar 1 124\n"
"star 1 42\n"
"plus 1 43\n"
"question 1 63\n"
"equal 1 61\n"
"colon 1 58\n"
"dash 1 45\n"
"angle_right 1 62\n"
"semicolon 1 59\n"
"hash 1 35\n"
"backslash 1 92\n"
"at 1 64\n"
"exclamation 1 33\n"
"illegal 14 34 36 37 38 39 44 60 91 93 94 96 123 125 126\n";

static const char *_token_ACTION_ROUTINE =
"start = 0\n"
"trap_state = 1\n"
"alphabet_size = 21\n"
"size = 5\n"
"states =\n"
"0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 2 1 1\n"
"0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"0 1 1 1 1 3 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"1 1 1 1 3 3 1 1 1 1 1 1 1 1 4 1 1 1 1 1 1 1\n"
"0 1 1 1 3 3 1 1 1 1 1 1 1 1 4 1 1 1 1 1 1 1\n"
"name = ACTION_ROUTINE\n"
"alphabet =\n"
"space 1 32\n"
"new_line 1 10\n"
"tab 1 9\n"
"digit 10 48 49 50 51 52 53 54 55 56 57\n"
"letter 55 46 47 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90 95 97 98 99 100 101 102 103 104 105 106 107 108 109 110 111 112 113 114 115 116 117 118 119 120 121 122\n"
"left_paren 1 40\n"
"right_paren 1 41\n"
"bar 1 124\n"
"star 1 42\n"
"plus 1 43\n"
"question 1 63\n"
"equal 1 61\n"
"colon 1 58\n"
"dash 1 45\n"
"angle_right 1 62\n"
"semicolon 1 59\n"
"hash 1 35\n"
"backslash 1 92\n"
"at 1 64\n"
"exclamation 1 33\n"
"illegal 14 34 36 37 38 39 44 60 91 93 94 96 123 125 126\n";

static const char *_token_EPR_HANDLER =
"start = 0\n"
"trap_state = 1\n"
"alphabet_size = 21\n"
"size = 5\n"
"states =\n"
"0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 2 1\n"
"0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"0 1 1 1 1 3 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"1 1 1 1 3 3 1 1 1 1 1 1 1 1 4 1 1 1 1 1 1 1\n"
"0 1 1 1 3 3 1 1 1 1 1 1 1 1 4 1 1 1 1 1 1 1\n"
"name = EPR_HANDLER\n"
"alphabet =\n"
"space 1 32\n"
"new_line 1 10\n"
"tab 1 9\n"
"digit 10 48 49 50 51 52 53 54 55 56 57\n"
"letter 55 46 47 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90 95 97 98 99 100 101 102 103 104 105 106 107 108 109 110 111 112 113 114 115 116 117 118 119 120 121 122\n"
"left_paren 1 40\n"
"right_paren 1 41\n"
"bar 1 124\n"
"star 1 42\n"
"plus 1 43\n"
"question 1 63\n"
"equal 1 61\n"
"colon 1 58\n"
"dash 1 45\n"
"angle_right 1 62\n"
"semicolon 1 59\n"
"hash 1 35\n"
"backslash 1 92\n"
"at 1 64\n"
"exclamation 1 33\n"
"illegal 14 34 36 37 38 39 44 60 91 93 94 96 123 125 126\n";

static const char *_token_EXPANDS =
"start = 0\n"
"trap_state = 1\n"
"alphabet_size = 21\n"
"size = 6\n"
"states =\n"
"0 1 1 1 1 1 1 1 1 1 1 1 1 2 3 1 1 1 1 1 1 1\n"
"0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"0 1 1 1 1 1 1 1 1 1 1 1 1 4 1 1 1 1 1 1 1 1\n"
"0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 5 1 1 1 1 1 1\n"
"0 1 1 1 1 1 1 1 1 1 1 1 5 1 1 1 1 1 1 1 1 1\n"
"1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
"name = EXPANDS\n"
"alphabet =\n"
"space 1 32\n"
"new_line 1 10\n"
"tab 1 9\n"
"digit 10 48 49 50 51 52 53 54 55 56 57\n"
"letter 55 46 47 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90 95 97 98 99 100 101 102 103 104 105 106 107 108 109 110 111 112 113 114 115 116 117 118 119 120 121 122\n"
"left_paren 1 40\n"
"right_paren 1 41\n"
"bar 1 124\n"
"star 1 42\n"
"plus 1 43\n"
"question 1 63\n"
"equal 1 61\n"
"colon 1 58\n"
"dash 1 45\n"
"angle_right 1 62\n"
"semicolon 1 59\n"
"hash 1 35\n"
"backslash 1 92\n"
"at 1 64\n"
"exclamation 1 33\n"
"illegal 14 34 36 37 38 39 44 60 91 93 94 96 123 125 126\n";

#define RULE_COUNT 23

static const char *_rules[RULE_COUNT] =
{
	"grammar_file -> @grammarStart rules @grammarEnd",
	"rules -> rule_def rules",
	"rules -> ",
	"rule_def -> @newRule ID @pushName EXPANDS or_expr END @endRule",
	"or_expr -> cat_expr more_or_expr",
	"more_or_expr -> @addRuleBody OR @appendOr cat_expr more_or_expr",
	"more_or_expr -> ",
	"cat_expr -> unary_expr more_cat_expr",
	"cat_expr -> ",
	"more_cat_expr -> unary_expr more_cat_expr",
	"more_cat_expr -> ",
	"unary_expr -> primary_expr unary_operators",
	"unary_expr -> handler",
	"unary_operators -> unary_operator unary_operators",
	"unary_operators -> ",
	"unary_operator -> KLEENE_STAR @star",
	"unary_operator -> AT_LEAST_ONCE @atLeastOnce",
	"unary_operator -> MAYBE @maybe",
	"unary_operator -> NUMBER @times",
	"primary_expr -> ID @push",
	"primary_expr -> PAREN_OPEN @parenStart or_expr @addRuleBody @parenEnd PAREN_CLOSE",
	"handler -> ACTION_ROUTINE @push",
	"handler -> EPR_HANDLER @push"
};

static const char *_grammar_table =
"grammar_file 1 0\n"
"grammar_file 1 1 ID\n"
"rules 3 0\n"
"rules 2 1 ID\n"
"(@grammarStart) 0 0\n"
"(@grammarStart) 0 1 ID\n"
"rule_def 6 1 ID\n"
"or_expr 7 1 ACTION_ROUTINE\n"
"or_expr 7 1 END\n"
"or_expr 7 1 EPR_HANDLER\n"
"or_expr 7 1 ID\n"
"or_expr 7 1 OR\n"
"or_expr 7 1 PAREN_CLOSE\n"
"or_expr 7 1 PAREN_OPEN\n"
"(@newRule) 4 1 ID\n"
"((@newRule)ID~@pushName) 5 1 ID\n"
"cat_expr 12 1 ACTION_ROUTINE\n"
"cat_expr 13 1 END\n"
"cat_expr 12 1 EPR_HANDLER\n"
"cat_expr 12 1 ID\n"
"cat_expr 13 1 OR\n"
"cat_expr 13 1 PAREN_CLOSE\n"
"cat_expr 12 1 PAREN_OPEN\n"
"more_or_expr 11 1 END\n"
"more_or_expr 10 1 OR\n"
"more_or_expr 11 1 PAREN_CLOSE\n"
"(@addRuleBody) 8 1 OR\n"
"((@addRuleBody)OR~@appendOr) 9 1 OR\n"
"unary_expr 17 1 ACTION_ROUTINE\n"
"unary_expr 17 1 EPR_HANDLER\n"
"unary_expr 16 1 ID\n"
"unary_expr 16 1 PAREN_OPEN\n"
"more_cat_expr 14 1 ACTION_ROUTINE\n"
"more_cat_expr 15 1 END\n"
"more_cat_expr 14 1 EPR_HANDLER\n"
"more_cat_expr 14 1 ID\n"
"more_cat_expr 15 1 OR\n"
"more_cat_expr 15 1 PAREN_CLOSE\n"
"more_cat_expr 14 1 PAREN_OPEN\n"
"primary_expr 24 1 ID\n"
"primary_expr 28 1 PAREN_OPEN\n"
"unary_operators 19 1 ACTION_ROUTINE\n"
"unary_operators 18 1 AT_LEAST_ONCE\n"
"unary_operators 19 1 END\n"
"unary_operators 19 1 EPR_HANDLER\n"
"unary_operators 19 1 ID\n"
"unary_operators 18 1 KLEENE_STAR\n"
"unary_operators 18 1 MAYBE\n"
"unary_operators 18 1 NUMBER\n"
"unary_operators 19 1 OR\n"
"unary_operators 19 1 PAREN_CLOSE\n"
"unary_operators 19 1 PAREN_OPEN\n"
"handler 29 1 ACTION_ROUTINE\n"
"handler 30 1 EPR_HANDLER\n"
"unary_operator 21 1 AT_LEAST_ONCE\n"
"unary_operator 20 1 KLEENE_STAR\n"
"unary_operator 22 1 MAYBE\n"
"unary_operator 23 1 NUMBER\n"
"(PAREN_OPEN~@parenStart) 25 1 PAREN_OPEN\n"
"((PAREN_OPEN~@parenStart)or_expr~@addRuleBody) 26 1 PAREN_OPEN\n"
"(((PAREN_OPEN~@parenStart)or_expr~@addRuleBody)@parenEnd) 27 1 PAREN_OPEN\n";

static void _lexer_eh(shLexer *lexer, char *fc, int *lb, int *f, int *l, int *c);
static bool _pleh(shParser *parser, const char *cur, shKtokens *la);

/* action routines */
static void _grammarStart(shParser *parser);
static void _grammarEnd(shParser *parser);
static void _newRule(shParser *parser);
static void _endRule(shParser *parser);
static void _pushName(shParser *parser);
static void _addRuleBody(shParser *parser);
static void _appendOr(shParser *parser);
static void _star(shParser *parser);
static void _atLeastOnce(shParser *parser);
static void _maybe(shParser *parser);
static void _times(shParser *parser);
static void _push(shParser *parser);
static void _parenStart(shParser *parser);
static void _parenEnd(shParser *parser);

typedef struct name_element
{
	bool is_compound;
	shVector/* char */ name;
	shVector/* shVector of grammar_symbol */ rule_bodies;
						/*
						 * usually rules are generated immediately after | or ; however, in case of (x|... the nonterminal of
						 * the rule (which is the compound name generated from the parenthesized expression) is not yet known.
						 * The rule bodies are added here so that when ) is met, they would be added altogether.
						 */
} name_element;

typedef struct body_element
{
	bool start;				/* marks start of rule body */
	grammar_symbol symbol;			/* if !start, then this could be a terminal, non-terminal, action routing or EPR handler */
} body_element;

typedef struct rules_data
{
	shVector/* grammar_rule */ grammar_rules;
	shVector/* grammar_rule */ aux_rules;	/* rules generated by operators (such as *, ? and ()) */
	shStack/* name_lement */ rule_names;
	shStack/* body_element */ rule_body;	/*
						 * body of current rule being read.  Note that if in the middle of rule there was parenthesis,
						 * its body would be added to the same stack, and later retrieved back until start is true
						 */
	bool evaluate;
	int error;
	int missing_token;
	bool eof_error_given;

	sh_grammar_type grammar_type;		/* used to know whether generated rules should be generated suitable for LL(k) or LR(k) */

	sh_lexer_error_handler default_eh;

	shParser *parser;			/* used to detect whether token is terminal or not, and if not, adds it to the parser */
	shParser *grammar_parser;		/* the parser that is reading the grammar file itself */
} rules_data;

static void _charcpy(void *to, void *from)
{
	*(char *)to = *(char *)from;
}

static void _name_element_cpy(void *to, void *from)
{
	*(name_element *)to = *(name_element *)from;
}

static void _body_element_cpy(void *to, void *from)
{
	*(body_element *)to = *(body_element *)from;
}

static void _rulecpy(void *to, void *from)
{
	*(grammar_rule *)to = *(grammar_rule *)from;
}

int sh_lexer_init_to_grammar_file(shLexer *lexer)
{
	int error;
	shDfa dfa;

	error = sh_lexer_init(lexer);
	if (error != SH_LEXER_SUCCESS)
		return error;

	rules_data *rd = malloc(sizeof *rd);
	lexer->user_data = rd;
	if (rd == NULL)
		goto exit_no_mem;
	*rd = (rules_data){ .error = 0 };
	sh_lexer_keywords(lexer, _keywords);
	sh_dfa_load(&dfa, _token_ID);			sh_lexer_add_dfa(lexer, &dfa);
	sh_dfa_load(&dfa, _token_EQUAL);		sh_lexer_add_dfa(lexer, &dfa);
	sh_dfa_load(&dfa, _token_END);			sh_lexer_add_dfa(lexer, &dfa);
	sh_dfa_load(&dfa, _token_WHITE_SPACE);		sh_lexer_add_dfa(lexer, &dfa);
	sh_dfa_load(&dfa, _token_PAREN_OPEN);		sh_lexer_add_dfa(lexer, &dfa);
	sh_dfa_load(&dfa, _token_PAREN_CLOSE);		sh_lexer_add_dfa(lexer, &dfa);
	sh_dfa_load(&dfa, _token_NUMBER);		sh_lexer_add_dfa(lexer, &dfa);
	sh_dfa_load(&dfa, _token_MAYBE);		sh_lexer_add_dfa(lexer, &dfa);
	sh_dfa_load(&dfa, _token_KLEENE_STAR);		sh_lexer_add_dfa(lexer, &dfa);
	sh_dfa_load(&dfa, _token_OR);			sh_lexer_add_dfa(lexer, &dfa);
	sh_dfa_load(&dfa, _token_AT_LEAST_ONCE);	sh_lexer_add_dfa(lexer, &dfa);
	sh_dfa_load(&dfa, _token_ACTION_ROUTINE);	sh_lexer_add_dfa(lexer, &dfa);
	sh_dfa_load(&dfa, _token_EPR_HANDLER);		sh_lexer_add_dfa(lexer, &dfa);
	sh_dfa_load(&dfa, _token_EXPANDS);		sh_lexer_add_dfa(lexer, &dfa);
	rd->missing_token = 0;
	rd->eof_error_given = false;
	rd->default_eh = sh_lexer_set_error_handler(lexer, _lexer_eh);
	lexer->initialized = true;
	return 0;
exit_no_mem:
	printf("insufficient memory when initializing grammar reader\n");
	return -1;
}

int sh_parser_init_to_grammar_file(shParser *parser, shLexer *lexer)
{
	int error;

	error = sh_parser_init(parser);
	if (error != SH_PARSER_SUCCESS)
		return error;

	error = sh_parser_load_llk(parser, lexer, _rules, RULE_COUNT, _grammar_table, 1, NULL);
	if (error)
	{
		_message("error: could not initialize tokens reader (%d)", error);
		return error;
	}
	PARSER_INTERNAL->llk_pleh = _pleh;
	sh_parser_set_action_routine(parser, "@grammarStart", _grammarStart);
	sh_parser_set_action_routine(parser, "@grammarEnd", _grammarEnd);
	sh_parser_set_action_routine(parser, "@newRule", _newRule);
	sh_parser_set_action_routine(parser, "@endRule", _endRule);
	sh_parser_set_action_routine(parser, "@pushName", _pushName);
	sh_parser_set_action_routine(parser, "@addRuleBody", _addRuleBody);
	sh_parser_set_action_routine(parser, "@appendOr", _appendOr);
	sh_parser_set_action_routine(parser, "@star", _star);
	sh_parser_set_action_routine(parser, "@atLeastOnce", _atLeastOnce);
	sh_parser_set_action_routine(parser, "@maybe", _maybe);
	sh_parser_set_action_routine(parser, "@times", _times);
	sh_parser_set_action_routine(parser, "@push", _push);
	sh_parser_set_action_routine(parser, "@parenStart", _parenStart);
	sh_parser_set_action_routine(parser, "@parenEnd", _parenEnd);

	rules_data *rd = lexer->user_data;
	rd->grammar_parser = parser;

	return 0;
}

static void _lexer_eh(shLexer *lexer, char *fc, int *lb, int *f, int *l, int *c)
{
	rules_data *rd = lexer->user_data;
	sh_lexer_error_custom(lexer, lexer->file_name, *l, *c, "warning: no token can be matched, probably because of an invalid character (is it '");
	if (fc[*f] == '\n')
		sh_lexer_error_custom(lexer, NULL, 0, 0, "\\n");
	else if (fc[*f] == '\r')
		sh_lexer_error_custom(lexer, NULL, 0, 0, "\\r");
	else if (fc[*f] == '\t')
		sh_lexer_error_custom(lexer, NULL, 0, 0, "\\t");
	else if (fc[*f] >= ' ' && fc[*f] < 127)
		sh_lexer_error_custom(lexer, NULL, 0, 0, "%c", fc[*f]);
	else
		sh_lexer_error_custom(lexer, NULL, 0, 0, "\\%o", (unsigned int)fc[*f]);
	sh_lexer_error_custom(lexer, NULL, 0, 0, "'?)\n");
	rd->default_eh(lexer, fc, lb, f, l, c);
	rd->error = SH_PARSER_GRAMMAR_ERROR;
}

#define stack_clear(s) while (!s.empty()) s.pop()

void sh_compiler_GF_init(shLexer *l, shParser *p, sh_grammar_type type)
{
	rules_data *rd = l->user_data;
	rd->error = SH_PARSER_SUCCESS;
	rd->parser = p;
	rd->grammar_type = type;
}

void sh_compiler_GF_cleanup(shLexer *l)
{
	rules_data *rd = l->user_data;
	grammar_rule *grammar_rules = sh_vector_data(&rd->grammar_rules);

	for (size_t i = 0; i < rd->grammar_rules.size; ++i)
		sh_vector_free(&grammar_rules[i].body);
	sh_vector_free(&rd->grammar_rules);
	sh_vector_free(&rd->aux_rules);
	sh_stack_free(&rd->rule_names);
	sh_stack_free(&rd->rule_body);

	free(rd);
}

shVector sh_compiler_GF_getRules(shLexer *l)
{
	rules_data *rd = l->user_data;
	return rd->grammar_rules;
}

int sh_compiler_GF_error(shLexer *l)
{
	rules_data *rd = l->user_data;
	return rd->error;
}

static void _grammarStart(shParser *parser)
{
	rules_data *rd = parser->lexer->user_data;
	rd->evaluate = true;
	sh_vector_init(&rd->grammar_rules, _rulecpy, sizeof(grammar_rule));
	sh_vector_init_similar(&rd->aux_rules, &rd->grammar_rules);
	sh_stack_init(&rd->rule_names, _name_element_cpy, sizeof(name_element));
	sh_stack_init(&rd->rule_body, _body_element_cpy, sizeof(body_element));
}

static void _grammarEnd(shParser *parser)
{
	rules_data *rd = parser->lexer->user_data;
	sh_vector_concat(&rd->grammar_rules, &rd->aux_rules);	/* append auxiliary rules to the end of rules */
}

static void _markNewRule(rules_data *rd)
{
	body_element b;
	b.start = true;
	sh_stack_push(&rd->rule_body, &b);
}

static inline bool _symbols_are_equal(const grammar_symbol *s1, const grammar_symbol *s2)
{
	return s1->type == s2->type && s1->id == s2->id;
}

static bool _rules_are_equal(grammar_rule *r1, grammar_rule *r2)
{
	if (!_symbols_are_equal(&r1->head, &r2->head) || r1->body.size != r2->body.size)
		return false;
	else
	{
		grammar_symbol *s1 = sh_vector_data(&r1->body);
		grammar_symbol *s2 = sh_vector_data(&r2->body);
		for (size_t i = 0; i < r1->body.size; ++i)
			if (!_symbols_are_equal(&s1[i], &s2[i]))
				return false;
	}
	return true;
}

static void _addRule(rules_data *rd, grammar_rule *rule)
{
	bool duplicate = false;
	grammar_rule *grammar_rules = sh_vector_data(&rd->grammar_rules);
	for (size_t i = 0; i < rd->grammar_rules.size; ++i)
		if (_rules_are_equal(&grammar_rules[i], rule))
		{
			duplicate = true;
			break;
		}
	if (!duplicate)
		sh_vector_append(&rd->grammar_rules, rule);
	else
	{
		shLexer *lexer = rd->grammar_parser->lexer;
		sh_lexer_error(lexer, "warning: duplicate rule");
		sh_lexer_error_custom(lexer, lexer->file_name, lexer->line, lexer->column, "note: %s ->",
				sh_parser_symbol_name(rd->parser, rule->head));

		grammar_symbol *symbols = sh_vector_data(&rule->body);
		for (size_t i = 0; i < rule->body.size; ++i)
			sh_lexer_error_custom(lexer, NULL, 0, 0, " %s", sh_parser_symbol_name(rd->parser, symbols[i]));
		sh_lexer_error_custom(lexer, NULL, 0, 0, "\n");

		sh_vector_free(&rule->body);
	}
}

static void _addAuxRule(rules_data *rd, grammar_rule *rule)
{
	bool duplicate = false;
	grammar_rule *aux_rules = sh_vector_data(&rd->aux_rules);
	for (size_t i = 0; i < rd->aux_rules.size; ++i)
		if (_rules_are_equal(&aux_rules[i], rule))
		{
			duplicate = true;
			break;
		}
	if (!duplicate)
		sh_vector_append(&rd->aux_rules, rule);
	else
		sh_vector_free(&rule->body);
}

static void _appendToName(rules_data *rd, const char *str)
{
	name_element *top = sh_stack_top(&rd->rule_names);
	char *name = sh_vector_data(&top->name);
	size_t last = top->name.size;
	if (str[0] != '(' && str[0] != '|' && str[0] != '*' && str[0] != '+' && str[0] != '?' && str[0] != ')'
			&& last != 0 && name[last - 1] != '(' && name[last - 1] != '|' && name[last - 1] != '*'
			&& name[last - 1] != '+' && name[last - 1] != '?' && name[last - 1] != ')')
	{
		char delim = '~';
		sh_vector_append(&top->name, &delim);
	}
	sh_vector_concat_array(&top->name, str, strlen(str));
}

static void _newRule(shParser *parser)
{
	rules_data *rd = parser->lexer->user_data;

	assert(rd->rule_body.size == 0);
	assert(rd->rule_names.size == 0);
	sh_stack_clear(&rd->rule_body);		/* they should be empty anyway! */
	sh_stack_clear(&rd->rule_names);	/* the above assertions are to test this */

	_markNewRule(rd);
}

static void _grammar_symbol_cpy(void *a, void *b)
{
	*(grammar_symbol *)a = *(grammar_symbol *)b;
}

static void _extract_body(rules_data *rd, shVector *body)
{
	/* the body needs to be prepended to the body.  A helper vector is used to optimize the transition */
	shVector help;

	sh_vector_init_similar(&help, body);
	while (rd->rule_body.size > 0)
	{
		body_element *top = sh_stack_top(&rd->rule_body);
		if (top->start)
			break;
		sh_vector_append(&help, &top->symbol);
		sh_stack_pop(&rd->rule_body);
	}

	grammar_symbol *help_symbols = sh_vector_data(&help);
	for (size_t i = help.size; i > 0; --i)
		sh_vector_append(body, &help_symbols[i - 1]);

	sh_vector_free(&help);
}

static void _endRule(shParser *parser)
{
	rules_data *rd = parser->lexer->user_data;
	grammar_rule r;
	int nul = '\0';
	sh_vector_init(&r.body, _grammar_symbol_cpy, sizeof(grammar_symbol));
	name_element *top_name = sh_stack_top(&rd->rule_names);
	sh_vector_append(&top_name->name, &nul);
	char *name = sh_vector_uninline(&top_name->name);
	r.head = sh_parser_add_symbol(rd->parser, name);
	sh_stack_pop(&rd->rule_names);
	_extract_body(rd, &r.body);
	if (rd->rule_body.size == 0)
	{
		sh_lexer_error(parser->lexer, "internal error: no start mark when retrieving rule body");
		sh_parser_stop(parser, -1);
		rd->error = SH_PARSER_INTERNAL_ERROR;
	}
	else
		sh_stack_pop(&rd->rule_body);
	_addRule(rd, &r);
	if (name == NULL)
	{
		sh_parser_stop(parser, -1);
		rd->error = SH_PARSER_NO_MEM;
	}
}

static void _pushName(shParser *parser)
{
	rules_data *rd = parser->lexer->user_data;
	name_element n;
	n.is_compound = false;
	sh_vector_init(&n.name, _charcpy, sizeof(char));
	sh_vector_concat_array(&n.name, parser->lexer->current_token, strlen(parser->lexer->current_token));
	sh_vector_init(&n.rule_bodies, sh_vector_assign, sizeof(shVector));
	const char *tmp = parser->lexer->current_token;
	if (tmp[0] == '@' || tmp[0] == '!' || sh_map_exists(&((parser_internal *)rd->parser->internal)->terminal_codes, &tmp))
	{
		sh_lexer_error(parser->lexer, "error: cannot have %s on the left side of a rule", tmp[0] == '@'?"an action routine":
				tmp[0] == '!'?"an error production rule":"a terminal token");
		char id[100];
		sprintf(id, "__missing_%d", rd->missing_token++);
		sh_lexer_insert_fake(parser->lexer, "ID", id);
		n.name.size = 0;
		sh_vector_concat_array(&n.name, id, strlen(id));
		rd->evaluate = false;
		rd->error = SH_PARSER_GRAMMAR_ERROR;
	}
	/* add the nonterminal so its id would be determined right now */
	char *name = sh_compiler_string("%.*s%c", (int)n.name.size, (char *)sh_vector_data(&n.name), '\0');
	sh_parser_add_symbol(rd->parser, name);
	sh_stack_push(&rd->rule_names, &n);
	if (name == NULL)
	{
		sh_parser_stop(parser, -1);
		rd->error = SH_PARSER_NO_MEM;
	}
}

static void _addRuleBody(shParser *parser)
{
	rules_data *rd = parser->lexer->user_data;
	shVector body;
	sh_vector_init(&body, _grammar_symbol_cpy, sizeof(grammar_symbol));
	_extract_body(rd, &body);
	if (rd->rule_body.size == 0)
	{
		sh_lexer_error(parser->lexer, "internal error: no start mark when retrieving rule body");
		sh_parser_stop(parser, -1);
		rd->error = SH_PARSER_INTERNAL_ERROR;
		_markNewRule(rd);
	}
	name_element *top_name = sh_stack_top(&rd->rule_names);
	if (top_name->is_compound)
		sh_vector_append(&top_name->rule_bodies, &body);
	else
	{
		grammar_rule r;
		char *name = sh_compiler_string("%.*s%c", (int)top_name->name.size, (char *)sh_vector_data(&top_name->name), '\0');
		r.head = sh_parser_add_symbol(rd->parser, name);		/* don't pop the name */
		r.body = body;
		_addRule(rd, &r);
		if (name == NULL)
		{
			sh_parser_stop(parser, -1);
			rd->error = SH_PARSER_NO_MEM;
		}
	}
}

static void _appendOr(shParser *parser)
{
	rules_data *rd = parser->lexer->user_data;
	name_element *top_name = sh_stack_top(&rd->rule_names);
	if (top_name->is_compound)
		_appendToName(rd, "|");
}

static void _addStar(rules_data *rd, grammar_symbol *symbol)
{
	grammar_rule r1, r2;
	sh_vector_init(&r1.body, _grammar_symbol_cpy, sizeof(grammar_symbol));
	sh_vector_init(&r2.body, _grammar_symbol_cpy, sizeof(grammar_symbol));
	char *new_name = sh_compiler_string("%s*", sh_parser_symbol_name(rd->parser, *symbol));
	r1.head = sh_parser_add_symbol(rd->parser, new_name);
	/* for LL(k), generate X* -> X X* so it won't be left-recursive */
	if (rd->grammar_type == SH_PARSER_LLK)
	{
		sh_vector_append(&r1.body, symbol);
		sh_vector_append(&r1.body, &r1.head);
	}
	/* for LR(k) instead, generate X* -> X* X so it would parse more efficiently */
	else
	{
		sh_vector_append(&r1.body, &r1.head);
		sh_vector_append(&r1.body, symbol);
	}
	r2.head = r1.head;
	_addAuxRule(rd, &r1);
	_addAuxRule(rd, &r2);
	if (new_name == NULL)
	{
		sh_parser_stop(rd->parser, -1);
		rd->error = SH_PARSER_NO_MEM;
	}
}

static void _star(shParser *parser)
{
	rules_data *rd = parser->lexer->user_data;
	body_element *top = sh_stack_top(&rd->rule_body);
	grammar_symbol last_symbol = top->symbol;
	if (last_symbol.type == ACTION_ROUTINE || last_symbol.type == EPR_HANDLER)
	{
		sh_lexer_error(parser->lexer, "warning: discarding * following %s", last_symbol.type == ACTION_ROUTINE?
				"an action routine":"an error production rule handler");
		return;
	}
	sh_stack_pop(&rd->rule_body);
	body_element b;
	b.start = false;
	char *new_name = sh_compiler_string("%s*", sh_parser_symbol_name(rd->parser, last_symbol));
	b.symbol = sh_parser_add_symbol(rd->parser, new_name);
	sh_stack_push(&rd->rule_body, &b);
	_addStar(rd, &last_symbol);
	name_element *top_name = sh_stack_top(&rd->rule_names);
	if (top_name->is_compound)
		_appendToName(rd, "*");
	if (new_name == NULL)
	{
		sh_parser_stop(parser, -1);
		rd->error = SH_PARSER_NO_MEM;
	}
}

static void _atLeastOnce(shParser *parser)
{
	rules_data *rd = parser->lexer->user_data;
	body_element *top = sh_stack_top(&rd->rule_body);
	grammar_symbol last_symbol = top->symbol;
	if (last_symbol.type == ACTION_ROUTINE || last_symbol.type == EPR_HANDLER)
	{
		sh_lexer_error(parser->lexer, "warning: discarding + following %s", last_symbol.type == ACTION_ROUTINE?
				"an action routine":"an error production rule handler");
		return;
	}
	sh_stack_pop(&rd->rule_body);
	body_element b;
	b.start = false;
	char *new_name = sh_compiler_string("%s+", sh_parser_symbol_name(rd->parser, last_symbol));
	b.symbol = sh_parser_add_symbol(rd->parser, new_name);
	sh_stack_push(&rd->rule_body, &b);
	if (new_name == NULL)
	{
		sh_parser_stop(parser, -1);
		rd->error = SH_PARSER_NO_MEM;
	}

	grammar_rule r;
	sh_vector_init(&r.body, _grammar_symbol_cpy, sizeof(grammar_symbol));
	r.head = b.symbol;
	new_name = sh_compiler_string("%s*", sh_parser_symbol_name(rd->parser, last_symbol));
	grammar_symbol with_star = sh_parser_add_symbol(rd->parser, new_name);
	/* for LL(k), generate X+ -> X X* so it won't be left-recursive */
	if (rd->grammar_type == SH_PARSER_LLK)
	{
		sh_vector_append(&r.body, &last_symbol);
		sh_vector_append(&r.body, &with_star);
	}
	/* for LR(k), generate X+ -> X* X so it would parse more efficiently */
	else
	{
		sh_vector_append(&r.body, &with_star);
		sh_vector_append(&r.body, &last_symbol);
	}
	_addAuxRule(rd, &r);
	_addStar(rd, &last_symbol);		/* in case last_symbol* was not previously defined */
	name_element *top_name = sh_stack_top(&rd->rule_names);
	if (top_name->is_compound)
		_appendToName(rd, "+");
	if (new_name == NULL)
	{
		sh_parser_stop(parser, -1);
		rd->error = SH_PARSER_NO_MEM;
	}
}

static void _maybe(shParser *parser)
{
	rules_data *rd = parser->lexer->user_data;
	body_element *top = sh_stack_top(&rd->rule_body);
	grammar_symbol last_symbol = top->symbol;
	if (last_symbol.type == ACTION_ROUTINE || last_symbol.type == EPR_HANDLER)
	{
		sh_lexer_error(parser->lexer, "warning: discarding ? following %s", last_symbol.type == ACTION_ROUTINE?
				"an action routine":"an error production rule handler");
		return;
	}
	sh_stack_pop(&rd->rule_body);
	body_element b;
	b.start = false;
	char *new_name = sh_compiler_string("%s?", sh_parser_symbol_name(rd->parser, last_symbol));
	b.symbol = sh_parser_add_symbol(rd->parser, new_name);
	sh_stack_push(&rd->rule_body, &b);
	if (new_name == NULL)
	{
		sh_parser_stop(parser, -1);
		rd->error = SH_PARSER_NO_MEM;
	}

	grammar_rule r1, r2;
	sh_vector_init(&r1.body, _grammar_symbol_cpy, sizeof(grammar_symbol));
	sh_vector_init(&r2.body, _grammar_symbol_cpy, sizeof(grammar_symbol));
	r1.head = b.symbol;
	sh_vector_append(&r1.body, &last_symbol);
	r2.head = b.symbol;
	_addAuxRule(rd, &r1);
	_addAuxRule(rd, &r2);
	name_element *top_name = sh_stack_top(&rd->rule_names);
	if (top_name->is_compound)
		_appendToName(rd, "?");
}

static void _times(shParser *parser)
{
	rules_data *rd = parser->lexer->user_data;
	body_element *top = sh_stack_top(&rd->rule_body);
	grammar_symbol last_symbol = top->symbol;
	int count = 0;
	count = strtol(parser->lexer->current_token, NULL, 10);
	if (last_symbol.type == ACTION_ROUTINE || last_symbol.type == EPR_HANDLER)
	{
		sh_lexer_error(parser->lexer, "warning: discarding repetition (%d) following %s", count,
				last_symbol.type == ACTION_ROUTINE? "an action routine":"an error production rule handler");
		return;
	}
	if (count == 0)			/* if count is 0, just drop the last_symbol */
		return;
	sh_stack_pop(&rd->rule_body);
	body_element b;
	b.start = false;
	char *new_name = sh_compiler_string("%s%s", sh_parser_symbol_name(rd->parser, last_symbol), parser->lexer->current_token);
	b.symbol = sh_parser_add_symbol(rd->parser, new_name);
	sh_stack_push(&rd->rule_body, &b);
	if (new_name == NULL)
	{
		sh_parser_stop(parser, -1);
		rd->error = SH_PARSER_NO_MEM;
	}

	grammar_rule r;
	sh_vector_init(&r.body, _grammar_symbol_cpy, sizeof(grammar_symbol));
	r.head = b.symbol;
	for (int i = 0; i < count; ++i)
		sh_vector_append(&r.body, &last_symbol);
	_addAuxRule(rd, &r);
	name_element *top_name = sh_stack_top(&rd->rule_names);
	if (top_name->is_compound)
		_appendToName(rd, parser->lexer->current_token);
}

static void _push(shParser *parser)
{
	rules_data *rd = parser->lexer->user_data;
	body_element b;
	b.start = false;
	char *name = sh_compiler_string("%s", parser->lexer->current_token);
	b.symbol = sh_parser_add_symbol(rd->parser, name);
	sh_stack_push(&rd->rule_body, &b);
	name_element *top_name = sh_stack_top(&rd->rule_names);
	if (top_name->is_compound)
		_appendToName(rd, sh_parser_symbol_name(rd->parser, b.symbol));
	if (name == NULL)
	{
		sh_parser_stop(parser, -1);
		rd->error = SH_PARSER_NO_MEM;
	}
}

static void _parenStart(shParser *parser)
{
	rules_data *rd = parser->lexer->user_data;
	char paren = '(';
	_markNewRule(rd);
	name_element n;
	n.is_compound = true;
	sh_vector_init(&n.name, _charcpy, sizeof(char));
	sh_vector_append(&n.name, &paren);
	sh_vector_init(&n.rule_bodies, sh_vector_assign, sizeof(shVector));
	sh_stack_push(&rd->rule_names, &n);
}

static void _parenEnd(shParser *parser)
{
	rules_data *rd = parser->lexer->user_data;
	_appendToName(rd, ")");
	grammar_rule r;
	char nul = '\0';
	sh_vector_init(&r.body, _grammar_symbol_cpy, sizeof(grammar_symbol));
	name_element *top_name = sh_stack_top(&rd->rule_names);
	sh_vector_append(&top_name->name, &nul);
	char *compound_name = sh_vector_uninline(&top_name->name);

	r.head = sh_parser_add_symbol(rd->parser, compound_name);
	shVector *rule_bodies = sh_vector_data(&top_name->rule_bodies);
	for (size_t i = 0, size = top_name->rule_bodies.size; i < size; ++i)
	{
		r.body = rule_bodies[i];
		_addAuxRule(rd, &r);
	}

	body_element b;
	b.start = false;
	b.symbol = r.head;
	if (rd->rule_body.size == 0)
	{
		sh_lexer_error(parser->lexer, "internal error: no start mark when closing parenthesis");
		sh_parser_stop(parser, -1);
		rd->error = SH_PARSER_INTERNAL_ERROR;
	}
	else
		sh_stack_pop(&rd->rule_body);
	sh_stack_push(&rd->rule_body, &b);
	sh_vector_free(&top_name->rule_bodies);
	sh_stack_pop(&rd->rule_names);
	top_name = sh_stack_top(&rd->rule_names);
	if (top_name->is_compound)	/* if it was parenthesis inside parenthesis */
		_appendToName(rd, sh_parser_symbol_name(rd->parser, r.head));
	if (compound_name == NULL)
	{
		sh_parser_stop(parser, -1);
		rd->error = SH_PARSER_NO_MEM;
	}
}

static bool _pleh(shParser *parser, const char *cur, shKtokens *la)
{
	bool recovered = true;
	shLexer *l = parser->lexer;
	rules_data *rd = l->user_data;
	if (strcmp(cur, "END") == 0)
	{
		sh_lexer_insert_fake(l, cur, NULL);
		sh_lexer_error(l, "error: missing semicolon, inserted ';'");
	}
	else if (strcmp(cur, "EXPANDS") == 0)
	{
		sh_lexer_insert_fake(l, cur, NULL);
		sh_lexer_error(l, "error: missing expand operator, inserted '->'");
	}
	else if (strcmp(cur, "ID") == 0)
	{
		sh_lexer_insert_fake(l, cur, NULL);
		sh_lexer_error(l, "error: missing identifier, inserted '%s'", l->current_token);
	}
	else if (strcmp(cur, "PAREN_CLOSE") == 0)
	{
		sh_lexer_insert_fake(l, cur, NULL);
		sh_lexer_error(l, "error: missing closing parenthesis, inserted ')'");
	}
	else
	{
		const char *type;
		sh_ktokens_at(la, &type, 0);
		if (type == NULL)
			type = "";
		if (strcmp(type, "EQUAL") == 0)
		{
			/* since lookaheads are not yet read, then to get the correct line and column, I shall peek one token ahead */
			int line, column;
			const char *ignore;
			sh_lexer_peek(l, &ignore, 0, &line, &column);
			sh_lexer_error_custom(l, l->file_name, line, column, "error: '=' cannot appear in this file\n");
			sh_parser_push(parser, cur);
			sh_parser_push(parser, type);
		}
		else if (strcmp(type, "") == 0)
		{
			if (!rd->eof_error_given)
				sh_lexer_error(l, "error: unexpected end of file before end of rule defintion");
			rd->eof_error_given = true;
			sh_parser_push(parser, "@grammarEnd");
			sh_parser_push(parser, "@endRule");
		}
		else if (strcmp(cur, "grammar_file") == 0 || strcmp(cur, "rules") == 0)
		{
			if (strcmp(type, "ACTION_ROUTINE") == 0)
			{
				sh_lexer_error(l, "error: unexpected action routine on the left side of rule");
				sh_parser_push(parser, cur);
				sh_parser_push(parser, type);
			}
			else if (strcmp(type, "AT_LEAST_ONCE") == 0)
			{
				sh_lexer_error(l, "error: unexpected '+' on the left side of rule");
				sh_parser_push(parser, cur);
				sh_parser_push(parser, type);
			}
			else if (strcmp(type, "END") == 0)		/* just a single semicolon, ignore it */
			{
				sh_parser_push(parser, cur);
				sh_parser_push(parser, type);
			}
			else if (strcmp(type, "EPR_HANDLER") == 0)
			{
				sh_lexer_error(l, "error: unexpected error production rule handler on the left side of rule");
				sh_parser_push(parser, cur);
				sh_parser_push(parser, type);
			}
			else if (strcmp(type, "EXPANDS") == 0)
			{
				sh_lexer_error(l, "error: missing non-terminal on the left side of rule");
				char id[100];
				sprintf(id, "__missing_%d", rd->missing_token++);
				sh_lexer_insert_fake(l, "ID", id);
				sh_parser_push(parser, "rules");
				sh_parser_push(parser, "@endRule");
				sh_parser_push(parser, "END");
				sh_parser_push(parser, "or_expr");
				sh_parser_push(parser, "EXPANDS");
				sh_parser_push(parser, "@pushName");
				/* sh_parser_push(parser, "ID");	** missing ID faked */
				sh_parser_push(parser, "@newRule");
			}
			else if (strcmp(type, "KLEENE_STAR") == 0)
			{
				sh_lexer_error(l, "error: unexpected '*' on the left side of rule");
				sh_parser_push(parser, cur);
				sh_parser_push(parser, type);
			}
			else if (strcmp(type, "MAYBE") == 0)
			{
				sh_lexer_error(l, "error: unexpected '?' on the left side of rule");
				sh_parser_push(parser, cur);
				sh_parser_push(parser, type);
			}
			else if (strcmp(type, "NUMBER") == 0)
			{
				sh_lexer_error(l, "error: unexpected repetition on the left side of rule");
				sh_lexer_error(l, "note:  identifiers cannot start with a digit");
				sh_parser_push(parser, cur);
				sh_parser_push(parser, type);
			}
			else if (strcmp(type, "OR") == 0)
			{
				sh_lexer_error(l, "error: unexpected '|' on the left side of rule");
				sh_parser_push(parser, cur);
				sh_parser_push(parser, type);
			}
			else if (strcmp(type, "PAREN_CLOSE") == 0)
			{
				sh_lexer_error(l, "error: unexpected ')' on the left side of rule");
				sh_parser_push(parser, cur);
				sh_parser_push(parser, type);
			}
			else if (strcmp(type, "PAREN_OPEN") == 0)
			{
				sh_lexer_error(l, "error: unexpected '(' on the left side of rule");
				sh_parser_push(parser, cur);
				sh_parser_push(parser, type);
			}
			else
				goto exit_unhandled;
		}
		else if (strcmp(cur, "rule_def") == 0)
		{
			if (strcmp(type, "ACTION_ROUTINE") == 0)
			{
				sh_lexer_error(l, "error: unexpected action routine on the left side of rule");
				sh_parser_push(parser, cur);
				sh_parser_push(parser, type);
			}
			else if (strcmp(type, "AT_LEAST_ONCE") == 0)
			{
				sh_lexer_error(l, "error: unexpected '+' on the left side of rule");
				sh_parser_push(parser, cur);
				sh_parser_push(parser, type);
			}
			else if (strcmp(type, "END") == 0)		/* just a single semicolon, ignore it */
			{
				sh_parser_push(parser, cur);
				sh_parser_push(parser, type);
			}
			else if (strcmp(type, "EPR_HANDLER") == 0)
			{
				sh_lexer_error(l, "error: unexpected error production rule handler on the left side of rule");
				sh_parser_push(parser, cur);
				sh_parser_push(parser, type);
			}
			else if (strcmp(type, "EXPANDS") == 0)
			{
				sh_lexer_error(l, "error: missing non-terminal on the left side of rule");
				char id[100];
				sprintf(id, "__missing_%d", rd->missing_token++);
				sh_lexer_insert_fake(l, "ID", id);
				sh_parser_push(parser, "@endRule");
				sh_parser_push(parser, "END");
				sh_parser_push(parser, "or_expr");
				sh_parser_push(parser, "EXPANDS");
				sh_parser_push(parser, "@pushName");
				/* sh_parser_push(parser, "ID");	** missing ID faked */
				sh_parser_push(parser, "@newRule");
			}
			else if (strcmp(type, "KLEENE_STAR") == 0)
			{
				sh_lexer_error(l, "error: unexpected '*' on the left side of rule");
				sh_parser_push(parser, cur);
				sh_parser_push(parser, type);
			}
			else if (strcmp(type, "MAYBE") == 0)
			{
				sh_lexer_error(l, "error: unexpected '?' on the left side of rule");
				sh_parser_push(parser, cur);
				sh_parser_push(parser, type);
			}
			else if (strcmp(type, "NUMBER") == 0)
			{
				sh_lexer_error(l, "error: unexpected repetition on the left side of rule");
				sh_lexer_error(l, "note:  identifiers cannot start with a digit");
				sh_parser_push(parser, cur);
				sh_parser_push(parser, type);
			}
			else if (strcmp(type, "OR") == 0)
			{
				sh_lexer_error(l, "error: unexpected '|' on the left side of rule");
				sh_parser_push(parser, cur);
				sh_parser_push(parser, type);
			}
			else if (strcmp(type, "PAREN_CLOSE") == 0)
			{
				sh_lexer_error(l, "error: unexpected ')' on the left side of rule");
				sh_parser_push(parser, cur);
				sh_parser_push(parser, type);
			}
			else if (strcmp(type, "PAREN_OPEN") == 0)
			{
				sh_lexer_error(l, "error: unexpected '(' on the left side of rule");
				sh_parser_push(parser, cur);
				sh_parser_push(parser, type);
			}
			else
				goto exit_unhandled;
		}
		else if (strcmp(cur, "or_expr") == 0 || strcmp(cur, "cat_expr") == 0 || strcmp(cur, "unary_expr") == 0 || strcmp(cur, "primary_expr") == 0)
		{
			if (strcmp(type, "AT_LEAST_ONCE") == 0)
			{
				sh_lexer_error(l, "error: expected expression before '+'");
				sh_parser_push(parser, cur);
				sh_parser_push(parser, type);
			}
			else if (strcmp(type, "EXPANDS") == 0)
			{
				sh_lexer_error(l, "error: missing semicolon at the end of previous rule definition");
			}
			else if (strcmp(type, "KLEENE_STAR") == 0)
			{
				sh_lexer_error(l, "error: expected expression before '*'");
				sh_parser_push(parser, cur);
				sh_parser_push(parser, type);
			}
			else if (strcmp(type, "MAYBE") == 0)
			{
				sh_lexer_error(l, "error: expected expression before '?'");
				sh_parser_push(parser, cur);
				sh_parser_push(parser, type);
			}
			else if (strcmp(type, "NUMBER") == 0)
			{
				sh_lexer_error(l, "error: expected expression before repetition");
				sh_parser_push(parser, cur);
				sh_parser_push(parser, type);
			}
			else
				goto exit_unhandled;
		}
		else if (strcmp(cur, "more_or_expr") == 0)
		{
			if (strcmp(type, "AT_LEAST_ONCE") == 0 || strcmp(type, "ID") == 0 || strcmp(type, "KLEENE_STAR") == 0 || strcmp(type, "MAYBE") == 0
					|| strcmp(type, "NUMBER") == 0 || strcmp(type, "PAREN_OPEN") == 0
					|| strcmp(type, "ACTION_ROUTINE") == 0 || strcmp(type, "EPR_HANDLER") == 0)
			{
				assert(0);	/* Note: this shouldn't happen */
				sh_lexer_error(l, "error: expected '|'");
				sh_parser_push(parser, "more_or_expr");
				sh_parser_push(parser, "cat_expr");
				sh_parser_push(parser, "@appendOr");
				/* sh_parser_push(parser, "OR");	** missing '|' ignored */
				sh_parser_push(parser, "@addRuleBody");
			}
			else if (strcmp(type, "EXPANDS") == 0)
			{
				sh_lexer_error(l, "error: missing semicolon at the end of previous rule definition");
			}
			else
				goto exit_unhandled;
		}
		else if (strcmp(cur, "more_cat_expr") == 0)
		{
			if (strcmp(type, "AT_LEAST_ONCE") == 0)
			{
				assert(0);	/* Note: this shouldn't happen */
				sh_lexer_error(l, "error: expected expression before '+'");
				sh_parser_push(parser, cur);
				sh_parser_push(parser, type);
			}
			else if (strcmp(type, "EXPANDS") == 0)
			{
				/* error will be given with more_or_expr */
				/* sh_lexer_error(l, "error: missing semicolon at the end of previous rule definition"); */
			}
			else if (strcmp(type, "KLEENE_STAR") == 0)
			{
				assert(0);	/* Note: this shouldn't happen */
				sh_lexer_error(l, "error: expected expression before '*'");
				sh_parser_push(parser, cur);
				sh_parser_push(parser, type);
			}
			else if (strcmp(type, "MAYBE") == 0)
			{
				assert(0);	/* Note: this shouldn't happen */
				sh_lexer_error(l, "error: expected expression before '?'");
				sh_parser_push(parser, cur);
				sh_parser_push(parser, type);
			}
			else if (strcmp(type, "NUMBER") == 0)
			{
				assert(0);	/* Note: this shouldn't happen */
				sh_lexer_error(l, "error: expected expression before repetition");
				sh_parser_push(parser, cur);
				sh_parser_push(parser, type);
			}
			else
				goto exit_unhandled;
		}
		else if (strcmp(cur, "unary_operators") == 0)
		{
			if (strcmp(type, "EXPANDS") == 0)
			{
				/* error will be given with more_or_expr */
				/* sh_lexer_error(l, "error: missing semicolon at the end of previous token definition"); */
			}
			else
				goto exit_unhandled;
		}
		else if (strcmp(cur, "handler") == 0)
		{
			if (strcmp(type, "AT_LEAST_ONCE") == 0 || strcmp(type, "END") == 0 || strcmp(type, "EXPANDS") == 0 || strcmp(type, "ID") == 0 || strcmp(type, "OR") == 0
					|| strcmp(type, "KLEENE_STAR") == 0 || strcmp(type, "MAYBE") == 0 || strcmp(type, "NUMBER") == 0
					|| strcmp(type, "PAREN_CLOSE") == 0 || strcmp(type, "PAREN_OPEN") == 0)

			{
				assert(0);	/* Note: this shouldn't happen */
				sh_lexer_error(l, "error: expected to match action routine or error production rule handler");
			}
			else
				goto exit_unhandled;
		}
		else if (strcmp(cur, "unary_operator") == 0)
		{
			if (strcmp(type, "END") == 0 || strcmp(type, "EXPANDS") == 0 || strcmp(type, "ID") == 0 || strcmp(type, "OR") == 0
					|| strcmp(type, "PAREN_CLOSE") == 0 || strcmp(type, "PAREN_OPEN") == 0
					|| strcmp(type, "ACTION_ROUTINE") == 0 || strcmp(type, "EPR_HANDLER") == 0)
			{
				assert(0);	/* Note: this shouldn't happen */
				sh_lexer_error(l, "error: expected unary operator: '*', '+', '?' or a number");
			}
			else
				goto exit_unhandled;
		}
		else
			goto exit_unhandled;
		goto exit_handled;
	exit_unhandled:
		sh_lexer_error(l, "error: unhandled error, expected to match '%s', but found '%s'", cur, type);
	}
exit_handled:
	rd->evaluate = false;
	rd->error = SH_PARSER_GRAMMAR_ERROR;
	return recovered;
}
