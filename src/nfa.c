/*
 * Copyright (C) 2007-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shCompiler.
 *
 * shCompiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shCompiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shCompiler.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <shnfa.h>

#define STATES(n) ((shNfaState *)sh_vector_data(&n->states))
#define STATES_CONST(n) ((const shNfaState *)sh_vector_data_const(&n->states))
#define LAMBDAS(n, s) ((int *)sh_vector_data(&STATES(n)[s].lambdas))
#define LAMBDAS_CONST(n, s) ((const int *)sh_vector_data_const(&STATES_CONST(n)[s].lambdas))
#define NEXTS(n, s, l) ((int *)sh_vector_data(&STATES(n)[s].nexts[l]))
#define NEXTS_CONST(n, s, l) ((const int *)sh_vector_data_const(&STATES_CONST(n)[s].nexts[l]))

static void _intcpy(void *to, void *from)
{
	*(int *)to = *(int *)from;
}

static void _nfacpy(void *to, void *from)
{
	*(shNfaState *)to = *(shNfaState *)from;
}

static void _state_init(shNfaState *s)
{
	sh_vector_init(&s->lambdas, _intcpy, sizeof(int));
	for (int i = 0; i < SH_NFA_MAX_LETTERS; ++i)
		sh_vector_init(&s->nexts[i], _intcpy, sizeof(int));
	s->final = false;
}

static void _state_free(shNfaState *s)
{
	if (s == NULL)
		return;

	sh_vector_free(&s->lambdas);
	for (int i = 0; i < SH_NFA_MAX_LETTERS; ++i)
		sh_vector_free(&s->nexts[i]);
}

static int _state_copy(shNfaState *to, const shNfaState *from)
{
	assert(to && from);

	to->final = from->final;
	sh_vector_init(&to->lambdas, _intcpy, sizeof(int));
	if (sh_vector_concat(&to->lambdas, &from->lambdas))
		return -1;

	for (size_t i = 0; i < SH_NFA_MAX_LETTERS; ++i)
		sh_vector_init(&to->nexts[i], _intcpy, sizeof(int));
	for (size_t i = 0; i < SH_NFA_MAX_LETTERS; ++i)
		if (sh_vector_concat(&to->nexts[i], &from->nexts[i]))
			goto exit_no_mem;

	return 0;
exit_no_mem:
	for (size_t i = 0; i < SH_NFA_MAX_LETTERS; ++i)
		sh_vector_free(&to->nexts[i]);
	return -1;
}

int sh_nfa_init(shNfa *nfa, int letter, int alphabet_size)
{
	shNfaState s;

	if (nfa == NULL)
		return -1;

	sh_vector_init(&nfa->states, _nfacpy, sizeof(shNfaState));
	nfa->start = 0;
	nfa->final = 1;
	/* no memory allocated in sh_vector_init through _state_init, therefore same s can be used for both start and final */
	_state_init(&s);
	s.final = false;
	if (sh_vector_append(&nfa->states, &s))
		goto exit_no_mem;
	s.final = true;
	if (sh_vector_append(&nfa->states, &s))
		goto exit_no_mem;
	if (letter >= 0)
	{
		if (sh_vector_append(&STATES(nfa)[nfa->start].nexts[letter], &nfa->final))
			goto exit_no_mem;
	}
	else
	{
		if (sh_vector_append(&STATES(nfa)[nfa->start].lambdas, &nfa->final))
			goto exit_no_mem;
	}
	nfa->alphabet_size = alphabet_size;
	memset(nfa->alphabet, 0, sizeof(nfa->alphabet));

	return 0;
exit_no_mem:
	sh_nfa_free(nfa);
	return -1;
}

void sh_nfa_free(shNfa *nfa)
{
	for (size_t i = 0; i < nfa->states.size; ++i)
		_state_free(&STATES(nfa)[i]);
	sh_vector_free(&nfa->states);
	memset(nfa->alphabet, 0, sizeof(nfa->alphabet));
}

void sh_nfa_shallow_free(shNfa *nfa)
{
	sh_vector_free(&nfa->states);
	memset(nfa->alphabet, 0, sizeof(nfa->alphabet));
}

int sh_nfa_copy(shNfa *nfa, const shNfa *second)
{
	shNfaState temp;
	const shNfaState *from;

	if (nfa == NULL || second == NULL)
		return -1;

	nfa->start = second->start;
	nfa->final = second->final;
	sh_vector_init(&nfa->states, _nfacpy, sizeof(shNfaState));
	sh_vector_reserve(&nfa->states, second->states.size);
	from = STATES_CONST(second);
	for (size_t i = 0, size = second->states.size; i < size; ++i)
	{
		if (_state_copy(&temp, &from[i]))
			goto exit_no_mem;
		if (sh_vector_append(&nfa->states, &temp))
		{
			_state_free(&temp);
			goto exit_no_mem;
		}
	}
	nfa->alphabet_size = second->alphabet_size;
	memcpy(nfa->alphabet, second->alphabet, sizeof(nfa->alphabet));

	return 0;
exit_no_mem:
	sh_nfa_free(nfa);
	return -1;
}

static int _states_cat_common(shNfa *nfa, const shNfa *second)
{
	size_t offset_second;

	offset_second = nfa->states.size;
	if (sh_vector_concat(&nfa->states, &second->states))
		return -1;

	for (size_t i = 0, size = second->states.size; i < size; ++i)
	{
		shNfaState *cur = &STATES(nfa)[offset_second + i];
		int *tos;

		/* Correct all indices: */
		tos = sh_vector_data(&cur->lambdas);
		for (size_t k = 0; k < cur->lambdas.size; ++k)
			tos[k] += offset_second;
		for (size_t j = 0; j < SH_NFA_MAX_LETTERS; ++j)
		{
			tos = sh_vector_data(&cur->nexts[j]);
			for (size_t k = 0; k < cur->nexts[j].size; ++k)
				tos[k] += offset_second;
		}
	}

	return 0;
}

int sh_nfa_or(shNfa *nfa, const shNfa *second)
{
	size_t offset_second;
	int temp;
	int res_start, res_final;
	shNfaState s;

	if (nfa == NULL || second == NULL)
		return -1;

	offset_second = nfa->states.size;
	if (_states_cat_common(nfa, second))
		return -1;

	res_start = nfa->states.size;
	res_final = nfa->states.size + 1;
	_state_init(&s);
	if (sh_vector_append(&nfa->states, &s))
		goto exit_no_mem;
	if (sh_vector_append(&nfa->states, &s))
		goto exit_no_mem;
	STATES(nfa)[res_final].final = true;
	STATES(nfa)[nfa->final].final = false;
	if (sh_vector_append(&STATES(nfa)[nfa->final].lambdas, &res_final))
		goto exit_no_mem;
	STATES(nfa)[second->final + offset_second].final = false;
	if (sh_vector_append(&STATES(nfa)[second->final + offset_second].lambdas, &res_final))
		goto exit_no_mem;
	if (sh_vector_append(&STATES(nfa)[res_start].lambdas, &nfa->start))
		goto exit_no_mem;
	temp = second->start + offset_second;
	if (sh_vector_append(&STATES(nfa)[res_start].lambdas, &temp))
		goto exit_no_mem;
	nfa->start = res_start;
	nfa->final = res_final;

	return 0;
exit_no_mem:
	/* hide shallow copied states from second so they won't get cleaned-up */
	nfa->states.size = offset_second;
	return -1;
}

int sh_nfa_cat(shNfa *nfa, const shNfa *second)
{
	size_t offset_second;
	int temp;

	if (nfa == NULL || second == NULL)
		return -1;

	offset_second = nfa->states.size;
	if (_states_cat_common(nfa, second))
		return -1;

	STATES(nfa)[nfa->final].final = false;
	temp = second->start + offset_second;
	if (sh_vector_append(&STATES(nfa)[nfa->final].lambdas, &temp))
	{
		/* hide shallow copied states from second so they won't get cleaned-up */
		nfa->states.size = offset_second;
		return -1;
	}
	nfa->final = second->final + offset_second;
	return 0;
}

static int _nfa_replicate(shNfa *nfa, int count)
{
	shNfa orig;
	int ret = -1;

	/* Take a copy first */
	if (sh_nfa_copy(&orig, nfa))
		return -1;

	for (int i = 1; i < count; ++i)
	{
		shNfa orig_copy;

		/* shVector shallow copies the memory of orig_copy, so each cycle the data needs to be renewed */
		if (sh_nfa_copy(&orig_copy, &orig))
			goto exit_no_mem;
		if (sh_nfa_cat(nfa, &orig_copy))
		{
			sh_nfa_free(&orig_copy);
			goto exit_no_mem;
		}
		sh_nfa_shallow_free(&orig_copy);
	}

	ret = 0;
exit_no_mem:
	sh_nfa_free(&orig);
	return ret;
}

int sh_nfa_unary(shNfa *nfa, int count)
{
	size_t res_start, res_final;
	shNfaState s;

	if (nfa == NULL || count == 1)
		return -1;

	else if (count > 1)
		return _nfa_replicate(nfa, count);

	res_start = nfa->states.size;
	res_final = nfa->states.size + 1;
	_state_init(&s);
	if (sh_vector_append(&nfa->states, &s))
		return -1;
	if (sh_vector_append(&nfa->states, &s))
		return -1;
	STATES(nfa)[res_final].final = true;
	STATES(nfa)[nfa->final].final = false;
	if (sh_vector_append(&STATES(nfa)[res_start].lambdas, &nfa->start))
		return -1;
	if (sh_vector_append(&STATES(nfa)[nfa->final].lambdas, &res_final))
		return -1;
	if (count == 0)
	{
		/* (NFA)+ has lambdas transition from res_final to res_start */
		if (sh_vector_append(&STATES(nfa)[res_final].lambdas, &res_start))
			return -1;
	}
	else if (count == -1)
	{
		/* (NFA)? has lambdas transition from res_start to res_final */
		if (sh_vector_append(&STATES(nfa)[res_start].lambdas, &res_final))
			return -1;
	}
	else
	{
		/* (NFA)* has both */
		if (sh_vector_append(&STATES(nfa)[res_start].lambdas, &res_final)
			|| sh_vector_append(&STATES(nfa)[res_final].lambdas, &res_start))
			return -1;
	}
	nfa->start = res_start;
	nfa->final = res_final;

	return 0;
}

void sh_nfa_dump(const shNfa *nfa)
{
	printf("Start: %d\n", nfa->start);
	printf("Final: %d\n", nfa->final);
	for (size_t i = 0, size_i = nfa->states.size; i < size_i; ++i)
	{
		printf("State: %d ", (int)i);
		if (STATES_CONST(nfa)[i].final)
			printf("Final\n");
		else
			printf("Not final\n");
		if (STATES_CONST(nfa)[i].lambdas.size > 0)
		{
			printf("  Lambda:");
			for (size_t k = 0, size_k = STATES_CONST(nfa)[i].lambdas.size; k < size_k; ++k)
				printf(" %d", LAMBDAS_CONST(nfa, i)[k]);
			printf("\n");
		}
		for (int j = 0; j < nfa->alphabet_size; ++j)
		{
			if (STATES_CONST(nfa)[i].nexts[j].size == 0)
				continue;
			if (nfa->alphabet[j].name == NULL)
				printf("  (letter %d):", (int)j);
			else
				printf("  %s:", nfa->alphabet[j].name);
			for (size_t k = 0, size_k = STATES_CONST(nfa)[i].nexts[j].size; k < size_k; ++k)
				printf(" %d", NEXTS_CONST(nfa, i, j)[k]);
			printf("\n");
		}
	}
}
