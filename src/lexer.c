/*
 * Copyright (C) 2007-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shCompiler.
 *
 * shCompiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shCompiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shCompiler.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <assert.h>
#include <shlexer.h>
#include <shparser.h>
#include "lexer_internal.h"
#include "parser_internal.h"
#include "utils.h"
#include "tokens.h"
#include "nonstd.h"

/*#define SH_DEBUG */

#define INTERNAL ((lexer_internal *)lexer->internal)

static void _token_typecpy(void *to, void *from)
{
	*(token_type *)to = *(token_type *)from;
}

static void _charptrcpy(void *to, void *from)
{
	*(char **)to = *(char **)from;
}

static void _token_infocpy(void *to, void *from)
{
	*(token_info *)to = *(token_info *)from;
}

static void _matchcpy(void *to, void *from)
{
	*(sh_lexer_match_function *)to = *(sh_lexer_match_function *)from;
}

static void _charcpy(void *to, void *from)
{
	*(char *)to = *(char *)from;
}

static int _strcmp(const void *a, const void *b)
{
	return strcmp(*(char * const *)a, *(char * const *)b);
}

static int _token_typecmp(const void *a, const void *b)
{
	return strcmp(((const token_type *)a)->dfa.name, ((const token_type *)b)->dfa.name);
}

static void _match_default(shLexer *lexer, char **t) {}
static void _no_match_default(shLexer *lexer, char *ic, int *lb, int *f, int *l, int *c)
{
	for (; *lb < *f; ++*lb, ++*c)
		if (ic[*lb] == '\n')
		{
			++*l;
			*c = INTERNAL->input.column_start;
		}
	if (ic[*f] == '\n')
	{
		++*l;
		*c = INTERNAL->input.column_start;
	}
}

typedef struct file_reader_data
{
	FILE *fin;
	shLexer *lexer;
} file_reader_data;

static size_t _file_reader(shLexer *lexer, char **text, void *data, bool cleanup)
{
	file_reader_data *d = data;
	size_t read = 0;

	if (cleanup)
	{
		if (text)
			CLEAN_FREE(*text);
		if (d && d->fin)
			fclose(d->fin);
		return 0;
	}

	assert(text != NULL);
	assert(d != NULL);
	assert(d->fin != NULL);
	assert(d->lexer != NULL);

	if (*text == NULL)
	{
		*text = malloc(PAGE_SIZE * sizeof **text);
		if (*text == NULL)
			goto exit_no_mem;
	}

	read = fread(*text, sizeof **text, PAGE_SIZE, d->fin);
	if (ferror(d->fin))
		sh_lexer_error(d->lexer, "error: I/O error while reading from file");

	return read;
exit_no_mem:
	sh_lexer_error(d->lexer, "error: file reader out of memory");
	return 0;
}

typedef struct string_reader_data
{
	char *text;
	size_t so_far;
	size_t size;
	shLexer *lexer;		/* for now, this is unused */
} string_reader_data;

static size_t _string_reader(shLexer *lexer, char **text, void *data, bool cleanup)
{
	string_reader_data *d = data;
	size_t read = PAGE_SIZE;

	if (cleanup)
	{
		if (d)
			CLEAN_FREE(d->text);
		return 0;
	}

	assert(text != NULL);
	assert(d != NULL);
	assert(d->text != NULL);
	assert(d->lexer != NULL);
	assert(d->size >= d->so_far);

	/* the first time, it would be set to 0 by shLexer */
	*text = d->text + d->so_far;
	if (d->size - d->so_far < PAGE_SIZE)
		read = d->size - d->so_far;
	d->so_far += read;

	return read;
}

typedef struct interactive_reader_data
{
	size_t size;
	shLexer *lexer;		/* for now, it is unused */
} interactive_reader_data;

#define READ_STEP 100

static size_t _interactive_reader(shLexer *lexer, char **text, void *data, bool cleanup)
{
	interactive_reader_data *d = data;
	size_t so_far = 0;

	if (cleanup)
	{
		if (text)
			CLEAN_FREE(*text);
		return 0;
	}

	assert(d != NULL);
	assert(d->lexer != NULL);

	while (true)
	{
		char *res;

		if (so_far + READ_STEP >= d->size)
		{
			void *enlarged;

			d->size += READ_STEP;
			enlarged = realloc(*text, d->size * sizeof **text);
			if (enlarged == NULL)
				goto exit_no_mem;

			*text = enlarged;
		}
		res = fgets(*text + so_far, READ_STEP, stdin);
		if (res == NULL)	/* EOF */
			break;

		so_far += strlen(res);
		assert(strlen(res) != 0);
		if ((*text)[so_far - 1] == '\n')
			break;
	}

	return so_far;
exit_no_mem:
	sh_lexer_error(d->lexer, "error: interactive reader out of memory");
	return so_far;
}

#undef READ_STEP

static void _clear_peeked(shList *list)
{
	shNode n;
	for (int res = sh_list_get_first(list, &n); res == 0; res = sh_list_get_next(&n, &n))
		CLEAN_FREE(((token_info *)n.data)->token);
	sh_list_clear(list);
}

int sh_lexer_init(shLexer *lexer)
{
	if (lexer == NULL)
		return SH_LEXER_FAIL;

	*lexer = (shLexer){
		.error_out = stderr,
	};

	lexer->internal = malloc(sizeof(lexer_internal));
	if (lexer->internal == NULL)
		goto exit_no_mem;

	*INTERNAL = (lexer_internal){
		.cur_token = {
			.line = 1,
			.column = 0,
		},
		.no_match = _no_match_default
	};

	sh_list_init(&INTERNAL->peeked, _token_infocpy, sizeof(token_info));
	sh_vector_init(&INTERNAL->types, _token_typecpy, sizeof(token_type), _token_typecmp);
	sh_vector_init(&INTERNAL->keywords, _charptrcpy, sizeof(char *));
	sh_map_init(&INTERNAL->match_functions, _charptrcpy, sizeof(char *),
			_matchcpy, sizeof(sh_lexer_match_function), _strcmp);

	return SH_LEXER_SUCCESS;
exit_no_mem:
	_message("error: lexer initialization out of memory");
	return SH_LEXER_NO_MEM;
}

static void _free_text(shLexer *lexer)
{
	if (lexer == NULL || lexer->internal == NULL)
		return;

	CLEAN_FREE(lexer->file_name);
	CLEAN_FREE(INTERNAL->input.text2lex);
	CLEAN_FREE(INTERNAL->cur_token.token);
	if (INTERNAL->input.reader != NULL)
	{
		INTERNAL->input.reader(lexer, &INTERNAL->input.user_text, INTERNAL->input.user_data, true);

		/* free user_data only if shLexer malloced it */
		if (INTERNAL->input.reader == _file_reader || INTERNAL->input.reader == _string_reader || INTERNAL->input.reader == _interactive_reader)
			CLEAN_FREE(INTERNAL->input.user_data);
	}
	INTERNAL->input.size = 0;
	INTERNAL->input.mem_size = 0;
	INTERNAL->input.reader = NULL;
	INTERNAL->input.user_text = NULL;
	INTERNAL->input.user_data = NULL;
	INTERNAL->input.finished = 0;
	INTERNAL->input.last_raw_char = '\0';
	INTERNAL->cur_token.token = NULL;
	INTERNAL->cur_token.type = NULL;
	INTERNAL->cur_token.space_met = false;
	INTERNAL->cur_token.line = 1;
	INTERNAL->cur_token.column = 0;
	INTERNAL->cur_token.next_lexeme = 0;
	INTERNAL->cur_token.finished = false;
	_clear_peeked(&INTERNAL->peeked);
	lexer->current_token = NULL;
	lexer->current_type = NULL;
	lexer->line = 0;
	lexer->column = 0;
	lexer->space_met = false;
	lexer->repeek = true;
}

int sh_lexer_tokens(shLexer *lexer, const char *letters_file, const char *tokens_file, const char *keywords_file, bool rebuild)
{
	shLexer l;
	shParser p;
	int error;
	char *lin_fc = NULL;
	char *cache_path = NULL;
	shVector/* shDfa */ dfas;
	shNode n;

	if (lexer == NULL || lexer->internal == NULL)
		return SH_LEXER_FAIL;

	if (sh_lexer_init_to_tokens_file(&l))
		return SH_LEXER_NO_MEM;

	if ((error = sh_parser_init_to_tokens_file(&p, &l)))
		goto exit_local_cleanup;

	lin_fc = sh_compiler_read_whole_file(letters_file, &error);
	if (lin_fc == NULL)
	{
		if (error == -1)
			goto exit_no_such_file;
		else
			goto exit_no_mem;
	}

	switch (sh_lexer_file(&l, tokens_file))
	{
	case SH_LEXER_NO_FILE:
		goto exit_no_such_file;
	case SH_LEXER_NO_MEM:
		goto exit_no_mem;
	case SH_LEXER_SUCCESS:
		break;
	default:
		goto exit_fail;
	}

	cache_path = sh_compiler_get_path_and_add(tokens_file, NULL, "cache/");
	if (cache_path == NULL)
		goto exit_no_mem;

	/* if could not create path (return value not zero), silently continue */
	(void)_make_dir(cache_path);

	if (sh_compiler_TF_init(&l, rebuild, lin_fc, cache_path))
		goto exit_fail;

	if (sh_parser_parse_llk(&p) != SH_PARSER_SUCCESS)
		goto exit_parse_error;

	dfas = sh_compiler_TF_get_dfas(&l);
	for (int res = sh_vector_get_first(&dfas, &n); res == 0; res = sh_vector_get_next(&n, &n))
		sh_lexer_add_dfa(lexer, n.data);

	switch (sh_lexer_keywordsf(lexer, keywords_file))
	{
	case SH_LEXER_NO_FILE:
		goto exit_no_such_file;
	case SH_LEXER_NO_MEM:
		goto exit_no_mem;
	case SH_LEXER_SUCCESS:
		break;
	default:
		goto exit_fail;
	}

	lexer->initialized = true;
	error = SH_LEXER_SUCCESS;

	goto exit_local_cleanup;
exit_no_such_file:
	error = SH_LEXER_NO_FILE;
	goto exit_cleanup;
exit_no_mem:
	_message("error: out of memory when reading tokens");
	error = SH_LEXER_NO_MEM;
	goto exit_cleanup;
exit_fail:
	error = SH_LEXER_FAIL;
	goto exit_cleanup;
exit_parse_error:
	error = sh_compiler_TF_error(&l);
exit_cleanup:
	dfas = sh_compiler_TF_get_dfas(&l);
	for (int res = sh_vector_get_first(&dfas, &n); res == 0; res = sh_vector_get_next(&n, &n))
		sh_dfa_free(n.data);
exit_local_cleanup:
	sh_compiler_TF_cleanup(&l);
	CLEAN_FREE(lin_fc);
	CLEAN_FREE(cache_path);
	sh_lexer_free(&l);
	sh_parser_free(&p);
	return error;
}

int sh_lexer_file(shLexer *lexer, const char *file_name)
{
	file_reader_data *rd = NULL;
	int ret;

	if (lexer == NULL || lexer->internal == NULL)
		return SH_LEXER_FAIL;

	if (file_name == '\0')
		return SH_LEXER_FAIL;

	/* if lexer was previously working on a text, free it! */
	_free_text(lexer);

	rd = malloc(sizeof *rd);
	if (rd == NULL)
		goto exit_no_mem;

	lexer->file_name = sh_compiler_get_global_path(file_name);
	if (lexer->file_name == NULL)
		goto exit_no_mem;

	rd->fin = fopen(lexer->file_name, "r");
	if (rd->fin == '\0')
		goto exit_no_such_file;

	rd->lexer = lexer;
	INTERNAL->input.reader = _file_reader;
	INTERNAL->input.user_text = NULL;
	INTERNAL->input.user_data = rd;

	return SH_LEXER_SUCCESS;
exit_no_mem:
	_message("error: open file out of memory");
	ret = SH_LEXER_NO_MEM;
	goto exit_cleanup;
exit_no_such_file:
	_message("error: no such file '%s'", file_name);
	ret = SH_LEXER_NO_FILE;
exit_cleanup:
	CLEAN_FREE(rd);
	CLEAN_FREE(lexer->file_name);
	return ret;
}

int sh_lexer_text(shLexer *lexer, const char *text, const char *buffer_name)
{
	string_reader_data *sd = NULL;
	char *text_copy = NULL;
	size_t len;

	if (lexer == NULL || lexer->internal == NULL || text == NULL)
		return SH_LEXER_FAIL;

	/* if lexer was previously working on a text, free it! */
	_free_text(lexer);

	len = strlen(text);
	text_copy = malloc(len * sizeof *text);
	sd = malloc(sizeof *sd);
	if (sd == NULL || text_copy == NULL)
		goto exit_no_mem;

	if (buffer_name != NULL)
		lexer->file_name = strdup(buffer_name);		/* if NULL returned, silently continue */
	sd->text = text_copy;
	sd->size = len;
	sd->so_far = 0;
	sd->lexer = lexer;
	INTERNAL->input.reader = _string_reader;
	INTERNAL->input.user_text = NULL;
	INTERNAL->input.user_data = sd;

	return SH_LEXER_SUCCESS;
exit_no_mem:
	_message("error: read text out of memory");
	CLEAN_FREE(sd);
	CLEAN_FREE(text_copy);
	CLEAN_FREE(lexer->file_name);
	return SH_LEXER_NO_MEM;
}

int sh_lexer_interactive(shLexer *lexer, sh_lexer_reader reader, const char *name, void *data)
{
	if (lexer == NULL || lexer->internal == NULL)
		return SH_LEXER_FAIL;

	/* if lexer was previously working on a text, free it! */
	_free_text(lexer);

	if (name != NULL)
		lexer->file_name = strdup(name);		/* if NULL returned, silently continue */
	if (reader == NULL)
	{
		interactive_reader_data *id = malloc(sizeof *id);

		if (id == NULL)
			goto exit_no_mem;

		id->size = 0;
		id->lexer = lexer;
		reader = _interactive_reader;
		data = id;
	}
	INTERNAL->input.reader = reader;
	INTERNAL->input.user_data = data;
	INTERNAL->input.user_text = NULL;

	return SH_LEXER_SUCCESS;
exit_no_mem:
	_message("error: interactive read out of memory");
	CLEAN_FREE(lexer->file_name);
	return SH_LEXER_NO_MEM;
}

const char *sh_lexer_next(shLexer *lexer, const char **type)
{
	shNode front;

	if (lexer == NULL || lexer->internal == NULL)
		return NULL;

	CLEAN_FREE(INTERNAL->cur_token.token);
	lexer->current_token = NULL;
	lexer->current_type = NULL;
	INTERNAL->cur_token.space_met = false;

	if (INTERNAL->cur_token.finished)
		return NULL;

	if (INTERNAL->peeked.size == 0)
		sh_lexer_peek(lexer, type, 0, NULL, NULL);

	sh_list_get_first(&INTERNAL->peeked, &front);
	INTERNAL->cur_token = *(token_info *)front.data;
	sh_list_delete(&front, NULL);
	if (type)
		*type = INTERNAL->cur_token.type;
	lexer->current_token = INTERNAL->cur_token.token;
	lexer->current_type = INTERNAL->cur_token.type;
	lexer->line = INTERNAL->cur_token.line;
	lexer->column = INTERNAL->cur_token.column;
	lexer->space_met = INTERNAL->cur_token.space_met;

	return INTERNAL->cur_token.token;
}

/* returns a shift value which should be used to fix indices to the text */
static size_t _get_more_input(shLexer *lexer)
{
	size_t i, j;
	size_t read, new_size, used_up;
	shNode n;
	char *append, *user_text;

	assert(lexer != NULL);

	read = INTERNAL->input.reader(lexer, &INTERNAL->input.user_text, INTERNAL->input.user_data, false);
	if (read == 0 || INTERNAL->input.user_text == NULL)
	{
		INTERNAL->input.finished = true;
		return 0;
	}

	used_up = INTERNAL->cur_token.next_lexeme;
	new_size = INTERNAL->input.size + read - used_up + 1;	/* +1 for the terminating '\0' */
	if (new_size > INTERNAL->input.mem_size)
	{
		char *enlarged = realloc(INTERNAL->input.text2lex, new_size * sizeof *INTERNAL->input.text2lex);
		if (enlarged == NULL)
			goto exit_no_mem;

		INTERNAL->input.text2lex = enlarged;
		INTERNAL->input.mem_size = new_size;
	}

	/* throw away used-up characters */
	memmove(INTERNAL->input.text2lex, INTERNAL->input.text2lex + used_up, INTERNAL->input.size - used_up);
	INTERNAL->input.size -= used_up;

	/* fix next_lexeme indices of current and peeked tokens */
	for (int res = sh_list_get_first(&INTERNAL->peeked, &n); res == 0; res = sh_list_get_next(&n, &n))
		((token_info *)n.data)->next_lexeme -= used_up;
	INTERNAL->cur_token.next_lexeme -= used_up;

	/* append newly read text, fixing end of lines */
	append = INTERNAL->input.text2lex + INTERNAL->input.size;
	user_text = INTERNAL->input.user_text;
	i = 0;
	if ((INTERNAL->input.last_raw_char == '\r' && user_text[0] == '\n') || (INTERNAL->input.last_raw_char == '\n' && user_text[0] == '\r'))
		++i;
	INTERNAL->input.last_raw_char = '\0';

	for (j = 0; i < read; ++i)
	{
		if ((user_text[i] < 32 || user_text[i] > 126) && user_text[i] != '\t' && user_text[i] != '\n' && user_text[i] != '\r')
		{
			sh_lexer_error(lexer, "warning: invalid character '\\%o'", (unsigned int)user_text[i]);
			continue;
		}

		append[j++] = (user_text[i] == '\r')?'\n':user_text[i];
		if (i + 1 == read)
		{
			if (user_text[i] == '\n' || user_text[i] == '\r')
				INTERNAL->input.last_raw_char = user_text[i];
		}
		else if ((user_text[i] == '\n' && user_text[i + 1] == '\r') || (user_text[i] == '\r' && user_text[i + 1] == '\n'))
		{
			++i;
			INTERNAL->input.last_raw_char = user_text[i];
		}
	}
	append[j] = '\0';
	INTERNAL->input.size += j;

	return used_up;
exit_no_mem:
	sh_lexer_error(lexer, "error: lexer out of memory");
	INTERNAL->input.finished = true;
	return 0;
}

const char *sh_lexer_peek(shLexer *lexer, const char **type, unsigned int n, int *line, int *column)
{
	int *pos_in_dfa;
	shNode cur;
	token_type *types = sh_vector_data(&INTERNAL->types);

	if (lexer == NULL || lexer->internal == NULL)
		return NULL;

	if (type)
		*type = NULL;
	if (INTERNAL->cur_token.finished)
	{
		if (line)
			*line = INTERNAL->cur_token.line;
		if (column)
			*column = INTERNAL->cur_token.column;
		return NULL;
	}

	/* for first time */
	if (INTERNAL->input.text2lex == NULL)
		_get_more_input(lexer);

	pos_in_dfa = malloc(INTERNAL->types.size * sizeof *pos_in_dfa);
	if (pos_in_dfa == NULL)
		goto exit_no_mem;

	while (n >= INTERNAL->peeked.size)
	{
		int possible_type = -1;
		token_info peek = {0};
		bool is_white_space = false;
		int cur_line;
		int cur_column;
		int last_match_line;
		int last_match_column;
		int forward;
		int last_match_pos;

		if (INTERNAL->peeked.size > 0)
		{
			shNode back;

			sh_list_get_last(&INTERNAL->peeked, &back);
			peek.line = ((token_info *)back.data)->line;
			peek.column = ((token_info *)back.data)->column;
			peek.next_lexeme = ((token_info *)back.data)->next_lexeme;
		}
		else
		{
			peek.line = INTERNAL->cur_token.line;
			peek.column = INTERNAL->cur_token.column;
			peek.next_lexeme = INTERNAL->cur_token.next_lexeme;
		}

		last_match_line = peek.line;
		last_match_column = peek.column;
		do
		{
			cur_line = peek.line;
			cur_column = peek.column + 1;
			forward = peek.next_lexeme;
			/* if last_match_pos remains -1, then could not match anything */
			last_match_pos = -1;

			/* check if already finished with the input.  Note that text2lex could be NULL if the input is completely empty */
			if (INTERNAL->input.finished && (INTERNAL->input.text2lex == NULL || INTERNAL->input.text2lex[forward] == '\0'))
			{
				token_info temp;

				CLEAN_FREE(pos_in_dfa);
				peek.finished = true;
				temp = (token_info){
					.space_met = peek.space_met,
					.line = peek.line,
					.column = peek.column,
					.next_lexeme = peek.next_lexeme,
					.finished = peek.finished,
				};

				if (sh_list_append(&INTERNAL->peeked, &temp))
					goto exit_no_mem;

				if (line)
					*line = temp.line;
				if (column)
					*column = temp.column;

				return NULL;
			}

			for (int i = 0, size = INTERNAL->types.size; i < size; ++i)
				if (types[i].enabled)
					pos_in_dfa[i] = types[i].dfa.start;
				else
					pos_in_dfa[i] = types[i].dfa.trap_state;

			for (; !INTERNAL->input.finished || INTERNAL->input.text2lex[forward] != '\0'; ++forward)
			{
				bool can_continue = false;

				if (INTERNAL->input.text2lex[forward] == '\0')
				{
					size_t used_up = _get_more_input(lexer);

					peek.next_lexeme -= used_up;
					forward -= used_up;
					if (last_match_pos != -1)
						last_match_pos -= used_up;
					if (INTERNAL->input.finished && INTERNAL->input.text2lex[forward] == '\0')
						/* can't `continue`, as that would change `forward` */
						break;
				}
#ifdef SH_DEBUG
				_message("checking for @%d = %c", forward, INTERNAL->input.text2lex[forward]);
#endif
				for (int i = 0, size = INTERNAL->types.size; i < size; ++i)
				{
					shDfa *cur_dfa = &types[i].dfa;

					/* if not in trap state and there is a letter associated with lexer character in lexer dfa */
					if (pos_in_dfa[i] != cur_dfa->trap_state)
					{
						if (cur_dfa->to_letter[(int)INTERNAL->input.text2lex[forward]] != -1)
						{
							pos_in_dfa[i] = cur_dfa->to[pos_in_dfa[i]]
								[cur_dfa->to_letter[(int)INTERNAL->input.text2lex[forward]]];

							if (cur_dfa->final[pos_in_dfa[i]])
							{
								possible_type = i;
								last_match_pos = forward;
								last_match_line = cur_line;
								last_match_column = cur_column;
								if (INTERNAL->input.text2lex[forward] == '\n')
								{
									++last_match_line;
									last_match_column = INTERNAL->input.column_start;
								}
							}

							if (pos_in_dfa[i] != cur_dfa->trap_state)
								can_continue = true;
#ifdef SH_DEBUG
							_message("going forwards in dfa: %s with character: %c", cur_dfa->name,
									INTERNAL->input.text2lex[forward]);
#endif
						}
						else
						{
#ifdef SH_DEBUG
							_message("undefined character: %c for dfa: %s",
									INTERNAL->input.text2lex[forward], cur_dfa->name);
#endif
							pos_in_dfa[i] = cur_dfa->trap_state;
						}
					}
#ifdef SH_DEBUG
					else
					{
						_message("cannot go any further in dfa: %s with character: %c",
								cur_dfa->name, INTERNAL->input.text2lex[forward]);
						if (cur_dfa->to_letter[INTERNAL->input.text2lex[forward]] != -1)
							_message("  which is part of letter: %s",
								cur_dfa->alphabet[cur_dfa->to_letter[INTERNAL->input.text2lex[forward]]].name);
					}
#endif
				}
				if (!can_continue)
					break;

				if (INTERNAL->input.text2lex[forward] == '\n')
				{
					++cur_line;
					cur_column = INTERNAL->input.column_start;
				}
				++cur_column;
			}

			if (last_match_pos == -1)
			{
				/* could not match even one token.  Call error handler */
				++peek.column;
				INTERNAL->no_match(lexer, INTERNAL->input.text2lex, &peek.next_lexeme, &forward, &peek.line, &peek.column);
				last_match_line = peek.line;
				last_match_column = peek.column;
				last_match_pos = peek.next_lexeme;
				possible_type = -1;
			}

			peek.line = last_match_line;
			peek.column = last_match_column;
			is_white_space = false;
			if (possible_type == -1 || types[possible_type].ignored)
			{
				is_white_space = true;
				peek.space_met = true;
			}
			else
			{
				peek.token = strndup(INTERNAL->input.text2lex + peek.next_lexeme, last_match_pos - peek.next_lexeme + 1);
				if (peek.token == NULL)
					goto exit_no_mem;
			}

			peek.next_lexeme = last_match_pos + 1;
		} while (is_white_space);

		if (sh_vector_bsearch(&INTERNAL->keywords, &peek.token) != -1)
			peek.type = peek.token;
		else if (possible_type == -1)
			peek.type = "WHITE_SPACE";
		else
			peek.type = types[possible_type].dfa.name;

		if (type)
			*type = peek.type;

		sh_lexer_match_function *match_func = sh_map_at(&INTERNAL->match_functions, &peek.type);
		assert(match_func != NULL);
		(*match_func)(lexer, &peek.token);

		if (sh_list_append(&INTERNAL->peeked, &peek))
			goto exit_no_mem;
	}

	CLEAN_FREE(pos_in_dfa);
	sh_list_get_first(&INTERNAL->peeked, &cur);

	for (unsigned int j = 0; j < n; ++j)
		sh_list_get_next(&cur, &cur);

	if (type)
		*type = ((token_info *)cur.data)->type;
	if (line)
		*line = ((token_info *)cur.data)->line;
	if (column)
		*column = ((token_info *)cur.data)->column;

	return ((token_info *)cur.data)->token;
exit_no_mem:
	sh_lexer_error(lexer, "error: lexer out of memory");
	CLEAN_FREE(pos_in_dfa);
	if (type)
		*type = "WHITE_SPACE";
	if (line)
		*line = 0;
	if (column)
		*column = 0;
	return NULL;
}

int sh_lexer_insert_fake(shLexer *lexer, const char *type, const char *token)
{
	shDfa *dfa = NULL;

	if (lexer == NULL || lexer->internal == NULL)
		return SH_LEXER_FAIL;

	if (type == NULL)
		return SH_LEXER_FAIL;

	token_type *types = sh_vector_data(&INTERNAL->types);
	char **keywords = sh_vector_data(&INTERNAL->keywords);

	lexer->current_token = NULL;
	lexer->current_type = NULL;
	CLEAN_FREE(INTERNAL->cur_token.token);

	for (int i = 0, size = INTERNAL->types.size; i < size; ++i)
	{
		token_type *t = &types[i];
		if (strcmp(t->dfa.name, type) == 0)
		{
			dfa = &t->dfa;
			INTERNAL->cur_token.type = dfa->name;
			break;
		}
	}

	if (dfa == NULL)
	{
		/* it's a keyword or no such type exists */
		int pos = sh_vector_bsearch(&INTERNAL->keywords, &type);

		if (pos != -1)
			INTERNAL->cur_token.type = keywords[pos];
		else
			goto exit_no_such_type;

		/* override token, as keywords can have only one value */
		token = INTERNAL->cur_token.type;
	}

	if (token)
	{
		INTERNAL->cur_token.token = strdup(token);
		if (INTERNAL->cur_token.token == NULL)
			goto exit_no_mem;
	}

	else	/* implifies dfa != NULL */
	{
		int state = dfa->start;
		shVector generated;
		bool *visited = malloc(dfa->states_count * sizeof *visited);
		bool has_unvisited_state = true;

		if (visited == NULL)
			goto exit_no_mem;

		if (sh_vector_init(&generated, _charcpy, sizeof(char)))
			goto exit_no_mem;

		memset(visited, false, dfa->states_count * sizeof *visited);

		/* make sure you don't fall in trap! */
		visited[dfa->trap_state] = true;

		/*
		 * if couldn't find another state, it is stuck in a loop.  I made sure it won't go to trap,
		 * but better have a check here just in case there was a bug elsewhere or the cache was tampered with
		 */
		while (!dfa->final[state] && has_unvisited_state)
		{
			has_unvisited_state = false;
			visited[state] = true;
			for (int i = 0; i < 256; ++i)
				if (dfa->to_letter[i] != -1)
				{
					int to = dfa->to[state][dfa->to_letter[i]];

					if (dfa->final[to] || !visited[to])
					{
						char c = i;

						/*
						 * not the most efficient, but this lexer function is usually called in
						 * case of error so efficiency isn't crucial.
						 */
						if (sh_vector_append(&generated, &c))
							goto exit_no_mem;

						state = to;
						has_unvisited_state = true;
						break;
					}
				}
		}
		CLEAN_FREE(visited);
		sh_vector_minimalize(&generated);
		INTERNAL->cur_token.token = sh_vector_uninline(&generated);
	}
	lexer->current_token = INTERNAL->cur_token.token;
	lexer->current_type = INTERNAL->cur_token.type;

	return SH_LEXER_SUCCESS;
exit_no_mem:
	return SH_LEXER_NO_MEM;
exit_no_such_type:
	return SH_LEXER_FAIL;
}

int sh_lexer_add_dfa(shLexer *lexer, const shDfa *dfa)
{
	token_type t = {
		.dfa = *dfa,
		.enabled = true,
	};
	sh_lexer_match_function def = _match_default;
	int ret;

	if (lexer == NULL || lexer->internal == NULL)
		return SH_LEXER_FAIL;

	if (dfa == NULL)
		return SH_LEXER_FAIL;

	if (strcmp(dfa->name, "WHITE_SPACE") == 0)
		t.ignored = true;

	ret = sh_vector_insert_sorted(&INTERNAL->types, &t, true);
	if (ret > 0)
		return SH_LEXER_EXISTS;
	if (ret < 0)
		return SH_LEXER_NO_MEM;

	if (sh_map_insert(&INTERNAL->match_functions, &t.dfa.name, &def))
		return SH_LEXER_NO_MEM;

	return SH_LEXER_SUCCESS;
}

int sh_lexer_keywords(shLexer *lexer, const char *buffer)
{
	char *s;
	size_t position = 0;
	int chars_read;
	shSet/* char * */ keywords;
	sh_lexer_match_function def = _match_default;

	if (lexer == NULL || lexer->internal == NULL)
		return SH_LEXER_FAIL;

	if (sh_set_init(&keywords, _charptrcpy, sizeof(char *), _strcmp))
		goto exit_no_mem;

	while ((s = sh_compiler_read_str(buffer + position, &chars_read)) != NULL)
	{
		position += chars_read;
		if (sh_set_insert(&keywords, &s) < 0)
			goto exit_no_mem;
		if (sh_map_insert(&INTERNAL->match_functions, &s, &def) < 0)
			goto exit_no_mem;
	}
	sh_set_to_vector(&keywords, &INTERNAL->keywords);
	sh_set_free(&keywords);

	return SH_LEXER_SUCCESS;
exit_no_mem:
	return SH_LEXER_NO_MEM;
}

int sh_lexer_keywordsf(shLexer *lexer, const char *file)
{
	int error;
	char *file_fc;

	if (lexer == NULL || lexer->internal == NULL)
		return SH_LEXER_FAIL;

	file_fc = sh_compiler_read_whole_file(file, &error);
	if (file_fc == NULL)
		return error == -1?SH_LEXER_NO_FILE:SH_LEXER_NO_MEM;

	error = sh_lexer_keywords(lexer, file_fc);
	free(file_fc);

	return error;
}

sh_lexer_match_function sh_lexer_set_match_function(shLexer *lexer, const char *type, sh_lexer_match_function func)
{
	shNode n;
	sh_lexer_match_function *prev;

	if (lexer == NULL || lexer->internal == NULL)
		return NULL;

	if (sh_map_insert(&INTERNAL->match_functions, &type, &func, &n) < 0)
		return NULL;

	prev = n.data;
	_matchcpy(n.data, &func);
	return *prev;
}

sh_lexer_error_handler sh_lexer_set_error_handler(shLexer *lexer, sh_lexer_error_handler eh)
{
	sh_lexer_error_handler prev;

	if (lexer == NULL || lexer->internal == NULL)
		return NULL;

	prev = INTERNAL->no_match;
	INTERNAL->no_match = eh;

	return prev;
}

void sh_lexer_ignore(shLexer *lexer, const char *type, bool ignore)
{
	token_type t = { .dfa = { .name = (char *)type } };
	int pos;

	if (lexer == NULL || lexer->internal == NULL)
		return;

	pos = sh_vector_bsearch(&INTERNAL->types, &t);
	if (pos >= 0)
	{
		token_type *res = sh_vector_at(&INTERNAL->types, pos);

		/*
		 * if ignore is being toggled, the next tokens could be changed,
		 * so make the lexer read them again
		 */
		if (res->ignored != ignore)
		{
			_clear_peeked(&INTERNAL->peeked);
			lexer->repeek = true;
		}
		res->ignored = ignore;
	}
}

bool sh_lexer_ignores(shLexer *lexer, const char *type)
{
	token_type t = { .dfa = { .name = (char *)type } };
	int pos;

	if (lexer == NULL || lexer->internal == NULL)
		return true;

	pos = sh_vector_bsearch(&INTERNAL->types, &t);

	return pos < 0 || ((token_type *)sh_vector_at(&INTERNAL->types, pos))->ignored;
}

void sh_lexer_match(shLexer *lexer, const char *type, bool match)
{
	token_type t = { .dfa = { .name = (char *)type } };
	int pos;

	if (lexer == NULL || lexer->internal == NULL)
		return;

	pos = sh_vector_bsearch(&INTERNAL->types, &t);
	if (pos >= 0)
	{
		token_type *res = sh_vector_at(&INTERNAL->types, pos);

		/*
		 * if enabled is being toggled, the next tokens could be changed,
		 * so make the lexer read them again
		 */
		if (res->enabled != match)
		{
			_clear_peeked(&INTERNAL->peeked);
			lexer->repeek = true;
		}
		res->enabled = match;
	}
}

bool sh_lexer_matches(shLexer *lexer, const char *type)
{
	token_type t = { .dfa = { .name = (char *)type } };
	int pos;

	if (lexer == NULL || lexer->internal == NULL)
		return false;

	pos = sh_vector_bsearch(&INTERNAL->types, &t);

	return pos >= 0 && ((token_type *)sh_vector_at(&INTERNAL->types, pos))->enabled;
}

void sh_lexer_set_position(shLexer *lexer, int line, int column)
{
	int line_diff;
	int column_diff;
	shNode n;

	line_diff = line - INTERNAL->cur_token.line;
	column_diff = column - INTERNAL->cur_token.column;

	for (int res = sh_list_get_first(&INTERNAL->peeked, &n); res == 0; res = sh_list_get_next(&n, &n))
	{
		/* column changes only for tokens on the current line */
		if (((token_info *)n.data)->line == INTERNAL->cur_token.line)
			((token_info *)n.data)->column += column_diff;

		/* line changes for all tokens */
		((token_info *)n.data)->line += line_diff;
	}

	INTERNAL->cur_token.line = line;
	INTERNAL->cur_token.column = column;
}

void sh_lexer_set_column_start(shLexer *lexer, int column)
{
	int column_diff;
	shNode n;

	column_diff = column - INTERNAL->input.column_start;

	for (int res = sh_list_get_first(&INTERNAL->peeked, &n); res == 0; res = sh_list_get_next(&n, &n))
		((token_info *)n.data)->column += column_diff;

	INTERNAL->cur_token.column += column_diff;

	INTERNAL->input.column_start = column;
}

static void _free_keywords(shLexer *lexer)
{
	char **keywords;

	if (lexer == NULL || lexer->internal == NULL)
		return;

	keywords = sh_vector_data(&INTERNAL->keywords);
	for (size_t i = 0, size = INTERNAL->keywords.size; i < size; ++i)
		free(keywords[i]);
	sh_vector_free(&INTERNAL->keywords);
}

static void _free_dfas(shLexer *lexer)
{
	token_type *types;

	if (lexer == NULL || lexer->internal == NULL)
		return;

	types = sh_vector_data(&INTERNAL->types);
	for (size_t i = 0, size = INTERNAL->types.size; i < size; ++i)
		sh_dfa_free(&types[i].dfa);
	sh_vector_free(&INTERNAL->types);
}

void sh_lexer_free(shLexer *lexer)
{
	if (lexer->internal == NULL)
		return;
	_free_text(lexer);
	_free_dfas(lexer);
	_free_keywords(lexer);
	sh_map_free(&INTERNAL->match_functions);
	sh_list_free(&INTERNAL->peeked);
	CLEAN_FREE(lexer->internal);
}

void _print_prefix(FILE *out, const char *fn, int l, int c)
{
	if (fn != NULL)
	{
		fprintf(out, "%s:", fn);
		if (l > 0)
		{
			fprintf(out, "%d:", l);
			if (c > 0)
				fprintf(out, "%d:", c);
		}
		fprintf(out, " ");
	}
}

void sh_lexer_error(shLexer *lexer, const char *fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	sh_lexer_errorv(lexer, fmt, args);
	va_end(args);
}

void sh_lexer_error_custom(shLexer *lexer, const char *file_name, int line, int column, const char *fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	sh_lexer_error_customv(lexer, file_name, line, column, fmt, args);
	va_end(args);
}

void sh_lexer_errorv(shLexer *lexer, const char *fmt, va_list args)
{
	FILE *out;

	if (lexer == NULL)
		return;

	if (lexer->internal != NULL && INTERNAL->error_suppress)
		return;

	out = lexer->error_out;
	if (out == NULL)
		return;

	_print_prefix(out, lexer->file_name, lexer->line, lexer->column);

	vfprintf(out, fmt, args);

	fprintf(out, "\n");
}

void sh_lexer_error_customv(shLexer *lexer, const char *file_name, int line, int column, const char *fmt, va_list args)
{
	FILE *out;

	if (lexer == NULL)
		return;

	if (lexer->internal != NULL && INTERNAL->error_suppress)
		return;

	out = lexer->error_out;
	if (out == NULL)
		return;

	_print_prefix(out, file_name, line, column);

	vfprintf(out, fmt, args);
}

void sh_lexer_suppress_errors(shLexer *lexer, bool suppress)
{
	if (lexer == NULL || lexer->internal == NULL)
		return;

	INTERNAL->error_suppress = suppress;
}
