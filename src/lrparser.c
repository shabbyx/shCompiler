/*
 * Copyright (C) 2007-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shCompiler.
 *
 * shCompiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shCompiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shCompiler.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <assert.h>
#include <shparser.h>
#include "parser_internal.h"
#include "utils.h"

//#define SH_DEBUG
//#define SH_DETAILED_DEBUG

#define INTERNAL ((parser_internal *)parser->internal)
#define LEXER (parser->lexer)

typedef struct rule_with_dot
{
	unsigned int rule;	/* the rule itself */
	unsigned int dot;	/* the position in rule's body where the dot is */
} rule_with_dot;

/*
 * the LR(k) state is a set of (rule with dot, {follow}), such as (T -> x.Ey, {xyz, xwy}) for LR(3).  The follows of a
 * rule_with_dot are grouped together as a set of follows for compactness.
 *
 * Although defined as a map, this is actually just a set.  It is defined as a map so that from rule_with_dot one could
 * look up the follows.
 *
 * the rule lambda -> .S (last rule in rules) is added initially and the LR(k) diagram is created including all reachable
 * states.  This makes state 0, i.e., the first state unique.  The action that reduces based on lambda -> S. is the accept
 * action of the parser.
 *
 * Note: the shVector of int of the follows are shallow copies of sentence_firsts's first field from the firsts map.
 *
 * Note: the follows of the state and the follows of nonterminals are *not* related.
 */
typedef shMap/* rule_with_dot->shSet of shVector of int */ lrk_state;

static bool default_pleh(shParser *p, shKtokens *la)
{
	// TODO: implement when the unhandled error of LR is fixed
	return false;
}

static void _symbolcpy(void *to, void *from)
{
	*(grammar_symbol *)to = *(grammar_symbol *)from;
}

static void _rule_with_dot_cpy(void *to, void *from)
{
	*(rule_with_dot *)to = *(rule_with_dot *)from;
}

static int _rule_with_dot_cmp(const void *a, const void *b)
{
	const rule_with_dot *first = a;
	const rule_with_dot *second = b;

	if (first->rule == second->rule)
		return (first->dot > second->dot) - (first->dot < second->dot);
	return (first->rule > second->rule) - (first->rule < second->rule);
}

static void _intcpy(void *to, void *from)
{
	*(int *)to = *(int *)from;
}

static int _intcmp(const void *a, const void *b)
{
	int first = *(const int *)a;
	int second = *(const int *)b;

	return (first > second) - (first < second);
}

static void _size_t_cpy(void *to, void *from)
{
	*(size_t *)to = *(size_t *)from;
}

static void _shnodecpy(void *to, void *from)
{
	*(shNode *)to = *(shNode *)from;
}

static void _state_ptr_cpy(void *to, void *from)
{
	*(lrk_state **)to = from;
}

static void _state_cpy(void *to, void *from)
{
	*(lrk_state *)to = *(lrk_state *)from;
}

static int _state_cmp(const void *a, const void *b)
{
	lrk_state first = *(const lrk_state *)a;
	lrk_state second = *(const lrk_state *)b;
	int ret;
	shNode n1, n2;

	ret = (first.size > second.size) - (first.size < second.size);
	if (ret)
		return ret;

	for (int res = sh_map_get_first(&first, &n1) + sh_map_get_first(&second, &n2); res == 0;
			res = sh_map_get_next(&n1, &n1) + sh_map_get_next(&n2, &n2))
	{
		ret = _rule_with_dot_cmp(n1.key, n2.key);
		if (ret)
			return ret;
		ret = sh_set_compare(n1.data, n2.data);
		if (ret)
			return ret;
	}
	return 0;
}

static void _lrk_diagram_data_cpy(void *to, void *from)
{
	*(lrk_diagram_data *)to = *(lrk_diagram_data *)from;
}

static int _lrk_diagram_data_cmp(const void *a, const void *b)
{
	const lrk_diagram_data *first = a;
	const lrk_diagram_data *second = b;

	if (first->state == second->state)
	{
		if (first->with_nonterminal == second->with_nonterminal)
		{
			if (first->with_nonterminal)
				return (first->nonterminal > second->nonterminal) - (first->nonterminal < second->nonterminal);
			else
				return sh_vector_compare(&first->look_ahead, &second->look_ahead);
		}
		return first->with_nonterminal?-1:1;
	}
	return (first->state > second->state) - (first->state < second->state);
}

static void _lrk_diagram_result_cpy(void *to, void *from)
{
	*(lrk_diagram_result *)to = *(lrk_diagram_result *)from;
}

#if defined(SH_DEBUG) || defined(SH_DETAILED_DEBUG)
static void _print_follow(shParser *parser, shVector *follow)
{
	int *symbols = sh_vector_data(follow);
	for (size_t i = 0; i < follow->size; ++i)
	{
		int t = symbols[i];
		printf(" %s", t == -1?"$":sh_parser_terminal_name(parser, t));
	}
}

static bool _print_state_item_follows(shNode *n, void *extra)
{
	printf("   ");
	_print_follow(extra, n->key);
	printf("\n");

	return true;
}

static bool _print_state_item(shNode *n, void *extra)
{
	rule_with_dot *rule_dot = n->key;
	shParser *parser = extra;
	grammar_rule *rule = sh_vector_at(&INTERNAL->rules, rule_dot->rule);
	grammar_symbol *rule_body = sh_vector_data(&rule->body);

	printf("%s ->", sh_parser_symbol_name(parser, rule->head));
	for (size_t i = 0; i < rule->body.size; ++i)
		printf("%s %s", i == rule_dot->dot?" .":"", sh_parser_symbol_name(parser, rule_body[i]));
	if (rule_dot->dot == rule->body.size)
		printf(" .");

	printf(" ------ with follows:\n");
	sh_set_inorder(n->data, _print_state_item_follows, parser);

	return true;
}

static void _print_state(shParser *parser, lrk_state *state)
{
	sh_map_inorder(state, _print_state_item, parser);
}

static void _print_states(shParser *parser, shMap *states)
{
	shNode n;

	printf("There exist(s) %zu state(s) in the CLR(k) diagram\n", states->size);
	for (int res = sh_map_get_first(states, &n); res == 0; res = sh_map_get_next(&n, &n))
	{
		printf("state[%zu]:\n", *(size_t *)n.data);
		_print_state(parser, (lrk_state *)n.key);
	}
}

static bool _print_lrk_diagram_entry(shNode *n, void *extra)
{
	shParser *parser = extra;
	lrk_diagram_data *d = n->key;
	lrk_diagram_result *r = n->data;

	printf("(%d,", d->state);
	if (d->with_nonterminal)
		printf(" %s", sh_parser_nonterminal_name(parser, d->nonterminal));
	else
	{
		int *look_aheads = sh_vector_data(&d->look_ahead);
		for (size_t i = 0; i < d->look_ahead.size; ++i)
		{
			int t = look_aheads[i];
			printf(" %s", t == -1?"$":sh_parser_terminal_name(parser, t));
		}
	}
	printf(") => ");
	switch (r->transition)
	{
	case SHIFT:
		printf("S%d", r->state);
		break;
	case GOTO:
		printf("G%d", r->state);
		break;
	case REDUCE:
		printf("R%d", r->rule);
		break;
	case ACCEPT:
		printf("A");
	}
	printf("\n");

	return true;
}

static void _print_lrk_diagram(shParser *parser)
{
	sh_map_inorder(&INTERNAL->lrk_diagram, _print_lrk_diagram_entry, parser);
}
#endif

static bool _add_follow(shNode *n, void *extra)
{
	return sh_set_insert(extra, n->key) >= 0;
}

static int closure(shParser *parser, lrk_state *state)
{
	grammar_rule *rules = sh_vector_data(&INTERNAL->rules);
	shQueue/* rule_with_dot */ Q;
	shNode n;
	int ret = -1;

	shSet new_follows = {0};

#ifdef SH_DETAILED_DEBUG
	printf("\nFinding closure for state:\n");
	_print_state(parser, state);
#endif

	sh_queue_init(&Q, _rule_with_dot_cpy, sizeof(rule_with_dot));
	for (int res = sh_map_get_first(state, &n); res == 0; res = sh_map_get_next(&n, &n))
		if (sh_queue_push(&Q, n.key))
			goto exit_no_mem;

	while (Q.size > 0)
	{
		shVector *rule_body;
		grammar_symbol after_dot;
		shSet *follows;
		rule_with_dot rule_dot = *(rule_with_dot *)sh_queue_top(&Q);
		sh_queue_pop(&Q);

#ifdef SH_DETAILED_DEBUG
		printf("Rule: %u with dot at position: %u\n", rule_dot.rule, rule_dot.dot);
#endif

		rule_body = &rules[rule_dot.rule].body;

		/* if nothing after dot, nothing added to closure */
		if (rule_body->size == rule_dot.dot)
			continue;

		after_dot = *(grammar_symbol *)sh_vector_at(rule_body, rule_dot.dot);

		/* if not a nonterminal, then nothing added to closure either */
		if (after_dot.type != NONTERMINAL)
			continue;

		follows = sh_map_at(state, &rule_dot);
		assert(follows != NULL);

		sh_set_init(&new_follows, sh_vector_assign, sizeof(shVector), sh_vector_compare);

		/*
		 * the rule is now A -> a . B b and there are a set of follows {f1, ..., fn}.
		 * Rules B -> . c need to be added, and their follow sets are the union of firsts(b fi).
		 * First, calculate this follow set.
		 */
		for (int res = sh_set_get_first(follows, &n); res == 0; res = sh_set_get_next(&n, &n))
		{
			shVector *follow = n.key;
			shNode adding_follows;
			grammar_symbol *temp;
			size_t j = 0;

			assert(follow->size > 0);
			temp = malloc(((rule_body->size - rule_dot.dot - 1) + follow->size) * sizeof *temp);
			if (temp == NULL)
				goto exit_no_mem;

			grammar_symbol *rule_body_symbols = sh_vector_data(rule_body);
			int *follow_terminals = sh_vector_data(follow);
			for (size_t i = rule_dot.dot + 1; i < rule_body->size; ++i)
				temp[j++] = rule_body_symbols[i];
			for (size_t i = 0; i < follow->size; ++i)
				temp[j++] = (grammar_symbol){
					.type = TERMINAL,
					.id = follow_terminals[i],
				};

			find_first_data adding_follows_data = {
				.sentence = temp,
				.k = INTERNAL->k_in_grammar,
				.sentence_malloced = true,
				.sentence_len = j,
			};
			int r = sh_parser_find_firsts(parser, &adding_follows_data, &adding_follows);
			if (r <= 0)
				free(temp);
			if (r < 0)
				goto exit_no_mem;

			if (sh_set_for_each(&((find_first_result *)adding_follows.data)->firsts, _add_follow, &new_follows))
				goto exit_no_mem;
		}

		/* now add the rules B -> . c with the just-calculated follow set */
		shVector *nont_rules = sh_vector_at(&INTERNAL->nonterminal_rules, after_dot.id);
#ifdef SH_DETAILED_DEBUG
		printf("Rules for this nonterminal:");
#endif
		bool new_follows_needs_duplicate = false;
		int *nont_rule_ids = sh_vector_data(nont_rules);
		for (size_t i = 0; i < nont_rules->size; ++i)
		{
			bool has_new_follow = false;
			rule_with_dot new_rule_dot = {
				.rule = nont_rule_ids[i],
				.dot = 0,	/* insert dot at the beginning of body */
			};

#ifdef SH_DETAILED_DEBUG
			printf(" %u", new_rule_dot.rule);
#endif

			shSet *s = sh_map_at(state, &new_rule_dot);
			if (s == NULL)
			{
#ifdef SH_DETAILED_DEBUG
				printf(" (new)");
#endif
				/* the first time setting this set, no need to duplicate.  Next times, it is necessary */
				shSet dup = {0};
				shSet *d = &new_follows;
				if (new_follows_needs_duplicate)
				{
					if (sh_set_duplicate(&new_follows, &dup))
						goto exit_no_mem;
					d = &dup;
				}
				new_follows_needs_duplicate = true;

				if (sh_map_insert(state, &new_rule_dot, d) < 0)
				{
					sh_set_free(&dup);
					goto exit_no_mem;
				}
				has_new_follow = true;
			}
			else
			{
				size_t prev_size = s->size;
				if (sh_set_for_each(&new_follows, _add_follow, s))
					goto exit_no_mem;
				has_new_follow = prev_size != s->size;
			}

			if (has_new_follow)
				if (sh_queue_push(&Q, &new_rule_dot) < 0)
					goto exit_no_mem;
		}

		if (!new_follows_needs_duplicate)
			sh_set_free(&new_follows);	/* if needs duplicate is set, new_follows is shallow copied and is in use */

#ifdef SH_DETAILED_DEBUG
		printf("\nState is updated to:\n");
		_print_state(parser, state);
#endif
	}

	ret = 0;
exit_no_mem:
	sh_queue_free(&Q);
	if (ret)
		sh_set_free(&new_follows);
	return ret;
}

static int _make_initial_state(shParser *parser, lrk_state *first_state, unsigned int init_rule)
{
	/*
	 * create the initial sentence by asking parser to find first of $$...$.  This way,
	 * its memory is handled by the firsts structure, and I don't need to clean it up.
	 */
	find_first_data all_dollars = {
		.sentence = malloc(INTERNAL->k_in_grammar * sizeof *all_dollars.sentence),
		.k = INTERNAL->k_in_grammar,
		.sentence_malloced = true,
		.sentence_len = INTERNAL->k_in_grammar,
	};
	shNode n;

	if (all_dollars.sentence == NULL)
		return -1;
	for (unsigned int i = 0; i < all_dollars.sentence_len; ++i)
		all_dollars.sentence[i] = (grammar_symbol){
			.type = TERMINAL,
			.id = -1,
		};

	int ret = sh_parser_find_firsts(parser, &all_dollars, &n);
	if (ret <= 0)
		free(all_dollars.sentence);
	if (ret < 0)
		return -1;

	find_first_result *first = n.data;
	if (first->firsts.size == 0)
		return -1;

	sh_set_get_first(&first->firsts, &n);
	sentence_first *sf = n.key;

	shSet follows;

	sh_set_init(&follows, sh_vector_assign, sizeof(shVector), sh_vector_compare);
	sh_map_init(first_state, _rule_with_dot_cpy, sizeof(rule_with_dot), sh_set_assign, sizeof(shSet), _rule_with_dot_cmp);
	if (sh_set_insert(&follows, &sf->first))
		goto exit_no_follows;
	if (sh_map_insert(first_state, &(rule_with_dot){ .rule = init_rule, .dot = 0 }, &follows))
		goto exit_no_first_state;
	if (closure(parser, first_state))
		goto exit_no_closure;
	return 0;
exit_no_closure:
	sh_map_free(first_state);
exit_no_first_state:
	sh_set_free(&follows);
exit_no_follows:
	return -1;
}

static bool _free_lrk_diagram(shNode *n, void *extra)
{
	lrk_diagram_data *d = n->key;
	if (!d->with_nonterminal)
		sh_vector_free(&d->look_ahead);
	return true;
}

static bool _output_state_item_follows(shNode *n, void *extra)
{
	shVector *follow = n->key;
	shParser *parser = extra;
	int *symbols = sh_vector_data(follow);

	sh_lexer_error_custom(LEXER, INTERNAL->grammar_file_name, 0, 0, "            ");
	for (size_t i = 0; i < follow->size; ++i)
	{
		int t = symbols[i];
		sh_lexer_error_custom(LEXER, NULL, 0, 0, " %s", t == -1?"$":sh_parser_terminal_name(parser, t));
	}
	sh_lexer_error_custom(LEXER, NULL, 0, 0, "\n");

	return true;
}

static bool _output_state_item(shNode *n, void *extra)
{
	rule_with_dot *rule_dot = n->key;
	shParser *parser = extra;
	grammar_rule *rule = sh_vector_at(&INTERNAL->rules, rule_dot->rule);
	grammar_symbol *rule_body = sh_vector_data(&rule->body);

	sh_lexer_error_custom(LEXER, INTERNAL->grammar_file_name, 0, 0, "         %s ->", sh_parser_symbol_name(parser, rule->head));
	for (size_t i = 0; i < rule->body.size; ++i)
		sh_lexer_error_custom(LEXER, NULL, 0, 0, "%s %s", i == rule_dot->dot?" .":"", sh_parser_symbol_name(parser, rule_body[i]));
	if (rule_dot->dot == rule->body.size)
		sh_lexer_error_custom(LEXER, NULL, 0, 0, " .");

	sh_lexer_error_custom(LEXER, NULL, 0, 0, " -- with follows:\n");
	sh_set_inorder(n->data, _output_state_item_follows, parser);

	return true;
}

static void _output_state(shParser *parser, lrk_state *state)
{
	sh_lexer_error_custom(LEXER, INTERNAL->grammar_file_name, 0, 0, "note: in state:\n");
	sh_map_inorder(state, _output_state_item, parser);
}

typedef enum conflict_resolution
{
	AMBIGUOUS,
	KEEP_OLD_ACTION,
	REPLACE_WITH_NEW_ACTION,
} conflict_resolution;

static conflict_resolution _resolve_conflict(shParser *parser, shNode *n, lrk_transition new_action, shVector *look_ahead,
		lrk_transition old_action, shNode *o, lrk_state *state, bool *state_printed)
{
	conflict_resolution resolution = AMBIGUOUS;
	const char *prioritized = NULL;
	int *look_aheads = sh_vector_data(look_ahead);

	if (!*state_printed)
		_output_state(parser, state);
	*state_printed = true;

	sh_lexer_error_custom(LEXER, INTERNAL->grammar_file_name, 0, 0, "warning: conflict with look-ahead");
	for (size_t i = 0; i < look_ahead->size; ++i)
	{
		int t = look_aheads[i];
		sh_lexer_error_custom(LEXER, NULL, 0, 0, " %s", t == -1?"$":sh_parser_terminal_name(parser, t));
	}
	sh_lexer_error_custom(LEXER, NULL, 0, 0, ":\n");

	sh_lexer_error_custom(LEXER, INTERNAL->grammar_file_name, 0, 0, "    %s because of:\n", old_action == SHIFT?"shift":"reduce");
	if (old_action == SHIFT)
	{
		shVector/* shNode */ *others = (shVector *)o->data;
		shNode *other_nodes = sh_vector_data(others);
		for (size_t i = 0; i < others->size; ++i)
			_output_state_item(&other_nodes[i], parser);
	}
	else
		_output_state_item(o->data, parser);

	sh_lexer_error_custom(LEXER, INTERNAL->grammar_file_name, 0, 0, "    %s because of:\n", new_action == SHIFT?"shift":"reduce");
	_output_state_item(n, parser);

	/* if shift or reduce are prioritized, resolve based on that */
	if ((INTERNAL->ambiguity_resolution & SH_PARSER_ACCEPT_SHIFT))
	{
		prioritized = "shift";
		if (old_action == SHIFT)
			resolution = KEEP_OLD_ACTION;
		else if (new_action == SHIFT)
			resolution = REPLACE_WITH_NEW_ACTION;
	}
	else if ((INTERNAL->ambiguity_resolution & SH_PARSER_ACCEPT_REDUCE) && (old_action != REDUCE || new_action != REDUCE))
	{
		prioritized = "reduce";
		if (old_action == REDUCE)
			resolution = KEEP_OLD_ACTION;
		else if (new_action == REDUCE)
			resolution = REPLACE_WITH_NEW_ACTION;
	}

	/*
	 * if still not resolved, then either shift or reduce is not selected as resolution strategy,
	 * or reduce is prioritized but it's a REDUCE/REDUCE conflict.
	 */
	if (resolution == AMBIGUOUS)
	{
		if ((INTERNAL->ambiguity_resolution & SH_PARSER_ACCEPT_FIRST))
		{
			prioritized = "the former action";
			resolution = KEEP_OLD_ACTION;
		}
		else if ((INTERNAL->ambiguity_resolution & SH_PARSER_ACCEPT_LAST))
		{
			prioritized = "the later action";
			resolution = REPLACE_WITH_NEW_ACTION;
		}
	}

	/* if resolved, inform the user about the decision */
	if (resolution != AMBIGUOUS)
		sh_lexer_error_custom(LEXER, INTERNAL->grammar_file_name, 0, 0, "note: ambiguity resolved by prioritizing %s\n", prioritized);

	/* if decided to replace with new action, remove the old action */
	if (resolution == REPLACE_WITH_NEW_ACTION)
		sh_map_delete(o);

	return resolution;
}

static int _find_state_transitions(shParser *parser, lrk_state *state, shMap *gotos, shMap *shifts, shMap *reduces)
{
	grammar_rule *rules = sh_vector_data(&INTERNAL->rules);
	shNode n;
	bool ambiguous = false;
	bool state_printed = false;

	/*
	 * gotos is a map from nonterminal NT to a set of state items IS: given NT, GOTO another state advancing dot in items in IS.
	 * shifts is a map from look-ahead LA to a set of state items IS: given LA, SHIFT to another state advancing dot in items in IS.
	 * reduces is a map from look-ahead LA to an item I: given LA, REDUCE according to rule in I.
	 */
	sh_map_init(gotos, _intcpy, sizeof(int), sh_vector_assign, sizeof(shVector), _intcmp);
	sh_map_init(shifts, sh_vector_assign, sizeof(shVector), sh_vector_assign, sizeof(shVector), sh_vector_compare);
	sh_map_init(reduces, sh_vector_assign, sizeof(shVector), _shnodecpy, sizeof(shNode), sh_vector_compare);

	for (int res = sh_map_get_first(state, &n); res == 0; res = sh_map_get_next(&n, &n))
	{
		rule_with_dot *rule_dot = n.key;
		shSet/* shVector of int */ *follows = n.data;
		grammar_symbol *rule_body;

		/* if nothing after the dot, add it to reduces */
		if (rule_dot->dot == rules[rule_dot->rule].body.size)
		{
			shNode f;

			for (res = sh_set_get_first(follows, &f); res == 0; res = sh_set_get_next(&f, &f))
			{
				shVector *follow = f.key;
				shNode other;
				bool assert_helper = false;

				/* check if already in shifts.  If so, it's a conflict */
				if (sh_map_find(shifts, follow, &other))
				{
					conflict_resolution decision = _resolve_conflict(parser, &n, REDUCE, follow, SHIFT, &other, state, &state_printed);
					assert_helper = true;

					ambiguous |= decision == AMBIGUOUS;
					/* if decision was to keep the old value, carry on with the next follow */
					if (decision != REPLACE_WITH_NEW_ACTION)
						continue;
				}

				/* check if already in reduces.  If so, it's a conflict */
				if (sh_map_find(reduces, follow, &other))
				{
					/* note: there shouldn't be two conflicts at the same time since that means there was an unresolved conflict before */
					assert(!assert_helper);	

					conflict_resolution decision = _resolve_conflict(parser, &n, REDUCE, follow, REDUCE, &other, state, &state_printed);

					ambiguous |= decision == AMBIGUOUS;
					/* if decision was to keep the old value, carry on with the next follow */
					if (decision != REPLACE_WITH_NEW_ACTION)
						continue;
				}

				/* it's not a conflict, or its old action is removed in conflict resolution */
				if (sh_map_insert(reduces, follow, &n) < 0)
					goto exit_no_mem;
#ifdef SH_DETAILED_DEBUG
				printf("Found a new REDUCE action with:");
				_print_follow(parser, follow);
				printf(" ------ because of item:\n");
				_print_state_item(&n, parser);
#endif
			}

			continue;
		}

		rule_body = sh_vector_data(&rules[rule_dot->rule].body);

		/* if after dot is a nonterminal, add it to gotos */
		if (rule_body[rule_dot->dot].type == NONTERMINAL)
		{
			int nonterminal = rule_body[rule_dot->dot].id;
			shNode items;

			if (!sh_map_find(gotos, &nonterminal, &items))
			{
				shVector empty;
				sh_vector_init(&empty, _shnodecpy, sizeof(shNode));
				if (sh_map_insert(gotos, &nonterminal, &empty, &items) < 0)
					goto exit_no_mem;
			}

			if (sh_vector_append(items.data, &n) < 0)
				goto exit_no_mem;
#ifdef SH_DETAILED_DEBUG
			printf("Found a new GOTO action with %s ------ because of item:\n", sh_parser_nonterminal_name(parser, nonterminal));
			_print_state_item(&n, parser);
#endif
			continue;
		}
		/* otherwise, after dot is a terminal, add it to shifts */
		else
		{
			shNode f;
			size_t rule_body_size = rules[rule_dot->rule].body.size;
			shSet dup;
			shSet *all_look_aheads = NULL;

			for (res = sh_set_get_first(follows, &f); res == 0; res = sh_set_get_next(&f, &f))
			{
				shVector *follow = f.key;
				find_first_data shift_sen = {
					.sentence = malloc((rule_body_size - rule_dot->dot + follow->size) * sizeof *shift_sen.sentence),
					.k = INTERNAL->k_in_grammar,
					.sentence_malloced = true,
					.sentence_len = rule_body_size - rule_dot->dot + follow->size,
				};
				shSet *look_aheads;
				shNode s;

				if (shift_sen.sentence == NULL)
					goto exit_no_mem;

				/* create a sentence out of the rest of the rule and what can follow it */
				memcpy(shift_sen.sentence, rule_body + rule_dot->dot, (rule_body_size - rule_dot->dot) * sizeof *shift_sen.sentence);
				int *follow_terminals = sh_vector_data(follow);
				for (size_t i = 0; i < follow->size; ++i)
					shift_sen.sentence[rule_body_size - rule_dot->dot + i] = (grammar_symbol){
						.type = TERMINAL,
						.id = follow_terminals[i],
					};

				/* and find what firsts of length K it can have */
				int ret = sh_parser_find_firsts(parser, &shift_sen, &s);
				if (ret <= 0)
					free(shift_sen.sentence);
				if (ret < 0)
					goto exit_no_mem;

				/* accumulate all these firsts in a set to remove duplicates */
				look_aheads = &((find_first_result *)s.data)->firsts;
				if (all_look_aheads == NULL)
					all_look_aheads = look_aheads;
				else
				{
					if (all_look_aheads != &dup)
					{
						sh_set_duplicate(all_look_aheads, &dup);
						all_look_aheads = &dup;
					}

					if (sh_set_for_each(look_aheads, _add_follow, &dup))
						goto exit_no_mem;
				}
			}

			assert(all_look_aheads);

			/* go through these firsts, and for each create a shift action */
			shNode la;
			for (res = sh_set_get_first(all_look_aheads, &la); res == 0; res = sh_set_get_next(&la, &la))
			{
				shVector *look_ahead = la.key;
				shNode other;
				shNode items;

				/* check if already in reduces.  If so, it's a conflict */
				if (sh_map_find(reduces, look_ahead, &other))
				{
					conflict_resolution decision = _resolve_conflict(parser, &n, SHIFT, look_ahead, REDUCE, &other, state, &state_printed);

					ambiguous |= decision == AMBIGUOUS;
					/* if decision was to keep the old value, carry on with the next follow */
					if (decision != REPLACE_WITH_NEW_ACTION)
						continue;
				}

				/* it's not a conflict, or its old action is removed in conflict resolution */
				if (!sh_map_find(shifts, look_ahead, &items))
				{
					shVector empty;
					sh_vector_init(&empty, _shnodecpy, sizeof(shNode));
					if (sh_map_insert(shifts, look_ahead, &empty, &items) < 0)
						goto exit_no_mem;
				}

				if (sh_vector_append(items.data, &n) < 0)
					goto exit_no_mem;
#ifdef SH_DETAILED_DEBUG
				printf("Found a new SHIFT action with:");
				_print_follow(parser, look_ahead);
				printf(" ------ because of item:\n");
				_print_state_item(&n, parser);
#endif
			}

			if (all_look_aheads == &dup)
				sh_set_free(&dup);
		}
	}

	return ambiguous;
exit_no_mem:
	return -1;
}

static bool _free_item_nodes(shNode *n, void *extra)
{
	sh_vector_free(n->data);
	return true;
}

static bool _free_item_follows(shNode *n, void *extra)
{
	sh_set_free(n->data);
	return true;
}

static void _free_state(lrk_state *state)
{
	sh_map_for_each(state, _free_item_follows);
	sh_map_free(state);
}

static bool _free_states_state(shNode *n, void *extra)
{
	_free_state(n->key);
	return true;
}

static int _add_to_state_by_advance(lrk_state *state, shVector *item_refs)
{
	shNode *irefs = sh_vector_data(item_refs);

	for (size_t i = 0; i < item_refs->size; ++i)
	{
		rule_with_dot rule_dot = *(rule_with_dot *)irefs[i].key;
		shSet follows;

		if (sh_set_duplicate(irefs[i].data, &follows))
			goto exit_no_mem;

		++rule_dot.dot;
		if (sh_map_insert(state, &rule_dot, &follows) < 0)
		{
			sh_set_free(&follows);
			goto exit_no_mem;
		}
	}

	return 0;
exit_no_mem:
	_free_state(state);
	return -1;
}

static int _lookup_add_state(shMap *states, shVector *state_refs, lrk_state *state, size_t *id)
{
	shNode n;
	int new_state = 0;

	int ret = sh_map_insert(states, state, &states->size, &n);
	if (ret < 0)
		return -1;
	if (ret == 0)
	{
		if (sh_vector_append(state_refs, n.key))
			return -1;
		new_state = 1;
	}

	if (id)
		*id = *(size_t *)n.data;

	return new_state;
}

static void _save_diagram(shParser *parser, const char *file)
{
	FILE *tout = fopen(file, "w");
	if (tout == NULL)
		return;

	shNode e;
	for (int res = sh_map_get_first(&INTERNAL->lrk_diagram, &e); res == 0; res = sh_map_get_next(&e, &e))
	{
		size_t size = 0;
		lrk_diagram_data *dd = e.key;
		lrk_diagram_result *dr = e.data;
		int *look_aheads = sh_vector_data(&dd->look_ahead);

		if (dd->with_nonterminal)
			size = 1;
		else
			for (size_t i = 0; i < dd->look_ahead.size; ++i, ++size)
				if (look_aheads[i] == -1)
					break;

		fprintf(tout, "%d %d %d %zu", dd->state, dr->transition, dr->state, size);

		if (dd->with_nonterminal)
			fprintf(tout, " %s", sh_parser_nonterminal_name(parser, dd->nonterminal));
		else
			/* do not write trailing $s.  It will be obvious because of the line having fewer tokens */
			for (size_t i = 0; i < size; ++i)
				fprintf(tout, " %s", sh_parser_terminal_name(parser, look_aheads[i]));
		fprintf(tout, "\n");
	}

	fclose(tout);
}

static int _calculate_lrk_diagram(shParser *parser, const char *diagram_file)
{
	int ret;

#ifdef SH_DEBUG
	printf("Rebuilding LRK table from grammar\n");
#endif

	size_t rules_count = INTERNAL->rules.size - 1;	/* don't expose the added lambda -> start rule */
	shMap/* lrk_state->size_t */ states;	/* contains all LR(k) states and maps them to their id */
	shVector/* lrk_state * */ state_refs;	/* reference to state given its id */
	lrk_state first_state = {0};

	bool ambiguous = false;
	shQueue/* size_t */ Q;
	size_t zero = 0;

	sh_map_init(&states, _state_cpy, sizeof(lrk_state), _size_t_cpy, sizeof(size_t), _state_cmp);
	sh_vector_init(&state_refs, _state_ptr_cpy, sizeof(lrk_state *));
	sh_queue_init(&Q, _size_t_cpy, sizeof(size_t));

	if (sh_parser_find_all_firsts(parser))
		goto exit_no_mem;

	/* set the first state as closure of lambda -> . S with "$$...$" as follows */
	if (_make_initial_state(parser, &first_state, rules_count))
		goto exit_no_mem;
	if (_lookup_add_state(&states, &state_refs, &first_state, NULL) < 0)
		goto exit_no_mem;

	if (sh_queue_push(&Q, &zero))
		goto exit_no_mem;

	while (Q.size > 0)
	{
		bool no_mem = true;
		size_t cur_state_id = *(size_t *)sh_queue_top(&Q);
		sh_queue_pop(&Q);

		lrk_state *cur_state = *(lrk_state **)sh_vector_at(&state_refs, cur_state_id);

		/*
		 * for each state, first it is determined what transitions it can have.  At this point,
		 * the conflicts are identified and possibly resolved.  Theses transitions are of three
		 * types:
		 *
		 * gotos: which items of state result in a GOTO with some nonterminal
		 * shifts: which items of state result in a SHIFT with some look-ahead
		 * reduces: which item of state results in a REDUCE with some look-ahead
		 *
		 * Note that ACCEPT is a kind of REDUCE.  The possible conflicts are either shift/reduce,
		 * which means the same look-ahead is trying to be both in shifts and reduces, or
		 * reduce/reduce which means the same look-ahead is trying to occur twice in reduces.
		 */
		shMap/* int->shVector of shNode */ gotos;
		shMap/* shVector of int->shVector of shNode */ shifts;
		shMap/* shVector of int->shNode */ reduces;

		ret = _find_state_transitions(parser, cur_state, &gotos, &shifts, &reduces);
		if (ret < 0)
			goto exit_inner_no_mem;
		if (ret > 0)
			ambiguous = true;

		/*
		 * once the transitions are found, they are processed as follows:
		 *
		 * gotos: for each (NT, {rule_with_dot}), make a GOTO transition to a state which
		 * is the closure of {rule_with_dot++} where ++ means the dot is moved forward by one.
		 *
		 * shifts: for each (look-ahead, {rule_with_dot}), make a SHIFT transition to a state
		 * which is the closure of {rule_with_dot++}.
		 *
		 * reduces: for each (look-ahead, rule_with_dot), make a REDUCE transition with rule of
		 * rule_with_dot.
		 */

		shNode n;
		for (int res = sh_map_get_first(&gotos, &n); res == 0; res = sh_map_get_next(&n, &n))
		{
			lrk_state new_state;
			size_t new_state_id;
			sh_map_init_similar(&new_state, cur_state);

			if (_add_to_state_by_advance(&new_state, n.data))
				goto exit_inner_no_mem;
			if (closure(parser, &new_state))
			{
				_free_state(&new_state);
				goto exit_inner_no_mem;
			}

			res = _lookup_add_state(&states, &state_refs, &new_state, &new_state_id);
			if (res <= 0)
				_free_state(&new_state);
			if (res < 0)
				goto exit_inner_no_mem;
			if (res > 0)
				if (sh_queue_push(&Q, &new_state_id))
					goto exit_inner_no_mem;

			/* create a GOTO transition from cur_state to new_state with this nonterminal */
			lrk_diagram_data ddata;
			ddata.state = cur_state_id;
			ddata.with_nonterminal = true;
			ddata.nonterminal = *(int *)n.key;
			lrk_diagram_result dresult;
			dresult.transition = GOTO;
			dresult.state = new_state_id;
			if (sh_map_insert(&INTERNAL->lrk_diagram, &ddata, &dresult) < 0)
				goto exit_inner_no_mem;
		}

		for (int res = sh_map_get_first(&shifts, &n); res == 0; res = sh_map_get_next(&n, &n))
		{
			lrk_state new_state;
			size_t new_state_id;
			sh_map_init_similar(&new_state, cur_state);

			if (_add_to_state_by_advance(&new_state, n.data))
				goto exit_inner_no_mem;
			if (closure(parser, &new_state))
			{
				_free_state(&new_state);
				goto exit_inner_no_mem;
			}

			res = _lookup_add_state(&states, &state_refs, &new_state, &new_state_id);
			if (res <= 0)
				_free_state(&new_state);
			if (res < 0)
				goto exit_inner_no_mem;
			if (res > 0)
				if (sh_queue_push(&Q, &new_state_id))
					goto exit_inner_no_mem;

			/* create a SHIFT transition from cur_state to new_state with this look-ahead */
			lrk_diagram_data ddata;
			ddata.state = cur_state_id;
			ddata.with_nonterminal = false;
			if (sh_vector_duplicate(n.key, &ddata.look_ahead) < 0)
				goto exit_inner_no_mem;
			lrk_diagram_result dresult;
			dresult.transition = SHIFT;
			dresult.state = new_state_id;
			if (sh_map_insert(&INTERNAL->lrk_diagram, &ddata, &dresult) < 0)
			{
				sh_vector_free(&ddata.look_ahead);
				goto exit_inner_no_mem;
			}
		}

		for (int res = sh_map_get_first(&reduces, &n); res == 0; res = sh_map_get_next(&n, &n))
		{
			/* create a REDUCE action using the rule of this node.  If rule is lambda -> start, it's an ACCEPT */
			shNode *item = n.data;
			unsigned int rule = ((rule_with_dot *)item->key)->rule;
			lrk_diagram_data ddata;
			ddata.state = cur_state_id;
			ddata.with_nonterminal = false;
			if (sh_vector_duplicate(n.key, &ddata.look_ahead) < 0)
				goto exit_inner_no_mem;
			lrk_diagram_result dresult;
			dresult.transition = rule == rules_count?ACCEPT:REDUCE;
			dresult.rule = rule;
			if (sh_map_insert(&INTERNAL->lrk_diagram, &ddata, &dresult) < 0)
			{
				sh_vector_free(&ddata.look_ahead);
				goto exit_inner_no_mem;
			}
		}

		no_mem = false;
	exit_inner_no_mem:
		sh_map_for_each(&gotos, _free_item_nodes);
		sh_map_for_each(&shifts, _free_item_nodes);
		sh_map_free(&gotos);
		sh_map_free(&shifts);
		sh_map_free(&reduces);
		if (no_mem)
			goto exit_no_mem;
	}
#ifdef SH_DEBUG
	_print_states(parser, &states);
#endif
#ifdef SH_DEBUG
	_print_lrk_diagram(parser);
#endif
	if (ambiguous)
		ret = SH_PARSER_AMBIGUOUS;
	else
	{
		_save_diagram(parser, diagram_file);
		ret = SH_PARSER_SUCCESS;
	}
	goto exit_cleanup;
exit_no_mem:
	sh_lexer_error_custom(LEXER, INTERNAL->grammar_file_name, 0, 0, "error: out of memory\n");
	sh_parser_cleanup(parser);
	ret = SH_PARSER_NO_MEM;
exit_cleanup:
	sh_queue_free(&Q);
	/* TODO: move states to INTERNAL, so that when generating PLEH for LR(k), they could be used */
	sh_map_for_each(&states, _free_states_state);
	sh_map_free(&states);
	sh_vector_free(&state_refs);
	sh_parser_cleanup(parser);
	return ret;
}

static int _load_lrk_diagram(shParser *parser, const char *table)
{
	lrk_diagram_data ddata = {0};
	lrk_diagram_result dresult;
	char *symbol = NULL;
	int chars_read = 0;
	int temp;
	int la_size, i;
	int dollar = -1;

	while (1)
	{
		char *end;

		errno = 0;
		ddata.state = strtol(table + chars_read, &end, 10);
		dresult.transition = (lrk_transition)strtoul(end, &end, 10);
		dresult.state = strtol(end, &end, 10);
		la_size = strtol(end, &end, 10);
		temp = end - (table + chars_read);
		if (errno || temp == 0)
			break;
		chars_read += temp;

		/* if GOTO, then expected to see exactly one nonterminal after */
		if (dresult.transition == GOTO)
		{
			symbol = sh_compiler_read_str(table + chars_read, &temp);
			chars_read += temp;

			int *nont = sh_map_at(&INTERNAL->nonterminal_codes, &symbol);
			if (nont == NULL)
				goto exit_unknown_nonterminal;

			ddata.with_nonterminal = true;
			ddata.nonterminal = *nont;
			free(symbol);
			symbol = NULL;
		}
		else
		{
			ddata.with_nonterminal = false;
			sh_vector_init(&ddata.look_ahead, _intcpy, sizeof(int), _intcmp);
			sh_vector_reserve(&ddata.look_ahead, INTERNAL->k_in_grammar);
			for (i = 0; i < la_size; ++i)
			{
				symbol = sh_compiler_read_str(table + chars_read, &temp);
				chars_read += temp;

				if (symbol == NULL)
					goto exit_incomplete;

				int *t = sh_map_at(&INTERNAL->terminal_codes, &symbol);
				if (t == NULL)
					goto exit_unknown_terminal;
				sh_vector_append(&ddata.look_ahead, t);
				free(symbol);
				symbol = NULL;
			}
			for (; i < (int)INTERNAL->k_in_grammar; ++i)
				sh_vector_append(&ddata.look_ahead, &dollar);
		}
		int ret = sh_map_insert(&INTERNAL->lrk_diagram, &ddata, &dresult);
		if (ret > 0)
		{
			sh_vector_free(&ddata.look_ahead);
			sh_lexer_error_custom(LEXER, INTERNAL->grammar_file_name, 0, 0, "warning: duplicate table entry at byte %d\n", chars_read);
		}
		else if (ret < 0)
			goto exit_no_mem;
	}

	return SH_PARSER_SUCCESS;
exit_incomplete:
	sh_lexer_error_custom(LEXER, INTERNAL->grammar_file_name, 0, 0, "error: incomplete table at byte %d\n", chars_read);
	goto exit_fail;
exit_unknown_nonterminal:
	sh_lexer_error_custom(LEXER, INTERNAL->grammar_file_name, 0, 0, "error: unknown nonterminal '%s' in table at byte %d\n", symbol, chars_read);
	goto exit_fail;
exit_unknown_terminal:
	sh_lexer_error_custom(LEXER, INTERNAL->grammar_file_name, 0, 0, "error: unknown terminal '%s' in table at byte %d\n", symbol, chars_read);
	goto exit_fail;
exit_no_mem:
	sh_lexer_error_custom(LEXER, INTERNAL->grammar_file_name, 0, 0, "error: out of memory at byte %d\n", chars_read);
	goto exit_fail;
exit_fail:
	free(symbol);
	return SH_PARSER_FAIL;
}

int sh_parser_prepare_lrk_parser(shParser *parser, const char *diagram_file, bool load)
{
	char *diagram = NULL;
	int error = SH_PARSER_FAIL;
	grammar_rule initial_rule;
	const char *grammar_file = INTERNAL->grammar_file_name;

	INTERNAL->lrk_diagram_built = false;
	INTERNAL->lrk_pleh = default_pleh;

	/* add the initial lambda -> S rule */
	sh_vector_init(&initial_rule.body, _symbolcpy, sizeof(grammar_symbol));
	initial_rule.head = (grammar_symbol){
		.type = NONTERMINAL,
		.id = -1,
	};
	int no_ar = -1;
	error = sh_vector_append(&initial_rule.body, &(grammar_symbol){ .type = NONTERMINAL, .id = 0 });
	error += sh_vector_append(&INTERNAL->rules, &initial_rule);
	error += sh_vector_append(&INTERNAL->rule_action_routines, &no_ar);
	if (error)
	{
		sh_vector_free(&initial_rule.body);
		return SH_PARSER_NO_MEM;
	}

	error = SH_PARSER_FAIL;
	sh_map_init(&INTERNAL->lrk_diagram, _lrk_diagram_data_cpy, sizeof(lrk_diagram_data),
			_lrk_diagram_result_cpy, sizeof(lrk_diagram_result), _lrk_diagram_data_cmp);
	if (load)
	{
		diagram = sh_compiler_read_whole_file(diagram_file, &error);
		INTERNAL->grammar_file_name = diagram_file;
		if (diagram != NULL)
			error = _load_lrk_diagram(parser, diagram);
	}
	if (error)
	{
		sh_map_for_each(&INTERNAL->lrk_diagram, _free_lrk_diagram);
		sh_map_clear(&INTERNAL->lrk_diagram);

		/* then either forced to recalculate, or load failed */
		INTERNAL->grammar_file_name = grammar_file;
		error = _calculate_lrk_diagram(parser, diagram_file);
	}

	free(diagram);
	INTERNAL->lrk_diagram_built = !error;

	return error;
}

int sh_parser_load_lrk(shParser *parser, shLexer *lexer, const char **rules, unsigned int rules_count,
		const char *diagram, unsigned int k, const char *diagram_file)
{
	int error;

	INTERNAL->grammar_type = SH_PARSER_LRK;
	INTERNAL->grammar_file_name = "<function argument>";

	sh_parser_bind(parser, lexer);
	error = sh_parser_load_tokens_from_lexer(parser);
	if (error)
		return error;

	INTERNAL->k_in_grammar = k;
	for (unsigned int i = 0; i < rules_count; ++i)
	{
		error = sh_parser_generate_and_add_rule(parser, rules[i]);
		if (error)
			return error;
	}
	error = sh_parser_rules_sanity_check(parser);
	if (error)
		return error;

#ifdef SH_DEBUG
	sh_parser_print_rules(parser);
#endif

	sh_map_init(&INTERNAL->lrk_diagram, _lrk_diagram_data_cpy, sizeof(lrk_diagram_data),
			_lrk_diagram_result_cpy, sizeof(lrk_diagram_result), _lrk_diagram_data_cmp);
	if (diagram)
		error = _load_lrk_diagram(parser, diagram);
	else
		/* this is for when the format of diagram file changes and they want to rewrite the diagram file */
		error = _calculate_lrk_diagram(parser, diagram_file);
	if (error)
		return error;
	INTERNAL->lrk_diagram_built = true;

#ifdef SH_DEBUG
	_print_lrk_diagram(parser);
#endif

	return SH_PARSER_SUCCESS;
}

static shVector _k_elements(shParser *parser, shKtokens *la)
{
	shVector res;
	int dollar = -1;

	sh_vector_init(&res, _intcpy, sizeof(int), _intcmp);
	shMap *terminal_codes = &INTERNAL->terminal_codes;

	for (unsigned int i = 0; i < la->k; ++i)
	{
		const char *type;
		sh_ktokens_at(la, &type, i);
		if (type)
		{
			int *t = sh_map_at(terminal_codes, &type);
			if (t == NULL)
				sh_lexer_error(LEXER, "internal error: unknown token type %s", type);
			sh_vector_append(&res, t);
		}
		else
			sh_vector_append(&res, &dollar);
	}

	return res;
}

static void _lrk_stack_element_cpy(void *to, void *from)
{
	*(lrk_stack_element *)to = *(lrk_stack_element *)from;
}

int sh_parser_parse_lrk(shParser *parser)
{
	int error;
	shKtokens look_ahead;
	shLexer *lexer = parser->lexer;
	shMap *lrk_diagram = &INTERNAL->lrk_diagram;
	grammar_rule *rules = sh_vector_data(&INTERNAL->rules);
	int *rule_action_routines = sh_vector_data(&INTERNAL->rule_action_routines);
	sh_parser_ar *action_routine_functions = sh_vector_data(&INTERNAL->action_routine_functions);
	int *rule_epr_handler = sh_vector_data(&INTERNAL->rule_epr_handler);
	sh_parser_eprh *epr_handler_functions = sh_vector_data(&INTERNAL->epr_handler_functions);
	shStack *parse_stack = &INTERNAL->parse_stack;
	unsigned int k_in_grammar = INTERNAL->k_in_grammar;

	if (!INTERNAL->lrk_diagram_built)
		return SH_PARSER_NO_RULES;

	error = sh_parser_parse_sanity_check(parser);
	if (error)
		return error;

	if (sh_ktokens_init(&look_ahead, k_in_grammar))
		return SH_PARSER_NO_MEM;

	sh_stack_init(parse_stack, _lrk_stack_element_cpy, sizeof(lrk_stack_element));

	const char *type, *token;
	lrk_stack_element e;
	for (unsigned int i = 0; i < k_in_grammar; ++i)
	{
		e.symbol.type = TERMINAL;
		e.symbol.id = -1;
		e.state = 0;
		sh_stack_push(parse_stack, &e);
		token = sh_lexer_peek(lexer, &type, i, NULL, NULL);
#ifdef SH_DEBUG
		printf("read %s from input which is of type: %s\n", token?token:"<null>", type?type:"<null>");
#endif
		sh_ktokens_push(&look_ahead, token, type);
	}
	e.symbol.type = NONTERMINAL;
	e.symbol.id = -1;
	e.state = 0;
	sh_stack_push(parse_stack, &e);
	while (!INTERNAL->terminate_early)
	{
		int eprh;
		shVector k_elements = _k_elements(parser, &look_ahead);
#ifdef SH_DEBUG
		printf("Stack top: (state: %d, %s) with look-ahead tokens:", ((lrk_stack_element *)sh_stack_top(parse_stack))->state,
				sh_parser_symbol_name(parser, ((lrk_stack_element *)sh_stack_top(parse_stack))->symbol));
		int *k_element_terminals = sh_vector_data(&k_elements);
		for (size_t i = 0; i < k_elements.size; ++i)
		{
			int t = k_element_terminals[i];
			printf(" %s", t == -1?"$":sh_parser_terminal_name(parser, t));
		}
		printf("\n");
#endif
		lrk_diagram_data lookup;
		lrk_diagram_result *transition;
		lrk_diagram_result *transition_with_nonterminal;

		lookup.state = ((lrk_stack_element *)sh_stack_top(parse_stack))->state;
		lookup.with_nonterminal = false;
		lookup.look_ahead = k_elements;
		transition = sh_map_at(lrk_diagram, &lookup);
		if (transition == NULL)
		{
			sh_vector_free(&k_elements);
			if (!(*INTERNAL->lrk_pleh)(parser, &look_ahead))
			{
				sh_ktokens_free(&look_ahead);
				sh_stack_free(parse_stack);
				return SH_PARSER_PARSE_ERROR;
			}
			else
				continue;
		}
		shVector/* grammar_symbol */ handle;
		grammar_rule *rule;
		int rule_ar;
		grammar_symbol *rule_body_symbols, *handle_symbols;

		sh_vector_free(&k_elements);
#ifdef SH_DEBUG
		printf(" -- gives transition: %s %s %d",
				transition->transition == SHIFT?"SHIFT":
				transition->transition == REDUCE?"REDUCE":
				transition->transition == GOTO?"GOTO":"ACCEPT",
				transition->transition == SHIFT || transition->transition == GOTO?"to":"with rule:",
				transition->state);
#endif
		token = sh_ktokens_at(&look_ahead, &type, 0);
		int *t;
		const char *tmp;
		switch (transition->transition)
		{
		case ACCEPT:
			if (token != NULL)
				sh_lexer_error(lexer, "warning: extra characters in file after termination of parse");
			sh_ktokens_free(&look_ahead);
			sh_stack_free(parse_stack);
			return SH_PARSER_SUCCESS;
		case SHIFT:
#ifdef SH_DEBUG
			printf("shifting %s\n", type?type:"<null>");
#endif
			assert(type != NULL);
			e.symbol.type = TERMINAL;
			tmp = type;
			t = sh_map_at(&INTERNAL->terminal_codes, &tmp);
			assert(t != NULL);
			e.symbol.id = *t;
			e.state = transition->state;
			sh_stack_push(parse_stack, &e);

			sh_lexer_next(lexer, &type);			/* This should be the one just matched */
			token = sh_lexer_peek(lexer, &type, k_in_grammar - 1, NULL, NULL);
#ifdef SH_DEBUG
			printf("read %s from input which is of type: %s\n", token?token:"<null>", type?type:"<null>");
#endif
			sh_ktokens_push(&look_ahead, token, type);
			break;
		case REDUCE:
			rule = &rules[transition->rule];
			rule_ar = rule_action_routines[transition->rule];
			eprh = rule_epr_handler[transition->rule];
			if (eprh != -1)
				epr_handler_functions[eprh](parser);
			sh_vector_init_similar(&handle, &rule->body);
			sh_vector_reserve(&handle, rule->body.size);
			for (unsigned int i = 0; i < rule->body.size; ++i)
			{
				sh_vector_append(&handle, &((lrk_stack_element *)sh_stack_top(parse_stack))->symbol);
				sh_stack_pop(parse_stack);
			}
			rule_body_symbols = sh_vector_data(&rule->body);
			handle_symbols = sh_vector_data(&handle);
			for (size_t i = 0; i < handle.size; ++i)
			{
				grammar_symbol *rs = &rule_body_symbols[i];
				grammar_symbol *hs = &handle_symbols[handle.size - i - 1];
				if (rs->type != hs->type || rs->id != hs->id)
				{
					// TODO: is this an internal error or something that the pleh could handle?
					sh_lexer_error_custom(lexer, lexer->file_name, lexer->line, lexer->column, "error: found handle '");
					for (size_t j = 0; j < handle.size; ++j)
					{
						grammar_symbol s = handle_symbols[handle.size - j - 1];
						sh_lexer_error_custom(lexer, NULL, 0, 0, "%s%s", j == 0?"":" ", s.id == -1?"$":sh_parser_symbol_name(parser, s));
					}
					sh_lexer_error_custom(lexer, NULL, 0, 0, "' but the rule to reduce is '%s ->", sh_parser_symbol_name(parser, rule->head));
					for (size_t j = 0; j < rule->body.size; ++j)
					{
						grammar_symbol s = rule_body_symbols[j];
						sh_lexer_error_custom(lexer, NULL, 0, 0, " %s", s.id == -1?"$":sh_parser_symbol_name(parser, s));
					}
					sh_lexer_error_custom(lexer, NULL, 0, 0, "'\n");
					sh_lexer_error(lexer, "could not recover from last error");
					sh_ktokens_free(&look_ahead);
					sh_stack_free(parse_stack);
					return SH_PARSER_PARSE_ERROR;
				}
			}
			sh_vector_free(&handle);
			if (rule_ar >= 0)
			{
				lexer->repeek = false;
				action_routine_functions[rule_ar](parser);
				if (lexer->repeek)
				{
					/* the action routine has issued sh_lexer_ignore and thus the look-aheads must be retaken */
					sh_ktokens_clear(&look_ahead);
					for (unsigned int i = 0; i < k_in_grammar; ++i)
					{
						token = sh_lexer_peek(lexer, &type, i, NULL, NULL);
#ifdef SH_DEBUG
						printf("read %s from input which is of type: %s\n", token?token:"<null>", type?type:"<null>");
#endif
						sh_ktokens_push(&look_ahead, token, type);
					}
				}
			}
#ifdef SH_DEBUG
			printf("reducing to %s\n", sh_parser_symbol_name(parser, rule->head));
#endif
			lookup.state = ((lrk_stack_element *)sh_stack_top(parse_stack))->state;
			lookup.with_nonterminal = true;
			lookup.nonterminal = rule->head.id;
			transition_with_nonterminal = sh_map_at(lrk_diagram, &lookup);
			if (transition_with_nonterminal == NULL)
			{
				sh_lexer_error(lexer, "internal error: no transition rule from state %d with nonterminal %s", ((lrk_stack_element *)sh_stack_top(parse_stack))->state,
						sh_parser_symbol_name(parser, rule->head));
				sh_lexer_error(lexer, "could not recover from last error");
				sh_ktokens_free(&look_ahead);
				sh_stack_free(parse_stack);
				return SH_PARSER_INTERNAL_ERROR;
			}

			e.symbol = rule->head;
			e.state = transition_with_nonterminal->state;
#ifdef SH_DEBUG
			printf(" -- goto with %s gives transition: %s %s %d",
					sh_parser_symbol_name(parser, rule->head),
					transition_with_nonterminal->transition == SHIFT?"SHIFT":
					transition_with_nonterminal->transition == REDUCE?"REDUCE":
					transition_with_nonterminal->transition == GOTO?"GOTO":"ACCEPT",
					transition_with_nonterminal->transition == SHIFT || transition_with_nonterminal->transition == GOTO?"to":"with rule:",
					transition_with_nonterminal->state);
#endif
			sh_stack_push(parse_stack, &e);
			break;
		case GOTO:
			sh_lexer_error(lexer, "internal error: unexpected GOTO transition");
			sh_lexer_error(lexer, "could not recover from last error");
			sh_ktokens_free(&look_ahead);
			sh_stack_free(parse_stack);
			return SH_PARSER_INTERNAL_ERROR;
		default:
			sh_lexer_error(lexer, "internal error: bad parser state");
			sh_lexer_error(lexer, "could not recover from last error");
			sh_ktokens_free(&look_ahead);
			sh_stack_free(parse_stack);
			return SH_PARSER_INTERNAL_ERROR;
		}
	}
	sh_ktokens_free(&look_ahead);
	sh_stack_free(parse_stack);

	return INTERNAL->terminate_early?INTERNAL->terminate_return_value:SH_PARSER_SUCCESS;
}

sh_parser_lrk_pleh sh_parser_set_lrk_pleh(shParser *parser, sh_parser_lrk_pleh h)
{
	sh_parser_lrk_pleh prev;
	prev = INTERNAL->lrk_pleh;
	INTERNAL->lrk_pleh = h;
	return prev;
}

int sh_parser_generate_lrk_pleh(shParser *parser, FILE *src, FILE *hdr)
{
	return SH_PARSER_FAIL;
}
