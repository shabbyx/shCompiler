/*
 * Copyright (C) 2007-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shCompiler.
 *
 * shCompiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shCompiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shCompiler.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef NONSTD_H_BY_CYCLOPS
#define NONSTD_H_BY_CYCLOPS

#include <stdbool.h>
#include <dirent.h>
#ifdef _WIN32
# include <direct.h>
# include <limits.h>
#else
# include <unistd.h>
# include <sys/stat.h>
# include <sys/types.h>
#endif

#ifndef PAGE_SIZE
# define PAGE_SIZE 4096
#endif

#ifdef _WIN32

# define PATH_SEPARATOR '\\'
# define _get_current_dir _getcwd
# define _make_dir(dir) (void)_mkdir(dir)

static inline bool _is_absolute(const char *path, size_t len)
{
	/*
	 * In Windows, a path could look like one of these:
	 * - C:\path\to\file
	 * - D:file
	 * - \\server_name\path\to\file
	 * - Maybe /path/to/somewhere
	 */
	return (len > 1 && path[1] == ':') || path[0] == '\\' || path[0] == '/';
}

static inline bool _is_path_separator(char c)
{
	return c == '\\' || c == '/' || c == ':';
}

#else /* _WIN32 */

# define PATH_SEPARATOR '/'

# define _get_current_dir getcwd
# define _make_dir(dir) (void)mkdir(dir, 0777)		/* permission would be the maximum given to running process */

static inline bool _is_absolute(const char *path, size_t len)
{
	return path[0] == '/';
}

static inline bool _is_path_separator(char c)
{
	return c == '/';
}

#endif /* _WIN32 */

#endif
