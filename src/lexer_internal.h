/*
 * Copyright (C) 2007-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shCompiler.
 *
 * shCompiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shCompiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shCompiler.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LEXER_INTERNAL_H_BY_CYCLOPS
#define LEXER_INTERNAL_H_BY_CYCLOPS

#include <stdbool.h>
#include <shdfa.h>
#include <shlexer.h>

typedef struct token_info
{
	char *token;			/* the token itself */
	const char *type;		/* the type of the token */
	bool space_met;			/* whether space was met before this token was matched */
	int line;			/* line in which this token was matched */
	int column;			/* column in which this token was matched */
	int next_lexeme;		/* index from where next token should be matched */
	bool finished;			/* whether the input has finished after this token.  TODO: seems like this can be handled with next_lexeme.  Do so after conversion */
					/* Or maybe it's not needed at all? */
} token_info;

typedef struct token_type
{
	shDfa dfa;
	bool enabled;			/* tells whether shLexer should try matching this token */
	bool ignored;			/* tells whether shLexer should skip this token if matched */
} token_type;

typedef struct input_info
{
	char *text2lex;			/* the current text being lexed */
	size_t size;			/* the size of it */
	size_t mem_size;		/* the size of memory allocated for text2lex */
	int column_start;		/* the start index of columns (default 0) */
	sh_lexer_reader reader;		/* the reader callback */
	char *user_text;		/* first argument to the reader callback */
	void *user_data;		/* second argument to the reader callback */
	bool finished;			/* whether input is finished, so reader callback shouldn't be called again */
	char last_raw_char;		/*
					 * since '\n\r', '\r\n' and '\r' are converted to '\n', it is important to
					 * keep the last unconverted character so that if for example '\n\r' was divided
					 * between two pages, the '\r' that comes at the beginning of next page would be
					 * ignored rather than converted to '\n'.
					 */
} input_info;

typedef struct lexer_internal
{
	shVector/* token_type */ types;
	shVector/* char * */ keywords;
	input_info input;
	token_info cur_token;		/* currently matched token */
	shList/* token_info */ peeked;	/*
					 * if peeked, they would be cached here so that next peeks/next tokens
					 * would not redo. This cache is emptied if sh_lexer_ignore/match is called.
					 */
	shMap/* char *->sh_lexer_match_function */ match_functions;
	sh_lexer_error_handler no_match;/*
					 * if no token can be matched, this function is called which can skip parts
					 * of the input (update the line number in the process) so that the lex
					 * process could be continued.
					 */
	bool error_suppress;
} lexer_internal;

int sh_lexer_init_to_tokens_file(shLexer *lexer);
int sh_lexer_init_to_grammar_file(shLexer *lexer);

#endif
