/*
 * Copyright (C) 2007-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shCompiler.
 *
 * shCompiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shCompiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shCompiler.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <shdfa.h>
#include "utils.h"

/*#define SH_DEBUG */

#define NFA_STATES(n) ((shNfaState *)sh_vector_data(&n->states))
#define NFA_STATES_CONST(n) ((shNfaState *)sh_vector_data_const(&n->states))
#define NFA_LAMBDAS(n, s) ((int *)sh_vector_data(&NFA_STATES(n)[s].lambdas))
#define NFA_LAMBDAS_CONST(n, s) ((const int *)sh_vector_data(&NFA_STATES_CONST(n)[s].lambdas))
#define NFA_NEXTS(n, s, l) ((int *)sh_vector_data(&NFA_STATES(n)[s].nexts[l]))
#define NFA_NEXTS_CONST(n, s, l) ((const int *)sh_vector_data(&NFA_STATES_CONST(n)[s].nexts[l]))

static void _intcpy(void *to, void *from)
{
	*(int *)to = *(int *)from;
}

static void _intptrcpy(void *to, void *from)
{
	*(int **)to = *(int **)from;
}

static int _intcmp(const void *a, const void *b)
{
	int first = *(const int *)a;
	int second = *(const int *)b;

	return (first > second) - (first < second);
}

#ifdef SH_DEBUG
static void _dump_dfa_state(const shVector *state)
{
	const int *states = sh_vector_data(state);
	for (size_t i = 0; i < state->size; ++i)
		printf("%s%d", i?" ":"", states[i]);
	printf("\n");
}
#endif

static int _letter_copy(shLetter *letter, const shLetter *other)
{
	letter->name = strdup(other->name);
	letter->chars = malloc(other->chars_count * sizeof *letter->chars);
	if (letter->name == NULL || letter->chars == NULL)
		return -1;

	letter->chars_count = other->chars_count;
	memcpy(letter->chars, other->chars, other->chars_count * sizeof *letter->chars);

	return 0;
}

void sh_dfa_init(shDfa *dfa)
{
	if (dfa == NULL)
		return;

	*dfa = (shDfa){0};
}

static void _dfs_on_lambda(const shNfa *nfa, int state, bool *mark)
{
	const int *lambdas;

	if (mark[state])
		return;
	mark[state] = true;

	lambdas = NFA_LAMBDAS_CONST(nfa, state);
	for (size_t i = 0, size = NFA_STATES_CONST(nfa)[state].lambdas.size; i < size; ++i)
		_dfs_on_lambda(nfa, lambdas[i], mark);
}

static int _get_dfs_state(shVector *dfa_state, bool *mark, size_t size)
{
	size_t count = 0;

	/* find number of NFA states marked */
	for (size_t i = 0; i < size; ++i)
		if (mark[i])
			++count;

	sh_vector_init(dfa_state, _intcpy, sizeof(int), _intcmp);
	if (count == 0)
		return 0;

	if (sh_vector_reserve(dfa_state, count))
		return -1;

	/* put all marked NFA states in DFA state */
	for (size_t i = 0; i < size; ++i)
		if (mark[i])
			sh_vector_append(dfa_state, &i);

	return 0;
}

static int _find_state(shVector *states_so_far, shVector *new_state)
{
	shVector *so_far = sh_vector_data(states_so_far);
	for (size_t i = 0, size = states_so_far->size; i < size; ++i)
		if (sh_vector_compare(&so_far[i], new_state) == 0)
			return i;
	return -1;
}

int sh_dfa_convert(shDfa *dfa, const shNfa *nfa, const char *name)
{
	/* each DFA state is a set of states of NFA */
	shVector/* shVector of int */ new_states = {0};
	shVector/* int */ dfa_state = {0};
	size_t alphabet_size = nfa->alphabet_size;
	shVector/* int * */ dfa_graph = {0};
	int *dfa_graph_row;
	bool *dfs_mark = NULL;
	int trap_state = -1;
	int ret = 0;
	/* a stack holding new DFA states to be inspected */
	shStack/* int */ S;
	int current;

	if (dfa == NULL || nfa == NULL || name == NULL)
		return -1;
	if (sh_vector_init(&new_states, sh_vector_assign, sizeof(shVector)))
		goto exit_no_mem;
	if (sh_vector_init(&dfa_graph, _intptrcpy, sizeof(int *)))
		goto exit_no_mem;
	if (sh_stack_init(&S, _intcpy, sizeof(int)))
		goto exit_no_mem;

	/* first, let's check if letters overlap.  If they do, it's a fail right at the beginning */
	sh_dfa_init(dfa);
	dfa->alphabet_size = alphabet_size;
	memset(dfa->to_letter, -1, sizeof dfa->to_letter);
	for (size_t i = 0, size_i = dfa->alphabet_size; i < size_i; ++i)
	{
		bool overlap;

		if (_letter_copy(&dfa->alphabet[i], &nfa->alphabet[i]))
			goto exit_no_mem;
		overlap = false;
		for (size_t j = 0, size_j = dfa->alphabet[i].chars_count; j < size_j; ++j)
		{
			int cur = dfa->alphabet[i].chars[j];

			if (dfa->to_letter[cur] != -1)
			{
				_message("error: in regular expression %s, letters %s and %s overlap in character '%c'",
						name, dfa->alphabet[i].name, dfa->alphabet[dfa->to_letter[cur]].name, cur);
				overlap = true;
			}
			else
				dfa->to_letter[cur] = i;
		}
		if (overlap)
			goto exit_fail;
	}

	dfa->name = strdup(name);
	if (dfa->name == NULL)
		goto exit_no_mem;

	dfs_mark = malloc(nfa->states.size * sizeof *dfs_mark);
	if (!dfs_mark)
		goto exit_no_mem;

	/* the first state is start (and where ever it can go with lambda) */
	dfa_graph_row = malloc(alphabet_size * sizeof *dfa_graph_row);
	if (dfa_graph_row == NULL)
		goto exit_no_mem;
	if (sh_vector_append(&dfa_graph, &dfa_graph_row))
		goto exit_no_mem;

	memset(dfs_mark, 0, nfa->states.size * sizeof *dfs_mark);
	_dfs_on_lambda(nfa, nfa->start, dfs_mark);
	if (_get_dfs_state(&dfa_state, dfs_mark, nfa->states.size))
		goto exit_no_mem;
	if (sh_vector_append(&new_states, &dfa_state))
		goto exit_no_mem;

	current = 0;
	sh_stack_push(&S, &current);
	while (S.size)
	{
		shVector *cur_state;

		/*
		 * a set of reachable nfa states (reachable with the same sequence,
		 * hence mapped to one state in the dfa)
		 */
		current = *(int *)sh_stack_top(&S);
		sh_stack_pop(&S);

#ifdef SH_DEBUG
		_message("picked from stack:");
		_dump_dfa_state(sh_vector_at(&new_states, current));
#endif

		/* for each letter, see where this state can go to */
		for (size_t letter = 0; letter < alphabet_size; ++letter)
		{
			int to_state;

			memset(dfs_mark, 0, nfa->states.size * sizeof *dfs_mark);

			/* recalculate this pointer since new_states.data may be realloced */
			cur_state = sh_vector_at(&new_states, current);
			int *cur_states = sh_vector_data(cur_state);

			/*
			 * for each NFA state in this DFA state, DFS on each of the NFA states they go to with
			 * the current letter and accumulate the marks.
			 */
			for (size_t i = 0, size_i = cur_state->size; i < size_i; ++i)
			{
				int cur_nfa_state = cur_states[i];
				const int *nfa_next = NFA_NEXTS_CONST(nfa, cur_nfa_state, letter);

				for (size_t j = 0, size_j = NFA_STATES_CONST(nfa)[cur_nfa_state].nexts[letter].size; j < size_j; ++j)
					_dfs_on_lambda(nfa, nfa_next[j], dfs_mark);
			}

			if (_get_dfs_state(&dfa_state, dfs_mark, nfa->states.size))
				goto exit_no_mem;
			to_state = _find_state(&new_states, &dfa_state);

			/* check if this is a new state */
			if (to_state == -1)
			{
				to_state = new_states.size;
				if (dfa_state.size == 0)
					trap_state = to_state;

				if (sh_vector_append(&new_states, &dfa_state))
					goto exit_no_mem;

				dfa_graph_row = malloc(alphabet_size * sizeof *dfa_graph_row);
				if (dfa_graph_row == NULL)
					goto exit_no_mem;

				if (sh_vector_append(&dfa_graph, &dfa_graph_row))
					goto exit_no_mem;

				if (sh_stack_push(&S, &to_state))
					goto exit_no_mem;
			}
			else
				sh_vector_free(&dfa_state);

			(*(int **)sh_vector_at(&dfa_graph, current))[letter] = to_state;
		}
	}

	dfa->states_count = new_states.size;
	dfa->trap_state = trap_state;
	dfa->final = malloc(new_states.size * sizeof *dfa->final);
	if (dfa->final == NULL)
		goto exit_no_mem;

	for (size_t i = 0, size_i = new_states.size; i < size_i; ++i)
	{
		shVector *cur_state;
		int *cur_states;

		dfa->final[i] = false;
		cur_state = sh_vector_at(&new_states, i);
		cur_states = sh_vector_data(cur_state);

		for (size_t j = 0, size_j = cur_state->size; j < size_j; ++j)
			if (NFA_STATES_CONST(nfa)[cur_states[j]].final)
			{
				dfa->final[i] = true;
				break;
			}
	}

	/* clear up extra memory */
	sh_vector_minimalize(&dfa_graph);

	/* note: don't free dfa_graph */
	dfa->to = sh_vector_uninline(&dfa_graph);
	dfa->alphabet_size = alphabet_size;
	dfa->start = 0;

	goto exit_normal;
exit_no_mem:
	/* at this point, dfa->final is not initialized and dfa->to only partially (if at all) and dfs_mark maybe */
	_message("error: NFA to DFA converter out of memory");
	dfa->start = -1;		/* indication of error */
	for (size_t i = 0, size_i = dfa_graph.size; i < size_i; ++i)
		CLEAN_FREE(*(int **)sh_vector_at(&dfa_graph, i));
	sh_vector_free(&dfa_graph);
exit_fail:
	/* at this point, dfa->alphabet is partially initialized (if at all) */
	sh_dfa_free(dfa);
	ret = -1;
exit_normal:
	CLEAN_FREE(dfs_mark);
	for (size_t i = 0, size_i = new_states.size; i < size_i; ++i)
		sh_vector_free(sh_vector_at(&new_states, i));
	sh_vector_free(&new_states);
	sh_stack_free(&S);
	return ret;
}

static int _belonging_set(int node, int *sets)
{
	int current = node;
	int parent;

	/* find parent */
	while (sets[current] != current)
		current = sets[current];
	parent = current;

	/*
	 * set parent of all in the path to the found parent
	 * to optimize future _belonging_set's of those nodes
	 */
	current = node;
	while (sets[current] != current)
	{
		int temp = sets[current];
		sets[current] = parent;
		current = temp;
	}

	return parent;
}

static bool _union_equals(const shDfa *dfa, int *sets)
{
	bool equals_exist = false;

	for (int i = 0, size = dfa->states_count; i < size; ++i)
		for (int j = i + 1; j < size; ++j)
		{
			bool are_equal;

			if (dfa->final[i] != dfa->final[j])
				continue;
			if (_belonging_set(i, sets) == _belonging_set(j, sets))
				continue;

			are_equal = true;
			for (int k = 0, size_k = dfa->alphabet_size; k < size_k; ++k)
			{
				if (_belonging_set(dfa->to[i][k], sets) != _belonging_set(dfa->to[j][k], sets)
					&& !((dfa->to[i][k] == i || dfa->to[i][k] == j)
					  && (dfa->to[j][k] == i || dfa->to[j][k] == j)))
				{
					are_equal = false;
					break;
				}
			}
			if (are_equal)
			{
#ifdef SH_DEBUG
				_message("unifying: %d and %d", _belonging_set(j, sets), _belonging_set(i, sets));
#endif
				sets[_belonging_set(j, sets)] = _belonging_set(i, sets);
				equals_exist = true;
			}
		}

	return equals_exist;
}

int sh_dfa_simplify(shDfa *simpdfa, const shDfa *dfa)
{
	shVector/* int */ remaining_states;
	int *state_map = NULL;
	int ret = 0;
	int maximum_size = 0;
	/* A disjoint set, with past-compression */
	int *sets = NULL;

	if (simpdfa == NULL || dfa == NULL)
		return -1;
	if (sh_vector_init(&remaining_states, _intcpy, sizeof(int)))
		goto exit_no_mem;

	sh_dfa_init(simpdfa);
	simpdfa->name = strdup(dfa->name);
	if (simpdfa->name == NULL)
		goto exit_no_mem;

	sets = malloc(dfa->states_count * sizeof *sets);
	if (sets == NULL)
		goto exit_no_mem;
	for (int i = 0, size = dfa->states_count; i < size; ++i)
		sets[i] = i;

	while (_union_equals(dfa, sets));

	for (int i = 0, size = dfa->states_count; i < size; ++i)
	{
		bool found = false;
		int this_set = _belonging_set(i, sets);
		int *rem_states = sh_vector_data(&remaining_states);

		for (size_t j = 0; j < remaining_states.size; ++j)
			if (rem_states[j] == this_set)
			{
				found = true;
				break;
			}
		if (!found)
		{
			if (sh_vector_append(&remaining_states, &this_set))
				goto exit_no_mem;
			if (maximum_size < this_set)
				maximum_size = this_set;
		}
	}

	state_map = malloc((maximum_size + 1) * sizeof *state_map);
	if (state_map == NULL)
		goto exit_no_mem;

	for (int i = 0, size = remaining_states.size; i < size; ++i)
	{
#ifdef SH_DEBUG
		_message("new assignment: %d to %d", *(int *)sh_vector_at(&remaining_states, i), i);
#endif
		state_map[*(int *)sh_vector_at(&remaining_states, i)] = i;
	}

	simpdfa->start = state_map[dfa->start];
	simpdfa->alphabet_size = dfa->alphabet_size;
	simpdfa->states_count = remaining_states.size;
	simpdfa->final = malloc(simpdfa->states_count * sizeof *simpdfa->final);
	simpdfa->to = malloc(simpdfa->states_count * sizeof *simpdfa->to);
	if (simpdfa->final == NULL || simpdfa->to == NULL)
		goto exit_no_mem;

	for (int i = 0, size = simpdfa->states_count; i < size; ++i)
	{
		simpdfa->to[i] = malloc(simpdfa->alphabet_size * sizeof *simpdfa->to[i]);
		if (simpdfa->to[i] == NULL)
			goto exit_no_mem;
	}

	for (int i = 0; i < dfa->alphabet_size; ++i)
		if (_letter_copy(&simpdfa->alphabet[i], &dfa->alphabet[i]))
			goto exit_no_mem;

	for (int i = 0, size = dfa->states_count; i < size; ++i)
	{
		int newI;

		if (_belonging_set(i, sets) != i)
			continue;

		newI = state_map[i];
		simpdfa->final[newI] = dfa->final[i];
		for (size_t j = 0, size2 = dfa->alphabet_size; j < size2; ++j)
			simpdfa->to[newI][j] = state_map[_belonging_set(dfa->to[i][j], sets)];
	}

	if (dfa->trap_state == -1)
		simpdfa->trap_state = -1;
	else
		simpdfa->trap_state = state_map[_belonging_set(dfa->trap_state, sets)];

	memcpy(simpdfa->to_letter, dfa->to_letter, sizeof dfa->to_letter);

	goto exit_normal;
exit_no_mem:
	_message("error: simplify DFA out of memory");
	sh_dfa_free(simpdfa);
	ret = -1;
exit_normal:
	CLEAN_FREE(sets);
	CLEAN_FREE(state_map);
	sh_vector_clear(&remaining_states);
	return ret;
}

void sh_dfa_store(const shDfa *dfa, FILE *fout)
{
	if (dfa == NULL || fout == NULL)
		return;

	fprintf(fout, "start = %d\n", dfa->start);
	fprintf(fout, "trap_state = %d\n", dfa->trap_state);
	fprintf(fout, "alphabet_size = %d\n", dfa->alphabet_size);
	fprintf(fout, "size = %d\n", dfa->states_count);
	fprintf(fout, "states =\n");
	for (int i = 0, size = dfa->states_count; i < size; ++i)
	{
		fprintf(fout, "%d", (int)dfa->final[i]);
		for (int j = 0; j < dfa->alphabet_size; ++j)
			fprintf(fout, " %d", dfa->to[i][j]);
		fprintf(fout, "\n");
	}
	fprintf(fout, "name = %s\n", dfa->name);
	fprintf(fout, "alphabet =\n");
	for (int i = 0; i < dfa->alphabet_size; ++i)
	{
		fprintf(fout, "%s %d", dfa->alphabet[i].name, dfa->alphabet[i].chars_count);
		for (int j = 0, size = dfa->alphabet[i].chars_count; j < size; ++j)
			fprintf(fout, " %d", (int)dfa->alphabet[i].chars[j]);
		fprintf(fout, "\n");
	}
}

int sh_dfa_load(shDfa *dfa, const char *buffer)
{
#define READ_INT(x) \
	do {\
		char *end_position;\
		errno = 0;\
		x = strtol(buffer + position, &end_position, 10);\
		chars_read = end_position - (buffer + position);\
		if (errno || chars_read == 0)\
			goto exit_bad_format;\
		position += chars_read;\
	} while (0)
#define READ_STR(x) \
	do {\
		x = sh_compiler_read_str(buffer + position, &chars_read);\
		position += chars_read;\
		if (x == NULL)\
		{\
			if (buffer[position] == '\0')\
				goto exit_bad_format;\
			else\
				goto exit_no_mem;\
		}\
	} while (0)
	size_t position = 0;
	int chars_read;
	char *s;

	if (dfa == NULL)
		return -1;

	sh_dfa_init(dfa);

	while ((s = sh_compiler_read_str(buffer + position, &chars_read)) != NULL)
	{
		position += chars_read;

		/* skip = */
		while (isspace(buffer[position]))
			++position;
		if (buffer[position] != '=')
			goto exit_bad_format;
		++position;

		/* read parts of dfa based on s */
		if (strcmp(s, "start") == 0)
			READ_INT(dfa->start);
		else if (strcmp(s, "trap_state") == 0)
			READ_INT(dfa->trap_state);
		else if (strcmp(s, "alphabet_size") == 0)
			READ_INT(dfa->alphabet_size);
		else if (strcmp(s, "size") == 0)
			READ_INT(dfa->states_count);
		else if (strcmp(s, "name") == 0)
			READ_STR(dfa->name);
		else if (strcmp(s, "states") == 0)
		{
			dfa->final = malloc(dfa->states_count * sizeof *dfa->final);
			dfa->to = malloc(dfa->states_count * sizeof *dfa->to);
			if (dfa->final == NULL || dfa->to == NULL)
				goto exit_no_mem;

			for (int i = 0; i < dfa->states_count; ++i)
			{
				int is_final;

				dfa->to[i] = malloc(dfa->alphabet_size * sizeof *dfa->to[i]);
				if (dfa->to[i] == NULL)
					goto exit_no_mem;
				READ_INT(is_final);
				dfa->final[i] = is_final;
				for (int j = 0; j < dfa->alphabet_size; ++j)
					READ_INT(dfa->to[i][j]);
			}
		}
		else if (strcmp(s, "alphabet") == 0)
		{
			for (int i = 0, size_i = dfa->alphabet_size; i < size_i; ++i)
			{
				int ascii_code;

				READ_STR(dfa->alphabet[i].name);
				READ_INT(dfa->alphabet[i].chars_count);
				dfa->alphabet[i].chars = malloc(dfa->alphabet[i].chars_count * sizeof *dfa->alphabet[i].chars);
				if (dfa->alphabet[i].chars == NULL)
					goto exit_no_mem;
				for (int j = 0, size_j = dfa->alphabet[i].chars_count; j < size_j; ++j)
				{
					READ_INT(ascii_code);
					dfa->alphabet[i].chars[j] = ascii_code;
				}
			}
		}
		else
		{
			_message("error: invalid property %s", s);
			goto exit_bad_format;
		}
		CLEAN_FREE(s);
	}

	if (dfa->final == NULL || dfa->to == NULL || dfa->name == NULL)
		goto exit_bad_format;

	memset(dfa->to_letter, -1, sizeof dfa->to_letter);
	for (int i = 0; i < dfa->alphabet_size; ++i)
	{
		for (int j = 0, size_j = dfa->alphabet[i].chars_count; j < size_j; ++j)
		{
			char cur = dfa->alphabet[i].chars[j];

			if (dfa->to_letter[(int)cur] != -1)
			{
				_message("error: in dfa of %s,: letters %s and %s overlap in character '%c'\n",
						dfa->name, dfa->alphabet[i].name, dfa->alphabet[dfa->to_letter[(int)cur]].name, cur);
				goto exit_fail;
			}
			else
				dfa->to_letter[(int)cur] = i;
		}
	}
	return 0;
exit_no_mem:
	_message("error: insufficient memory when loading dfa");
	_message("note:  buffer was:\n%s", buffer);
	goto exit_fail;
exit_bad_format:
	_message("error: dfa cache corrupted");
	_message("note:  buffer was:\n%s", buffer);
exit_fail:
	CLEAN_FREE(s);
	sh_dfa_free(dfa);
	return -1;
}

void sh_dfa_free(shDfa *dfa)
{
	if (dfa == NULL)
		return;

	CLEAN_FREE(dfa->final);
	if (dfa->to)
	{
		for (size_t i = 0, size = dfa->states_count; i < size; ++i)
			CLEAN_FREE(dfa->to[i]);
		CLEAN_FREE(dfa->to);
	}
	CLEAN_FREE(dfa->name);
	for (size_t i = 0; i < SH_DFA_MAX_LETTERS; ++i)
	{
		CLEAN_FREE(dfa->alphabet[i].name);
		CLEAN_FREE(dfa->alphabet[i].chars);
	}
	memset(dfa->alphabet, 0, sizeof dfa->alphabet);
}

void sh_dfa_dump(const shDfa *dfa)
{
	if (dfa == NULL)
		return;

	for (int i = 0, size = dfa->states_count; i < size; ++i)
	{
		printf("%s", dfa->final[i]?"Final":"     ");
		for (int j = 0; j < dfa->alphabet_size; ++j)
			printf(" %d", dfa->to[i][j]);
		if (i == dfa->trap_state)
			printf(" Trap!");
		printf("\n");
	}
}
