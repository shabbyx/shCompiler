/*
 * Copyright (C) 2007-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shCompiler.
 *
 * shCompiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shCompiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shCompiler.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <shparser.h>
#include "lexer_internal.h"
#include "parser_internal.h"
#include "utils.h"
#include "grammar.h"
#include "nonstd.h"

//#define SH_DEBUG
//#define SH_DETAILED_DEBUG

#define INTERNAL ((parser_internal *)parser->internal)
#define LEXER (parser->lexer)
#define LEXER_INTERNAL ((lexer_internal *)LEXER->internal)

static void _no_action_routine(shParser *parser) {}
static void _no_epr_handler(shParser *parser) {}

static void _strptrcpy(void *to, void *from)
{
	*(const char **)to = from;
}

static int _strptrcmp(const void *a, const void *b)
{
	return strcmp(*(char * const *)a, *(char * const *)b);
}

static void _intcpy(void *to, void *from)
{
	*(int *)to = *(int *)from;
}

static void _arcpy(void *to, void *from)
{
	*(sh_parser_ar *)to = *(sh_parser_ar *)from;
}

static void _eprhcpy(void *to, void *from)
{
	*(sh_parser_eprh *)to = *(sh_parser_eprh *)from;
}

static void _symbolcpy(void *to, void *from)
{
	*(grammar_symbol *)to = *(grammar_symbol *)from;
}

static void _rulecpy(void *to, void *from)
{
	*(grammar_rule *)to = *(grammar_rule *)from;
}

int sh_parser_init(shParser *parser)
{
	if (parser == NULL)
		return SH_LEXER_FAIL;

	*parser = (shParser){0};

	parser->internal = malloc(sizeof(parser_internal));
	if (parser->internal == NULL)
		goto exit_no_mem;

	*INTERNAL = (parser_internal){0};

	sh_vector_init(&INTERNAL->terminals, _strptrcpy, sizeof(const char *));
	sh_vector_init(&INTERNAL->nonterminals, _strptrcpy, sizeof(char *));
	sh_vector_init(&INTERNAL->action_routines, _strptrcpy, sizeof(char *));
	sh_vector_init(&INTERNAL->epr_handlers, _strptrcpy, sizeof(char *));
	sh_map_init(&INTERNAL->terminal_codes, _strptrcpy, sizeof(const char *), _intcpy, sizeof(int), _strptrcmp);
	sh_map_init(&INTERNAL->nonterminal_codes, _strptrcpy, sizeof(const char *), _intcpy, sizeof(int), _strptrcmp);
	sh_map_init(&INTERNAL->action_routine_codes, _strptrcpy, sizeof(const char *), _intcpy, sizeof(int), _strptrcmp);
	sh_map_init(&INTERNAL->epr_handler_codes, _strptrcpy, sizeof(const char *), _intcpy, sizeof(int), _strptrcmp);
	sh_vector_init(&INTERNAL->action_routine_functions, _arcpy, sizeof(sh_parser_ar));
	sh_vector_init(&INTERNAL->epr_handler_functions, _eprhcpy, sizeof(sh_parser_eprh));
	sh_vector_init(&INTERNAL->rule_epr_handler, _intcpy, sizeof(int));
	sh_vector_init(&INTERNAL->rules, _rulecpy, sizeof(grammar_rule));
	sh_vector_init(&INTERNAL->rule_action_routines, _intcpy, sizeof(int));
	sh_vector_init(&INTERNAL->nonterminal_rules, sh_vector_assign, sizeof(shVector));

	return 0;
exit_no_mem:
	_message("error: parser initialization out of memory");
	return SH_PARSER_NO_MEM;
}

static bool _free_llk_table(shNode *n, void *extra)
{
	sh_vector_free(&((llk_table_data *)n->key)->look_ahead);
	return true;
}

static bool _free_lrk_diagram(shNode *n, void *extra)
{
	lrk_diagram_data *d = n->key;
	if (!d->with_nonterminal)
		sh_vector_free(&d->look_ahead);
	return true;
}

void sh_parser_free(shParser *parser)
{
	if (INTERNAL == NULL)
		return;
	char **nonterminals = sh_vector_data(&INTERNAL->nonterminals);
	char **action_routines = sh_vector_data(&INTERNAL->action_routines);
	char **epr_handlers = sh_vector_data(&INTERNAL->epr_handlers);
	shVector *nont_rules = sh_vector_data(&INTERNAL->nonterminal_rules);
	grammar_rule *rules = sh_vector_data(&INTERNAL->rules);

	for (size_t i = 0, size = INTERNAL->nonterminals.size; i < size; ++i)
		free(nonterminals[i]);
	for (size_t i = 0, size = INTERNAL->action_routines.size; i < size; ++i)
		free(action_routines[i]);
	for (size_t i = 0, size = INTERNAL->epr_handlers.size; i < size; ++i)
		free(epr_handlers[i]);
	sh_vector_free(&INTERNAL->terminals);
	sh_vector_free(&INTERNAL->nonterminals);
	sh_vector_free(&INTERNAL->action_routines);
	sh_vector_free(&INTERNAL->epr_handlers);
	sh_map_free(&INTERNAL->terminal_codes);
	sh_map_free(&INTERNAL->nonterminal_codes);
	sh_map_free(&INTERNAL->action_routine_codes);
	sh_map_free(&INTERNAL->epr_handler_codes);
	sh_vector_free(&INTERNAL->action_routine_functions);
	sh_vector_free(&INTERNAL->epr_handler_functions);
	sh_vector_free(&INTERNAL->rule_epr_handler);
	INTERNAL->llk_pleh = NULL;
	INTERNAL->lrk_pleh = NULL;
	for (size_t i = 0; i < INTERNAL->rules.size; ++i)
		sh_vector_free(&rules[i].body);
	sh_vector_free(&INTERNAL->rules);
	sh_vector_free(&INTERNAL->rule_action_routines);
	for (size_t i = 0; i < INTERNAL->nonterminal_rules.size; ++i)
		sh_vector_free(&nont_rules[i]);
	sh_vector_free(&INTERNAL->nonterminal_rules);

	sh_map_for_each(&INTERNAL->llk_table, _free_llk_table);
	sh_map_free(&INTERNAL->llk_table);

	sh_map_for_each(&INTERNAL->lrk_diagram, _free_lrk_diagram);
	sh_map_free(&INTERNAL->lrk_diagram);

	CLEAN_FREE(parser->internal);
}

int sh_parser_load_tokens_from_lexer(shParser *parser)
{
	char **keywords;
	token_type *token_types;

	if (LEXER == NULL || LEXER_INTERNAL == NULL || !LEXER->initialized)
		return SH_PARSER_BAD_LEXER;
	sh_vector_clear(&INTERNAL->terminals);
	sh_map_clear(&INTERNAL->terminal_codes);
	token_types = sh_vector_data(&LEXER_INTERNAL->types);
	for (size_t i = 0, size = LEXER_INTERNAL->types.size; i < size; ++i)
	{
		char *name = token_types[i].dfa.name;
		if (sh_parser_add_terminal(parser, name) == -1)
			goto exit_no_mem;
	}
	keywords = sh_vector_data(&LEXER_INTERNAL->keywords);
	for (size_t i = 0, size = LEXER_INTERNAL->keywords.size; i < size; ++i)
	{
		char *name = keywords[i];
		if (sh_parser_add_terminal(parser, name) == -1)
			goto exit_no_mem;
	}

	return SH_PARSER_SUCCESS;
exit_no_mem:
	return SH_PARSER_NO_MEM;
}

static char *_make_compound_name(shParser *parser, grammar_rule *rule, grammar_symbol action_routine, size_t take_to)
{
	size_t len = 2, len_so_far = 0;
	char *new_name = NULL;
	const char *ar_name;
	size_t ar_name_len;
	bool last_was_paren = true;
	grammar_symbol *body = sh_vector_data(&rule->body);

	for (size_t i = 0; i < rule->body.size; ++i)
	{
		grammar_symbol s = body[i];
		const char *name = sh_parser_symbol_name(parser, s);
		size_t name_len;
		if (name == NULL)
			goto exit_unknown_symbol;

		name_len = strlen(name);
		len += name_len;

		if (!last_was_paren && name[0] != '(')
			++len;

		last_was_paren = name[name_len - 1] == ')';
	}

	ar_name = sh_parser_symbol_name(parser, action_routine);
	if (ar_name == NULL)
		goto exit_unknown_symbol;
	ar_name_len = strlen(ar_name);
	len += ar_name_len + !last_was_paren;

	new_name = malloc((len + 1) * sizeof *new_name);
	if (new_name == NULL)
		goto exit_no_mem;

	last_was_paren = true;
	new_name[len_so_far++] = '(';
	for (size_t i = 0; i < rule->body.size; ++i)
	{
		grammar_symbol s = body[i];
		const char *name = sh_parser_symbol_name(parser, s);
		size_t name_len = strlen(name);

		if (!last_was_paren && name[0] != '(')
			new_name[len_so_far++] = '~';

		sprintf(new_name + len_so_far, "%s", name);
		len_so_far += name_len;

		last_was_paren = name[name_len - 1] == ')';
	}
	if (!last_was_paren)
		new_name[len_so_far++] = '~';
	sprintf(new_name + len_so_far, "%s", ar_name);
	len_so_far += ar_name_len;
	new_name[len_so_far++] = ')';
	new_name[len_so_far] = '\0';

exit_unknown_symbol:
exit_no_mem:
	return new_name;
}

static int _add_rule(shParser *parser, grammar_rule *r)
{
	int rule_id;
	int eprh_id;
	int err;
	grammar_rule rule;
	grammar_symbol *body = sh_vector_data(&r->body);
	int action_routine = -1;

	rule.head = r->head;
	sh_vector_init(&rule.body, _symbolcpy, sizeof(grammar_symbol));

	rule_id = INTERNAL->rules.size;
	eprh_id = -1;
	for (size_t i = 0; i < r->body.size; ++i)
	{
		grammar_symbol s = body[i];
		/*
		 * an action routine must be on the far right of the rule.
		 * This is a restriction for CLR parse, but for simplicity applied
		 * to LL also.
		 *
		 * If not last, a nonterminal will be created that expands to the first
		 * part of the rule.  That is
		 *
		 *     T -> A B @ar C ...
		 *
		 * becomes
		 *
		 *     T -> (A~B~@ar) C ...
		 *     (A~B~@ar) -> A B @ar
		 */
		if (s.type == ACTION_ROUTINE)
		{
			if (i == r->body.size - 1)
			{
				action_routine = s.id;
				continue;
			}

			grammar_symbol new_nonterminal;
			size_t prev_count = INTERNAL->nonterminals.size;
			char *new_name = _make_compound_name(parser, &rule, s, i);
			if (new_name == NULL)
				goto exit_no_mem;

			new_nonterminal = sh_parser_add_symbol(parser, new_name);
			if (new_nonterminal.id == -1)
				goto exit_no_mem;

			/* if it was actually new, make a rule for it */
			if (prev_count < INTERNAL->nonterminals.size)
			{
				grammar_rule new_rule;
				int id = -1;

				err = sh_vector_append(&INTERNAL->rule_epr_handler, &id);

				new_rule.head = new_nonterminal;
				/* set the new nonterminal's body as the beginning of the current rule's body up to (but not including) the action routine */
				new_rule.body = rule.body;
				err += sh_vector_append(&INTERNAL->rules, &new_rule);
				err += sh_vector_append(&INTERNAL->rule_action_routines, &s.id);
				err += sh_vector_append(sh_vector_at(&INTERNAL->nonterminal_rules, new_nonterminal.id), &rule_id);
				if (err)
					goto exit_no_mem;

				/* make the rule empty, ready for the nonterminal to be added to it (its memory now belongs to new_rule.body) */
				sh_vector_init_similar(&rule.body, &new_rule.body);

				++rule_id;
			}
			else
				/* flush everything added so far, to be replaced by the nonterminal */
				sh_vector_clear(&rule.body);

			/* set it as the first of the rule's body */
			if (sh_vector_append(&rule.body, &new_nonterminal))
				goto exit_no_mem;
		}
		else if (s.type == EPR_HANDLER)
		{
			eprh_id = s.id;
			if (eprh_id == -1)
				goto exit_no_mem;
		}
		else	/* A nonterminal or a terminal */
			if (sh_vector_append(&rule.body, &s))
				goto exit_no_mem;
	}
	err = sh_vector_append(&INTERNAL->rules, &rule);
	err += sh_vector_append(&INTERNAL->rule_action_routines, &action_routine);
	err += sh_vector_append(sh_vector_at(&INTERNAL->nonterminal_rules, rule.head.id), &rule_id);
	err += sh_vector_append(&INTERNAL->rule_epr_handler, &eprh_id);
	if (err)
		goto exit_no_mem;

	return SH_PARSER_SUCCESS;
exit_no_mem:
	return SH_PARSER_NO_MEM;
}

static int _load_rules(shParser *parser, shVector *rules_)
{
	grammar_rule *rules = sh_vector_data(rules_);
	for (size_t i = 0; i < rules_->size; ++i)
	{
		int error = _add_rule(parser, &rules[i]);
		if (error)
			return error;
	}

	return sh_parser_rules_sanity_check(parser);
}

int sh_parser_load_grammar(shParser *parser, const char *grammar_file, unsigned int k, sh_grammar_type type)
{
	shLexer l;
	shParser p;
	int error;
	shVector/* grammar_rule */ rules;

	INTERNAL->grammar_file_name = grammar_file;

	error = sh_parser_load_tokens_from_lexer(parser);
	if (error)
		return error;

	if (sh_lexer_init_to_grammar_file(&l))
		return SH_PARSER_NO_MEM;

	if ((error = sh_parser_init_to_grammar_file(&p, &l)))
		goto exit_local_cleanup;

	switch (sh_lexer_file(&l, grammar_file))
	{
	case SH_LEXER_NO_FILE:
		goto exit_no_such_file;
	case SH_LEXER_NO_MEM:
		goto exit_no_mem;
	case SH_LEXER_SUCCESS:
		break;
	default:
		goto exit_fail;
	}

	sh_compiler_GF_init(&l, parser, type);
	if (sh_parser_parse_llk(&p) != SH_PARSER_SUCCESS)
		goto exit_parse_error;
	rules = sh_compiler_GF_getRules(&l);

	switch (_load_rules(parser, &rules))
	{
	case SH_PARSER_GRAMMAR_ERROR:
		goto exit_bad_rules;
	case SH_PARSER_NO_MEM:
		goto exit_no_mem;
	case SH_PARSER_SUCCESS:
		break;
	default:
		goto exit_fail;
	}
	INTERNAL->k_in_grammar = k;

	error = SH_PARSER_SUCCESS;
	goto exit_local_cleanup;
exit_no_such_file:
	error = SH_PARSER_NO_FILE;
	goto exit_cleanup;
exit_bad_rules:
	error = SH_PARSER_GRAMMAR_ERROR;
	goto exit_cleanup;
exit_no_mem:
	_message("error: out of memory when reading grammar");
	error = SH_PARSER_NO_MEM;
	goto exit_cleanup;
exit_fail:
	error = SH_PARSER_FAIL;
	goto exit_cleanup;
exit_parse_error:
	error = sh_compiler_GF_error(&l);
exit_cleanup:
exit_local_cleanup:
	sh_compiler_GF_cleanup(&l);
	sh_lexer_free(&l);
	sh_parser_free(&p);
	return error;
}

int (sh_parser_read_grammar)(shParser *parser, const char *grammar_file, sh_grammar_type type, unsigned int k, bool new_grammar, ...)
{
	int error;

	INTERNAL->grammar_type = type;

	error = sh_parser_load_grammar(parser, grammar_file, k, type);
	if (error != SH_PARSER_SUCCESS)
		return error;

#ifdef SH_DEBUG
	sh_parser_print_rules(parser);
#endif

	error = SH_PARSER_NO_MEM;
	const char *grammar_name = NULL;
	char *cache_path = sh_compiler_get_path_and_add(grammar_file, &grammar_name, "cache/");
	if (cache_path == NULL)
		goto exit_no_cache_path;
	(void)_make_dir(cache_path);		/* if could not create path (return value not zero), silently continue */
	char *cache_file = sh_compiler_string("%s%s.L%cKtable", cache_path, grammar_name, type == SH_PARSER_LLK?'L':'R');
	if (cache_file == NULL)
		goto exit_no_cache_file;

	if (type == SH_PARSER_LLK)
		error = sh_parser_prepare_llk_parser(parser, cache_file, !new_grammar);
	else
		error = sh_parser_prepare_lrk_parser(parser, cache_file, !new_grammar);

	free(cache_path);
exit_no_cache_file:
	free(cache_file);
exit_no_cache_path:
	return error;
}

/* for every nonterminal, finds whether it is nullable */
static bool *_find_nullables(shParser *parser)
{
	/*
	 * takes the rules that don't have any terminals and puts them in rules_to_inspect,
	 * then goes over the list finding the ones that are nullable, keeping indices
	 * to ignore the ones already seen on the next passes.  If a rule in the middle
	 * is not useful anymore, whether knowing it made the nonterminal nullable or
	 * not nullable, it is swapped outside the range.
	 *
	 * possible_rules keeps for each nonterminal, the number of rules that is yet
	 * unknown whether they can make it nullable or not.  If the nonterminal becomes
	 * known to be nullable, all of its rules will be swapped out in the next pass.
	 * If possible_rules of a nonterminal becomes zero, then the nonterminal is not
	 * nullable and all the rules that have it in their body are swapped out in
	 * the next pass.
	 */
	grammar_rule *rules = sh_vector_data(&INTERNAL->rules);
	int *rules_to_inspect = NULL;
	bool *nullable = NULL;
	int *possible_rules = NULL;
	int end;

	nullable = malloc(INTERNAL->nonterminals.size * sizeof *nullable);
	possible_rules = malloc(INTERNAL->nonterminals.size * sizeof *possible_rules);
	rules_to_inspect = malloc(INTERNAL->rules.size * sizeof *rules_to_inspect);
	if (nullable == NULL || possible_rules == NULL || rules_to_inspect == NULL)
		goto exit_no_mem;
	memset(nullable, false, INTERNAL->nonterminals.size * sizeof *nullable);
	memset(possible_rules, 0, INTERNAL->nonterminals.size * sizeof *possible_rules);

	/* initialization */
	end = 0;
	for (int i = 0; i < (int)INTERNAL->rules.size; ++i)
	{
		size_t j;
		grammar_symbol *body = sh_vector_data(&rules[i].body);
		/* in CLR(k), there is an added lambda -> start rule that needs to be ignored */
		if (rules[i].head.id == -1)
			continue;
		for (j = 0; j < rules[i].body.size; ++j)
			if (body[j].type == TERMINAL)
				break;
		if (j == rules[i].body.size)
		{
			++possible_rules[rules[i].head.id];
			rules_to_inspect[end++] = i;
		}
#ifdef SH_DETAILED_DEBUG
		else
			printf("nullability check: rule %d rejected because body[%zu] is a terminal\n", i, j);
#endif
	}

	/* the passes as described above */
	while (end > 0)
	{
#define SWAP_WITH_LAST						\
do {								\
	int _temp;						\
	--end;							\
	--possible_rules[rules[rules_to_inspect[i]].head.id];	\
	_temp = rules_to_inspect[i];				\
	rules_to_inspect[i] = rules_to_inspect[end];		\
	rules_to_inspect[end] = _temp;				\
} while (0)
		for (int i = 0; i < end; ++i)
		{
			size_t j;
			grammar_rule *rule = &rules[rules_to_inspect[i]];
			grammar_symbol *body = sh_vector_data(&rule->body);
			bool is_nullable = true;
			if (nullable[rule->head.id])
			{
				SWAP_WITH_LAST;
#ifdef SH_DETAILED_DEBUG
				printf("nullability check: rule %d ignored because its head is known to be nullable\n", rules_to_inspect[i]);
#endif
				continue;
			}
			for (j = 0; j < rule->body.size; ++j)
				if (!nullable[body[j].id])
				{
					is_nullable = false;
					if (possible_rules[body[j].id] == 0)
						break;
				}
			/* if is nullable, or absolutely not nullable, either way the rule is not useful anymore */
			if (is_nullable || j != rule->body.size)
			{
				SWAP_WITH_LAST;
#ifdef SH_DETAILED_DEBUG
				if (is_nullable)
					printf("nullability check: rule %d ignored because its head just turned out to be nullable\n", rules_to_inspect[i]);
				else
					printf("nullability check: rule %d ignored because body[%zu] is known to be not nullable\n", rules_to_inspect[i], j);
#endif
			}
			nullable[rule->head.id] = is_nullable;
		}
#undef SWAP_WITH_LAST
	}

#ifdef SH_DEBUG
	for (size_t j = 0; j < INTERNAL->nonterminals.size; ++j)
		printf("%s %s nullable\n", *(const char **)sh_vector_at(&INTERNAL->nonterminals, j), nullable[j]?"IS":"is not");
#endif

exit_cleanup:
	free(rules_to_inspect);
	free(possible_rules);
	return nullable;
exit_no_mem:
	CLEAN_FREE(nullable);
	goto exit_cleanup;
}

typedef struct left_recursive_stack_element
{
	int nonterminal;
	int unvisit;		/* when recursing back up, the nonterminal should be unvisited */
} left_recursive_stack_element;

static void _lrsecpy(void *to, void *from)
{
	*(left_recursive_stack_element *)to = *(left_recursive_stack_element *)from;
}

int sh_parser_is_left_recursive(shParser *parser)
{
	bool *visited = NULL;
	bool *not_left_recursive = NULL;
	shStack stack;
	grammar_rule *rules;
	size_t nont;
	int error;
	shVector *nonterminal_rules;

	bool *nullable = _find_nullables(parser);
	if (nullable == NULL)
		goto exit_no_mem;

	rules = sh_vector_data(&INTERNAL->rules);
	nonterminal_rules = sh_vector_data(&INTERNAL->nonterminal_rules);
	sh_stack_init(&stack, _lrsecpy, sizeof(left_recursive_stack_element));

	visited = malloc(INTERNAL->nonterminals.size * sizeof *visited);
	not_left_recursive = malloc(INTERNAL->nonterminals.size * sizeof *not_left_recursive);
	if (visited == NULL || not_left_recursive == NULL)
		goto exit_no_mem;

	memset(not_left_recursive, false, INTERNAL->nonterminals.size * sizeof *not_left_recursive);
	for (nont = 0; nont < INTERNAL->nonterminals.size; ++nont)
	{
		left_recursive_stack_element e;

		if (not_left_recursive[nont])
			continue;

		memset(visited, false, INTERNAL->nonterminals.size * sizeof *visited);

		e.nonterminal = nont;
		e.unvisit = 0;
		sh_stack_push(&stack, &e);
		while (stack.size > 0)
		{
			size_t nt;
			int *nt_rules;

			e = *(left_recursive_stack_element *)sh_stack_top(&stack);
			sh_stack_pop(&stack);
			nt = e.nonterminal;
			if (e.unvisit)
			{
				visited[nt] = false;
				continue;
			}

			e.unvisit = 1;
			sh_stack_push(&stack, &e);

			if (visited[nt])
				goto exit_left_recursive;
			if (not_left_recursive[nt])
				continue;

			/*
			 * if in the future I visit nont, it's left recursive.  If I never visit, until I recurse back,
			 * then nont was not left recursive, so I might as well mark it right here
			 */
			visited[nt] = true;
			not_left_recursive[nt] = true;

			nt_rules = sh_vector_data(&nonterminal_rules[nt]);
			for (size_t i = 0; i < nonterminal_rules[nt].size; ++i)
			{
				grammar_rule *rule = &rules[nt_rules[i]];
				grammar_symbol *body = sh_vector_data(&rule->body);
				for (size_t j = 0; j < rule->body.size; ++j)
				{
					if (body[j].type != NONTERMINAL)
						break;
					e.nonterminal = body[j].id;
					e.unvisit = 0;
					sh_stack_push(&stack, &e);
					if (!nullable[body[j].id])
						break;
				}
			}
		}
	}

	error = SH_PARSER_SUCCESS;
exit_cleanup:
	free(visited);
	free(not_left_recursive);
	free(nullable);
	sh_stack_free(&stack);
	return error;
exit_no_mem:
	error = SH_PARSER_NO_MEM;
	goto exit_cleanup;
exit_left_recursive:
	error = nont + 1;
	goto exit_cleanup;
}

void sh_parser_bind(shParser *parser, shLexer *lexer)
{
	LEXER = lexer;
}

sh_parser_ar sh_parser_set_action_routine(shParser *parser, const char *name, sh_parser_ar func)
{
	char *fixed = NULL;
	sh_parser_ar prev = NULL;
	sh_parser_ar *funcs = sh_vector_data(&INTERNAL->action_routine_functions);
	int *ar;

	/* if user forgot @, I'll add it myself */
	if (name[0] != '@')
	{
		fixed = sh_compiler_string("@%s", name);
		if (fixed == NULL)
			goto exit_fail;
		name = fixed;
	}

	/* find the action routine */
	ar = sh_map_at(&INTERNAL->action_routine_codes, &name);
	if (ar == NULL)
		goto exit_fail;

	prev = funcs[*ar];
	funcs[*ar] = func;

exit_fail:
	free(fixed);
	return prev;
}

sh_parser_eprh sh_parser_set_epr_handler(shParser *parser, const char *name, sh_parser_eprh func)
{
	char *fixed = NULL;
	sh_parser_eprh prev = NULL;
	sh_parser_eprh *funcs = sh_vector_data(&INTERNAL->epr_handler_functions);
	int *eprh;

	/* if user forgot !, I'll add it myself */
	if (name[0] != '!')
	{
		fixed = sh_compiler_string("!%s", name);
		if (fixed == NULL)
			goto exit_fail;
		name = fixed;
	}

	/* find the EPR handler */
	eprh = sh_map_at(&INTERNAL->epr_handler_codes, &name);
	if (eprh == NULL)
		goto exit_fail;

	prev = funcs[*eprh];
	funcs[*eprh] = func;

exit_fail:
	free(fixed);
	return prev;
}

void sh_parser_ambiguity_resolution(shParser *parser, int resolution)
{
	INTERNAL->ambiguity_resolution = resolution;
}

void sh_parser_stop(shParser *parser, int return_value)
{
	if (parser == NULL || INTERNAL == NULL)
		return;

	INTERNAL->terminate_early = true;
	INTERNAL->terminate_return_value = return_value;
}

int sh_parser_push(shParser *parser, const char *symbol)
{
	int *id;
	grammar_symbol s;
	if (parser == NULL || INTERNAL == NULL)
		return -1;
	if (symbol == NULL)
		symbol = "";
	if ((id = sh_map_at(&INTERNAL->terminal_codes, &symbol)) != NULL)
		s.type = TERMINAL;
	else if ((id = sh_map_at(&INTERNAL->nonterminal_codes, &symbol)) != NULL)
		s.type = NONTERMINAL;
	else
		return -1;
	s.id = *id;
	if (INTERNAL->grammar_type == SH_PARSER_LLK)
		sh_stack_push(&INTERNAL->parse_stack, &(llk_stack_element){ .symbol = s });
	else
		sh_stack_push(&INTERNAL->parse_stack, &(lrk_stack_element){ .symbol = s, .state = -1 /* TODO: fix for LR(k) */});
	return 0;
}

static bool _free_first(shNode *n, void *extra)
{
	sentence_first *sf = n->key;

	if (sf->needs_free)
		sh_vector_free(&sf->first);

	return true;
}

static bool _free_firsts(shNode *n, void *extra)
{
	find_first_data *d = n->key;
	find_first_result *r = n->data;

	if (d->sentence_malloced)
		free(d->sentence);
	sh_set_for_each(&r->firsts, _free_first);
	sh_set_free(&r->firsts);

	return true;
}

static bool _free_parent_and_after(shNode *n, void *extra)
{
	sh_set_free(n->data);
	return true;
}

static bool _free_follow_grap(shNode *n, void *extra)
{
	shSet *adj = n->data;

	sh_set_free(adj);

	return true;
}

static bool _free_follow(shNode *n, void *extra)
{
	nonterminal_follow *nf = n->key;

	if (nf->needs_free)
		sh_vector_free(&nf->follow);

	return true;
}

static bool _free_follows(shNode *n, void *extra)
{
	find_follow_result *r = n->data;

	sh_set_for_each(&r->follows, _free_follow);
	sh_set_free(&r->follows);

	return true;
}

void sh_parser_cleanup(shParser *parser)
{
	sh_map_for_each(&INTERNAL->firsts, _free_firsts);
	sh_map_free(&INTERNAL->firsts);
	sh_map_for_each(&INTERNAL->parent_and_after, _free_parent_and_after);
	sh_map_free(&INTERNAL->parent_and_after);
	sh_map_for_each(&INTERNAL->follow_graph, _free_follow_grap);
	sh_map_free(&INTERNAL->follow_graph);
	sh_map_free(&INTERNAL->follow_representative);
	sh_map_for_each(&INTERNAL->follows, _free_follows);
	sh_map_free(&INTERNAL->follows);
}

static void _print_header_guard(FILE *hdr, const char *file)
{
	for (const char *p = file; *p; ++p)
		if (!isalnum(*p))
			fprintf(hdr, "_");
		else
			fprintf(hdr, "%c", islower(*p)?toupper(*p):*p);
	fprintf(hdr, "_H_AUTOGENERATED_BY_SH_COMPILER");
}

int sh_parser_generate_pleh(shParser *parser, const char *pleh_file)
{
	int ret;

	if (pleh_file == NULL)
		return SH_PARSER_NO_FILE;
	if (!INTERNAL->llk_table_built && !INTERNAL->lrk_diagram_built)
		return SH_PARSER_NO_RULES;

	/* open .h and .c files */
	char *pleh_source = sh_compiler_string("%s.c", pleh_file);
	if (pleh_source == NULL)
		return SH_PARSER_NO_MEM;

	FILE *src = fopen(pleh_source, "w");
	size_t len = strlen(pleh_source);
	pleh_source[len - 1] = 'h';
	FILE *hdr = fopen(pleh_source, "w");
	free(pleh_source);
	if (src == NULL || hdr == NULL)
	{
		if (src) fclose(src);
		if (hdr) fclose(hdr);
		return SH_PARSER_NO_FILE;
	}

	fprintf(hdr,	"#ifndef ");
	_print_header_guard(hdr, pleh_file);
	fprintf(hdr,	"\n"
			"#define ");
	_print_header_guard(hdr, pleh_file);
	fprintf(hdr,	"\n"
			"\n"
			"#include <shcompiler.h>\n"
			"\n"
			"#ifdef __cplusplus\n"
			"extern \"C\" {\n"
			"#endif\n"
			"\n");

	fprintf(src,	"#include <string.h>\n"
			"#include <stdlib.h>\n"
			"#include \"%s.h\"\n"
			"\n", pleh_file);

	if (INTERNAL->llk_table_built)
		ret = sh_parser_generate_llk_pleh(parser, src, hdr);
	else
		ret = sh_parser_generate_lrk_pleh(parser, src, hdr);

	fprintf(hdr,	"\n"
			"#ifdef __cplusplus\n"
			"}\n"
			"#endif\n"
			"\n"
			"#endif\n");

	fclose(hdr);
	fclose(src);

	return ret;
}

/* add symbol to vector and map, without checking if it exists */
static int _add_symbol(char *symbol, shVector *v, shMap *m)
{
	int id = -1;

	if (sh_vector_append(v, symbol))
		return -1;

	id = v->size - 1;
	if (sh_map_insert(m, symbol, &id))
	{
		sh_vector_remove_last(v);
		return -1;
	}

	return id;
}

/* check if symbol already exsits, if not, add it to vector and map */
static int _add_check_symbol(char *symbol, shVector *v, shMap *m)
{
	int *code;
	int id;

	if ((code = sh_map_at(m, &symbol)) == NULL)
		id = _add_symbol(symbol, v, m);
	else
	{
		id = *code;
		free(symbol);
	}

	return id;
}

int sh_parser_add_terminal(shParser *parser, char *name)
{
	return _add_symbol(name, &INTERNAL->terminals, &INTERNAL->terminal_codes);
}

int sh_parser_add_nonterminal(shParser *parser, char *name)
{
	size_t before = INTERNAL->nonterminals.size;
	int id = _add_check_symbol(name, &INTERNAL->nonterminals, &INTERNAL->nonterminal_codes);
	if (before != INTERNAL->nonterminals.size)
	{
		shVector nont_rules;
		sh_vector_init(&nont_rules, _intcpy, sizeof(int));
		if (sh_vector_append(&INTERNAL->nonterminal_rules, &nont_rules))
			return -1;
	}
	return id;
}

int sh_parser_add_action_routine(shParser *parser, char *name)
{
	size_t before = INTERNAL->action_routines.size;
	int id = _add_check_symbol(name, &INTERNAL->action_routines, &INTERNAL->action_routine_codes);
	if (before != INTERNAL->action_routines.size)
	{
		/* assign a dummy action routine to this new action routine */
		sh_parser_ar dummy = _no_action_routine;
		sh_vector_append(&INTERNAL->action_routine_functions, &dummy);
	}

	return id;
}

int sh_parser_add_epr_handler(shParser *parser, char *name)
{
	size_t before = INTERNAL->epr_handlers.size;
	int id = _add_check_symbol(name, &INTERNAL->epr_handlers, &INTERNAL->epr_handler_codes);
	if (before != INTERNAL->epr_handlers.size)
	{
		/* assign a dummy handler to this new error production rule handler */
		sh_parser_eprh dummy = _no_epr_handler;
		sh_vector_append(&INTERNAL->epr_handler_functions, &dummy);
	}

	return id;
}

grammar_symbol sh_parser_add_symbol(shParser *parser, char *name)
{
	int *t;
	grammar_symbol symbol;

	if (parser == NULL || name == NULL)
	{
		symbol.type = NONTERMINAL;
		symbol.id = -2;
	}
	else if (name[0] == '@')
	{
		symbol.type = ACTION_ROUTINE;
		symbol.id = sh_parser_add_action_routine(parser, name);
	}
	else if (name[0] == '!')
	{
		symbol.type = EPR_HANDLER;
		symbol.id = sh_parser_add_epr_handler(parser, name);
	}
	else if ((t = sh_map_at(&INTERNAL->terminal_codes, &name)) != NULL)
	{
		symbol.type = TERMINAL;
		symbol.id = *t;
		free(name);
	}
	else
	{
		symbol.type = NONTERMINAL;
		symbol.id = sh_parser_add_nonterminal(parser, name);
	}

	return symbol;
}

int sh_parser_generate_and_add_rule(shParser *parser, const char *str)
{
	char *s;
	int chars_read = 0;
	int temp;
	int error;
	grammar_rule rule;

	s = sh_compiler_read_str(str, &chars_read);
	if (s == NULL)
		return SH_PARSER_NO_MEM;

	sh_vector_init(&rule.body, _symbolcpy, sizeof(grammar_symbol));

	rule.head = sh_parser_add_symbol(parser, s);
	chars_read += sh_compiler_skip_str(str + chars_read);	/* ignore -> */
	while ((s = sh_compiler_read_str(str + chars_read, &temp)) != NULL)
	{
		grammar_symbol symbol = sh_parser_add_symbol(parser, s);

		chars_read += temp;
		if (sh_vector_append(&rule.body, &symbol))
			goto exit_no_mem;
	}
	error = _add_rule(parser, &rule);
exit_cleanup:
	sh_vector_free(&rule.body);
	return error;
exit_no_mem:
	error = SH_PARSER_NO_MEM;
	goto exit_cleanup;
}

int sh_parser_rules_sanity_check(shParser *parser)
{
	bool valid;
	char **nonterminals;
	shVector *nonterminal_rules;

	/* make sure there were any rules */
	if (INTERNAL->rules.size == 0)
	{
		sh_lexer_error_custom(LEXER, INTERNAL->grammar_file_name, 0, 0, "error: there are no rules in the given file\n");
		return SH_PARSER_GRAMMAR_ERROR;
	}

	/* make sure all nonterminals expanded to something */
	valid = true;
	nonterminals = sh_vector_data(&INTERNAL->nonterminals);
	nonterminal_rules = sh_vector_data(&INTERNAL->nonterminal_rules);
	for (size_t i = 0; i < INTERNAL->nonterminals.size; ++i)
		if (nonterminal_rules[i].size == 0)
		{
			sh_lexer_error_custom(LEXER, INTERNAL->grammar_file_name, 0, 0, "error: no rule found for nonterminal: %s\n", nonterminals[i]);
			valid = false;
		}

	return valid?SH_PARSER_SUCCESS:SH_PARSER_GRAMMAR_ERROR;
}

int sh_parser_parse_sanity_check(shParser *parser)
{
	size_t action_routine_count = INTERNAL->action_routine_functions.size;
	size_t epr_handler_count = INTERNAL->epr_handler_functions.size;
	sh_parser_ar *action_routine_functions = sh_vector_data(&INTERNAL->action_routine_functions);
	sh_parser_eprh *epr_handler_functions = sh_vector_data(&INTERNAL->epr_handler_functions);
	char **action_routines = sh_vector_data(&INTERNAL->action_routines);
	char **epr_handlers = sh_vector_data(&INTERNAL->epr_handlers);

	if (LEXER == NULL || !LEXER->initialized)
		return SH_PARSER_BAD_LEXER;

	assert(action_routine_count == INTERNAL->action_routines.size);
	for (size_t i = 0; i < action_routine_count; ++i)
		if (action_routine_functions[i] == _no_action_routine)
			sh_lexer_error(LEXER, "warning: no function is assigned to action routine '%s' (%zu)", action_routines[i], i);

	assert(epr_handler_count == INTERNAL->epr_handlers.size);
	for (size_t i = 0; i < epr_handler_count; ++i)
		if (epr_handler_functions[i] == _no_epr_handler)
			sh_lexer_error(LEXER, "warning: no function is assigned to error production rule handler '%s' (%zu)", epr_handlers[i], i);

	return SH_PARSER_SUCCESS;
}

const char *sh_parser_symbol_name(shParser *parser, grammar_symbol s)
{
	shVector *source = NULL;

	if (s.id == -1)
		return "";

	switch (s.type)
	{
	case TERMINAL:
		source = &INTERNAL->terminals;
		break;
	case NONTERMINAL:
		source = &INTERNAL->nonterminals;
		break;
	case ACTION_ROUTINE:
		source = &INTERNAL->action_routines;
		break;
	case EPR_HANDLER:
		source = &INTERNAL->epr_handlers;
		break;
	default:
		break;
	}

	if (source == NULL || s.id > (int)source->size)
		return NULL;

	return *(const char **)sh_vector_at(source, s.id);
}

void sh_parser_print_rule(shParser *parser, int r)
{
	grammar_symbol *body;
	grammar_rule *rule;

	if (r < 0 || r >= (int)INTERNAL->rules.size)
	{
		sh_lexer_error_custom(LEXER, NULL, 0, 0, "internal error: rule %d doesn't exist\n", r);
		return;
	}

	rule = sh_vector_at(&INTERNAL->rules, r);
	body = sh_vector_data(&rule->body);
	sh_lexer_error_custom(LEXER, INTERNAL->grammar_file_name, 0, 0, "  rule %d is: ", r);
	sh_lexer_error_custom(LEXER, NULL, 0, 0, "%s ->", sh_parser_symbol_name(parser, rule->head));
	for (size_t i = 0; i < rule->body.size; ++i)
		sh_lexer_error_custom(LEXER, NULL, 0, 0, " %s", sh_parser_symbol_name(parser, body[i]));
	sh_lexer_error_custom(LEXER, NULL, 0, 0, " ;\n");
}

#ifndef NDEBUG
#define TYPE_STRING(x) ((x) == TERMINAL?"T":(x) == NONTERMINAL?"NT":(x) == ACTION_ROUTINE?"AR":(x) == EPR_HANDLER?"EH":"??")

static void _print_debug_symbol(shParser *parser, const char *pre, grammar_symbol s, const char *su)
{
	printf("%s%s<%s,%d>%s", pre, sh_parser_symbol_name(parser, s), TYPE_STRING(s.type), s.id, su);
}

void sh_parser_print_rules(shParser *parser)
{
	grammar_rule *rules = sh_vector_data(&INTERNAL->rules);
	int *rule_action_routines = sh_vector_data(&INTERNAL->rule_action_routines);
	char **action_routines = sh_vector_data(&INTERNAL->action_routines);
	shVector *nonterminal_rules = sh_vector_data(&INTERNAL->nonterminal_rules);
	const char **nonterminals = sh_vector_data(&INTERNAL->nonterminals);

	printf("Rules are:\n");
	for (size_t i = 0; i < INTERNAL->rules.size; ++i)
	{
		grammar_rule *rule = &rules[i];
		grammar_symbol *body = sh_vector_data(&rule->body);
		int ar_id;

		printf("\t%zu: ", i);
		_print_debug_symbol(parser, "", rule->head, " ->");
		for (size_t j = 0; j < rule->body.size; ++j)
			_print_debug_symbol(parser, " ", body[j], "");
		ar_id = rule_action_routines[i];
		if (ar_id >= 0)
			printf(" [[%s<%s,%d>]]", action_routines[ar_id], TYPE_STRING(ACTION_ROUTINE), ar_id);
		printf("\n");
	}
	printf("Rules per nonterminal:\n");
	for (size_t i = 0; i < INTERNAL->nonterminals.size; ++i)
	{
		int *nont_rules = sh_vector_data(&nonterminal_rules[i]);
		printf("Nonterminal #%zu (%s):", i, nonterminals[i]);
		for (size_t j = 0; j < nonterminal_rules[i].size; ++j)
			printf(" %d", nont_rules[j]);
		printf("\n");
	}
}
#endif

int sh_parser_parse(shParser *parser)
{
	if (INTERNAL->grammar_type == SH_PARSER_LLK)
		return sh_parser_parse_llk(parser);
	else
		return sh_parser_parse_lrk(parser);
}
